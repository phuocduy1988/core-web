<?php
namespace App\Exceptions;

class ProcessException extends \Exception {
    public function __construct ($message = null, $code = 422) {
        $message = $message ?: 'Encountered an error. Please try again later.';
        throw new \Exception($message, $code);
    }
}