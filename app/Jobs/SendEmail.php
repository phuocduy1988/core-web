<?php

namespace App\Jobs;

use App\Helpers\Helper;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Log;

class SendEmail implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $mailData;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($mailData)
	{
		$this->mailData = $mailData;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(Mailer $mailer)
	{
		$mailData = $this->mailData;
		$mailer->send('backend.emails.sendmail', ['mailData' => $mailData], function ($m) use ($mailData) {
			$m->to($mailData['email'])
			  ->cc(env('GROUP_MAIL_REVEIVE'))
			  ->subject($mailData['subject']);
		});

		if ($this->attempts() > 3) {
			Log::info('Run queue attempt: '.$this->attempts());
		}
	}
	/**
	 * The job failed to process.
	 *
	 * @param  Exception  $exception
	 * @return void
	 */
	public function failed(Exception $exception)
	{
		Helper::writeLogException($exception);
		// Send mail notification of failure, etc...
	}
}
