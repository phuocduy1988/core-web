<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Models\PageSpeed;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SEOCommand extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The name of the module uppercase first character.
     *
     * @var string
     */
    protected $plugin;

    /**
     * @var string
     */
    protected $location;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'seo:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'SEO check';


    /**
     * Execute the console command.
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
		
    }
}
