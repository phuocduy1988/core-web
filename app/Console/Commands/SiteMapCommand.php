<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Spatie\Crawler\Crawler;
use Spatie\Sitemap\SitemapGenerator;

class SiteMapCommand extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The name of the module uppercase first character.
     *
     * @var string
     */
    protected $plugin;

    /**
     * @var string
     */
    protected $location;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'SEO check';


    /**
     * Execute the console command.
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
    	$path = public_path('sitemap.xml');
		SitemapGenerator::create('https://'.env('APP_URL', ''))
			->configureCrawler(function (Crawler $crawler) {
				$crawler->setMaximumDepth(5);
			})
			->writeToFile($path);
		$this->line('------------------');
        $this->line('<info>OK</info>');
        $this->line('------------------');

        return true;
    }
}
