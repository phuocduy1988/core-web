<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: ApiAuth.php
 */

namespace App\Helpers;

use App\AppTraits\SettingTrait;
use App\Models\ApiToken;
use App\Models\Member;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * @Class ApiAuth
 * @package App\Helpers
 * @Description
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class ApiAuth
{
	use SettingTrait;
	/**
	 * @var string
	 */
	public $errorType = "1";
	/**
	 * @var string
	 */
	public $statusCode = "200";
	/**
	 * @var string
	 */
	public $newToken = "";
	/**
	 * @var string
	 */
	public $memId = "";
	/**
	 * @var array
	 */
	public $result = array();

	/**
	 * @param string $message
	 * @Description show json error message
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function error($message = '')
	{
		//Return http json
	  $response =  response()->json([
		  'status'  => Response::HTTP_UNAUTHORIZED,
		  'message'   => $message,
		  'line' => 'ApiAuth'
	  ], Response::HTTP_UNAUTHORIZED);
	  $response->send();
	  die();
	}

	/**
	 * @param \App\Helpers\Client $client
	 * @return \Illuminate\Database\Eloquent\Model|null|object|static
	 * @Description check api authentication after check midleWare auth.api = true
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function auth(Client $client)
	{
		//Get list middleware defined
		$midleWare = \Request::route()->middleware();

		if(in_array('auth.api', $midleWare))
		{
			//Check header param: device-id, token
			if(!$this->isValid($client)) //Data missing
			{
				//Show error message when header param invalid
				$this->error(trans('error.client_invalid_data'));
			}
		}
		//Progress auth and get user info
		$mem = $this->authenticate($client);
		return $mem;
	}

	/**
	 * @param \App\Helpers\Client $client
	 * @return bool
	 * @Description check param from header is valid
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function isValid(Client $client)
	{
		//Check header token
		if(strlen($client->token) <= 0)
		{
			return false;
		}
		//Check header device-id
		if(strlen($client->deviceId) <= 0)
		{
			return false;
		}

		return true;
	}

	/**
	 * @param \App\Helpers\Client $client
	 * @return \Illuminate\Database\Eloquent\Model|null|object|static
	 * @Description $client has basic info:device_id, token, user-agent, ip
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function authenticate(Client $client)
	{
		try
		{
			$user = null;
			//get userId via token
			$userToken = ApiToken::where('token', $client->token)->where('status', 1)->first();
			//if the conditions meet
			if(!is_null($userToken))
			{
				//check the expired token
				$tokenExpiredate = isset($userToken->expire_date)? $userToken->expire_date : Helper::dateYesterday();
				$checkTokenExpire = Helper::getMinuteOfTwoDate($tokenExpiredate, Helper::dateNow());
				//$checkTokenExpire <= 0 token expried
				if ($checkTokenExpire >= 0)
				{
					//token is not exprire continue get user info
					$id = $userToken->user_id;
					if(!is_null($id))
					{
						$user = User::where('id', $id)->where('status', 1)->first();
						if(!is_null($user))
						{
							feAuth()->login($user);
						}
						//Set Cache key = token+userType ; value => User, set 10p
					}
				}
			}
			return $user;
		}
		catch(\Exception $e)
		{
			$this->error($e->getMessage());
		}
	}
}
