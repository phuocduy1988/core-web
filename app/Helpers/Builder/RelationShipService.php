<?php
namespace App\Helpers\Builder;

use File;
use Schema;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Storage;
use Artisan;
use Carbon\Carbon;
use SplFileObject;
use Monolog\Logger;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\Filesystem;

class RelationShipService {
    /**
     * Request
     * */
    protected $request;

    /**
    * The filesystem instance.
    *
    * @var Filesystem
    */
    protected $files;

    /**
    * The Stubs default path.
    *
    * @var Filesystem
    */
    protected $stubsPath;
    /**
     * @var model
     * */
    protected $modelName;
	protected $routeUrl;

    protected $tableName;
    protected $pluralModelName;
    /**
     * @var controller
     * */
    protected $controllerName;
    protected $relationControllerName;
    /**
     * @var migration
     * */
    protected $migrationClass;

    /**
     * @var request
     * */
    protected $requestName;
    /**
     * @var model entity
     * */
    protected $modelEntity;
    /**
     * @var model entities
     * */
    protected $modelEntities;

    /**
     * @var relation entity
     * */
    protected $relationModelName;
    protected $relationClassPlural;
    protected $relationEntity;
    protected $relationRequestName;
    protected $relationField;
    protected $relationColumnName;
    /**
     * @var relation entities
     * */
    protected $relationEntities;

    /**
     * @var Laravels' Artisan Instance
     * */
    protected $artisan;

    /**
     * @var store process errors
     * */
    protected $errorsArray;
    /**
     * @var process informations
     * */
    protected $infoArray;

    /**
     * @var current operation ID and directory
     * */
    protected $currentOperationId;
    protected $operationDirectory;

    /**
     * Service construct
     * @param Request $request
     * */
    public function __construct () {
        $logger = new Logger('BuilderServiceLog');
        $logger->pushHandler(new StreamHandler(storage_path('BuilderService.log'), Logger::INFO));
        $this->logger = $logger;
        $this->files = new Filesystem;
        $this->errorsArray = [];
        $this->infoArray = [];
        $this->currentOperationId = str_random(12);
        $this->operationDirectory = 'onetech/'.date('Y_m_d_his').'_'.$this->currentOperationId;
        $this->stubsPath = storage_path($this->operationDirectory.'/stubs');
    }

    /**
     * Store errors encountered by the process
     * @param String $string
     * @return bool
     * */
    private function error ($string = null) {
        if (!is_null($string)) {
            $this->logger->addInfo($string);
        }
        return true;
    }

    /**
     * Store informations encountered by the process
     * @param String $string
     * @return bool
     * */
    private function info ($string = null) {
        if (!is_null($string)) {
            $this->infoArray[] = $string;
        }
        return true;
    }

    /**
     * Get a list of current relationships
     * */
    public function getRelations () {
        $models = File::files(app_path('Models/'));
        $modelData = [];
        foreach ($models as $model) {
            // Get file data as array
            $fileData = file($model->getPathname());
            $modelName = $model->getBasename('.php');
			if(!in_array($modelName, [
				'ApiToken','BaseModel','LogActivity','LogError','Permission','Permissible','Role','Setting'
			])) {
				$modelData[$modelName] = ['model' => $modelName, 'data' => $this->extractRelations($fileData, $modelName)];
			}

        }
        return $modelData;
    }

    public function extractRelations ($data, $baseModelName) {
        $db = new Database();
        $relationshipIdentifiers = $db->relationshipIdentifiers();
        $relationshipData = [];
        $matchPatern = '#\((.*?)\)#';

        foreach ($data as $line) {
            foreach ($relationshipIdentifiers as $id) {
                if (strpos($line, $id)) {
                    if(preg_match_all($matchPatern, $line, $matches)) {
                        if (isset($matches)) {
//							if($baseModelName == 'Province') {
//								dd($matches);
//							}
                            $modelData = explode(',', $matches[1][0]);
                            $modelName = $this->stripString($modelData[0]);
                            $foreignKey = $this->stripString(isset($modelData[1]) ? $modelData[1] : snake_case($modelName).'_id');
                            $localKey = $this->stripString(isset($modelData[2]) ? $modelData[2] : 'id');
                        }
                        // May contain relationship table fields
                        $relationshipData[] = [
                            'type' => $id, 
                            'model' => $modelName, 
                            'foreign_key' => $foreignKey,
                            'local_key' => $localKey
                        ];
                    }
                }
            }
        }
        return collect($relationshipData);
    }

    /**
     * Strip strings from slashes, App, class and ::
     * */
    public function stripString ($string) {
        $string = str_replace('App', '', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace("\\", '', $string);
        $string = str_replace("Models", '', $string);
        $string = str_replace("::", '', $string);
        $string = str_replace("class", '', $string);
        return $string;
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory ($path, $commit = false) {
        $permission = $commit ? 0755 : 0755;
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), $permission, true, true);
        }
    }

    /**
     * Write relationship methods into the Model
     * */
    public function writeRelation (Request $request) {

		$this->relationColumnName = $request->get('select_column');
		if(empty($this->relationColumnName)) {
			$this->relationColumnName = 'name';
		}
        $model = $request->get('select_model');
        $with = $request->get('select_with');
        $relation = $request->get('select_relationship');
		$inForm = $request->get('in_form');
		$showIndex = $request->get('show_index');

        $currentRelations = $this->getRelations();
        $currentExisting = array_key_exists($model, $currentRelations) ? $currentRelations[$model] : [];

		// Generate file names
		$this->modelName = studly_case($model);
		$this->relationModelName = studly_case($with);
		$this->pluralModelName = str_plural($this->modelName);
		$this->modelEntity = camel_case($model);
		$this->modelEntities = camel_case(str_plural($this->modelName));
		$this->relationEntity = camel_case($this->relationModelName);
		$this->relationEntities = camel_case(str_plural($this->relationModelName));
		$this->relationClassPlural = str_plural($this->relationModelName);
		$this->relationRequestName = $this->relationModelName.'Request';
		$this->relationField = snake_case($this->relationEntity).'_id';
		$this->tableName = snake_case(str_plural($this->modelName));
		//text-samples
		$this->routeUrl = snake_case(str_plural($this->modelName), '-');
		$this->migrationClass = studly_case('add_'.$this->relationField.'_to_'.$this->tableName.'_table');

		$this->controllerName = $this->modelName.'Controller';
		$this->relationControllerName = $this->relationModelName.'Controller';
		$this->requestName = $this->modelName.'Request';

        $message = 'Relationship added successfully.';
        $code = 201;

        if ($model === $with) {
            $message = 'Invalid relationship.';
            return ['code' => 422, 'message' => $message];
        }

        if (count($currentExisting)) {
            $currentRelationshipCollection = collect($currentExisting['data']);
            //$exists = $currentRelationshipCollection->where('type', $relation)->where('model', $with)->count();
            $exists = $currentRelationshipCollection->where('model', $with)->count();
            if ($exists)  {
                $message = 'Relationship already exists.';
                return ['code' => 422, 'message' => $message];
            }
        }

        $this->makeDirectory(storage_path($this->operationDirectory.'/stubs'));
		//Copy model
		$this->files->copyDirectory(public_path('builder/stubs/'), storage_path($this->operationDirectory.'/stubs'));
        $this->files->copy(app_path('Models/'.$model.'.php'), storage_path($this->operationDirectory.'/'.$model.'.php'));
		//Copy with
		$this->files->copyDirectory(public_path('builder/stubs/'), storage_path($this->operationDirectory.'/stubs'));
		$this->files->copy(app_path('Models/'.$with.'.php'), storage_path($this->operationDirectory.'/'.$with.'.php'));

        //$oneOnOne = ($relation === 'hasOne' || $relation === 'belongsTo');

        $stubPath = $this->stubsPath.'/relations/relationship.stub';
		$stubBelongsTo = '';
		$stubHasMany = '';

        // if relation is of type hasMany, we'll attempt to write additional routes and few form lines
        //if ($relation === 'hasMany') {
        if ($relation === 'belongsTo') {

			$stubBelongsTo = $this->files->get($stubPath);
			$stubBelongsTo = str_replace('{{WITH}}', $with, $stubBelongsTo);
			$stubBelongsTo = str_replace('{{MODEL}}', $model, $stubBelongsTo);
			$stubBelongsTo = str_replace('{{WITH_ENTITIES}}', $this->relationEntity, $stubBelongsTo);
			$stubBelongsTo = str_replace('{{MODEL_ENTITY}}', $this->relationEntity, $stubBelongsTo);
			$stubBelongsTo = str_replace('{{RELATIONSHIP}}', $relation, $stubBelongsTo);
			$stubBelongsTo = $stubBelongsTo."\n }";

			$stubHasMany = $this->files->get($stubPath);
			$stubHasMany = str_replace('{{WITH}}', $model, $stubHasMany);
			$stubHasMany = str_replace('{{MODEL}}', $model, $stubHasMany);
			$stubHasMany = str_replace('{{WITH_ENTITIES}}', $this->modelEntities, $stubHasMany);
			$stubHasMany = str_replace('{{MODEL_ENTITY}}', $this->modelEntity, $stubHasMany);
			$stubHasMany = str_replace('{{RELATIONSHIP}}', 'hasMany', $stubHasMany);
			$stubHasMany = $stubHasMany."\n }";

			//Create Mrgration if field relation is not exist
			$isMigration = $this->writeMigration($this->tableName);
			if($isMigration) {
				Artisan::call('migrate');
			}

			if($showIndex) {
				$writeController = $this->writeRelationshipMethodsInModelController();
				$writeControllerApi = $this->writeRelationshipMethodsInModelControllerApi();
				if($writeController) {
					$this->writeRelationshipSelectInForm();
					$this->writeRelationshipInIndex();
				}
			}
        }
        else {
			return ['code' => 422, 'message' => 'Please use function belongsTo, because this feature has no action!'];
		}
		//Update Model file path
		$modelFilePath = storage_path($this->operationDirectory.'/'.$model.'.php');
		$modelFile = file($modelFilePath);
        $lastCurlyBrace = max(array_keys(preg_grep("~}~", $modelFile)));
        $modelFile[$lastCurlyBrace] = $stubBelongsTo;

        //Update relationship file path
		$modelFilePath2 = storage_path($this->operationDirectory.'/'.$with.'.php');
		$modelFile2 = file($modelFilePath2);
		$lastCurlyBrace2 = max(array_keys(preg_grep("~}~", $modelFile2)));
		$modelFile2[$lastCurlyBrace2] = $stubHasMany;

		$this->files->put($modelFilePath, $modelFile);
		$this->files->copy($modelFilePath, app_path('Models/'.$model.'.php'));

		$this->files->put($modelFilePath2, $modelFile2);
		$this->files->copy($modelFilePath2, app_path('Models/'.$with.'.php'));


        return ['code' => $code, 'message' => $message];
    }

	protected function writeRelationshipMethodsInModelControllerApi () {
		// Active controller file path
		$controllerFilePath = app_path('Http/Controllers/Api/v1/'.$this->controllerName.'.php');

		if (!$this->files->exists($controllerFilePath)) {
			return false;
		}
		// Copy controller file
		$controllerFileWritePath = storage_path($this->operationDirectory.'/Http/Controllers/Api/v1/'.$this->controllerName.'.php');
		$this->makeDirectory($controllerFileWritePath);
		$this->files->copy($controllerFilePath, $controllerFileWritePath);
		$controllerFile = file($controllerFileWritePath);

		foreach ($controllerFile as $line) {
			//Update index
			if(strpos($line, $this->modelName. '::query()')) {
				$strTmp = '$'. $this->modelEntities . ' = ' . '$'. $this->modelEntities . "->with('$this->relationEntity');";
				$controllerFile = str_replace($line, $line . PHP_EOL . "\t\t\t" . $strTmp . PHP_EOL, $controllerFile);
			}
			// Create
			if(strpos($line, '--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				//$duyTest->name = $request->get('name');
				$strTmp =  "\t\t\t" . '$'. $this->modelEntity. '->'. $this->relationField .' = $request->get(\''. $this->relationField .'\');' . PHP_EOL.$line;

				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}
			//Update
			if(strpos($line, '--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				//$duyTest->name = $request->get('name');
				$strTmp = "\t\t\t".'if ($request->get(\''.$this->relationField.'\')) {'.PHP_EOL;
				$strTmp .= "\t\t\t\t".'$'. $this->modelEntity. '->'. $this->relationField .' = $request->get(\''. $this->relationField .'\');' . PHP_EOL;
				$strTmp .= "\t\t\t".'}'.PHP_EOL.$line;

				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}

			// HASH_STRING_CREATE_
			if(strpos($line, '--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				//$duyTest->name = $request->get('name');
				$strTmp =  "\t\t\t\t" .'$request->get(\''.$this->relationField.'\', \'\').'. PHP_EOL.$line;

				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}

			// HASH_STRING_UPDATE_
			if(strpos($line, '--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				//$duyTest->name = $request->get('name');
				$strTmp =  "\t\t\t\t" .'$request->get(\''.$this->relationField.'\', \'\').'. PHP_EOL.$line;

				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}
		}

		//var_dump($controllerFile);exit();
		$written = $this->files->put($controllerFileWritePath, $controllerFile);
		$this->files->copy($controllerFileWritePath, $controllerFilePath);

		//Write to postman
		$writePostMan = $this->writeToPostman();

		return true;
	}

	protected function writeToPostman () {
		// Active postman file path
		$postmanFilePath = base_path('public/builder/postman/api_postman_collection.json');

		if (!$this->files->exists($postmanFilePath)) {
			return false;
		}
		// Copy postman file
		$postmanFileWritePath = storage_path($this->operationDirectory.'public/builder/postman/api_postman_collection.json');
		$this->makeDirectory($postmanFileWritePath);
		$this->files->copy($postmanFilePath, $postmanFileWritePath);
		$postmanFile = file($postmanFileWritePath);

		//POSTMAN replace --UPDATE_GENCODE_FIELD_POSTMAN--
		foreach ($postmanFile as $line) {
			$apiField = "\t\t\t{
						\"key\": \"$this->relationField\",
						\"value\": \"\",
						\"description\": \"\",
						\"type\": \"text\"
					},";

			if(strpos($line, '--'.$this->routeUrl.'_CREATE_GENCODE_FIELD_POSTMAN--')) {
				$strTmp =  $apiField. PHP_EOL.$line;
				$postmanFile = str_replace($line, $strTmp, $postmanFile);
			}
			if(strpos($line, '--'.$this->routeUrl.'_UPDATE_GENCODE_FIELD_POSTMAN--')) {
				$strTmp =  $apiField. PHP_EOL.$line;
				$postmanFile = str_replace($line, $strTmp, $postmanFile);
			}
			$hashStringSearch = '--'.$this->routeUrl.'_CREATE_HASH_KEY--';
			if(strpos($line, $hashStringSearch)) {
				$strTmp =  str_replace($hashStringSearch,$this->relationField.'.\n'.$hashStringSearch, $line);
				$postmanFile = str_replace($line, $strTmp, $postmanFile);
			}
			$hashStringSearch = '--'.$this->routeUrl.'_UPDATE_HASH_KEY--';
			if(strpos($line, $hashStringSearch)) {
				$strTmp =  str_replace($hashStringSearch,$this->relationField.'.\n'.$hashStringSearch, $line);
				$postmanFile = str_replace($line, $strTmp, $postmanFile);
			}
		} //End foreach

		//var_dump($postmanFile);exit();
		$written = $this->files->put($postmanFileWritePath, $postmanFile);
		$this->files->copy($postmanFileWritePath, $postmanFilePath);

		return true;
	}

	/**
	 * Write relationship methods in index.blade.php.
	 */
	protected function writeRelationshipInIndex () {
		// Active controller file path
		$formFilePath = resource_path('views/backend/'.$this->modelEntities.'/index.blade.php');
		// Copy index view file
		$formFileWritePath = storage_path($this->operationDirectory.'/resources/views/backend/'.$this->relationEntities.'/index.blade.php');
		$this->makeDirectory($formFileWritePath);
		$this->files->copy($formFilePath, $formFileWritePath);

		$formFile = file($formFileWritePath);
		$index = 0;
		foreach ($formFile as $line) {
			//Update index
			if(strpos($line, '$'. $this->modelEntity .'->'. $this->relationField)) {
				$index += 1;
				//Update relationField
				$strTemp = '<td class="text-left"> {{ isset($'. $this->modelEntity .'->'. $this->relationEntity .'->'. $this->relationColumnName .') ? $'. $this->modelEntity .'->'. $this->relationEntity .'->'. $this->relationColumnName .' : $'. $this->modelEntity .'->'. $this->relationField .' }}</td>';
				$formFile = str_replace($line, $strTemp, $formFile);
			}
		}
		if($index < 1) {
			//Add relationField to index
			$strTemp = '<td class="text-left"> {{ isset($' . $this->modelEntity . '->' . $this->relationEntity . '->' . $this->relationColumnName . ') ? $' . $this->modelEntity . '->' . $this->relationEntity . '->' . $this->relationColumnName . ' : "" }}</td>'
						.PHP_EOL."\t\t\t\t\t\t\t\t\t\t\t\t\t". '<td class="text-center add-relation">'.PHP_EOL;
			$formFile = str_replace('<td class="text-center add-relation">', $strTemp, $formFile);
			$strTemp = '<th>{{ trans(\''. $this->relationEntities .'.'. $this->relationEntities .'\') }}</th>'.PHP_EOL."\t\t\t\t\t\t\t\t\t\t".'<th>{{ trans(\'label.edit\') }}</th>';
			$formFile = str_replace('<th>{{ trans(\'label.edit\') }}</th>', $strTemp, $formFile);
		}
		$this->files->put($formFileWritePath, $formFile);
		$this->files->copy($formFileWritePath, $formFilePath);
		return true;
	}

    /**
     * Write relationship methods in form.blade.php.
     */
    protected function writeRelationshipSelectInForm () {
        // Active controller file path
        $formFilePath = resource_path('views/backend/'.$this->modelEntities.'/form.blade.php');
        // Copy form view file
        $formFileWritePath = storage_path($this->operationDirectory.'/resources/views/backend/'.$this->relationEntities.'/form.blade.php');
        $this->makeDirectory($formFileWritePath);
        $this->files->copy($formFilePath, $formFileWritePath);
        $formFile = file($formFileWritePath);
        $index = 0;
		foreach ($formFile as $line) {
			//Update index
			if(strpos($line, 'name="'.$this->relationField.'"')) {
				$index += 1;
				//Update relationField
				$stub = $this->files->get($this->stubsPath . '/relations/views/select.stub');
				$stub = str_replace('{{RELATION_ID}}',$this->relationField, $stub);
				$stub = str_replace('{{MODEL_ENTITY}}',$this->modelEntity, $stub);
				$stub = str_replace('{{RELATION_ENTITIES}}',$this->relationEntities, $stub);
				$stub = str_replace('{{RELATION_ENTITY}}',$this->relationEntity, $stub);
				$stub = PHP_EOL. str_replace('{{RELATION_FILED_NAME}}',$this->relationColumnName, $stub);
				$formFile = str_replace($line, $stub, $formFile);
			}
		}
		if($index < 1) {
			//Add relationField to form
			$stub = $this->files->get($this->stubsPath . '/relations/views/formSelect.stub');
			$stub = str_replace('{{FIELD_NAME_LABEL}}','{{ trans(\''. $this->relationEntities. '.' . $this->relationEntities .'\') }}', $stub);
			$stub = str_replace('{{RELATION_ID}}',$this->relationField, $stub);
			$stub = str_replace('{{MODEL_ENTITY}}',$this->modelEntity, $stub);
			$stub = str_replace('{{RELATION_ENTITIES}}',$this->relationEntities, $stub);
			$stub = str_replace('{{RELATION_ENTITY}}',$this->relationEntity, $stub);
			$stub = str_replace('{{RELATION_FILED_NAME}}',$this->relationColumnName, $stub);
			$stub = '<fieldset>'.PHP_EOL."\t\t\t\t\t\t\t\t". $stub;
			$formFile = str_replace('<fieldset>', $stub, $formFile);

		}
        $this->files->put($formFileWritePath, $formFile);
        $this->files->copy($formFileWritePath, $formFilePath);
        return true;
    }

    /**
     * Write relationship methods in Model Controller.
     */
    protected function writeRelationshipMethodsInModelController () {
        // Active controller file path
        $controllerFilePath = app_path('Http/Controllers/Backend/'.$this->controllerName.'.php');

		if (!$this->files->exists($controllerFilePath)) {

            return false;
        }
        // Copy controller file
        $controllerFileWritePath = storage_path($this->operationDirectory.'/Http/Controllers/Backend/'.$this->controllerName.'.php');
        $this->makeDirectory($controllerFileWritePath);
        $this->files->copy($controllerFilePath, $controllerFileWritePath);
        $controllerFile = file($controllerFileWritePath);

		foreach ($controllerFile as $line) {
			//Update index
			if(strpos($line, $this->modelName. '::query()')) {
				$strTmp = '$'. $this->modelEntities . ' = ' . '$'. $this->modelEntities . "->with('$this->relationEntity');";
				$controllerFile = str_replace($line, $line . PHP_EOL . "\t\t\t" . $strTmp . PHP_EOL, $controllerFile);
			}
			//Update form
			if(strpos($line, "backend.$this->modelEntities.form")) {
				$strTmp = str_replace("with('$this->modelEntity', $$this->modelEntity)",
							"with('$this->modelEntity', $$this->modelEntity)->with('$this->relationEntities', \\App\\Models\\$this->relationModelName::get())", $line);
				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}
			//Updaate post
			if(strpos($line, 'save()')) {
				//$duyTest->name = $request->get('name');
				$strTmp =  "\t\t\t" . '$'. $this->modelEntity. '->'. $this->relationField .' = $request->get(\''. $this->relationField .'\');' . PHP_EOL.$line;

				$controllerFile = str_replace($line, $strTmp, $controllerFile);
			}
		}

		//var_dump($controllerFile);exit();
        $written = $this->files->put($controllerFileWritePath, $controllerFile);
        $this->files->copy($controllerFileWritePath, $controllerFilePath);
		return true;
    }

    /**
     * Write Migration file for relation
     * */
    public function writeMigration ($targetTable) {

        // Check if this model needs the migration
        $targetColumn = $this->relationField;
        $tableHasColumn = Schema::hasColumn($targetTable, $targetColumn);
        if ($tableHasColumn) {
            // Migration not needed
            return false;
        }
        $stub = $this->files->get($this->stubsPath.'/relations/database/migrations/add_field.stub');
        $stub = $this->processStub($stub);
        $targetFileName = date('Y_m_d_his').'_'.snake_case($this->migrationClass).'.php';
        // Target to copy
        $targetPath = base_path('database/migrations/'.$targetFileName);
        // Write here
        $targetWritePath = storage_path($this->operationDirectory.'/database/migrations/'.$targetFileName);
        $this->makeDirectory($targetWritePath);
        $this->files->put($targetWritePath, $stub);
        $this->files->copy($targetWritePath, $targetPath);
        return true;
    }

   
    /**
     * @param string $stub
     * @return string
     * */
    public function processStub ($stub) {
        $stub = str_replace('{{CONTROLLER_CLASS}}',$this->controllerName, $stub);
        $stub = str_replace('{{MODEL_CLASS}}',$this->modelName, $stub);
        $stub = str_replace('{{PLURAL_MODEL_NAME}}',$this->pluralModelName, $stub);
        $stub = str_replace('{{MODEL_ENTITY}}',$this->modelEntity, $stub);
        $stub = str_replace('{{MODEL_ENTITIES}}',$this->modelEntities, $stub);
        $stub = str_replace('{{RELATION_ENTITY}}',$this->relationEntity, $stub);
        $stub = str_replace('{{RELATION_ENTITIES}}',$this->relationEntities, $stub);
        $stub = str_replace('{{RELATION_CLASS}}',$this->relationModelName, $stub);
        $stub = str_replace('{{RELATION_CLASS_PLURAL}}',$this->relationClassPlural, $stub);
        $stub = str_replace('{{RELATION_FIELD}}',$this->relationField, $stub);
        $stub = str_replace('{{CAMEL_FIELD}}',camel_case($this->relationField), $stub);
        $stub = str_replace('{{REQUEST_CLASS}}',$this->requestName, $stub);
        $stub = str_replace('{{MIGRATION_CLASS}}',$this->migrationClass, $stub);
        $stub = str_replace('{{TABLE_NAME}}',$this->tableName, $stub);

        return $stub;
    }

}
