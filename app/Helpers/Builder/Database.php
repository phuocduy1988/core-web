<?php

namespace App\Helpers\Builder;

use Illuminate\Database\Eloquent\Model;

class Database extends Model
{

    /**
     * Currently supported datatypes
     * */
    public function dataTypes () {
        return [
//            'primary',
//            'unique',
//            // 'index',
//            // 'spatialIndex',
//            // 'foreign',
//            'increments',
//            'tinyIncrements',
//            'smallIncrements',
//            'mediumIncrements',
//            'bigIncrements',
//            'char',
            'string',
            'text',
			//'mediumText',
			'longText',
			'dateTime',
			'boolean',
            'integer',
            //'tinyInteger',
//            'smallInteger',
//            'mediumInteger',
            'bigInteger',
            'unsignedInteger',
//            'unsignedTinyInteger',
//            'unsignedSmallInteger',
//            'unsignedMediumInteger',
            'unsignedBigInteger',
            'float',
            'double',
            //'decimal',
            //'unsignedDecimal',
            //'enum',
            //'json',
//            'jsonb',
            //'date',
//            'dateTimeTz',
//            'time',
//            'timeTz',
//            'timestamp',
//            'timestampTz',
            // 'timestamps',
            // 'nullableTimestamps',
            // 'timestampsTz',
            // 'softDeletes',
            // 'softDeletesTz',
//            'binary',
//            'uuid',
//            'ipAddress',
//            'macAddress',
//            'geometry',
//            'point',
//            'lineString',
//            'polygon',
//            'geometryCollection',
//            'multiPoint',
//            'multiLineString',
//            'multiPolygon',
            // 'morphs',
            // 'nullableMorphs',
            // 'rememberToken',
        ];
    }

    public function timeTypes () {
        return [
            'date',
            'dateTime',
            'dateTimeTz',
            'time',
            'timeTz',
            'timestamp',
            'timestampTz',
            'timestamps',
            'nullableTimestamps',
            'timestampsTz',
            'softDeletes',
            'softDeletesTz'
        ];
    }

    public function longTextDataTypes () {
        return [
            'mediumText',
            'longText'
        ];
    }

    public function relationshipIdentifiers () {
        return [
			//'hasOne',
            'hasMany',
            'belongsTo',
            //'belongsToMany',
            //'hasManyThrough'
        ];
    }
}
