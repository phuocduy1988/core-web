<?php
namespace App\Helpers\Builder;

use Artisan;
use Validator;
use Monolog\Logger;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\Filesystem;
use App\Exceptions\ProcessException;

class MakerService {
    /**
     * Request
     * */
    protected $request;

    /**
     * @var store process errors
     * */
    protected $errorsArray;

    /**
     * @var process informations
     * */
    protected $infoArray;

    /**
     * @var process informations
     * */
    protected $files;

    /**
     * Service construct
     * @param Request $request
     * */
    public function __construct(Request $request)
    {
        $logger = new Logger('BuilderServiceLog');
        $logger->pushHandler(new StreamHandler(storage_path('BuilderService.log'), Logger::INFO));
        $this->errorsArray = [];
        $this->infoArray = [];
        $this->request = $request;
        $this->files = new Filesystem;
    }

    /**
     * Store errors encountered by the process
     * @param String $string
     * @return bool
     * */
    private function error($string = null)
    {
        if (!is_null($string)) {
            $this->logger->addInfo($string);
        }
        return true;
    }

    /**
     * Store informations encountered by the process
     * @param String $string
     * @return bool
     * */
    private function info($string = null)
    {
        if (!is_null($string)) {
            $this->infoArray[] = $string;
        }
        return true;
    }

    /**
     * run a Make command from request
     * @param Request $request
     * @return mixed
     * */
    public function makeFromRequest()
    {
        $this->cleanup();
        return $this->call();
    }

    /**
     * Do a general cleanup for autoloads and compiled class names
     * @return
     * */
    public function cleanup()
    {
        // Dump Composer autoload
        try {
            $process = system('composer dump-autoload');
        } catch (\Exception $e) {
            // Silence!
        }

        Artisan::call('clear-compiled');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
    }

    /**
     * Root path for package files
     * */
    private function packagePath($path)
    {
        return __DIR__."/../../$path";
    }

    public function runGitCommand($type, $command, $param, $option) {
		$type = env('GIT', '/usr/bin/git'); //test
		return shell_exec("$type $command $param $option");
	}

	public function runNginx($command) {
		$nginx = env('NGINX', '/etc/init.d/nginx');
    	if($command === 'reload') {
			$cmd = $nginx. ' reload';
		}elseif ($command === 'restart') {
			$cmd = $nginx. ' restart';
		}elseif ($command === 'status') {
			$cmd = $nginx. ' status';
		}elseif ($command === 'start') {
			$cmd = $nginx. ' start';
		}else {
			$cmd = $nginx. ' status';
		}
		$outPut = shell_exec($cmd);
		return $outPut;
	}

    /**
     * Call artisan
     * @return
     * */
    public function call()
    {
        $command = $this->request->get('command');
        $type = $this->request->get('type');
        $param = $this->request->get('param');
        $option = $this->request->get('option');
        $arguments = [];

        if ($type === 'git') {
        	return $this->runGitCommand($type, $command, $param, $option);
		}

		if($type === 'nginx') {
        	return $this->runNginx($command);
		}

        if ($command === 'view') {
            // Laravel doesn't have it.
            // Explode by dot if there's a dot
            $hasDot = strpos($param, '.') !== FALSE;

            if($hasDot) {
                $param = str_replace('.', '/', $param);
            }

            $destinationViewPath = resource_path('views/'.$param.'.blade.php');

            $permission = 0644;

            if (!$this->files->isDirectory(dirname($destinationViewPath))) {
                $this->files->makeDirectory(dirname($destinationViewPath), $permission, true, true);
            }

            $viewStubPath = $this->packagePath('stubs/views/vanilla.stub');
            $this->files->copy($viewStubPath, $destinationViewPath);
            return 'View file '.$param.'.blade.php created successfully!';
        }

        switch ($type) {
            case 'make':
                $generatedCommand = 'make:'.$command;
                break;
            case 'artisan':
                $generatedCommand = $command;
                break;
            default:
                $generatedCommand = $command;
                break;
        }


        if ($param) {
            $arguments['name'] = $param;
        }

        if ($option) {
            $arguments['--'.$option] = true;
        }

        if ($type === 'composer') {
            $output = $this->systemCommand('composer '.$generatedCommand);
            if (strlen($output['stderr']) > 2) {
                throw new \Exception($output['stderr'], 422);
            }
            return $output['stdout'];
        }

        Artisan::call($generatedCommand, $arguments);
        return Artisan::output();
    }

    public function systemCommand($cmd, $input = '')
    {
        $proc = proc_open(
            $cmd,
            [
                0 => ['pipe', 'r'],
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w']
            ],
            $pipes,
            base_path(),
            NULL
        );

        fwrite($pipes[0], $input);
        fclose($pipes[0]);

        $stderr = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        $stdout = stream_get_contents($pipes[2]);
        fclose($pipes[2]);

        $return = proc_close($proc);

        return [
            'stdout' => $stdout,
            'stderr' => $stderr,
            'return' => $return
        ];
    }

}
