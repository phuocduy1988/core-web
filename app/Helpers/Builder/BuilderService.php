<?php
namespace App\Helpers\Builder;

use App\Exceptions\ProcessException;
use File;
use Schema;
use Artisan;
use Validator;
use Carbon\Carbon;
use SplFileObject;
use Monolog\Logger;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\Filesystem;
use App\Helpers\Builder\Database;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BuilderService {
	/**
	 * Request
	 * */
	protected $request;

	/**
	* The filesystem instance.
	*
	* @var Filesystem
	*/
	protected $files;

	protected $updateApiPostman;
	/**
	* The Stubs default path.
	*
	* @var Filesystem
	*/
	protected $stubsPath;
	/**
	 * @var namespae
	 * */
	protected $namespace;
	protected $namespacePath;
	protected $generalNamespace;
	protected $useNamespace;
	protected $routeNamespace;
	/**
	 * @var model
	 * */
	protected $modelName;
	protected $pluralModelName;
	/**
	 * @var controller
	 * */
	protected $controllerName;

	/**
	 * @var migration
	 * */
	protected $migrationFileName;

	/**
	 * @var  $tableName
	 * */
	protected  $tableName;

	/**
	 * @var database
	 * */
	protected $database;
	/**

	/**
	 * @var request
	 * */
	protected $requestName;

	protected $routeUrl;
	/**
	 * @var model entity
	 * */
	protected $modelEntity;
	/**
	 * @var model entities
	 * */
	protected $modelEntities;

	/**
	 * @var Laravels' Artisan Instance
	 * */
	protected $artisan;

	/**
	 * @var store process errors
	 * */
	protected $errorsArray;
	/**
	 * @var process informations
	 * */
	protected $infoArray;
	/**
	 * @var current operation ID and directory
	 * */
	protected $currentOperationId;
	protected $operationDirectory;

	/**
	 * Service construct
	 * @param Request $request
	 * */
	public function __construct(Request $request)
	{
		$this->database  = new Database();
		$logger = new Logger('BuilderServiceLog');
		$logger->pushHandler(new StreamHandler(storage_path('BuilderService.log'), Logger::INFO));
		$this->logger = $logger;
		$this->files = new Filesystem;
		$this->errorsArray = [];
		$this->infoArray = [];
		$this->request = $request;
		$this->currentOperationId = str_random(12);
		$this->operationDirectory = 'onetech/'.date('Y_m_d_his').'_'.$this->currentOperationId;
		$this->stubsPath = storage_path($this->operationDirectory.'/stubs');
	}

	/**
	 * Store errors encountered by the process
	 * @param String $string
	 * @return bool
	 * */
	private function error($string = null)
	{
		if (!is_null($string)) {
			$this->logger->addInfo($string);
		}
		return true;
	}

	/**
	 * Store informations encountered by the process
	 * @param String $string
	 * @return bool
	 * */
	private function info($string = null)
	{
		if (!is_null($string)) {
			$this->infoArray[] = $string;
		}
		return true;
	}

	/**
	 * Build a CRUD from request
	 * @param Request $request
	 * @return mixed
	 * */
	public function buildFromRequest()
	{
		$this->initialize();
		$this->validate();
		$this->cleanup();
		// Optimize
		$this->stubOptimizer();
		$this->makeModel();

		$this->makeMigration();

		// WEB
		$this->makeWebRoutes();
		$this->makeController();
		$this->makeRequest();
		$this->makeViewIndex();
		$this->makeViewForm();
		$this->makeLang();
		$this->buildMenu();
		// $this->makeRoutes();

		// API
		$this->makeApiRoutes();
		$this->makeApiController();
		$this->makeModelResource();
		$this->makeModelResourceCollection();


		// All good! Copy files
		$this->copyGeneratedFiles();
		$this->cleanup();
		Artisan::call('migrate');
		return $this->routeUrl;
	}

	/**
	*Initialize filesystem and decide names for files
	* @return
	* */
	public function initialize () {

		$modelNameString = $this->request->get('model');
		$explodedModelNameString = explode('\\', $modelNameString);
		$namespaceSegmentsCount = count($explodedModelNameString);
		$modelNamePart = end($explodedModelNameString);

		$namespacePart = str_replace('\\'.$modelNamePart, '', $modelNameString);
		$namespacePart = str_replace('App\\', '', $namespacePart);
		$namespacePart = str_replace('App', '', $namespacePart);
		$explodedNamespacePart = explode('\\', $namespacePart);

		$namespaceString = 'App';

		if (count($explodedNamespacePart) >= 1) {
			$namespaceString = $namespaceString.'\\';
		}

		$lastNamespacePart = end($explodedNamespacePart);

		foreach ($explodedNamespacePart as $namespaceSegment) {
			if ($namespaceSegment !== $modelNamePart) {
				$namespaceString = $namespaceString.studly_case($namespaceSegment);
				if ($namespaceSegment !== $lastNamespacePart) {
					$namespaceString = $namespaceString.'\\';
				}
			}
		}

		$this->namespace = $namespaceString;
		$this->useNamespace = $this->namespace;

		if (count($explodedNamespacePart) >= 1) {
			$this->generalNamespace = str_replace('App\\', '', $this->namespace);
			$this->generalNamespace = str_replace('App', '', $this->namespace);
			$this->namespacePath = str_replace('\\', '/', $this->generalNamespace);
			if ($explodedNamespacePart[0] !== '' && $explodedNamespacePart[0] !== $modelNamePart) {
				$this->useNamespace = $this->useNamespace.'\\';
			}
		} else {
			$this->generalNamespace = '';
			$this->namespacePath = '';
		}

		$this->routeNamespace = $this->generalNamespace;

		if (strlen($this->generalNamespace) >= 1) {
			$this->routeNamespace = $this->routeNamespace.'\\';
		}

		if (strlen($this->namespacePath) >= 2) {
			$this->namespacePath = $this->namespacePath.'/';
		}

		if ($this->routeNamespace[0] === '\\') {
			$this->routeNamespace = substr_replace($this->routeNamespace, '', 0, 1);
		}

		if ($this->generalNamespace === '\\') {
			$this->generalNamespace = '';
		}

		if ($this->routeNamespace === '\\') {
			$this->routeNamespace = '';
		}

		if ($this->namespace === 'App\\') {
			$this->namespace = 'App';
		}

		//TestSample
		$this->modelName = studly_case(str_singular($modelNamePart));
		//TestSamples
		$this->pluralModelName = str_plural($this->modelName);
		//TestSample
		$this->modelEntity = camel_case($this->modelName);
		$this->modelEntities = camel_case(str_plural($this->modelName));
		$this->controllerName = $this->modelName.'Controller';
		$this->requestName = $this->modelName.'Request';
		$this->tableName = snake_case(str_plural($this->modelName));
		$this->routeUrl = snake_case(str_plural($this->modelName), '-');
		$this->migrationFileName = date('Y_m_d_his').'_create_'.$this->tableName.'_table';

		//Debug Code
	//        $resourceCollection = [
	//            'namespace' => $this->namespace,
	//            'generalNamespace' => $this->generalNamespace,
	//            'useNamespace' => $this->useNamespace,
	//            'routeNamespace' => $this->routeNamespace,
	//            'namespacePath' => $this->namespacePath,
	//            'modelName' => $this->modelName,
	//            'pluralModelName' => $this->pluralModelName,
	//            'modelEntity' => $this->modelEntity,
	//            'modelEntities' => $this->modelEntities,
	//            'controllerName' => $this->controllerName,
	//            'requestName' => $this->requestName,
	//            'tableName' => $this->tableName,
	//            'migrationFileName' => $this->migrationFileName
	//        ];
	//
	//        echo "<pre>"; print_r($resourceCollection); exit;


		$this->makeDirectory(storage_path($this->operationDirectory.'/stubs'));
		$this->makeDirectory(storage_path($this->operationDirectory.'/backup'));
		$this->files->copyDirectory(public_path('builder/stubs/'), storage_path($this->operationDirectory.'/stubs'));

	}

	/**
	 * Get a list of current migration files
	 * @return  Array list of migration file names
	 * */
	public function getMigrationFiles () {
		$migrations = File::files(base_path('database/migrations/'));
		$migrationData = [];

		foreach ($migrations as $migration) {
			$migrationData[] = $migration->getBasename('.php');
		}

		return $migrationData;
	}

	/**
	 * Check if a migration file already exists
	 * @return  bool
	**/
	public function migrationExists ($fileName) {
		$migrationFiles = $this->getMigrationFiles();
		$exists = false;
		foreach ($migrationFiles as $file) {
			if (strpos($file, $fileName)) {
				$exists = true;
			}
		}

		return $exists;
	}

	/**
	 * Check if table already exists in Schema
	 * @return  bool
	**/
	public function tableExists () {
		return Schema::hasTable($this->tableName);
	}

	/**
	 * Validate this operation
	 * @return  mixed
	 * */
	public function validate () {

		$modelFilePath = 'app/Models/';
		$controllerPath = 'app/Http/Controllers/Backend/';
		$requestPath = 'app/Http/Requests/';

		$namespacePath = $this->namespacePath;
		if ($this->namespace !== '') {

			$modelFilePath = $modelFilePath.$namespacePath;
			$controllerPath = $controllerPath.$namespacePath;
			$requestPath = $requestPath.$namespacePath;
		}

		$modelFileExists = $this->files->exists(base_path($modelFilePath.$this->modelName.'.php'));
		$controllerFileExists = $this->files->exists(base_path($controllerPath.$this->controllerName.'.php'));
		$requestFileExists = $this->files->exists(base_path($requestPath.$this->requestName.'.php'));

		$migrationFileBaseName = '_create_'.$this->tableName.'_table';
		$migrationFileExists = $this->migrationExists($migrationFileBaseName);

		$tableExists = $this->tableExists();

		$indexFileExists = $this->files->exists(base_path('resources/views/backend/'.$this->modelEntities.'/index.blade.php'));
		$formFileExists = $this->files->exists(base_path('resources/views/backend/'.$this->modelEntities.'/form.blade.php'));

		$migrationStatements = $this->request->get('migration');


		$validator = Validator::make($this->request->all(), []);

		$that = $this;

		$validator->after(function($validator)
			use (
				$modelFileExists,
				$controllerFileExists,
				$requestFileExists,
				$migrationFileExists,
				$indexFileExists,
				$formFileExists,
				$tableExists,
				$migrationStatements,
				$that
			) {

			if (strlen($that->modelName) < 2 || strlen($that->modelName) > 50) {
				$validator->errors()->add('Model', 'Model name must be between 2 and 50 characters.');
			}

			if (preg_match('/[\'^£$%&*()}{@#~?><>.,|=_+¬-]/', $that->modelName)){
				$validator->errors()->add('Model', 'Model name contains invalid characters. It should only contain letters and numbers.');
			}

			if (
				preg_match('/^\d/', $that->modelName) === 1
			) {
				$validator->errors()->add('Model', 'Model name cannot start with a number.');
			}

			if ($modelFileExists) {
				$validator->errors()->add('Model', 'Model file with name <code>'.$this->modelName.'.php</code> already exists!');
			}

			if ($controllerFileExists) {
				$validator->errors()->add('Controller', 'Controller file with name <code>'.$this->controllerName.'.php</code> already exists!');
			}

			if ($requestFileExists) {
				$validator->errors()->add('Request', 'Request file with name <code>'.$this->requestName.'.php</code> already exists!');
			}

			if ($migrationFileExists) {
				$validator->errors()->add('Migration', 'Migration file to create <code>'.$this->tableName.'</code> table already exists!');
			}

			if ($indexFileExists) {
				$validator->errors()->add('View Index', 'View index file with name <code>index.blade.php</code> already exists!');
			}

			if ($formFileExists) {
				$validator->errors()->add('View Form', 'View form file with name <code>form.blade.php</code> already exists!');
			}

			if ($tableExists) {
				$validator->errors()->add('Table', 'Table <code>'.$this->tableName.'</code> already exists!');
			}

			$migrationTableColumnNamePattern = '/^[a-zA-Z_][a-zA-Z0-9_]*$/';

			$atLeastOneFieldInForm = false;

			// Validate migration statements
			foreach ($migrationStatements as $migration) {

				$dataType = $migration['data_type'];
				$fieldName = $migration['field_name'];
				$nullable = $migration['nullable'];
				$default = $migration['default'];
				$index = $migration['index'];
				$unique = $migration['unique'];
				$showIndex = $migration['show_index'];
				$canSearch = $migration['can_search'];
				$isFile = $migration['is_file'];
				$fileType = $migration['file_type'];
				$inForm = $migration['in_form'];

				if ($inForm) {
					$atLeastOneFieldInForm = true;
				}

				if ($dataType === 'enum' && strlen($default) === 0) {
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is of type ENUM. It must have default value set.');
				}

				if (!$fieldName) {
					$validator->errors()->add('Migration', 'Migration statement must have a field name.');
				}

				if ($isFile && $dataType !== 'string') {
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is of type FILE. It must have a data type String.');
				}

				if (preg_match('/[\'^£$%&*()}{@#~?><>.,|=+¬-]/', $fieldName)){
					$validator->errors()->add('Migration', 'Field <code>'.$fieldName.'</code> name contains invalid characters. It should only contain letters and numbers.');
				}

				if (preg_match('/[\'^£$%&*()}{@#~?><>.|=_+¬-]/', $default)){
					$validator->errors()->add('Migration', 'Field <code>'.$fieldName.'</code> default value contains illegal characters.');
				}

				if ($dataType === 'increments' && $fieldName !== 'id') {
					$validator->errors()->add('Migration', 'Only <code>id</code> field can have <code>increments</code> data type. Please change data type of <code>'.$fieldName.'</code> to something else.');
				}

				if(!preg_match($migrationTableColumnNamePattern, $fieldName)){
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is invalid. The field name must start with letters and must not contain special characters.');
				}

			}

			if ($atLeastOneFieldInForm === false) {
				$validator->errors()->add('Migration', 'At least one field should exist in the form. Please check <code>In Form</code> for at least one field below.');
			}

		});

		$validator->validate();
		return true;
	}

	/**
	 * Do a general cleanup for autoloads and compiled class names
	 * @return
	 * */
	public function cleanup () {
		// Dump Composer autoload
		try {
			$process = system('composer dump-autoload');
		} catch (\Exception $e) {
			// Silence!
		}

		Artisan::call('clear-compiled');
		Artisan::call('route:clear');
		Artisan::call('config:clear');
		Artisan::call('view:clear');
	}

	/**
	 * Make a Model
	 * */
	public function makeModel()
	{
		return $this->writeFile($this->modelName,'model');
	}


	/**
	 * Make Model Resource
	 * */
	public function makeModelResource()
	{
		return $this->writeFile($this->modelName,'resource');
	}

	/**
	 * Make Model Resource
	 * */
	public function makeModelResourceCollection()
	{
		return $this->writeFile($this->modelName,'resource_collection');
	}


	/**
	 * Make migration
	 * */
	public function makeMigration()
	{
		$this->writeFile($this->migrationFileName, 'migration');
	}

	/**
	 * Generate Controller
	 */
	protected function makeController()
	{
		return $this->writeFile($this->controllerName,'controller');
	}



	/**
	 * Generate API Controller
	 */
	protected function makeApiController()
	{
		if ($this->request->get('api_code')) {
			return $this->writeFile($this->controllerName,'api_controller');
		}
	}

	/**
	 * Generate Form validation Request.
	 */
	protected function makeRequest()
	{
		return $this->writeFile($this->requestName,'request');
	}

	/**
	 * Generate index view file.
	 */
	protected function makeViewIndex()
	{
		return $this->writeFile('index','view');
	}
	/**
	 * Generate Form.
	 */
	protected function makeViewForm()
	{
		return $this->writeFile('form','view');
	}

	/**
	 * Generate Lang.
	 */
	protected function makeLang()
	{
		return $this->writeFile('lang','lang');
	}


	/**
	 * Make Routes.
	 */
	protected function makeWebRoutes()
	{
		return $this->makeRoutes('backend');
	}


	/**
	 * Make API Routes.
	 */
	protected function makeApiRoutes()
	{
		if ($this->request->get('api_code')) {
			return $this->makeRoutes('api');
		}
		return false;
	}

	/**
	 * Make Routes.
	 */
	protected function makeRoutes($type = 'backend')
	{
		$basePath = $type ==='backend' ? 'routes/backend.php' : 'routes/api.php';
		// Active routes file path
		$routesFilePath = base_path($basePath);
		// Copy routes file
		$routesFileWritePath = storage_path($this->operationDirectory.'/'.$basePath);
		$this->makeDirectory($routesFileWritePath);
		$this->files->copy($routesFilePath, $routesFileWritePath);

		$routeFile = file($routesFileWritePath);
		// Write before the last closing brace
		// Does this file have a route group ?
		$hasRouteGroup = preg_grep("~}\);~", $routeFile);
		$lastClosingBrace = $hasRouteGroup ? (max(array_keys(preg_grep("~}\);~", $routeFile))) - 1) : max(array_keys($routeFile));
		// Write data
		$stub = $this->getRoutesStub($type);
		$routeFile[$lastClosingBrace] = $routeFile[$lastClosingBrace].PHP_EOL.$stub.PHP_EOL;
		$written = $this->files->put($routesFileWritePath, $routeFile);
		return $written;
	}

	public function buildMenu()
	{
		$menuItem = [
			'text'        => $this->modelEntities,
			'url'        => $this->routeUrl,
			'route'       => $this->modelEntity.'.index',
			'icon'        => $this->request->get('icon') ?: 'icon-folder',
		];

		$currentMenu = config('backend-menu');

		$builtMenu = $currentMenu;
		$builtMenu[] = $menuItem;
		$builtMenu = var_export(array_values($builtMenu), true);

		$content = $this->files->get($this->stubsPath . '/config/backend-menu.php');
		$content = str_replace('//MENU', $builtMenu, $content);
		$content = str_replace('array', '', $content);
		$content = str_replace('(', '[', $content);
		$content = str_replace(')', ']', $content);

		$filePath = storage_path($this->operationDirectory.'/config/backend-menu.php');

		$this->makeDirectory($filePath);
		$this->files->put($filePath, $content);
	}

	/**
	 * Write file
	 *
	 * @param string $name
	 * @param string $intent
	 * */
	protected function writeFile($name, $intent, $commit = false)
	{
		$content = '';
		$basePath = storage_path($this->operationDirectory);
		$filePath = null;
		switch ($intent) {
			case 'controller':
				$filePath = $basePath.'/app/Http/Controllers/Backend/'.$this->namespacePath.$name.'.php';
				$content = $this->getControllerStub(true);
				break;
			case 'api_controller':
				$filePath = $basePath.'/app/Http/Controllers/Api/v1'.$this->namespacePath.$name.'.php';
				$content = $this->getControllerStub(false);
				break;
			case 'request':
				$filePath = $basePath.'/app/Http/Requests/'.$this->namespacePath.$name.'.php';
				$content = $this->getRequestStub();
				break;

			case 'view':
				$filePath = $basePath.'/resources/views/backend/'.$this->modelEntities.'/'.$name.'.blade.php';
				$content = $this->getViewStub($name);
				break;
			case 'lang':
				$filePath = $basePath.'/resources/lang/jp/'.$this->modelEntities.'.php';
				//START - Tanmnt
                $filePathVn = $basePath.'/resources/lang/vn/'.$this->modelEntities.'.php';
                $filePathEn = $basePath.'/resources/lang/en/'.$this->modelEntities.'.php';
                // End - Tanmnt
                $content = $this->getLangStub($name);
                break;

			case 'model':
				$filePath = $basePath.'/app/Models/'.$this->namespacePath.$this->modelName.'.php';
				$content = $this->getModelStub($name);
				break;
			case 'resource':
				$filePath = $basePath.'/app/Http/Resources/'.$this->namespacePath.$this->modelName.'.php';
				$content = $this->getModelResourceStub($name);
				break;
			case 'resource_collection':
				$filePath = $basePath.'/app/Http/Resources/'.$this->namespacePath.$this->modelName.'Collection.php';
				$content = $this->getModelResourceCollectionStub($name);
				break;

			case 'migration':
				$filePath = $basePath.'/database/migrations/'.$this->migrationFileName.'.php';
				$content = $this->getMigrationStub($name);
				break;

			default:
				$filePath = $basePath.$filePath;
				break;
		}

		if ($this->files->exists($filePath)) {
			return $this->error($intent.' already exists!');
		}
        /**
         * @author tanmnt
         */
        if(isset($filePathVn)){
            // created File VN
            $this->makeDirectory($filePathVn);
            $this->files->put($filePathVn, $content);
        }
        if(isset($filePathEn)){
            //created File EN
            $this->makeDirectory($filePathEn);
            $this->files->put($filePathEn, $content);
        }
        // END //
        $this->makeDirectory($filePath);
		$this->files->put($filePath, $content);
		$this->info($intent.' for: '.$this->modelName.' created successfully');
	}

	/**
	 * Build the directory for the class if necessary.
	 *
	 * @param  string $path
	 * @return string
	 */
	protected function makeDirectory ($path, $commit = false) {
		$permission = $commit ? 0775 : 0775;
		if (!$this->files->isDirectory(dirname($path))) {
			$this->files->makeDirectory(dirname($path), $permission, true, true);
		}
	}

	/**
	 * Compose the Model file stub.
	 *
	 * @return string
	 */
	protected function getModelStub($fileName)
	{
		$modelStub = $this->request->get('softdeletes') ? 'model_with_softdeletes.stub' : 'model.stub';
		$stub = $this->files->get($this->stubsPath . '/models/'.$modelStub);
		return $this->processStub($stub);
	}

	/**
	 * Compose the Model Resource file stub.
	 *
	 * @return string
	 */
	protected function getModelResourceStub($fileName)
	{
		$stub = $this->files->get($this->stubsPath . '/resources/model.stub');
		return $this->processStub($stub);
	}

	/**
	 * Compose the Model Resource Collection file stub.
	 *
	 * @return string
	 */
	protected function getModelResourceCollectionStub($fileName)
	{
		if(!$this->request->get('api_code')) { return false; }
		$stub = $this->files->get($this->stubsPath . '/resources/model_collection.stub');
		return $this->processStub($stub);
	}


	/**
	 * Compose the migration file stub.
	 *
	 * @return string
	 */
	protected function getMigrationStub($fileName)
	{
		$stub = $this->files->get($this->stubsPath . '/database/migrations/migration.stub');
		return $this->processStub($stub);
	}


	/**
	 * Compose widget file stub.
	 *
	 * @return string
	 */
	protected function getControllerStub($web = true)
	{
		$prefix = $web ? '' : 'api_';
		// Don't generate api controllers if not requested
		if (!$web && !$this->request->get('api_code')) {
			return false;
		}
		$controllerStub = $this->request->get('softdeletes') ? $prefix.'controller_with_softdeletes.stub' : $prefix.'controller.stub';
		$stub = $this->files->get($this->stubsPath . '/controllers/'.$controllerStub);
		return $this->processStub($stub);
	}

	/**
	 * Compose widget file stub.
	 *
	 * @return string
	 */
	protected function getRequestStub()
	{
		$stub = $this->files->get($this->stubsPath . '/requests/request.stub');
		return $this->processStub($stub);
	}

	/**
	 * Compose widget file stub.
	 *
	 * @return string
	 */
	protected function getViewStub($fileName)
	{
		$stubName = $fileName;
		if ($fileName === 'index') {
			$stubName = $this->request->get('softdeletes') ? 'index_with_softdeletes' : 'index';
		}
		$stub = $this->files->get($this->stubsPath . '/views/'.$stubName.'.stub');
		return $this->processStub($stub);
	}

	/**
	 * Compose widget file stub.
	 *
	 * @return string
	 */
	protected function getLangStub($fileName)
	{
		$stubName = $fileName;
		$stub = $this->files->get($this->stubsPath . '/lang/'.$stubName.'.stub');
		return $this->processStub($stub);
	}

	/**
	 * Compose the Routes file stub.
	 *
	 * @return string
	 */
	protected function getRoutesStub ($type = 'backend')
	{
		$prefix = $type === 'backend' ? 'backend' : 'api';
		$routesStub = $this->request->get('softdeletes') ? $prefix.'_with_softdeletes.stub' : $prefix.'.stub';
		$stub = $this->files->get($this->stubsPath . '/routes/'.$routesStub);
		return $this->processStub($stub);
	}

	/**
	 * @param string $stub
	 * @return string
	 * */
	public function processStub ($stub)
	{
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$stub = str_replace('{{DATE}}',$date, $stub);
		$stub = str_replace('{{TIME}}',$time, $stub);
		$stub = str_replace('{{CONTROLLER_CLASS}}',$this->controllerName, $stub);
		$stub = str_replace('{{NAMESPACE}}',$this->namespace, $stub);
		$stub = str_replace('{{GENERAL_NAMESPACE}}',$this->generalNamespace, $stub);
		$stub = str_replace('{{USE_NAMESPACE}}',$this->useNamespace, $stub);
		$stub = str_replace('{{ROUTE_NAMESPACE}}',$this->routeNamespace, $stub);
		$stub = str_replace('{{MODEL_CLASS}}',$this->modelName, $stub);
		$stub = str_replace('{{PLURAL_MODEL_NAME}}',$this->pluralModelName, $stub);
		$stub = str_replace('{{MODEL_ENTITY}}',$this->modelEntity, $stub);
		$stub = str_replace('{{MODEL_ENTITIES}}',$this->modelEntities, $stub);
		$stub = str_replace('{{ROUTE_URL}}',$this->routeUrl, $stub);
		$stub = str_replace('{{REQUEST_CLASS}}',$this->requestName, $stub);
		$stub = str_replace('{{TABLE_NAME}}',$this->tableName, $stub);
		$protectedTableNameStatement = 'protected $table = \''.$this->tableName.'\';';
		$stub = str_replace('//PROTECTED_TABLE_NAME', $protectedTableNameStatement, $stub);

		return $stub;
	}

	/**
	 * Copy generated files
	 * */
	public function copy($file, $conflict = true)
	{
		$source = storage_path($this->operationDirectory.'/'.$file);
		$destination = base_path($file);

		if ($this->files->exists($destination) && $conflict) {
			throw new ProcessException('File already exists: '.$destination.'. Please delete all related files or try restore function if you want to roll back an operation.');
		}

		if ($this->files->exists($source)) {
			$this->makeDirectory($destination);
			$this->files->copy($source, $destination);
		}
	}

	/**
	 * Copy generated files
	 * @return void
	 * */
	public function copyGeneratedFiles()
	{
		$this->copy('routes/backend.php', false); // Non conflicting
		$this->copy('routes/api.php', false); // Non conflicting
		$this->copy('config/backend-menu.php', false); // Non conflicting
		$this->copy('app/Models/'.$this->namespacePath.$this->modelName.'.php');
		$this->copy('database/migrations/'.$this->migrationFileName.'.php');
		$this->copy('app/Http/Requests/'.$this->namespacePath.$this->requestName.'.php');
		$this->copy('app/Http/Controllers/Backend/'.$this->namespacePath.$this->controllerName.'.php');
		$this->copy('resources/views/backend/'.$this->modelEntities.'/index.blade.php');
		$this->copy('resources/lang/jp/'.$this->modelEntities.'.php');
        $this->copy('resources/lang/en/'.$this->modelEntities.'.php');
        $this->copy('resources/lang/vn/'.$this->modelEntities.'.php');
		$this->copy('resources/views/backend/'.$this->modelEntities.'/form.blade.php');
		if ($this->request->get('api_code')) {
			$this->copy('app/Http/Controllers/Api/v1/'.$this->namespacePath.$this->controllerName.'.php');
			$this->copy('app/Http/Resources/'.$this->namespacePath.$this->modelName.'.php');
			$this->copy('app/Http/Resources/'.$this->namespacePath.$this->modelName.'Collection.php');
		}
		//Update api_post_man_collection
		$pathApiPostMan = public_path('builder/postman/api_postman_collection.json');
		$dataApiPostMan = file($pathApiPostMan);
		$lastClosingBrace = max(array_keys($dataApiPostMan)) - 2;
		// Write data
		$dataApiPostMan[$lastClosingBrace] = $dataApiPostMan[$lastClosingBrace].PHP_EOL.$this->updateApiPostman.PHP_EOL;
		$this->files->put($pathApiPostMan, $dataApiPostMan);
	}

	/**
	 * Get Line position of an Splfileobject Object by an identifier string
	 * @return signed int
	 * */
	public function getFileLineByIdentifier($file, $identifier)
	{
		$desiredLine = -10;
		foreach ($file as $lineNumber => $lineContent) {
			if (FALSE !== strpos($lineContent, $identifier)) {
				$desiredLine = $lineNumber; // zero-based
				break;
			}
		}
		return $desiredLine;
	}

	/**
	 * Optimize file stubs
	 * */
	public function stubOptimizer()
	{

		$softdeletes = $this->request->get('softdeletes');
		$timeDataTypes = $this->database->timeTypes();
		$longTextDataTypes = $this->database->longTextDataTypes();

		$apiCode = $this->request->get('api_code');

		$requestStubFilePath = $this->stubsPath.'/requests/request.stub';
		$controllerStubFilePath = $softdeletes ? $this->stubsPath.'/controllers/controller_with_softdeletes.stub' : $this->stubsPath.'/controllers/controller.stub';

		//API Controller
		$apiControllerStubFilePath = $softdeletes ? $this->stubsPath.'/controllers/api_controller_with_softdeletes.stub' : $this->stubsPath.'/controllers/api_controller.stub';

		$formViewStubFilePath = $this->stubsPath.'/views/form.stub';
		$migrationStubFilePath = $this->stubsPath.'/database/migrations/migration.stub';
		$indexViewStubFilePath = $softdeletes ? $this->stubsPath.'/views/index_with_softdeletes.stub' : $this->stubsPath.'/views/index.stub';
		$langStubFilePath = $this->stubsPath.'/lang/lang.stub';

		$requestStub = $this->files->get($requestStubFilePath);
		$controllerStub = $this->files->get($controllerStubFilePath);
		$apiControllerStub = $this->files->get($apiControllerStubFilePath);
		$formViewStub = $this->files->get($formViewStubFilePath);
		$migrationStub = $this->files->get($migrationStubFilePath);
		$indexViewStub = $this->files->get($indexViewStubFilePath);
		$langStub = $this->files->get($langStubFilePath);

		$numberInputStub = $this->files->get($this->stubsPath.'/views/number-input.stub');
		$textInputStub = $this->files->get($this->stubsPath.'/views/text-input.stub');
		$editorInputStub = $this->files->get($this->stubsPath.'/views/editor-input.stub');
		$dateInputStub = $this->files->get($this->stubsPath.'/views/date-input.stub');
		$radioInputStub = $this->files->get($this->stubsPath.'/views/radio-input.stub');
		$stringInputStub = $this->files->get($this->stubsPath.'/views/string-input.stub');
		$fileInputStub = $this->files->get($this->stubsPath.'/views/file-input.stub');
		$imageInputStub = $this->files->get($this->stubsPath.'/views/image-input.stub');
		$indexSearchLineStub = $this->files->get($this->stubsPath.'/views/index-search.stub');

		//Stubs postman
		$apiPostmanStub = $this->files->get(
			$softdeletes ?
					$this->stubsPath.'/postman/api_with_softdeletes.stub'
				: 	$this->stubsPath.'/postman/api.stub'
		);

		// Write migration lines
		$migrationStatements = $this->request->get('migration');

		// check if migration contains `id` field. If it was accidentally deleted (user error), lets add it back
		$foundIdField = false;

		foreach ($migrationStatements as $migrationLine) {
			if ($migrationLine['field_name'] === 'id') {
				$foundIdField = true;
			}
		}

		if ($foundIdField === false) {
			$newFields = [];
			$newFields[] = [
				'data_type' => 'increments',
				'field_name' => 'id',
				'nullable' => false,
				'default' => null,
				'index' => null,
				'unique' => null,
				'show_index' => true,
				'can_search' => false,
				'is_file' => false,
				'file_type' => null,
				'in_form' => false
			];

			$migrationStatements = array_merge($newFields, $migrationStatements);
		}

		$migrationLines = '';
		$controllerLines = '';
		$viewFormLines = '';
		$indexTableHeaderLines = '';
		$indexTableRows = '';
		$indexTableSearchLines = '';
		$controllerSearchLines = '';
		$requestLines = '';
		$messageRequestLines = '';
		$langLines = '';
		$searchIndex = 0;

		//Api field
		$apiField = '';
		//API_UPDATE_POST_STATEMENTS
		$apiUpdatePostStatements = '';
		$apiHashString = '';
		$apiHashStringPostMan = '';

		// Prepare stub
		foreach ($migrationStatements as $line) {

			$dataType = $line['data_type'];
			$fieldName = $line['field_name'];
			$transFieldName = $line['trans_field_name'];
			$col_sm = $line['col_sm'];
			$nullable = $line['nullable'];
			// Not letting users set a default value for date fields and enum. Fault prone
			$default = (in_array($dataType, $timeDataTypes) || $dataType === 'enum') ? '' : $line['default'];
			$dataTypeParameter = ($dataType === 'enum') ? $line['default'] : null;
			$index = $line['index'];
			$unique = $line['unique'];
			$showIndex = $line['show_index'];
			$canSearch = $line['can_search'];
			$isFile = $line['is_file'];
			$fileType = $line['file_type'];
			$inForm = $line['in_form'];
			$email = $line['email'];

			// Process $dataTypeParameter
			if ($dataTypeParameter) {
				$sanitizedDataTypeParameter = '[';
				$params = explode(',', $line['default']);
				foreach ($params as $param) {
					$endingComma = ($param !== end($params)) ? "," : '';
					$param = str_replace("'", '', $param);
					$param = str_replace('"', '', $param);
					$sanitizedDataTypeParameter = $sanitizedDataTypeParameter."'".trim($param)."'".$endingComma;
				}
				$sanitizedDataTypeParameter = $sanitizedDataTypeParameter.']';
				$dataTypeParameter = $sanitizedDataTypeParameter;
			}
			$dataTypeParameterPreceedingComma = $dataTypeParameter ? ', ' : '';

			$statement = '$table->'."{TYPE}('{FIELD}'{PARAM})";
			/**
			 * Migration optimization
			 * */
			$statement = str_replace('{TYPE}', $dataType, $statement);
			$statement = str_replace('{FIELD}', $fieldName, $statement);
			$statement = str_replace('{PARAM}', $dataTypeParameterPreceedingComma.$dataTypeParameter, $statement);


			if ($nullable) {
				$statement = $statement.'->nullable()';
			}

			if ($index) {
				$statement = $statement.'->index()';
			}

			if ($unique) {
				$statement = $statement.'->unique()';
			}

			if ($default) {
				$defaultVal = is_numeric($default) ? $default : "'".$default."'";
				$statement = $statement.'->default('.$defaultVal.')';
			}

			$migrationLines = $migrationLines.$statement.';'.PHP_EOL."\t\t\t";

			/**
			 * Request Rules optimization
			 * */

			if (!in_array($fieldName, ['created_at', 'updated_at', 'deleted_at', 'id']) && $inForm) {
				$tab = strlen($requestLines) > 0 ? "\t\t\t" : '';
				$requestTab = strlen($requestLines) > 0 ? "\t\t\t" : '';

				/**
				 * Controller Optimization
				 * */
				$tabController = strlen($controllerLines) < 1 ? '' : "\t\t\t";

				if ($apiCode && $isFile) {
					//Must setting  'upload_max_filesize' or 'post_max_size' php.ini
					$fileUploadStatement = 'if ($request->hasFile(\''.$fieldName.'\')) {'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t\t".'${{MODEL_ENTITY}}->'.$fieldName.' = MediaHelper::uploadFileToServer($request->file(\''.$fieldName.'\'), \'/'.$this->modelEntities.'/\');'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t".'} else {'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t\t".'${{MODEL_ENTITY}}->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t".'}'.PHP_EOL;
					$controllerLines = $controllerLines.$tabController.$fileUploadStatement;
				}
				elseif ($dataType === 'boolean') {
					$controllerLines = $controllerLines.$tabController.'${{MODEL_ENTITY}}->'.$fieldName.' = $request->get(\''.$fieldName.'\', 0);'.PHP_EOL;

					$apiHashString .= '$request->get(\''.$fieldName.'\', \'\').';
					$apiHashStringPostMan .= $fieldName.'.\n';
				}
				else {
					$controllerLines = $controllerLines.$tabController.'${{MODEL_ENTITY}}->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;

					$apiHashString .= '$request->get(\''.$fieldName.'\', \'\').';
					$apiHashStringPostMan .= $fieldName.'.\n';
				}

				//API_UPDATE_POST_STATEMENTS
				$apiUpdatePostStatements .= $this->apiUpdatePostStatements($apiCode, $isFile, $tabController, $fieldName);

				// Write rules stub
				$required = $nullable ? "'nullable'" : "'required'";
				$fileRequest = '';//($isFile && $fileType === 'image') ? ", 'image'" : "";
				$dateFormat = '';//", 'date_format:Y-m-d H:i:s' ";
				$dateTime = in_array($dataType, $timeDataTypes) ? ", 'date' ". $dateFormat : "";
				$bool = $dataType === "boolean" ? ", 'boolean'" : "";
				$maxString = $dataType === "string" ? ", 'max:191'" : "";
				//, 'unique:table,column,'. $this->get('id') .',id' -> set id to update method
				//$isUnique = $unique ? ', \'unique:'.$this->tableName .','.$fieldName.',\'. $this->get(\'id\') .\',id\'' : '';
				$isUnique = $unique ? ', Rule::unique(\''.$this->tableName .'\')->ignore($this->get(\'id\'))' : '';
				$isEmail = $email ? ", 'email'" : "";
				$integer = str_contains($dataType, 'int') ? ", 'integer'" : "";
				$float = str_contains($dataType, 'double') ? ", 'numeric'" : "";
				$in = $dataType === 'enum' ? ", Rule::in(".$dataTypeParameter.")" : "";
				$requestLines = $requestLines.$requestTab."'$fieldName' => [".$required.$isEmail.$maxString.$isUnique.$dateTime.$fileRequest.$bool.$in.$integer.$float."],".PHP_EOL;
				//$messageRequestLines
				if(!$nullable) {
					$messageRequestLines = $messageRequestLines. "'$fieldName.required' => trans('validation.required', ['attribute' => trans('$this->modelEntities.$fieldName')]),". PHP_EOL. "\t\t\t";
				}
				/**
				 * View Form optimization
				 * */
				if (in_array($dataType, $timeDataTypes)) {
					$viewFormLines = $viewFormLines.$dateInputStub.PHP_EOL;
				}
				elseif ($dataType === 'boolean') {
					$viewFormLines = $viewFormLines.$radioInputStub.PHP_EOL;
				}
				elseif ($dataType === 'text') {
					$viewFormLines = $viewFormLines.$textInputStub.PHP_EOL;
				}
				elseif (str_contains($dataType, 'int')) {
					$viewFormLines = $viewFormLines.$numberInputStub.PHP_EOL;
				}
				elseif (in_array($dataType, $longTextDataTypes)) {
					$viewFormLines = $viewFormLines.$editorInputStub.PHP_EOL;
				}
				elseif ($isFile) {
					if ($fileType === 'image') {
						$viewFormLines = $viewFormLines.$imageInputStub.PHP_EOL;
						//Update content stub postman
					}
	//                    else {
	//                        $viewFormLines = $viewFormLines.$fileInputStub.PHP_EOL;
	//                    }
				}
				else {
					$viewFormLines = $viewFormLines.$stringInputStub.PHP_EOL;
					//Update content stub postman
				}

				//update json postman
				if($isFile) {
					$apiField .= "{
						\"key\": \"$fieldName\",
						\"value\": \"\",
						\"description\": \"\",
						\"type\": \"file\"
					},";
				}
				else {
					$apiField .= "{
						\"key\": \"$fieldName\",
						\"value\": \"\",
						\"description\": \"\",
						\"type\": \"text\"
					},";
				}
			}

			if ($showIndex) {
				//Check is sort
				$isSort = $line['is_sort'];
				$strSort = $isSort ? "@include('backend._partials.controls.sort-icon', ['field' => '$fieldName'])" : '';

				//Set class td
				$class = '';
				if (str_contains($dataType, 'date') || $fieldName == 'id') {
					$class = 'text-center';
				}
				elseif (str_contains($dataType, 'int')) {
					$class = 'text-right';
				}
				else {
					$class = 'text-left';
				}

				// Write index stub
				$indexTableHeaderLines = $indexTableHeaderLines.'<th>'."{{ trans('$this->modelEntities.$fieldName') }} $strSort".'</th>'.PHP_EOL. "\t\t\t\t\t\t\t\t\t\t";
				if($isFile && $fileType === 'image') {
					$indexTableRows = $indexTableRows.'<td class="text-center"><img width="150" src="{{ strlen($'.$this->modelEntity.'->'.$fieldName.') > 0 ? $'.$this->modelEntity.'->'.$fieldName.' : "/uploads/image/default.jpg" }}"></td>'.PHP_EOL. "\t\t\t\t\t\t\t\t\t\t\t\t";
				} //Create boolean field
				elseif ($dataType == 'boolean') {
					$indexTableRows = $indexTableRows.'<td class="text-center"><a href="javascript:void(0);" table="{{ $'. $this->modelEntity .'->getTable() }}" id-value="{{ isset($'. $this->modelEntity .'->id) ? $' . $this->modelEntity . '->id : \'\' }}" column-name="'. $fieldName .'" boolean="{{ $'. $this->modelEntity .'->'. $fieldName .' }}" class="js-table-change-boolean label @if($'. $this->modelEntity .'->'. $fieldName .' == 1) label-success @else label-warning @endif"> {{ showBoolean($'. $this->modelEntity .'->'. $fieldName .') }} </a>'. '</td>'.PHP_EOL. "\t\t\t\t\t\t\t\t\t\t\t\t";
				}
				else {
					$indexTableRows = $indexTableRows.'<td class="'.$class.'"> {!! $'.$this->modelEntity.'->'.$fieldName.' !!}'.'</td>'.PHP_EOL. "\t\t\t\t\t\t\t\t\t\t\t\t";
				}
			}

			if ($canSearch) {
				$indexTableSearchLines = $indexTableSearchLines.$indexSearchLineStub;
				if($searchIndex < 1) {
					$controllerSearchLines =  $controllerSearchLines.'->where(\''.$fieldName.'\', \'LIKE\', \'%\'.$request->get(\''.'keyword'.'\').\'%\')'.PHP_EOL."\t\t\t\t\t\t\t\t";

				}
				else {
					$controllerSearchLines =  $controllerSearchLines.'->orWhere(\''.$fieldName.'\', \'LIKE\', \'%\'.$request->get(\''.'keyword'.'\').\'%\')'.PHP_EOL."\t\t\t\t\t\t\t\t";

				}
				$searchIndex += 1;
			}

			// Process view form lines
			$viewFormLines = str_replace('{{FIELD_NAME_LABEL}}', "{{ trans('$this->modelEntities.$fieldName') }}", $viewFormLines);
			$viewFormLines = str_replace('{{FIELD_NAME}}', $fieldName, $viewFormLines);
			$isRequired = $nullable ? 'false' : 'true';
			$viewFormLines = str_replace('{{IS_REQUIRED}}', $isRequired, $viewFormLines);

			// Process view form responsiveness
			$viewFormLines = str_replace('COL_SM', 'col-sm-'.$col_sm, $viewFormLines);

			//Process Lang
			if(strlen($transFieldName) <= 0) {
				$transFieldName = $fieldName;
			}
			$langLines = $langLines . "'$fieldName' => '$transFieldName'," .PHP_EOL. "\t";
		}

		$migrationLines = $migrationLines.'$table->timestamps();'.PHP_EOL."\t\t\t\t";

		if ($this->request->get('softdeletes')) {
			$migrationLines = $migrationLines.'$table->softdeletes();'.PHP_EOL."\t\t\t\t";
		}

		// Optimize and rewrite Stubs
		// Migration
		$migrationStub = str_replace('//MIGRATION_LINES', $migrationLines, $migrationStub);
		$this->files->put($migrationStubFilePath, $migrationStub);
		// Controller
		$controllerStub = str_replace('//POST_STATEMENTS', $controllerLines, $controllerStub);
		$controllerStub = str_replace('//SEARCH_STATEMENTS', $controllerSearchLines, $controllerStub);
		$this->files->put($controllerStubFilePath, $controllerStub);

		if ($apiCode) {
			$apiControllerStub = str_replace('//POST_STATEMENTS', $controllerLines, $apiControllerStub);
			$apiControllerStub = str_replace('//HASH_STRING', $apiHashString, $apiControllerStub);
			$apiControllerStub = str_replace('//API_UPDATE_POST_STATEMENTS', $apiUpdatePostStatements, $apiControllerStub);
			$apiControllerStub = str_replace('//API_RULES', $requestLines, $apiControllerStub);
			$apiControllerStub = str_replace('//SEARCH_STATEMENTS', $controllerSearchLines, $apiControllerStub);
			$this->files->put($apiControllerStubFilePath, $apiControllerStub);
		}

		// Form View
		$formViewStub = str_replace('//FORM_LINES', $viewFormLines, $formViewStub);
		$this->files->put($formViewStubFilePath, $formViewStub);


		$indexViewStub = str_replace('//TABLE_HEADER_LINES', $indexTableHeaderLines, $indexViewStub);
		$indexViewStub = str_replace('//TABLE_ROW_LINES', $indexTableRows, $indexViewStub);
	//        $indexViewStub = str_replace('//SEARCH_VIEW_LINES', $indexTableSearchLines, $indexViewStub);
		$this->files->put($indexViewStubFilePath, $indexViewStub);

		// Request Rules
		$requestStub = str_replace('//RULE_LINES', $requestLines, $requestStub);
		$requestStub = str_replace('//MESSAGE_RULE_LINES', $messageRequestLines, $requestStub);
		$this->files->put($requestStubFilePath, $requestStub);

		//Lang rules
		$transModel = $this->request->get('trans_model');
		if(strlen($transModel) <= 0) {
			$transModel = $this->modelEntities;
		}

		$langStub = str_replace('//FIELD_LANG', $langLines, $langStub);
		$langStub = str_replace('{{ENTITIES}}', $this->modelEntities, $langStub);
		$langStub = str_replace('{{TRANS_ENTITIES}}', $transModel, $langStub);
		$this->files->put($langStubFilePath, $langStub);

		//Postman api
		$apiPostmanStub = str_replace('//API_FIELD_UPDATE', $apiField,$apiPostmanStub);
		$this->updateApiPostman = str_replace('{{ROUTE_URL}}', $this->routeUrl,$apiPostmanStub);
//		var_dump($this->updateApiPostman);exit;
		$hashStringSearch = '--'.$this->routeUrl.'_CREATE_HASH_KEY--';
		$this->updateApiPostman = str_replace($hashStringSearch, $apiHashStringPostMan.$hashStringSearch,$this->updateApiPostman);
		$hashStringSearch = '--'.$this->routeUrl.'_UPDATE_HASH_KEY--';
		$this->updateApiPostman = str_replace($hashStringSearch, $apiHashStringPostMan.$hashStringSearch,$this->updateApiPostman);
		//add to POSTMANFILE in function copy file
	}

	private function apiUpdatePostStatements($apiCode, $isFile, $tabController, $fieldName)
	{
		//Must setting  'upload_max_filesize' or 'post_max_size' php.ini
		$str = '';
		if ($apiCode && $isFile) {
			$fileUploadStatement = 'if ($request->hasFile(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'${{MODEL_ENTITY}}->'.$fieldName.' = MediaHelper::uploadFileToServer($request->file(\''.$fieldName.'\'), \'/'.$this->modelEntities.'/\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'} else {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'if ($request->get(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t\t".'${{MODEL_ENTITY}}->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'}'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'}'.PHP_EOL;
			$str = $tabController.$fileUploadStatement;
		} else {
			$fileUploadStatement = 'if ($request->get(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'${{MODEL_ENTITY}}->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'}'.PHP_EOL;
			$str = $tabController.$fileUploadStatement;
		}

		return $str;
	}
}
