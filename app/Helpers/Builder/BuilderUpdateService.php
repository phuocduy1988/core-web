<?php
namespace App\Helpers\Builder;

use App\Exceptions\ProcessException;
use App\Helpers\Helper;
use File;
use Validator;
use Schema;
use Storage;
use Artisan;
use Carbon\Carbon;
use SplFileObject;
use Monolog\Logger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\Filesystem;

/**
 * @Class BuilderUpdateService
 * @package App\Helpers\Builder
 * @Description
 * @Author DuyLBP
 * @Date 7/12/18
 */
class BuilderUpdateService {
	/**
	 * Request
	 * */
	protected $request;

	/**
	 * The filesystem instance.
	 *
	 * @var Filesystem
	 */
	protected $files;

	/**
	 * The Stubs default path.
	 *
	 * @var Filesystem
	 */
	protected $stubsPath;
	protected $backupPath;
	protected $requestPath;
	protected $backendControllerPath;
	protected $apiControllerPath;
	protected $modelPath;
	protected $migrationPath;
	protected $viewFormPath;
	protected $viewIndexPath;
	protected $langPathJp;
	protected $langPathVn;
	protected $langPathEn;
	protected $postManPath;
	/**
	 * @var model
	 * */
	protected $modelName;
	protected $routeUrl;
	/**
	 * @var \App\Helpers\Builder\Database
	 */
	protected $database;
	/**
	 * @var
	 */
	protected $tableName;
	/**
	 * @var
	 */
	protected $pluralModelName;
	/**
	 * @var controller
	 * */
	protected $controllerName;
	/**
	 * @var migration
	 * */
	protected $migrationClass;
	/**
	 * @var request
	 * */
	protected $requestName;
	/**
	 * @var model entity
	 * */
	protected $modelEntity;
	/**
	 * @var model entities
	 * */
	protected $modelEntities;
	/**
	 * @var Laravels' Artisan Instance
	 * */
	protected $artisan;
	protected $migrationFileName;
	/**
	 * @var current operation ID and directory
	 * */
	protected $currentOperationId;
	/**
	 * @var string
	 */
	protected $operationDirectory;

	/**
	 * Service construct
	 * @param Request $request
	 * */
	public function __construct (Request $request) {
		$this->request = $request;
		$this->database  = new Database();
		$this->files = new Filesystem;
		$this->errorsArray = [];
		$this->infoArray = [];
		$this->currentOperationId = str_random(12);
		$this->operationDirectory = 'onetech/'.date('Y_m_d_his').'_'.$this->currentOperationId;
		$this->stubsPath = storage_path($this->operationDirectory.'/stubs');
		$this->backupPath = storage_path($this->operationDirectory.'/backup');
	}

	/**
	 * @Description
	 * @Author DuyLBP
	 * @Date 7/12/18
	 */
	public function buildFromRequest() {

		$this->initialize();
		$this->validate();
		$this->cleanup();
		$this->processingUpdateModel();

		// All good! Copy files
		$this->copyGenFiles();
		$this->cleanup();
		Artisan::call('migrate');
		return $this->routeUrl;
	}

	/**
	 *Initialize filesystem and decide names for files
	 * @return
	 * */
	public function initialize () {

		$modelName = $this->request->get('model');

		//TextSample
		$this->modelName = studly_case($modelName);
		//text-samples
		$this->routeUrl = snake_case(str_plural($this->modelName), '-');
		//TextSamples
		$this->pluralModelName = str_plural($this->modelName);
		//textSample
		$this->modelEntity = camel_case($this->modelName);
		//textSamples
		$this->modelEntities = camel_case(str_plural($this->modelName));
		//TextSampleController
		$this->controllerName = $this->modelName.'Controller';
		//TextSampleRequest
		$this->requestName = $this->modelName.'Request';
		//text_sample
		$this->tableName = snake_case(str_plural($this->modelName));
		//2018_07_12_045846_update_unique_token_code_text_sample_table
		$keyGen = Helper::genKey(6);
		$this->migrationFileName = date('Y_m_d_his').'_update_'. $keyGen .'_'.$this->tableName.'_table';
		$this->migrationClass = studly_case('update_'. $keyGen .'_'.$this->tableName.'_table');

		/* //Debug Code
		$resourceCollection = [
			'modelName' => $this->modelName,
			'pluralModelName' => $this->pluralModelName,
			'modelEntity' => $this->modelEntity,
			'modelEntities' => $this->modelEntities,
			'controllerName' => $this->controllerName,
			'requestName' => $this->requestName,
			'tableName' => $this->tableName,
			'migrationFileName' => $this->migrationFileName
		];
		echo "<pre>"; print_r($resourceCollection); exit;
		*/

		$this->makeDirectory($this->stubsPath);
		$this->makeDirectory($this->backupPath);

		$this->requestPath 				= 'app/Http/Requests/'.$this->requestName.'.php';
		$this->backendControllerPath 	= 'app/Http/Controllers/Backend/'.$this->controllerName.'.php';
		$this->apiControllerPath 		= 'app/Http/Controllers/Api/v1/'.$this->controllerName.'.php';
		$this->viewIndexPath 			= 'resources/views/backend/'.$this->modelEntities.'/index.blade.php';
		$this->viewFormPath 			= 'resources/views/backend/'.$this->modelEntities.'/form.blade.php';
		$this->langPathJp 				= 'resources/lang/jp/'.$this->modelEntities.'.php';
		$this->langPathEn 				= 'resources/lang/en/'.$this->modelEntities.'.php';
		$this->langPathVn 				= 'resources/lang/vn/'.$this->modelEntities.'.php';
		$this->modelPath 				= 'app/Models/'.$this->modelName.'.php';
		$this->migrationPath 			= 'database/migrations/'.$this->migrationFileName.'.php';
		$this->postManPath 				= 'public/builder/postman/api_postman_collection.json';

		$this->files->copyDirectory(public_path('builder/stubs/'), $this->stubsPath);

	}

	/**
	 * Check if table already exists in Schema
	 * @return  bool
	 **/
	public function tableExists () {
		return Schema::hasTable($this->tableName);
	}

	/**
	 * Validate this operation
	 * @return  mixed
	 * */
	public function validate () {


		$modelFileExists = $this->files->exists(base_path($this->modelPath));
		$backendControllerFileExists = $this->files->exists(base_path($this->backendControllerPath));
		$apiControllerFileExists = $this->files->exists(base_path($this->apiControllerPath));
		$requestFileExists = $this->files->exists(base_path($this->requestPath));

		$tableExists = $this->tableExists();

		$indexFileExists = $this->files->exists(base_path($this->viewIndexPath));
		$formFileExists = $this->files->exists(base_path($this->viewFormPath));

		$migrationStatements = $this->request->get('migration');


		$validator = Validator::make($this->request->all(), []);

		$that = $this;

		$validator->after(function($validator)
		use (
			$modelFileExists,
			$backendControllerFileExists,
			$apiControllerFileExists,
			$requestFileExists,
			$indexFileExists,
			$formFileExists,
			$tableExists,
			$migrationStatements,
			$that
		) {

			if (strlen($that->modelName) < 2 || strlen($that->modelName) > 50) {
				$validator->errors()->add('Model', 'Model name must be between 2 and 50 characters.');
			}

			if (preg_match('/[\'^£$%&*()}{@#~?><>.,|=_+¬-]/', $that->modelName)){
				$validator->errors()->add('Model', 'Model name contains invalid characters. It should only contain letters and numbers.');
			}

			if (
				preg_match('/^\d/', $that->modelName) === 1
			) {
				$validator->errors()->add('Model', 'Model name cannot start with a number.');
			}

			if (!$modelFileExists) {
				$validator->errors()->add('Model', 'Model file with name <code>'.$this->modelName.'.php</code> do not exists!');
			}

			if (!$backendControllerFileExists) {
				$validator->errors()->add('Backend Controller', 'Backend Controller file with name <code>'.$this->controllerName.'.php</code> do not exists!');
			}

			if (!$apiControllerFileExists) {
				$validator->errors()->add('Api Controller', 'Api Controller file with name <code>'.$this->controllerName.'.php</code> do not exists!');
			}

			if (!$requestFileExists) {
				$validator->errors()->add('Request', 'Request file with name <code>'.$this->requestName.'.php</code> do not exists!');
			}

			if (!$indexFileExists) {
				$validator->errors()->add('View Index', 'View index file with name <code>index.blade.php</code> do not exists!');
			}

			if (!$formFileExists) {
				$validator->errors()->add('View Form', 'View form file with name <code>form.blade.php</code> do not exists!');
			}

			if (!$tableExists) {
				$validator->errors()->add('Table', 'Table <code>'.$this->tableName.'</code> do not exists!');
			}

			$migrationTableColumnNamePattern = '/^[a-zA-Z_][a-zA-Z0-9_]*$/';

			$atLeastOneFieldInForm = false;

			// Validate migration statements
			foreach ($migrationStatements as $migration) {

				$dataType = $migration['data_type'];
				$fieldName = $migration['field_name'];
				$default = $migration['default'];
				$isFile = $migration['is_file'];
				$inForm = $migration['in_form'];

				if ($inForm) {
					$atLeastOneFieldInForm = true;
				}

				if ($dataType === 'enum' && strlen($default) === 0) {
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is of type ENUM. It must have default value set.');
				}

				if(Schema::hasColumn($this->tableName, $fieldName)) {
					$validator->errors()->add('Migration', 'Field name <code>'.$fieldName.'</code> already exist!');
				}

				if (!$fieldName) {
					$validator->errors()->add('Migration', 'Migration statement must have a field name.');
				}

				if ($isFile && $dataType !== 'string') {
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is of type FILE. It must have a data type String.');
				}

				if (preg_match('/[\'^£$%&*()}{@#~?><>.,|=+¬-]/', $fieldName)){
					$validator->errors()->add('Migration', 'Field <code>'.$fieldName.'</code> name contains invalid characters. It should only contain letters and numbers.');
				}

				if (preg_match('/[\'^£$%&*()}{@#~?><>.|=_+¬-]/', $default)){
					$validator->errors()->add('Migration', 'Field <code>'.$fieldName.'</code> default value contains illegal characters.');
				}

				if ($dataType === 'increments' && $fieldName !== 'id') {
					$validator->errors()->add('Migration', 'Only <code>id</code> field can have <code>increments</code> data type. Please change data type of <code>'.$fieldName.'</code> to something else.');
				}

				if(!preg_match($migrationTableColumnNamePattern, $fieldName)){
					$validator->errors()->add('Migration', 'Migration statement for field <code>'.$fieldName.'</code> is invalid. The field name must start with letters and must not contain special characters.');
				}

			}

//			if ($atLeastOneFieldInForm === false) {
//				$validator->errors()->add('Migration', 'At least one field should exist in the form. Please check <code>In Form</code> for at least one field below.');
//			}

		});

		$validator->validate();
		return true;
	}

	/**
	 * Copy generated files
	 * */
	public function copy($file, $conflict = true)
	{
		$source = storage_path($this->operationDirectory.'/'.$file);
		$destination = base_path($file);

		if ($this->files->exists($destination) && $conflict) {
			throw new ProcessException('File already exists: '.$destination.'. Please delete all related files or try restore function if you want to roll back an operation.');
		}

		if ($this->files->exists($source)) {
			$this->makeDirectory($destination);
			//Duy update 2018-7-27
			if(str_contains($destination, 'resources/lang')) {
				if(str_contains($destination, 'resources/lang/vn')) {
					$destinationVN = base_path('resources/lang/vn/'.$this->modelEntities.'.php');
					$this->files->copy($source, $destinationVN);
				}

				if(str_contains($destination, 'resources/lang/en')) {
					$destinationEN = base_path('resources/lang/en/'.$this->modelEntities.'.php');
					$this->files->copy($source, $destinationEN);

				}
			}
			//Duy end 2018-7-27


			$this->files->copy($source, $destination);
		}
	}

	public function copyGenFiles() {
		//Copy file from TEMP to BASEPATH
		$this->copy($this->migrationPath, false);
		$this->copy($this->backendControllerPath, false);
		$this->copy($this->apiControllerPath, false);
		$this->copy($this->viewFormPath, false);
		$this->copy($this->viewIndexPath, false);
		$this->copy($this->langPathJp, false);
		$this->copy($this->langPathEn, false);
		$this->copy($this->langPathVn, false);
		$this->copy($this->requestPath, false);
		$this->copy($this->postManPath, false);

		//		$this->requestPath 				= 'app/Http/Requests/'.$this->requestName.'.php';
		//		$this->backendControllerPath 	= 'app/Http/Controllers/Backend/'.$this->controllerName.'.php';
		//		$this->apiControllerPath 		= 'app/Http/Controllers/Api/v1/'.$this->controllerName.'.php';
		//		$this->viewIndexPath 			= 'resources/views/backend/'.$this->modelEntities.'/index.blade.php';
		//		$this->viewFormPath 			= 'resources/views/backend/'.$this->modelEntities.'/form.blade.php';
		//		$this->langPath 				= 'resources/lang/jp/'.$this->modelEntities.'.php';
		//		$this->modelPath 				= 'app/Models/'.$this->modelName.'.php';
		//		$this->migrationPath 			= 'database/migrations/'.$this->migrationFileName.'.php';
		//		$this->postManPath 				= 'builder/postman/api_postman_collection.json';

	}

	/**
	 * Build the directory for the class if necessary.
	 *
	 * @param  string $path
	 * @return string
	 */
	protected function makeDirectory ($path, $commit = false) {
		$permission = $commit ? 0775 : 0775;
		if (!$this->files->isDirectory(dirname($path))) {
			$this->files->makeDirectory(dirname($path), $permission, true, true);
		}
	}

	/**
	 * Do a general cleanup for autoloads and compiled class names
	 * @return
	 * */
	public function cleanup () {
		// Dump Composer autoload
		try {
			$process = system('composer dump-autoload');
		} catch (\Exception $e) {
			// Silence!
		}

		Artisan::call('clear-compiled');
		Artisan::call('route:clear');
		Artisan::call('config:clear');
		Artisan::call('view:clear');
	}

	/**
	 * Optimize file stubs
	 * */
	public function processingUpdateModel()
	{

		$timeDataTypes = $this->database->timeTypes();
		$longTextDataTypes = $this->database->longTextDataTypes();

		// Copy backend controller file
		$beControllerFilePath = base_path($this->backendControllerPath);
		$beControllerFileTmpPath = storage_path($this->operationDirectory.'/'.$this->backendControllerPath);
		$beControllerFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->backendControllerPath);
		$this->makeDirectory($beControllerFileTmpPath);
		$this->makeDirectory($beControllerFileBackupPath);
		//Copy to file tmp to action
		$this->files->copy($beControllerFilePath, $beControllerFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$this->files->copy($beControllerFilePath, $beControllerFileBackupPath);
		$beControllerFile = file($beControllerFileTmpPath);

		// Copy Api controller file
		$apiControllerFilePath = base_path($this->apiControllerPath);
		$apiControllerFileTmpPath = storage_path($this->operationDirectory.'/'.$this->apiControllerPath);
		$this->makeDirectory($apiControllerFileTmpPath);
		//Copy to file tmp to action
		$this->files->copy($apiControllerFilePath, $apiControllerFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$apiControllerFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->apiControllerPath);
		$this->makeDirectory($apiControllerFileBackupPath);
		$this->files->copy($apiControllerFilePath,$apiControllerFileBackupPath);
		$apiControllerFile = file($apiControllerFileTmpPath);

		// Copy Request file
		$requestFilePath = base_path($this->requestPath);
		$requestFileTmpPath = storage_path($this->operationDirectory.'/'.$this->requestPath);
		$this->makeDirectory($requestFileTmpPath);
		//Copy to file tmp to action
		$this->files->copy($requestFilePath, $requestFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$requestFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->requestPath);
		$this->makeDirectory($requestFileBackupPath);
		$this->files->copy($requestFilePath, $requestFileBackupPath);
		$requestFile = file($requestFileTmpPath);

		// Copy lang JP file
		$langFilePathJp = base_path($this->langPathJp);
		$langFileTmpPathJp = storage_path($this->operationDirectory.'/'.$this->langPathJp);
		$this->makeDirectory($langFileTmpPathJp);
		//Copy to file tmp to action
		$this->files->copy($langFilePathJp, $langFileTmpPathJp);
		//Copy file to backup, can restore if have any errors
		$langFileBackupPathJp = storage_path($this->operationDirectory.'/backup/'.$this->langPathJp);
		$this->makeDirectory($langFileBackupPathJp);
		$this->files->copy($langFilePathJp,$langFileBackupPathJp);
		$langFileJp = file($langFileTmpPathJp);

		// Copy lang En file
		$langFilePathEn = base_path($this->langPathEn);
		$langFileTmpPathEn = storage_path($this->operationDirectory.'/'.$this->langPathEn);
		$this->makeDirectory($langFileTmpPathEn);
		//Copy to file tmp to action
		$this->files->copy($langFilePathEn, $langFileTmpPathEn);
		//Copy file to backup, can restore if have any errors
		$langFileBackupPathEn = storage_path($this->operationDirectory.'/backup/'.$this->langPathEn);
		$this->makeDirectory($langFileBackupPathEn);
		$this->files->copy($langFilePathEn,$langFileBackupPathEn);
		$langFileEn = file($langFileTmpPathEn);

		// Copy lang Vn file
		$langFilePathVn = base_path($this->langPathVn);
		$langFileTmpPathVn = storage_path($this->operationDirectory.'/'.$this->langPathVn);
		$this->makeDirectory($langFileTmpPathVn);
		//Copy to file tmp to action
		$this->files->copy($langFilePathVn, $langFileTmpPathVn);
		//Copy file to backup, can restore if have any errors
		$langFileBackupPathVn = storage_path($this->operationDirectory.'/backup/'.$this->langPathVn);
		$this->makeDirectory($langFileBackupPathVn);
		$this->files->copy($langFilePathVn,$langFileBackupPathVn);
		$langFileVn = file($langFileTmpPathVn);

		// Copy view index file
		$indexFilePath = base_path($this->viewIndexPath);
		$indexFileTmpPath = storage_path($this->operationDirectory.'/'.$this->viewIndexPath);
		$this->makeDirectory($indexFileTmpPath);
		//Copy to file tmp to action
		$this->files->copy($indexFilePath, $indexFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$indexFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->viewIndexPath);
		$this->makeDirectory($indexFileBackupPath);
		$this->files->copy($indexFilePath,$indexFileBackupPath);
		$indexFile = file($indexFileTmpPath);

		// Copy view form file
		$formFilePath = base_path($this->viewFormPath);
		$formFileTmpPath = storage_path($this->operationDirectory.'/'.$this->viewFormPath);
		$this->makeDirectory($formFileTmpPath);
		//Copy to file tmp to action
		$this->files->copy($formFilePath, $formFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$formFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->viewFormPath);
		$this->makeDirectory($formFileBackupPath);
		$this->files->copy($formFilePath, $formFileBackupPath);
		$formFile = file($formFileTmpPath);

		// Copy postman file
		$postManFilePath = base_path($this->postManPath);
		$postManFileTmpPath = storage_path($this->operationDirectory.'/'.$this->postManPath);
		$this->makeDirectory($postManFileTmpPath);
		//Copy to file tmp to action
		$this->files->copy($postManFilePath, $postManFileTmpPath);
		//Copy file to backup, can restore if have any errors
		$postManFileBackupPath = storage_path($this->operationDirectory.'/backup/'.$this->postManPath);
		$this->makeDirectory($postManFileBackupPath);
		$this->files->copy($postManFilePath, $postManFileBackupPath);
		$postManFile = file($postManFileTmpPath);

		$numberInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/number-input.stub'));
		$textInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/text-input.stub'));
		$editorInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/editor-input.stub'));
		$dateInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/date-input.stub'));
		$radioInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/radio-input.stub'));
		$stringInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/string-input.stub'));
		$imageInputStub = $this->processStub($this->files->get($this->stubsPath.'/views/image-input.stub'));

		// Write migration lines
		$migrationStatements = $this->request->get('migration');
		$migrationStubFilePath = $this->stubsPath.'/database/migrations/update-migration.stub';
		$migrationStub = $this->files->get($migrationStubFilePath);

		$migrationLines = '';
		$dropMigrationLines = '';
		$controllerLines = '';
		$viewFormLines = '';
		$indexTableHeaderLines = '';
		$indexTableRows = '';
		$controllerSearchLines = '';
		$requestLines = '';
		$messageRequestLines = '';
		$langLines = '';
		$searchIndex = 0;

		//API
		$apiCode = $this->request->get('api_code');
		//Api field
		$apiField = '';
		//API_UPDATE_POST_STATEMENTS
		$apiUpdatePostStatements = '';
		$apiHashString = '';
		$apiHashStringPostMan = '';

		// Prepare stub
		foreach ($migrationStatements as $line) {

			$dataType = $line['data_type'];
			$fieldName = $line['field_name'];
			$transFieldName = $line['trans_field_name'];
			$col_sm = $line['col_sm'];
			$nullable = $line['nullable'];
			// Not letting users set a default value for date fields and enum. Fault prone
			$default = (in_array($dataType, $timeDataTypes) || $dataType === 'enum') ? '' : $line['default'];
			$dataTypeParameter = ($dataType === 'enum') ? $line['default'] : null;
			$index = $line['index'];
			$unique = $line['unique'];
			$showIndex = $line['show_index'];
			$canSearch = $line['can_search'];
			$isFile = $line['is_file'];
			$fileType = $line['file_type'];
			$inForm = $line['in_form'];
			$email = $line['email'];

			// Process $dataTypeParameter
			if ($dataTypeParameter) {
				$sanitizedDataTypeParameter = '[';
				$params = explode(',', $line['default']);
				foreach ($params as $param) {
					$endingComma = ($param !== end($params)) ? "," : '';
					$param = str_replace("'", '', $param);
					$param = str_replace('"', '', $param);
					$sanitizedDataTypeParameter = $sanitizedDataTypeParameter."'".trim($param)."'".$endingComma;
				}
				$sanitizedDataTypeParameter = $sanitizedDataTypeParameter.']';
				$dataTypeParameter = $sanitizedDataTypeParameter;
			}
			$dataTypeParameterPreceedingComma = $dataTypeParameter ? ', ' : '';

			$statement = '$table->'."{TYPE}('{FIELD}'{PARAM})";
			/**
			 * Migration optimization
			 * */
			$statement = str_replace('{TYPE}', $dataType, $statement);
			$statement = str_replace('{FIELD}', $fieldName, $statement);
			$statement = str_replace('{PARAM}', $dataTypeParameterPreceedingComma.$dataTypeParameter, $statement);


			if ($nullable) {
				$statement = $statement.'->nullable()';
			}

			if ($index) {
				$statement = $statement.'->index()';
			}

			if ($unique) {
				$statement = $statement.'->unique()';
			}

			if ($default) {
				$defaultVal = is_numeric($default) ? $default : "'".$default."'";
				$statement = $statement.'->default('.$defaultVal.')';
			}

			$migrationLines = $migrationLines.$statement.';'.PHP_EOL."\t\t\t";
			$dropMigrationLines = $dropMigrationLines.'$table->dropColumn(\''. $fieldName .'\');'.PHP_EOL."\t\t\t";

			/**
			 * Request Rules optimization
			 * */

			if (!in_array($fieldName, ['created_at', 'updated_at', 'deleted_at', 'id']) && $inForm) {
				$requestTab = "\t\t\t";
				/**
				 * Controller Optimization
				 * */
				$tabController = "\t\t\t";

				if ($apiCode && $isFile) {
					//Must setting  'upload_max_filesize' or 'post_max_size' php.ini
					$fileUploadStatement = 'if ($request->hasFile(\''.$fieldName.'\')) {'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t\t".'$'.$this->modelEntity.'->'.$fieldName.' = MediaHelper::uploadFileToServer($request->file(\''.$fieldName.'\'), \'/'.$this->modelEntities.'/\');'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t".'} else {'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t\t".'$'.$this->modelEntity.'->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
					$fileUploadStatement = $fileUploadStatement."\t\t\t".'}'.PHP_EOL;
					$controllerLines = $controllerLines.$tabController.$fileUploadStatement;
				}
				elseif ($dataType === 'boolean') {
					$controllerLines = $controllerLines.$tabController.'$'.$this->modelEntity.'->'.$fieldName.' = $request->get(\''.$fieldName.'\', 0);'.PHP_EOL;

					$apiHashString .= '$request->get(\''.$fieldName.'\', \'\').';
					$apiHashStringPostMan .= $fieldName.'.\n';
				}
				else {
					$controllerLines = $controllerLines.$tabController.'$'.$this->modelEntity.'->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;

					$apiHashString .= '$request->get(\''.$fieldName.'\', \'\').';
					$apiHashStringPostMan .= $fieldName.'.\n';
				}

				//API_UPDATE_POST_STATEMENTS
				$apiUpdatePostStatements .= $this->apiUpdatePostStatements($apiCode, $isFile, $tabController, $fieldName);

				// Write rules stub
				$required = $nullable ? "'nullable'" : "'required'";
				$fileRequest = '';//($isFile && $fileType === 'image') ? ", 'image'" : "";
				$dateFormat = '';//", 'date_format:Y-m-d H:i:s' ";
				$dateTime = in_array($dataType, $timeDataTypes) ? ", 'date' ". $dateFormat : "";
				$bool = $dataType === "boolean" ? ", 'boolean'" : "";
				$maxString = $dataType === "string" ? ", 'max:191'" : "";
				//, 'unique:table,column,'. $this->get('id') .',id' -> set id to update method
				$isUnique = $unique ? ', Rule::unique(\''.$this->tableName .'\')->ignore($this->get(\'id\'))' : '';
				//$isUnique = $unique ? ', \'unique:'.$this->tableName .','.$fieldName.',\'. $this->get(\'id\') .\',id\'' : '';
				$isEmail = $email ? ", 'email'" : "";
				$integer = str_contains($dataType, 'int') ? ", 'integer'" : "";
				$float = str_contains($dataType, 'double') ? ", 'numeric'" : "";
				$in = $dataType === 'enum' ? ", Rule::in(".$dataTypeParameter.")" : "";
				$requestLines = $requestLines.$requestTab."'$fieldName' => [".$required.$isEmail.$maxString.$isUnique.$dateTime.$fileRequest.$bool.$in.$integer.$float."],".PHP_EOL;
				//$messageRequestLines
				if(!$nullable) {
					$messageRequestLines = $messageRequestLines.$requestTab. "'$fieldName.required' => trans('validation.required', ['attribute' => trans('$this->modelEntities.$fieldName')]),". PHP_EOL. "\t\t\t";
				}
				/**
				 * View Form optimization
				 * */
				if (in_array($dataType, $timeDataTypes)) {
					$viewFormLines = $viewFormLines.$dateInputStub.PHP_EOL;
				}
				elseif ($dataType === 'boolean') {
					$viewFormLines = $viewFormLines.$radioInputStub.PHP_EOL;
				}
				elseif ($dataType === 'text') {
					$viewFormLines = $viewFormLines.$textInputStub.PHP_EOL;
				}
				elseif (str_contains($dataType, 'int')) {
					$viewFormLines = $viewFormLines.$numberInputStub.PHP_EOL;
				}
				elseif (in_array($dataType, $longTextDataTypes)) {
					$viewFormLines = $viewFormLines.$editorInputStub.PHP_EOL;
				}
				elseif ($isFile) {
					if ($fileType === 'image') {
						$viewFormLines = $viewFormLines.$imageInputStub.PHP_EOL;
					}
				}
				else {
					$viewFormLines = $viewFormLines.$stringInputStub.PHP_EOL;
				}

				//update json postman
				if($isFile) {
					$apiField .= "{
						\"key\": \"$fieldName\",
						\"value\": \"\",
						\"description\": \"\",
						\"type\": \"file\"
					},";
				}
				else {
					$apiField .= "{
						\"key\": \"$fieldName\",
						\"value\": \"\",
						\"description\": \"\",
						\"type\": \"text\"
					},";
				}
			}

			if ($showIndex) {
				//Check is sort
				$isSort = $line['is_sort'];
				$strSort = $isSort ? "@include('backend._partials.controls.sort-icon', ['field' => '$fieldName'])" : '';

				//Set class td
				$class = '';
				if (str_contains($dataType, 'date') || $fieldName == 'id') {
					$class = 'text-center';
				}
				elseif (str_contains($dataType, 'int')) {
					$class = 'text-right';
				}
				else {
					$class = 'text-left';
				}

				// Write index stub
				$indexTableHeaderLines = $indexTableHeaderLines. "\t\t\t\t\t\t\t\t\t\t".'<th>'."{{ trans('$this->modelEntities.$fieldName') }} $strSort".'</th>'.PHP_EOL;
				if($isFile && $fileType === 'image') {
					$indexTableRows = $indexTableRows. "\t\t\t\t\t\t\t\t\t\t\t\t".'<td class="text-center"><img width="150" src="{{ strlen($'.$this->modelEntity.'->'.$fieldName.') > 0 ? $'.$this->modelEntity.'->'.$fieldName.' : "/uploads/image/default.jpg" }}"></td>'.PHP_EOL;
				} //Create boolean field
				elseif ($dataType == 'boolean') {
					$indexTableRows = $indexTableRows. "\t\t\t\t\t\t\t\t\t\t\t\t".'<td class="text-center"><a href="javascript:void(0);" table="{{ $'. $this->modelEntity .'->getTable() }}" id-value="{{ isset($'. $this->modelEntity .'->id) ? $' . $this->modelEntity . '->id : \'\' }}" column-name="'. $fieldName .'" boolean="{{ $'. $this->modelEntity .'->'. $fieldName .' }}" class="js-table-change-boolean label @if($'. $this->modelEntity .'->'. $fieldName .' == 1) label-success @else label-warning @endif"> {{ showBoolean($'. $this->modelEntity .'->'. $fieldName .') }} </a>'. '</td>'.PHP_EOL;
				}
				else {
					$indexTableRows = $indexTableRows. "\t\t\t\t\t\t\t\t\t\t\t\t".'<td class="'.$class.'"> {!! $'.$this->modelEntity.'->'.$fieldName.' !!}'.'</td>'.PHP_EOL;
				}
			}

			if ($canSearch) {
				$controllerSearchLines =  $controllerSearchLines."\t\t\t\t\t\t\t\t".'->orWhere(\''.$fieldName.'\', \'LIKE\', \'%\'.$request->get(\''.'keyword'.'\').\'%\')'.PHP_EOL;
			}

			// Process view form lines
			$viewFormLines = str_replace('{{FIELD_NAME_LABEL}}', "{{ trans('$this->modelEntities.$fieldName') }}", $viewFormLines);
			$viewFormLines = str_replace('{{FIELD_NAME}}', $fieldName, $viewFormLines);
			$isRequired = $nullable ? 'false' : 'true';
			$viewFormLines = str_replace('{{IS_REQUIRED}}', $isRequired, $viewFormLines);

			// Process view form responsiveness
			$viewFormLines = str_replace('COL_SM', 'col-sm-'.$col_sm, $viewFormLines);

			//Process Lang
			if(strlen($transFieldName) <= 0) {
				$transFieldName = $fieldName;
			}
			$langLines = $langLines ."\t". "'$fieldName' => '$transFieldName'," .PHP_EOL;
		}
		//End foreach Migration


		// Optimize and rewrite Stubs
		// Migration
		$migrationStub = str_replace('//MIGRATION_LINES', $migrationLines, $migrationStub);
		$migrationStub = str_replace('{{UPDATE_MIGRATION_CLASS}}', $this->migrationClass, $migrationStub);
		$migrationStub = str_replace('{{TABLE_NAME}}', $this->tableName, $migrationStub);
		$migrationStub = str_replace('//DROP_MIGRATION_LINES', $dropMigrationLines, $migrationStub);
		$migrationFilePath = storage_path($this->operationDirectory.'/'.$this->migrationPath);
		$this->makeDirectory($migrationFilePath);
		$this->files->put($migrationFilePath, $migrationStub);

		//Update backend controller
		foreach ($beControllerFile as $line) {
			//Update search on index()
			if(strpos($line, "->where('id'")) {
				$beControllerFile = str_replace($line, $line . PHP_EOL .$controllerSearchLines, $beControllerFile);
			}
			//Update post $controllerLines
			if(strpos($line, '--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $controllerLines. PHP_EOL.$line;
				$beControllerFile = str_replace($line, $strTmp, $beControllerFile);
			}
		} //End foreach
		//var_dump($beControllerFile);exit();
		$this->files->put($beControllerFileTmpPath, $beControllerFile);

		//Update API Controller
		foreach ($apiControllerFile as $line) {
			//Update search on index()
			if(strpos($line, "->where('id'")) {
				$apiControllerFile = str_replace($line, $line . PHP_EOL .$controllerSearchLines, $apiControllerFile);
			}
			//Update Validation UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS
			if(strpos($line, '--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $requestLines. PHP_EOL.$line;
				$apiControllerFile = str_replace($line, $strTmp, $apiControllerFile);
			}
			//Update search on index()
			//Update post $controllerLines
			if(strpos($line, '--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $controllerLines. PHP_EOL.$line;
				$apiControllerFile = str_replace($line, $strTmp, $apiControllerFile);
			}
			if(strpos($line, '--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $apiUpdatePostStatements. PHP_EOL.$line;
				$apiControllerFile = str_replace($line, $strTmp, $apiControllerFile);
			}
			//Update HASH_TRING FOR API
			// HASH_STRING_CREATE_
			if(strpos($line, '--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  "\t\t\t\t".$apiHashString. PHP_EOL.$line;
				$apiControllerFile = str_replace($line, $strTmp, $apiControllerFile);
			}

			// HASH_STRING_UPDATE_
			if(strpos($line, '--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  "\t\t\t\t".$apiHashString. PHP_EOL.$line;
				$apiControllerFile = str_replace($line, $strTmp, $apiControllerFile);
			}

		} //End foreach
		//var_dump($apiControllerFile);exit();
		$this->files->put($apiControllerFileTmpPath, $apiControllerFile);

		// Form View --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($formFile as $line) {
			if(strpos($line, '--UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $viewFormLines. PHP_EOL.$line;
				$formFile = str_replace($line, $strTmp, $formFile);
			}
		} //End foreach
		//var_dump($formFile);exit();
		$this->files->put($formFileTmpPath, $formFile);

		// Index View replace --UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--, --UPDATE_GENCODE_ROW_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($indexFile as $line) {
			if(strpos($line, '--UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $indexTableHeaderLines. PHP_EOL.$line;
				$indexFile = str_replace($line, $strTmp, $indexFile);
			}

			if(strpos($line, '--UPDATE_GENCODE_ROW_LINE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $indexTableRows. PHP_EOL.$line;
				$indexFile = str_replace($line, $strTmp, $indexFile);
			}
		} //End foreach
		//var_dump($indexFile);exit();
		$this->files->put($indexFileTmpPath, $indexFile);

		//Request replace --UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--, --UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($requestFile as $line) {
			if(strpos($line, '--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $requestLines. PHP_EOL.$line;
				$requestFile = str_replace($line, $strTmp, $requestFile);
			}

			if(strpos($line, '--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $messageRequestLines. PHP_EOL.$line;
				$requestFile = str_replace($line, $strTmp, $requestFile);
			}
		} //End foreach
		//var_dump($requestFile);exit();
		$this->files->put($requestFileTmpPath, $requestFile);

		//Lang JP replace --UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($langFileJp as $line) {
			if(strpos($line, '--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $langLines. PHP_EOL.$line;
				$langFileJp = str_replace($line, $strTmp, $langFileJp);
			}
		} //End foreach
		//var_dump($langFileJp);exit();
		$this->files->put($langFileTmpPathJp, $langFileJp);

		//Lang En replace --UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($langFileEn as $line) {
			if(strpos($line, '--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $langLines. PHP_EOL.$line;
				$langFileEn = str_replace($line, $strTmp, $langFileEn);
			}
		} //End foreach
		//var_dump($langFileEn);exit();
		$this->files->put($langFileTmpPathEn, $langFileEn);

		//Lang Vn replace --UPDATE_GVnCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
		foreach ($langFileVn as $line) {
			if(strpos($line, '--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--')) {
				$strTmp =  $langLines. PHP_EOL.$line;
				$langFileVn = str_replace($line, $strTmp, $langFileVn);
			}
		} //Vnd foreach
		//var_dump($langFileVn);exit();
		$this->files->put($langFileTmpPathVn, $langFileVn);

		//POSTMAN replace --UPDATE_GENCODE_FIELD_POSTMAN--
		foreach ($postManFile as $line) {
			if(strpos($line, '--'.$this->routeUrl.'_CREATE_GENCODE_FIELD_POSTMAN--')) {
				$strTmp =  $apiField. PHP_EOL.$line;
				$postManFile = str_replace($line, $strTmp, $postManFile);
			}
			if(strpos($line, '--'.$this->routeUrl.'_UPDATE_GENCODE_FIELD_POSTMAN--')) {
				$strTmp =  $apiField. PHP_EOL.$line;
				$postManFile = str_replace($line, $strTmp, $postManFile);
			}
			//Update hashkey
			$hashStringSearch = '--'.$this->routeUrl.'_CREATE_HASH_KEY--';
			if(strpos($line, $hashStringSearch)) {
				$strTmp =  str_replace($hashStringSearch,$apiHashStringPostMan.$hashStringSearch, $line);
				$postManFile = str_replace($line, $strTmp, $postManFile);
			}
			$hashStringSearch = '--'.$this->routeUrl.'_UPDATE_HASH_KEY--';
			if(strpos($line, $hashStringSearch)) {
				$strTmp =  str_replace($hashStringSearch,$apiHashStringPostMan.$hashStringSearch, $line);
				$postManFile = str_replace($line, $strTmp, $postManFile);
			}
		} //End foreach
//		var_dump('--'.$this->routeUrl.'_UPDATE_GENCODE_FIELD_POSTMAN--');exit();
		$this->files->put($postManFileTmpPath, $postManFile);
	}

	private function apiUpdatePostStatements($apiCode, $isFile, $tabController, $fieldName)
	{
		//Must setting  'upload_max_filesize' or 'post_max_size' php.ini
		$str = '';
		if ($apiCode && $isFile) {
			$fileUploadStatement = 'if ($request->hasFile(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'$'.$this->modelEntity.'->'.$fieldName.' = MediaHelper::uploadFileToServer($request->file(\''.$fieldName.'\'), \'/'.$this->modelEntities.'/\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'} else {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'if ($request->get(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t\t".'$'.$this->modelEntity.'->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'}'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'}'.PHP_EOL;
			$str = $tabController.$fileUploadStatement;
		} else {
			$fileUploadStatement = 'if ($request->get(\''.$fieldName.'\')) {'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t\t".'$'.$this->modelEntity.'->'.$fieldName.' = $request->get(\''.$fieldName.'\');'.PHP_EOL;
			$fileUploadStatement .= "\t\t\t".'}'.PHP_EOL;
			$str = $tabController.$fileUploadStatement;
		}

		return $str;
	}
	/**
	 * @param string $stub
	 * @return string
	 * */
	public function processStub ($stub)
	{
		$stub = str_replace('{{MODEL_ENTITY}}',$this->modelEntity, $stub);

		return $stub;
	}
}
