<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

class MediaHelper
{
	/**
	 * @param $link
	 * @param string $specificPath
	 *
	 * @return string
	 */
	public static function uploadLink($link, $specificPath = '')
	{
		$specificPath = '/uploads/'.$specificPath;
		/* @var \Intervention\Image\Image $image */
		$image = Image::make($link)->encode();

		$type         = $image->mime();
		$guesser      = ExtensionGuesser::getInstance();
		$extension    = ($guesser->guess($type));
		$name         = Str::random(40) . '.' . $extension;
		$now          = Carbon::now();
		$specificPath .= '/' . $now->year . '/' . $now->month . '/' . $now->day;

		if (Storage::disk('public')->put($specificPath . '/' . $name, $image)) {

			return $specificPath . '/' . $name;
		}

		return '';
	}

    /**
     * @param UploadedFile $file
     * @param string $specificPath
     *
     * @return string
     */
    public static function uploadFileToServer(UploadedFile $file, $specificPath = '/')
    {
		$specificPath = '/uploads'.$specificPath;
    	$specificPath .= $file->getClientOriginalName();
        if (Storage::disk('public')->put($specificPath, file_get_contents($file))) {

            return $specificPath;
        }

        return '';
    }

    /**
     * @param UploadedFile $file
     * @param string $specificPath
     *
     * @return string
     */
    public static function deleteFileOnServer($specificPath = '')
    {
        //$specificPath = str_replace('/upload', '', $specificPath);
        if (Storage::disk('public')->delete($specificPath))
        {
            return true;
        }

        return false;
    }
}