<?php

/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: function.php
 */

if (!function_exists('isActive')) {
	/**
	 * Set the active class to the current opened menu.
	 *
	 * @param string|array $route
	 * @param string       $className
	 *
	 * @return string
	 * @Author DuyLBP
	 * @Date   2018-06-27
	 */
	function isActive($route, $className = 'active')
	{
		if (is_array($route)) {
			return in_array(Route::currentRouteName(), $route) ? $className : '';
		}
		if (Route::currentRouteName() == $route) {
			return $className;
		}
		if (strpos(URL::current(), $route)) return $className;
	}
}

function removeStyle($str)
{
	$str = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $str);
	$str = preg_replace('/style=\\"[^\\"]*\\"/', '', $str);
	return preg_replace('/(<(script|style)\b[^>]*>).*?(<\/\2>)/is', "$1$3", $str);
}

function removeHTMLTag($text, $flagReplaceEnterCharacter = true, $allowed_tags = '<a><ul><li><b><i><sup><sub><em><strong><u><br><br/><br /><p><h2><h3><h4><h5><h6>')
{
	mb_regex_encoding('UTF-8');
	//replace MS special characters first
	$search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
	$replace = array('\'', '\'', '"', '"', '-');
	$text = preg_replace($search, $replace, $text);
	//make sure _all_ html entities are converted to the plain ascii equivalents - it appears
	//in some MS headers, some html entities are encoded and some aren't
	//$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
	//try to strip out any C style comments first, since these, embedded in html comments, seem to
	//prevent strip_tags from removing html comments (MS Word introduced combination)
	if (mb_stripos($text, '/*') !== false) {
		$text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
	}
	//introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
	//'<1' becomes '< 1'(note: somewhat application specific)
	$text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
	$text = strip_tags($text, $allowed_tags);
	//eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
	$text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
	//strip out inline css and simplify style tags
	$search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
	$replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
	$text = preg_replace($search, $replace, $text);
	//on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
	//that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
	//some MS Style Definitions - this last bit gets rid of any leftover comments */
	$num_matches = preg_match_all("/\<!--/u", $text, $matches);
	if ($num_matches) {
		$text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
	}
	$text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
	
	if ($flagReplaceEnterCharacter === true) {
		$text = str_replace('\n', '', $text);
		$text = str_replace('\r', '', $text);
	}
	
	
	return trim($text);
	//	$string = strip_tags($string,"</br><b><span><div><p><strong><br>");
	//	return removeStyle($string);
}

function removeArrayLetter($text)
{
	$text = str_replace('"', '', $text);
	$text = str_replace('[', '', $text);
	$text = str_replace(']', '', $text);
	return trim($text);
}

if (!function_exists('isActiveRoute')) {
	/**
	 * @param        $route
	 * @param string $output
	 *
	 * @return string
	 * @Description Active menu via route
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function isActiveRoute($route, $output = 'active')
	{
		
		$pos = strrpos($route, '|' . Route::currentRouteName() . '|');
		
		if (!($pos === false)) {
			// not found...
			return $output;
		}
	}
}

if (!function_exists('isActiveUrl')) {
	/**
	 * @param        $urlStr
	 * @param string $output
	 *
	 * @return string
	 * @Description active menu via url
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function isActiveUrl($urlStr, $output = 'active')
	{
		$url = Request::url();
		$url = urldecode($url);
		if (str_contains($url, $urlStr)) {
			return $output;
		}
	}
}

if (!function_exists('activeAdministrationMenu')) {
	/**
	 * @param        $url_str
	 * @param string $output
	 *
	 * @return string
	 * @Description active menu via url
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function activeAdministrationMenu()
	{
		$checkActive = array_search(Route::currentRouteName(), array_column(config('backend-menu'), 'route'));
		if ($checkActive > -1) {
			return 'active';
		}
	}
}

if (!function_exists('isShowRoute')) {
	/**
	 * @param        $route
	 * @param string $output
	 *
	 * @return string
	 * @Description
	 * @Author DuyLBP
	 * @Date   2018-06-27
	 */
	function isShowRoute($route, $output = 'show')
	{
		$pos = strpos('|' . Route::currentRouteName() . '|', $route);
		
		if (!($pos === false)) {
			// not found...
			return $output;
		}
	}
}

if (!function_exists('zenkakuToShiftJIS')) {
	/**
	 * @param $zenkaku
	 *
	 * @return string
	 * @Description convert utf-8 to shift-jis
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function zenkakuToShiftJIS($zenkaku)
	{
		$shiftJis = iconv('utf-8', 'shift-jis' . '//TRANSLIT', $zenkaku);
		$shiftJis = urlencode($shiftJis);
		
		return $shiftJis;
	}
}


if (!function_exists('removeWhiteSpace')) {
	/**
	 * @param $string
	 *
	 * @return null|string|string[]
	 * @Description remove white in string
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function removeWhiteSpace($string)
	{
		return preg_replace('/\s*/m', '', $string);
	}
}

/**
 * @return \Illuminate\Config\Repository|mixed
 * @Description
 * @Author DuyLBP
 * @Date   2018-06-27
 */
function getFePrefix()
{
	$prefix = config('site.frontend_prefix', 'fe');
	return $prefix;
}

/**
 * @param string $path
 *
 * @return string
 * @Description
 * @Author DuyLBP
 * @Date   2018-06-27
 */
function getFeUrl($path = '')
{
	$prefix = config('site.frontend_prefix', 'fe');
	return '/' . $prefix . '/' . $path;
}

/**
 * @return \Illuminate\Config\Repository|mixed
 * @Description
 * @Author DuyLBP
 * @Date   2018-06-27
 */
function getBePrefix()
{
	$prefix = config('site.backend_prefix', 'be');
	return $prefix;
}

/**
 * @param string $path
 *
 * @return string
 * @Description
 * @Author DuyLBP
 * @Date   2018-06-27
 */
function getBeUrl($path = '')
{
	$prefix = config('site.backend_prefix', 'be');
	return '/' . $prefix . '/' . $path;
}

function seo_friendly_url($string)
{
	$string = str_replace(array('[\', \']'), '', $string);
	$string = preg_replace('/\[.*\]/U', '', $string);
	$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
	$string = htmlentities($string, ENT_COMPAT, 'utf-8');
	$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
	$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), '-', $string);
	return strtolower(trim($string, '-'));
}

function recursive_array_replace($find, $replace, $array)
{
	if (!is_array($array)) {
		return str_replace($find, $replace, $array);
	}
	
	$newArray = [];
	foreach ($array as $key => $value) {
		$newArray[$key] = recursive_array_replace($find, $replace, $value);
	}
	return $newArray;
}


function showBoolean($status = 0)
{
	if ($status == 1) {
		return trans('label.true');
	}
	else {
		return trans('label.false');
	}
}

function feAuth()
{
	return Auth::guard('frontend');
}

function clientAuth()
{
	return Auth::guard('client');
}

function formatDate($date, $format = 'Y-m-d H:i:s')
{
	$date = date_create($date);
	return date_format($date, $format);
}

if (!function_exists('otAsset')) {
	/**
	 * Generate an asset path for the application.
	 *
	 * @param string $path
	 * @param bool   $secure
	 *
	 * @return string
	 */
	
	function otAsset($path, $secure = null)
	{
		$lang = Request::route('lang');
		$alias = $lang == 'jp' ? env('ALIAS_JP') : env('ALIAS_VN');
		$path = $alias . '/' . $path;
		return asset($path, $secure);
	}
}

if (!function_exists('otGetUrl')) {
	/**
	 * Generate an asset path for the application.
	 *
	 * @param string $path
	 * @param bool   $secure
	 *
	 * @return string
	 */
	function otGetUrl($subUrl)
	{
		return $subUrl;
	}
}


if (!function_exists('compressPNG')) {
	/**
	 * Optimizes PNG file with pngquant 2 or later (reduces file size of 24-bit/32-bit PNG images).
	 *
	 * You need to install pngquant 1.8 on the server (ancient version 1.0 won't work).
	 * There's package for Debian/Ubuntu and RPM for other distributions on http://pngquant.org
	 *
	 * @param $path_to_png_file string - path to any PNG file, e.g. $_FILE['file']['tmp_name']
	 * @param $max_quality      int - conversion quality, useful values from 60 to 100 (smaller number = smaller file)
	 *
	 * @return string - content of PNG file after conversion
	 */
	function compressPNG($pathFile, $maxQuality = 90, $minQuality = 80)
	{
		if (!file_exists($pathFile)) {
			throw new Exception("File does not exist: $pathFile");
		}
		
		// guarantee that quality won't be worse than that.
		// '-' makes it use stdout, required to save to $compressed_png_content variable
		// '<' makes it read from the given file path
		// escapeshellarg() makes this safe to use with any path
		$command = env('PNGQUANT') . " --quality=$minQuality-$maxQuality - < " . escapeshellarg($pathFile);
		$compressedPngContent = shell_exec($command);
		if (!$compressedPngContent) {
			throw new Exception("Conversion to compressed PNG failed. Is pngquant 1.8+ installed on the server?");
		}
		
		return $compressedPngContent;
	}
}

if (!function_exists('compressJPG')) {
	function compressJPG($pathFile, $maxQuality = 90)
	{
		if (!file_exists($pathFile)) {
			throw new Exception("File does not exist: $pathFile");
		}
		
		// guarantee that quality won't be worse than that.
		// '-' makes it use stdout, required to save to $compressed_png_content variable
		// '<' makes it read from the given file path
		// escapeshellarg() makes this safe to use with any path
		$command = env('JPEGOPTIM') . " --max=$maxQuality --strip-all --all-progressive - < " . escapeshellarg($pathFile);
		
		$compressedPngContent = shell_exec($command);
		if (!$compressedPngContent) {
			throw new Exception("Conversion to compressed JPG failed. Is jpegoptim installed on the server?");
		}
		
		return $compressedPngContent;
	}
}

if (!function_exists('compressImageSEO')) {
	function compressContentSEO($content)
	{
		$arrContent = explode("\n", $content);
		foreach ($arrContent as $line) {
			if (str_contains($line, '<img')) {
				preg_match('/(src=["\'](.*?)["\'])/', $line, $match);  //find src="X" or src='X'
				$split = preg_split('/["\']/', isset($match[0]) ? $match[0] : null); // split by quotes
				$img = isset($split[1]) ? $split[1] : '';
				if (strlen($img) > 0) {
					$process = processImage($img);
				}
			}
		}
		return true;
	}
}
if (!function_exists('processImage')) {
	function processImage($img, $maxSizeWidth = 1080, $newPath = false)
	{
		$img = parse_url($img)['path'];
		$imgPath = public_path($img);
		//$imgPath = public_path('/uploads/IMG_0815.JPG');
		if (file_exists($imgPath)) {
			$imgFile = \Image::make($imgPath);
			$command = '';
			try {
				if (str_contains($imgFile->mime(), 'image/jpeg') || str_contains($imgFile->mime(), 'image/jpg')) {
					$command = env('JPEGOPTIM') . " --max=100 --strip-all --all-progressive - < ";
				}
				
				if (str_contains($imgFile->mime(), 'image/png')) {
					$command = env('PNGQUANT') . " --quality=100 - < ";
				}
				
				//				//Backup Origin Image
				//				$bkPath = $imgFile->dirname.'/'.$imgFile->filename.'origin'.'.'.$imgFile->extension;
				//				if(!file_exists($bkPath)) {
				//					$imgFileBK = \Image::make($imgPath);
				//					$imgFileBK->save($bkPath);
				//				}
				
				//Resize image to 1080px
				if ($imgFile->width() > $maxSizeWidth && $maxSizeWidth > 0) {
					$imgFile->resize(
						$maxSizeWidth, null, function ($constraint) {
						$constraint->aspectRatio();
						$constraint->upsize();
					}
					);
				}
				//Create new file
				if ($newPath) {
					if (!str_contains($imgFile->filename, 'new01')) {
						$imgPath = $imgFile->dirname . '/' . $imgFile->filename . 'new01' . '.' . $imgFile->extension;
					}
				}
				$imgFile->save($imgPath);
				
				$compressedContent = shell_exec($command . escapeshellarg($imgPath));
				if ($compressedContent) {
					file_put_contents($imgPath, $compressedContent);
					$imgPath = str_replace(public_path(''), '', $imgPath);
					return $imgPath;
				}
			}
			catch (Exception $e) {
				\Helper::writeLogException($e);
				return '';
			}
		}
		return '';
	}
}

if (!function_exists('genKey')) {
	/**
	 * @param int $length
	 *
	 * @return string
	 * @Description generate key from ramdom character
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function genKey($length = 6)
	{
		$keyGen = "";
		$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHGKLMNPQSTUVWZYZ";
		for ($p = 0; $p < $length; $p++) {
			$keyGen .= $characters[mt_rand(0, strlen($characters) - 1)];
		}
		return $keyGen;
	}
}
if (!function_exists('checkPageSpeed')) {
	function checkPageSpeed($url, $platform = 'desktop')
	{
		set_time_limit(0);
		$apiKey = env('PAGESPEED_API_KEY');
		$googleUrl = "https://www.googleapis.com/pagespeedonline/v4/runPagespeed?strategy=$platform&screenshot=false&url=$url&key=$apiKey";
		
		if (function_exists('file_get_contents')) {
			$result = @file_get_contents($googleUrl);
		}
		if ($result == '') {
			$ch = curl_init();
			$timeout = 60;
			curl_setopt($ch, CURLOPT_URL, $googleUrl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$result = curl_exec($ch);
			curl_close($ch);
		}
		
		return $result;
	}
}

if (!function_exists('locale')) {
	/**
	 * @param int $length
	 *
	 * @return string
	 * @Description generate key from ramdom character
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function locale()
	{
		return \App::getLocale();
	}
}
if (!function_exists('langCv')) {
	/**
	 * @param int $length
	 *
	 * @return string
	 * @Description generate key from ramdom character
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function langCv()
	{
		return 'en';
	}
}
if (!function_exists('getLang')) {
	/**
	 * @param int $length
	 *
	 * @return string
	 * @Description generate key from ramdom character
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function getLang()
	{
		$lang = 'vi_VN';
		if (locale() === 'jp') {
			$lang = 'ja_JP';
		}
		return $lang;
	}
}
if (!function_exists('settingLang')) {
	/**
	 * @param int $length
	 *
	 * @return string
	 * @Description generate key from ramdom character
	 * @Author      DuyLBP
	 * @Date        2018-06-27
	 */
	function settingLang(\Illuminate\Http\Request $request, $user = null)
	{
		//Setting lang
		$lang = $request->get('lang');
		$cacheKey = 'cacheSessionKey--' . $request->userAgent() . '--' . $request->ip();
		
		if ($lang === 'vn') {
			\App::setLocale('vn');
			\App\Helpers\Helper::setCache($cacheKey, locale());
		}
		
		if ($lang === 'jp') {
			\App::setLocale('jp');
			\App\Helpers\Helper::setCache($cacheKey, locale());
		}
		else {
			$cacheLang = \App\Helpers\Helper::getCache($cacheKey);
			if (!is_null($cacheLang)) {
				\App::setLocale($cacheLang);
			}
			else {
				if (!is_null($user)) {
					if (isset($user->lang) && strlen($user->lang) > 0 && in_array($user->lang, ['en', 'vn', 'jp'])) {
						\App::setLocale($user->lang);
					}
				}
			}
		}
	}
}

if (!function_exists('otSetClientLocale')) {
	function otSetClientLocale(\Illuminate\Http\Request $request)
	{
		if (str_contains($request->url(), '/en')) {
			\App::setLocale('en');
		}
		else {
			\App::setLocale('vn');
		}
	}
}

if (!function_exists('otSetLocale')) {
	function otSetLocale(\Illuminate\Http\Request $request)
	{
		if (str_contains($request->url(), '/en')) {
			\App::setLocale('en');
		}
		else {
			\App::setLocale('vn');
		}
	}
}

if (!function_exists('isMobile')) {
	function isMobile()
	{
		$agent = new \Jenssegers\Agent\Agent();
		return $agent->isMobile() ? 1 : 0;
	}
}

if (!function_exists('checkClientLogin')) {
	function checkClientLogin()
	{
		$user = clientAuth()->user();
		if (isset($user->activated) && $user->activated == '1') {
			return true;
		}
		return false;
	}
}

if (!function_exists('replaceNToBr')) {
	function replaceNToBr($str)
	{
		return str_replace("\n", '<br>', $str);
	}
}

if (!function_exists('cutName')) {
	function cutName($fullname)
	{
		$nameArray = explode(' ', $fullname);
		$indexName = count($nameArray);
		return $nameArray[$indexName - 1];
	}
}

if (!function_exists('strWrap')) {
	function strWrap($str)
	{
		$wrapped = wordwrap($str, 25);
		$lines = explode("\n", $wrapped);
		$newStr = $lines[0];
		if (strlen($newStr) > 25) {
			$newStr = $newStr . "<a href='#' title='$str'>...</a>";
		}
		return $newStr;
	}
}

if (!function_exists('refUrlKey')) {
	function refUrlKey()
	{
		return 'GrowUpJVRefKeyToTracking';
	}
}


if (!function_exists('getAge')) {
	function getAge($date)
	{
		return intval(date('Y', time() - strtotime($date))) - 1970;
	}
}

if (!function_exists('convertViToEn')) {
	function convertViToEn($str)
	{
		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
		$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
		$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
		$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
		$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
		$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
		$str = preg_replace("/(đ)/", "d", $str);
		$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
		$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
		$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
		$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
		$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
		$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
		$str = preg_replace("/(Đ)/", "D", $str);
		//$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
		return $str;
	}
}

// Function to get all the dates in given range
function getDatesFromRange($start, $end, $format = 'Y-m-d')
{
	
	// Declare an empty array
	$array = array();
	
	// Variable that store the date interval
	// of period 1 day
	$interval = new DateInterval('P1D');
	
	$realEnd = new DateTime($end);
	$realEnd->add($interval);
	
	$period = new DatePeriod(new DateTime($start), $interval, $realEnd);
	
	// Use loop to store date into array
	foreach ($period as $date) {
		$array[] = $date->format($format);
	}
	
	// Return the array elements
	return $array;
}

/**
 * slug text uppercase every string
 *
 * @param        $title
 * @param string $separator
 *
 * @return string
 * @author tanmnt
 */
function strSlugUpperCase($title, $separator = '')
{
	$title = ucwords($title);
	$string = str_replace(array('[\', \']'), '', $title);
	$string = preg_replace('/\[.*\]/U', '', $string);
	$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', $separator, $title);
	$string = htmlentities($string, ENT_COMPAT, 'utf-8');
	$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
	$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), $separator, $string);
	return trim($string, '-');
}


if (!function_exists('sluggable')) {
	function sluggable($str)
	{
		
		$before = array(
			'ÁÀẢÃẠÄĂẮẰẲẴẶÂẤẦẬẨẪÃÅČÇĆĎÉĚËÈẺẼẸÊẾỀỂỄỆĔȆĞỊÍÌỈĨÎÏİŇÑÓÖÒỎÕỌÔỐỒỔỖỘƠỚỜỢỞỠØŘŔŠŞŤỤỦŨÚŮÜÙÛƯỰỨỪỬỮỴỶỸÝŸŽáàảãạäăắằặẳẵâấầậẩẫåčçćďẻẽẹếềệểễéěëèêĕȇğịíìỉĩîïıňñóöòỏõọôốồổỗộơớờợởỡøðřŕšşťụủũúůüùûưựứừửữỵỷỹýÿžþÞĐđßÆa·/_,:;',
			'/[^a-z0-9\s]/',
			array('/\s/', '/--+/', '/---+/')
		);
		
		$after = array(
			'AAAAAAAAAAAAAAAAAAAACCCDEEEEEEEEEEEEEEEGIIIIIIIINNOOOOOOOOOOOOOOOOOOORRSSTUUUUUUUUUUUUUUYYYYYZaaaaaaaaaaaaaaaaaaacccdeeeeeeeeeeeeeeegiiiiiiiinnoooooooooooooooooooorrsstuuuuuuuuuuuuuuyyyyyzbBDdBAa------',
			'',
			'-'
		);
		
		$str = strtolower($str);
		$str = strtr($str, $before[0], $after[0]);
		$str = preg_replace($before[1], $after[1], $str);
		$str = trim($str);
		$str = preg_replace($before[2], $after[2], $str);
		
		return $str;
	}
}

if (!function_exists('br2nl')) {
	function br2nl($str)
	{
		$str = preg_replace('/^\s*<br\s*\/?>\s*/', "", $str);// start with space + <br\s*/> + space
		$str = preg_replace('/\s*<br\s*\/?>\s*$/', "", $str);// end with space + <br\s*/> + space
		$str = preg_replace('/\s*<br\s*\/?>\s*/', "\n", $str);// replace <br\s*/>
		return $str;
	}
}


function matchRedirect($urlRedirects, $url, $recursive = 0)
{
	foreach ($urlRedirects as $urlRedirect) {
		try {
			$regexOption = $urlRedirect->regex_option;
			if ($recursive == 0 && request()->url() == $regexOption) {
				redirectNow($urlRedirect->url_to, 301);
			}
			if ($recursive == 1 && str_contains(request()->url(), $regexOption)) {
				redirectNow($urlRedirect->url_to, 301);
			}
			if ($recursive == 2 && strlen($url) > 0 && checkPregMatch($regexOption, $url)) {
				redirectNow($urlRedirect->url_to, 301);
			}
		}
		catch (Exception $e) {
			\App\Helpers\Helper::writeLogException($e);
			continue;
		}
	}
	$recursive = $recursive + 1;
	if ($recursive <= 2) {
		matchRedirect($urlRedirects, $url, $recursive);
	}
}

function checkPregMatch($regexOption, $str)
{
	if (substr($regexOption, 0, 1) != '/') {
		$regexOption = "/" . preg_quote($regexOption, "/") . "/";
	}
	
	if (preg_match($regexOption, $str)) {
		return true;
	}
	return false;
}

/**
 * Redirect the user no matter what. No need to use a return
 * statement. Also avoids the trap put in place by the Blade Compiler.
 *
 * @param string $url
 * @param int    $code http code for the redirect (should be 302 or 301)
 */
function redirectNow($url, $code = 302)
{
	try {
		\App::abort($code, '', ['Location' => $url]);
	}
	catch (\Exception $exception) {
		// the blade compiler catches exceptions and rethrows them
		// as ErrorExceptions :(
		//
		// also the __toString() magic method cannot throw exceptions
		// in that case also we need to manually call the exception
		// handler
		$previousErrorHandler = set_exception_handler(
			function () {
			}
		);
		restore_error_handler();
		call_user_func($previousErrorHandler, $exception);
		die;
	}
}

/**
 * chunks, partition array
 *
 * @author hungnv@onetech.vn
 */
if (!function_exists('partition')) {
	function partition($list, $p)
	{
		$listlen = count($list);
		$partlen = floor($listlen / $p);
		$partrem = $listlen % $p;
		$partition = array();
		$mark = 0;
		for ($px = 0; $px < $p; $px++) {
			$incr = ($px < $partrem) ? $partlen + 1 : $partlen;
			$partition[$px] = array_slice($list, $mark, $incr);
			$mark += $incr;
		}
		return $partition;
	}
}
/**
 * chunks, partition array
 */
if (!function_exists('setDataPage')) {
	function setDataPage($slug, $lang = 'vn')
	{
		$locale = locale();
		$cacheKey = 'page.' . $slug . $locale;
		if (\App\Helpers\Helper::checkCache($cacheKey)) {
			//Get data from cache
			$page = \App\Helpers\Helper::getCache($cacheKey);
		}
		else {
			//Set cache
			$page = \App\Models\Page::whereRaw("status = 1 AND lang = '$locale'")
			                        ->where('slug', $slug)->first();
			\App\Helpers\Helper::setCache($cacheKey, $page);
		}
		if (is_null($page)) {
			return null;
		}
		
		$data['slug'] = $page->slug;
		$data['title'] = $page->meta_title ? $page->meta_title : $page->title;
		$data['description'] = $page->meta_description ? $page->meta_description : $page->description;
		$page->content = htmlModule($page->content);
		$data['content'] = $page->content;
		$data['schema'] = isset($page->schema) ? $page->schema : '';
		return $data;
	}
}


function apiKey($index = 0)
{
	$apiKey = 'AIzaSyAXHWZW3wpDlJBRKC_S3Kxv0k-Zu5ppXPU';
	return $apiKey;
}

function apiDetectLanguage($str)
{
	try {
		$ld = new \LanguageDetection\Language();
		$detect = $ld->detect($str)->whitelist('vi', 'en', 'ja')->close();
		$lang = array_key_first($detect);
		if ($lang == 'vi') {
			$lang = 'vn';
		}
		elseif ($lang == 'ja') {
			$lang = 'jp';
		}
		return $lang;
	}
	catch (Exception $e) {
		\Helper::writeLogException($e);
		return null;
	}
}

function googleTranslate($str, $langTo = 'en', $langFrom = 'ja')
{
	try {
		logInfo('Count letter -----');
		logInfo(strlen($str));
		$translate = new \Google\Cloud\Translate\TranslateClient(
			[
				'key' => apiKey()
			]
		);
		$result = $translate->translate(
			$str, [
				    'target' => $langTo
			    ]
		);
		$str = isset($result['text']) ? $result['text'] : $str;
		return $str;
	}
	catch (Exception $e) {
		Log::info('Translate error ');
		return $str;
	}
}

function crawlRemoveHtml($str)
{
	$str = removeStyle($str);
	return $str;
}

function logInfo($log)
{
	\Log::info($log);
}

function modelCacheGet($model, $key)
{
	try {
		$cacheKey = 'ModelCacheGet' . \App\Helpers\Helper::dateNow('Y-m-d') . $key;
		$data = \App\Helpers\Helper::getCache($cacheKey);
		if (!$data) {
			$data = $model->get();
			\App\Helpers\Helper::setCache($cacheKey, $data);
		}
		return $data;
	}
	catch (\Exception $e) {
		\Helper::writeLogException($e);
		return null;
	}
}

function modelCacheFirst($model, $key = 'ModelName', $id = 0)
{
	try {
		$cacheKey = 'ModelCacheFirst' . $key . $id;
		$data = null;
		if (\App\Helpers\Helper::checkCache($cacheKey)) {
			//Get data from cache
			$data = \App\Helpers\Helper::getCache($cacheKey);
		}
		else {
			//Set cache
			$data = $model->first();
			\App\Helpers\Helper::setCache($cacheKey, $data);
		}
		return $data;
	}
	catch (Exception $e) {
		\Helper::writeLogException($e);
		return $model->first();
	}
}

function genLangUrl($url, $lang)
{
	try {
		$urlArr = parse_url($url);
		$url = isset($urlArr['path']) ? $urlArr['path'] : '/';
		if ($lang == 'jp') {
			$url = str_replace('/en', '', $url);
			$url = str_replace('/vn', '', $url);
			if (!$url) {
				$url = '/';
			}
		}
		elseif ($lang == 'en') {
			if (!str_contains($url, '/en')) {
				if (str_contains($url, 'vn')) {
					$url = str_replace('/vn', '/en', $url);
				}
				else {
					$url = '/en' . $url;
				}
			}
		}
		return $url;
	}
	catch (Exception $e) {
		\Helper::writeLogException($e);
		return $url;
	}
}

function slugString($string, $options = array())
{
	//Bản đồ chuyển ngữ
	$slugTransliterationMap = array(
		'á' => 'a',
		'à' => 'a',
		'ả' => 'a',
		'ã' => 'a',
		'ạ' => 'a',
		'â' => 'a',
		'ă' => 'a',
		'Á' => 'A',
		'À' => 'A',
		'Ả' => 'A',
		'Ã' => 'A',
		'Ạ' => 'A',
		'Â' => 'A',
		'Ă' => 'A',
		'ấ' => 'a',
		'ầ' => 'a',
		'ẩ' => 'a',
		'ẫ' => 'a',
		'ậ' => 'a',
		'Ấ' => 'A',
		'Ầ' => 'A',
		'Ẩ' => 'A',
		'Ẫ' => 'A',
		'Ậ' => 'A',
		'ắ' => 'a',
		'ằ' => 'a',
		'ẳ' => 'a',
		'ẵ' => 'a',
		'ặ' => 'a',
		'Ắ' => 'A',
		'Ằ' => 'A',
		'Ẳ' => 'A',
		'Ẵ' => 'A',
		'Ặ' => 'A',
		'đ' => 'd',
		'Đ' => 'D',
		'é' => 'e',
		'è' => 'e',
		'ẻ' => 'e',
		'ẽ' => 'e',
		'ẹ' => 'e',
		'ê' => 'e',
		'É' => 'E',
		'È' => 'E',
		'Ẻ' => 'E',
		'Ẽ' => 'E',
		'Ẹ' => 'E',
		'Ê' => 'E',
		'ế' => 'e',
		'ề' => 'e',
		'ể' => 'e',
		'ễ' => 'e',
		'ệ' => 'e',
		'Ế' => 'E',
		'Ề' => 'E',
		'Ể' => 'E',
		'Ễ' => 'E',
		'Ệ' => 'E',
		'í' => 'i',
		'ì' => 'i',
		'ỉ' => 'i',
		'ĩ' => 'i',
		'ị' => 'i',
		'Í' => 'I',
		'Ì' => 'I',
		'Ỉ' => 'I',
		'Ĩ' => 'I',
		'Ị' => 'I',
		'ó' => 'o',
		'ò' => 'o',
		'ỏ' => 'o',
		'õ' => 'o',
		'ọ' => 'o',
		'ô' => 'o',
		'ơ' => 'o',
		'Ó' => 'O',
		'Ò' => 'O',
		'Ỏ' => 'O',
		'Õ' => 'O',
		'Ọ' => 'O',
		'Ô' => 'O',
		'Ơ' => 'O',
		'ố' => 'o',
		'ồ' => 'o',
		'ổ' => 'o',
		'ỗ' => 'o',
		'ộ' => 'o',
		'Ố' => 'O',
		'Ồ' => 'O',
		'Ổ' => 'O',
		'Ỗ' => 'O',
		'Ộ' => 'O',
		'ớ' => 'o',
		'ờ' => 'o',
		'ở' => 'o',
		'ỡ' => 'o',
		'ợ' => 'o',
		'Ớ' => 'O',
		'Ờ' => 'O',
		'Ở' => 'O',
		'Ỡ' => 'O',
		'Ợ' => 'O',
		'ú' => 'u',
		'ù' => 'u',
		'ủ' => 'u',
		'ũ' => 'u',
		'ụ' => 'u',
		'ư' => 'u',
		'Ú' => 'U',
		'Ù' => 'U',
		'Ủ' => 'U',
		'Ũ' => 'U',
		'Ụ' => 'U',
		'Ư' => 'U',
		'ứ' => 'u',
		'ừ' => 'u',
		'ử' => 'u',
		'ữ' => 'u',
		'ự' => 'u',
		'Ứ' => 'U',
		'Ừ' => 'U',
		'Ử' => 'U',
		'Ữ' => 'U',
		'Ự' => 'U',
		'ý' => 'y',
		'ỳ' => 'y',
		'ỷ' => 'y',
		'ỹ' => 'y',
		'ỵ' => 'y',
		'Ý' => 'Y',
		'Ỳ' => 'Y',
		'Ỷ' => 'Y',
		'Ỹ' => 'Y',
		'Ỵ' => 'Y'
	);
	
	//Ghép cài đặt do người dùng yêu cầu với cài đặt mặc định của hàm
	$options = array_merge(
		array(
			'delimiter'     => '-',
			'transliterate' => true,
			'replacements'  => array(),
			'lowercase'     => true,
			'encoding'      => 'UTF-8'
		), $options
	);
	
	//Chuyển ngữ các ký tự theo bản đồ chuyển ngữ
	if ($options['transliterate']) {
		$string = str_replace(array_keys($slugTransliterationMap), $slugTransliterationMap, $string);
	}
	
	//Nếu có bản đồ chuyển ngữ do người dùng cung cấp thì thực hiện chuyển ngữ
	if (is_array($options['replacements']) && !empty($options['replacements'])) {
		$string = str_replace(array_keys($options['replacements']), $options['replacements'], $string);
	}
	
	//Thay thế các ký tự không phải ký tự latin
	$string = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $string);
	
	//Chỉ giữ lại một ký tự phân cách giữa 2 từ
	$string = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', trim($string, $options['delimiter']));
	
	//Chuyển sang chữ thường nếu có yêu cầu
	if ($options['lowercase']) {
		$string = mb_strtolower($string, $options['encoding']);
	}
	
	//Trả kết quả
	return $string;
}

function mergeUrl($domain, $url = '')
{
	$url = str_replace('./', '', $url);
	$url = str_replace('../', '', $url);
	$url = str_replace('.../', '', $url);
	
	if (!str_contains($url, '://')) {
		if (mb_substr($url, 0, 1) == '/') {
			return $domain . $url;
		}
		else {
			return $domain . '/' . $url;
		}
	}
	return $url;
}

/**
 * @return mixed|null
 * @Description get list setting from database, then cache to server
 * @Author DuyLBP
 * @Date 2018-06-27
 */
function getAllSetting()
{
	$cacheKey = 'setting1'.env('CACHE_KEY');
	//Get cache from server
	$setting = Helper::getCache($cacheKey);
	//Check cache null if null get data from table settings
	if(is_null($setting))
	{
		$setting = App\Models\Setting::getSetting();
		//Set cache on server with expiration 30 minutes
		Helper::setCache($cacheKey, $setting, env('CACHE_MINUTE', 30));
		return $setting;
	}
	return $setting;
}

/**
 * @param      $key
 * @param null $default
 * @param null $setting
 * @return null
 * @Description get value config on table setting by $key
 * @Author DuyLBP
 * @Date 2018-06-27
 */
function getSetting($key, $default=null, $setting = null)
{
	//Check setting is null
	if(is_null($setting))
	{
		// get setting data
		$setting = getAllSetting();
	}
	return isset($setting[$key])?$setting[$key]:$default;
}


function addUrlToImgAndLink($content)
{
	$contentArr = explode("\n", $content);
	foreach ($contentArr as $line) {
		if (strpos($line, 'src="')) {
			$tmp1 = \App\Helpers\Helper::getBetweenContent($line, 'src="', '"');
			$tmp2 = mergeUrl('https://' . env('APP_URL'), $tmp1);
			$tmp3 = str_replace($tmp1, $tmp2, $line);
			$contentArr = str_replace($line, $tmp3, $contentArr);
		}
		if (strpos($line, "src='")) {
			$tmp1 = \App\Helpers\Helper::getBetweenContent($line, "src='", "'");
			$tmp2 = mergeUrl('https://' . env('APP_URL'), $tmp1);
			$tmp3 = str_replace($tmp1, $tmp2, $line);
			$contentArr = str_replace($line, $tmp3, $contentArr);
		}
	}
	return implode("\n", $contentArr);
}
