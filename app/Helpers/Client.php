<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: Client.php
 */
namespace App\Helpers;

use Request;

/**
 * @Class Client
 * @package App\Helpers
 * @Description
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class Client  {
	/**
	 * @var array|string
	 */
	public $deviceId        = "";
	/**
	 * @var array|string
	 */
	public $token           = "";
	/**
	 * @var array|string
	 */
	public $appVersion      = "";
	/**
	 * @var array|string
	 */
	public $userAgent       = "";
	/**
	 * @var string
	 */
	public $ip              = "";
	/**
	 * @var string
	 */
	public $prefix          = "";

	/**
	 * Client constructor.
	 * Init data from http HEADER
	 */
	function __construct()
    {
        $this->deviceId         = Request::header('device-id');
        $this->token            = Request::header('token');
        $this->appVersion       = Request::header('app_version');
        $this->userAgent        = Request::server('HTTP_USER_AGENT');
        $this->ip               = Request::ip();
        $this->prefix           = Request::route()->getPrefix();

    }
}
