<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: User.php
 */
namespace App\Models;

use App\Helpers\Client;
use App\Helpers\Helper;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @Class User
 * @package App\Models
 * @Description
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class User extends Permissible
{
    use Notifiable, SoftDeletes;

	/**
	 * @var string
	 */
	protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 *
	 */
	const RULE = [

    ];

	/**
	 *
	 */
	const NAME_VALIDATOR = [
        'email' => 'メール',
        'password' => 'パスワード',
    ];


    /**
     * Get full name attribute
     * */

    public function getNameAttribute(){
        return $this->first_name.' '.$this->last_name;
    }

	/**
	 * @param $value
	 * @Description
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function setEmailAttribute($value) {
        $this->attributes['email'] = self::normalizeEmail($value);
    }

	/**
	 * @param $value
	 * @return string
	 * @Description
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public static function normalizeEmail($value) {
        return trim(strtolower($value));
    }

	/**
	 * @param                     $user
	 * @param \App\Helpers\Client $client
	 * @return string
	 * @Description
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function updateToken($user, Client $client)
    {
        $token      = Helper::gentToken();

        $tokenUser = ApiToken::where('token', $token)->first();
        if(is_null($tokenUser))
        {
            $tokenUser = new ApiToken();
        }

        $tokenUser->user_id         = $user->id;
        $tokenUser->device_id       = $client->deviceId;
        $tokenUser->token           = $token;
        $tokenUser->status          = 1;
        $tokenUser->expire_date     = Helper::dateAddDay(env('EXPIRE_TOKEN_DAY', 30));
        $tokenUser->save();

        return $token;
    }

	//Write activity log
	public static function boot() {

		parent::boot();

		// create a event to happen on updating
		static::created(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$createData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Created',
					'user_agent' => \Request::header('User-Agent'),
					'original' => null,
					'attribute' => $createData,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];

				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		static::updated(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$originalData = json_encode($table->getOriginal());
				$changeData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Updated',
					'user_agent' => \Request::header('User-Agent'),
					'original' => $originalData,
					'attribute' => $changeData,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];
				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		// create a event to happen on deleting
		static::deleted(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$deleteData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Deleted',
					'user_agent' => \Request::header('User-Agent'),
					'original' => $deleteData,
					'attribute' => null,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];
				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		// create a event to happen on retrieved
		static::retrieved(function($table)  {
			//
		});
	}

    /**
     * clientMessage hasMany clientMessages
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * */
    public function clientMessages()
	{
		return $this->hasMany(\App\Models\ClientMessage::class);
	}

    public function clientMemberMessages()
    {
        return $this->hasMany(\App\Models\ClientMemberMessage::class);
    }
 }