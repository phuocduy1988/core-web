<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: LogError.php
 */
namespace App\Models;

/**
 * @Class LogError
 * @package App\Models
 * @Description write log error to database
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class LogError extends BaseModel
{
	/**
	 * @var define table name
	 */
	protected $table = 'log_errors';
}
