<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: LogActivity.php
 */
namespace App\Models;

/**
 * @Class LogActivity
 * @package App\Models
 * @Description transfer client data to database
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class LogActivity extends BaseModel
{
	/**
	 * @var define table name in Database
	 */
	protected $table = 'log_activities';

	/**
	 * @Description write actvity log
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function writeActivityLog($dataInsert = null)
	{
		if(!is_null($dataInsert)) {
			$this->insert($dataInsert);

		}
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function member() {
		return $this->belongsTo(Member::class, 'user_id', 'id');
	}
}
