<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: ApiToken.php
 */
namespace App\Models;

/**
 * @Class ApiToken
 * @package App\Models
 * @Description
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class ApiToken extends BaseModel
{
	/**
	 * @var string
	 * define table name
	 */
	protected $table = 'api_tokens';
}
