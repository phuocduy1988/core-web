<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: BaseModel.php
 */
namespace App\Models;
use App\AppTraits\SettingTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @Class BaseModel
 * @package App\Models
 * @Description Base model for all model
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class BaseModel extends Model
{
	//Define config info from table setting via  SettingTrait
    use SettingTrait;
	/**
	 * @var
	 */
	protected $user;

	/**
	 * BaseModel constructor.
	 */
	public function __construct()
    {
        parent::__construct();
    }
}