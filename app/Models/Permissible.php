<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: Permissible.php
 */
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * @Class Permissible
 * @package App\Models
 * @Description define authentication base model
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class Permissible extends BaseModel implements AuthenticatableContract,
	AuthorizableContract,
	CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword;
}