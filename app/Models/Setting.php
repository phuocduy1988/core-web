<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: Setting.php
 */
namespace App\Models;

/**
 * @Class Setting
 * @package App\Models
 * @Description
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class Setting extends BaseModel
{
	/**
	 * @var define table name
	 */
	protected $table = 'settings';

	/**
	 *	 call param or function via static
	 */
	const STATUS_ACTIVE   = 1;
	/**
	 *	call param or function via static
	 */
	const STATUS_INACTIVE = 0;


	//Write activity log
	public static function boot() {

		parent::boot();

		// create a event to happen on updating
		static::created(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$createData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Created',
					'user_agent' => \Request::header('User-Agent'),
					'original' => null,
					'attribute' => $createData,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];

				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		static::updated(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$originalData = json_encode($table->getOriginal());
				$changeData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Updated',
					'user_agent' => \Request::header('User-Agent'),
					'original' => $originalData,
					'attribute' => $changeData,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];
				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		// create a event to happen on deleting
		static::deleted(function($table)  {
			if(isset($table) && !is_null($table)) {
				$userId = \Auth::id();
				$deleteData = json_encode($table->getAttributes());
				$dataInsert = [
					'table_model' => $table->getTable().'_'.$table->id,
					'type' => 'Deleted',
					'user_agent' => \Request::header('User-Agent'),
					'original' => $deleteData,
					'attribute' => null,
					'user_id' => strlen($userId) > 0 ? $userId : 1,
					'user_type' => 1,
					'created_at' => date('Y-m-d H:i:s'),
				];
				$logActivity = new LogActivity();
				$logActivity->writeActivityLog($dataInsert);
			}
		});

		// create a event to happen on retrieved
		static::retrieved(function($table)  {
			//
		});
	}

	/**
	 * @param $query
	 * @return array
	 * @Description get setting from table settings
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function scopeGetSetting($query)
    {
        $setting = $query->select('key', 'value')->get();
        $data = array();
        foreach($setting as $obj)
        {
            $data1 = array($obj->key => $obj->value);
            $data = array_merge($data, $data1);
        }
        return $data;

    }


}
