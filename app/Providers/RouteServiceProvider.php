<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {

		$this->mapApiRoutes();

        $this->mapFrontendRoutes();

        $this->mapLogRoutes();

        $this->mapBackendRoutes();

        $this->mapBuilderRoutes();

        //
    }

    /**
     * Define the "builder" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapBuilderRoutes()
    {
        Route::middleware('builder')
            ->namespace($this->namespace.'\Builder')
            ->group(base_path('routes/builder.php'));
    }

    /**
     * Define the "backend" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapBackendRoutes()
    {
        Route::group([
            'namespace'     => $this->namespace.'\Backend',
            'prefix'        => 'be',
            'middleware'    => 'backend',
        ], function ($router) {
            require base_path('routes/backend.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapFrontendRoutes()
    {
		Route::middleware('web')
             ->namespace($this->namespace.'\Frontend')
             ->group(base_path('routes/frontend.php'));
    }

	/**
	 * Define the "log" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapLogRoutes()
	{
		Route::group([
			'namespace'     => $this->namespace,
			'prefix'        => 'be',
			'middleware'    => 'web',
		], function ($router) {
			require base_path('routes/log.php');
		});
	}

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api/v1')
             ->middleware('api')
             ->namespace($this->namespace.'\Api\v1')
             ->group(base_path('routes/api.php'));
    }
}
