<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: SettingTrait.php
 */

namespace App\AppTraits;

use App\Helpers\Helper;
use App\Models\Setting;

/**
 * Trait SettingTrait
 * @package App\AppTraits
 * @Author DuyLBP
 * @Date 2018-06-27
 * @Description get config value from table setting
 */
trait SettingTrait
{
	/**
	 * @return mixed|null
	 * @Description get list setting from database, then cache to server
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function traitGetAllSetting()
    {
        $cacheKey = 'setting1'.env('CACHE_KEY');
        //Get cache from server
        $setting = Helper::getCache($cacheKey);
        //Check cache null if null get data from table settings
        if(is_null($setting))
        {
            $setting = Setting::getSetting();
            //Set cache on server with expiration 30 minutes
            Helper::setCache($cacheKey, $setting, env('CACHE_MINUTE', 30));
            return $setting;
        }
        return $setting;
    }

	/**
	 * @param      $key
	 * @param null $default
	 * @param null $setting
	 * @return null
	 * @Description get value config on table setting by $key
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function traitGetSetting($key, $default=null, $setting = null)
    {
    	//Check setting is null
        if(is_null($setting))
        {
        	// get setting data
            $setting = $this->traitGetAllSetting();
        }
        return isset($setting[$key])?$setting[$key]:$default;
    }

	/**
	 * @param string $message
	 * @Description show message if traits have any error
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function traitError($message = '')
    {
        $response =  response()->json([
            'status'  => 500,
            'message'   => $message,
        ], 500);
        $response->send();
        die();
    }
}