<?php

namespace App\Http\Controllers\Builder;

use File;
use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CodeEditorController extends Controller
{

    /**
     * Get a file content from file path
     * @param Request $request
     * @return JSON
     * */
    public function getFileContent(Request $request)
    {
        $filePath = $request->get('path');
        $filePath = str_replace('/', '\\', $filePath);
        $filePath = str_replace('\\', '/', $filePath);
        $fileContent = File::get(base_path($filePath));
        return response()->json(['content' => $fileContent]);
    }

    /**
     * Put file content to a file
     * @param Request $request
     * @return JSON
     * */
    public function putFileContent(Request $request)
    {
        $filePath = $request->get('path');
        $filePath = str_replace('/', '\\', $filePath);
        $filePath = str_replace('\\', '/', $filePath);
        $fileContent = File::put(base_path($filePath), $request->get('content'));
        return response()->json(['content' => $fileContent]);
    }

}
