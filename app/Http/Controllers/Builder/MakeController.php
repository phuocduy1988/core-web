<?php

namespace App\Http\Controllers\Builder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Builder\MakerService;

class MakeController extends Controller
{
  public function index () {
    return view('builder.make-tools');
  }

  public function post (Request $request) {
    $maker = new MakerService($request);
    $response = $maker->makeFromRequest($request);
    return response()->json(['message' => $response]);
  }
}
