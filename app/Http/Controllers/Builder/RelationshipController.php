<?php

namespace App\Http\Controllers\Builder;

use App\Helpers\Builder\Database;
use App\Helpers\Builder\RelationShipService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelationshipController extends Controller
{
    public function index () {
    	$relationService = new RelationShipService();
        $relations = $relationService->getRelations();
        $models = array_keys($relations);
        $listColumns = [];
        foreach ($models as $model) {
			$tableName = snake_case(str_plural($model));
			$listColumns["$model"] = \Schema::getColumnListing($tableName);
		}
        $database = new Database();
        $relationshipTypes = $database->relationshipIdentifiers();
        return view('builder.relationship')
        				->with('relations', $relations)
        				->with('models', $models)
        				->with('relationshipTypes', $relationshipTypes)
        				->with('listColumns', $listColumns);
    }

    public function postNew (Request $request) {
		$relationService = new RelationShipService();
    	$build = $relationService->writeRelation($request);
		return response()->json(['code' => $build['code'],'message' => $build['message'],'route' => route('builder.relationships')]);
    }
}
