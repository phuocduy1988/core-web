<?php

namespace App\Http\Controllers\Builder\Installer;

use App\Helpers\Builder\EnvironmentManager;
use App\Helpers\Builder\FinalInstallManager;
use App\Helpers\Builder\InstalledFileManager;

class FinalController extends BaseController
{
    /**
     * Update installed file and display finished view.
     *
     * @param InstalledFileManager $fileManager
     * @return \Illuminate\View\View
     */
    public function finish(InstalledFileManager $fileManager, FinalInstallManager $finalInstall, EnvironmentManager $environment)
    {
        $finalMessages = $finalInstall->runFinal();
        $finalStatusMessage = $fileManager->update();
        $finalEnvFile = $environment->getEnvContent();

        return view('builder.installer.finished', compact('finalMessages', 'finalStatusMessage', 'finalEnvFile'));
    }
}
