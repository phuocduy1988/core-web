<?php

namespace App\Http\Controllers\Builder\Installer;

use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{

    /**
     * Display the installer welcome page.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('builder.installer.welcome');
    }

}
