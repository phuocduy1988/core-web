<?php

namespace App\Http\Controllers\Builder\Installer;

use App\Helpers\Builder\RequirementsChecker;

class RequirementsController extends BaseController
{
    /**
     * @var RequirementsChecker
     */
    protected $requirements;

    /**
     * @param RequirementsChecker $checker
     */
    public function __construct(RequirementsChecker $checker)
    {
        $this->requirements = $checker;
    }

    /**
     * Display the requirements page.
     *
     * @return \Illuminate\View\View
     */
    public function requirements()
    {
        $phpSupportInfo = $this->requirements->checkPHPversion(
            config('installer.core.minPhpVersion')
        );
        $requirements = $this->requirements->check(
            config('installer.requirements')
        );

        return view('builder.installer.requirements', compact('requirements', 'phpSupportInfo'));
    }
}