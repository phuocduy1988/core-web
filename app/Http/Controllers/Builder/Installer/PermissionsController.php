<?php

namespace App\Http\Controllers\Builder\Installer;

use App\Helpers\Builder\PermissionsChecker;

class PermissionsController extends BaseController
{

    /**
     * @var PermissionsChecker
     */
    protected $permissions;

    /**
     * @param PermissionsChecker $checker
     */
    public function __construct(PermissionsChecker $checker)
    {
        $this->permissions = $checker;
    }

    /**
     * Display the permissions check page.
     *
     * @return \Illuminate\View\View
     */
    public function permissions()
    {
        $permissions = $this->permissions->check(
            config('installer.permissions')
        );

        return view('builder.installer.permissions', compact('permissions'));
    }
}
