<?php
namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;

class UpdateFileController extends Controller
{

    /**
     * Load Update file
     * */
    public function updateFile () {
      //$models = $this->getFiles('app', 'php', true, ['Http', 'Console', 'Providers', 'Traits', 'Exceptions', 'Helpers']);
      $models = $this->getFiles('app/Models', 'php', true, ['Builder']);
      $controllers = $this->getFiles('app/Http/Controllers', 'php', true, ['Builder']);
      $views = $this->getFiles('resources/views', 'html' ,true, ['builder', 'backend/_partials', 'backend/layouts', 'errors']);
      $migrations = $this->getFiles('database/migrations', 'php', true, ['Builder']);
      $routes = $this->getFiles('routes', 'php', true, ['builder']);
      $lang = $this->getFiles('resources/lang', 'php', true, ['builder']);

      return view('builder.update-file')
            ->with('routes', $routes)
            ->with('langs', $lang)
            ->with('models', $models)
            ->with('controllers', $controllers)
            ->with('views', $views)
            ->with('migrations', $migrations);
    }


    /**
     * Get a list of current files on a dir
     * @return  Array list of file names
     * */
    public function getFiles ($path, $lang, $recursive = false, $exclude = []) {
        
        if ($recursive) {
          $files = File::allFiles(base_path($path), $recursive);
        } else {
          $files = File::files(base_path($path), $recursive);
        }
        
        $filesData = [];

        foreach ($files as $file) {
            $shouldBeExcluded = false;
            foreach ($exclude as $excludedPath) {
              if ($shouldBeExcluded === false) {
                $shouldBeExcluded = (strpos($file->getRelativePath(), $excludedPath) !== false);
              }
            }
            if (!$shouldBeExcluded) {
              $filesData[$file->getRelativePath()][] = [
                'name' =>  $file->getBasename('.php'),
                'lang' => $lang,
                'path' => $path.'/'.$file->getRelativePath().'/'.$file->getFilename()
              ];
            }
        }

        return $filesData;
    }

    /**
     * Load API Tester
     * */
    public function apiTester(Request $request)
    {
        return view('builder.api');
    }

}
