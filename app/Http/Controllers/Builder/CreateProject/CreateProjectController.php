<?php

namespace App\Http\Controllers\Builder\CreateProject;

use App\Http\Controllers\Controller;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;
use Validator;

class CreateProjectController extends Controller
{
    /**
     * @param
     */
    public function __construct( )
    {

    }

    /**
     * Display the Environment menu page.
     *
     * @return \Illuminate\View\View
     */
    public function createProject(Request $request)
    {
        if($request->isMethod('POST')) {
            return $this->saveProject($request);
        }
        return view('builder.create-project.create-project');
    }
    private function saveProject(Request $request)
    {
        $projectName = $request->get('project_name');
        $subDomain = $request->get('sub_domain');
        $subDomain = seo_friendly_url($subDomain);
        $projectFolder = str_slug($projectName);
        $url     = $subDomain.'.tek1.top';
        print_r('pro=>'.$projectFolder. '=>subdomain=>'.$subDomain);
        $files = new Filesystem();
        $path = '/var/www/project-manager';
        //$path = '/Volumes/Data/OneDrive/Working/OT-Production/WebAutoGenerator';
        $pathCms = $path . '/cms/';
        //$pathCms = $path . '/demo-autogen';
        $pathNewProject = $path . '/' . $projectFolder. '/';
        $permission = 0775;

        if ($files->isDirectory($pathNewProject)) {
            return redirect()->back()->withErrors('Folder existed!');
            //$files->makeDirectory($pathNewProject, $permission, true, true);
        }

        //Create file nginx config
        $fileNginxStumb = public_path('/builder/stubs/nginx/nginx.stub');
        $fileNginxBackup = public_path('/nginx-config/'.$subDomain.'.conf');
        $nginxConfig = $files->get($fileNginxStumb);
        $nginxConfig = str_replace('{{DOMAIN}}', $url, $nginxConfig);
        $nginxConfig = str_replace('{{PATH_NEW_PROJECT}}', $pathNewProject, $nginxConfig);
        $nginxConfig = str_replace('{{PROJECT_NAME}}', $projectFolder, $nginxConfig);
        $files->put($fileNginxBackup, $nginxConfig);
        //Copy to nginx config
        $pathNginx = '/etc/nginx/autogencode/';
        //Check exist nginx config
        $pathNginxCheckExist = $pathNginx. $subDomain .'.conf';
        echo 'Path nginx =>'.$pathNginxCheckExist;
        print_r($files->isDirectory($pathNginxCheckExist));
        echo 'end check exist nginx';
        if ($files->isFile($pathNginxCheckExist)) {
            return redirect()->back()->withErrors('Nginx config existed!');
            //$files->makeDirectory($pathNewProject, $permission, true, true);
        }
        $execCopy  = "cp -R $fileNginxBackup $pathNginx";
        $copyNginx = system($execCopy);
        echo '02=>'.$execCopy;

        $execCopy = "cp -R $pathCms $pathNewProject";
        //Create Project
        $process = system($execCopy);
        echo '01=>'.$execCopy;


        //Create nginx config
        $outLog = $this->cleanup();

        $exec = system("chmod -R 777 $pathNewProject/storage/");
        $exec = system("chmod -R 777 $pathNewProject/bootstrap/");
        $exec = system("chmod -R 777 $pathNewProject/public/nginx-config/");

        echo '=>> Who is ==>>' . shell_exec('whoami');

        return view('builder.create-project.finished')->with('url', $url.'/install')->with('outLog', $outLog);
    }

    /**
     * Do a general cleanup for autoloads and compiled class names
     * @return
     * */
    public function cleanup () {
        // Dump Composer autoload
        try {
            $reloadNginx = system('/etc/init.d/nginx reload');

            return $reloadNginx;
        } catch (\Exception $e) {
            // Silence!
        }

    }

}
