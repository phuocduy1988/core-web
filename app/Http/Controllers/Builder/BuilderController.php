<?php

namespace App\Http\Controllers\Builder;

use App\Helpers\Builder\BuilderUpdateService;
use App\Helpers\Builder\Database;
use App\Helpers\Builder\RelationShipService;
use DB;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Builder\BuilderService;
use App\Http\Requests\BuilderRequest;

class BuilderController extends Controller
{
    public function index () {
        $db = new Database();
        $dataTypes = $db->dataTypes();
        $relationshipIdentifiers = $db->relationshipIdentifiers();
        $relationShip = new RelationShipService();
        $relations = $relationShip->getRelations();
        $models = array_keys($relations);
//
        return view('builder.build')
            ->with('relationshipIdentifiers',$relationshipIdentifiers)
            ->with('models',$models)
            ->with('dataTypes',$dataTypes);
    }

    public function build (BuilderRequest $request) {
//        \Log::info($request->all());
//        dd($request->all());
        $builder = new BuilderService($request);
        $response = $builder->buildFromRequest();
        return response()->json(['route' => getBeUrl($response)]);
    }

//    public function buildVerbose (Request $request) {
//        $builder = new BuilderService($request);
//        $response = $builder->buildFromRequest($request);
//    }

	public function update() {
		$db = new Database();
		$dataTypes = $db->dataTypes();
		$relationShip = new RelationShipService();
		$relations = $relationShip->getRelations();
		$models = array_keys($relations);
		$relationshipIdentifiers = $db->relationshipIdentifiers();
		$listColumns = [];
		$listColumnWithTypes = [];
		foreach ($models as $model) {
			$tableName = snake_case(str_plural($model));
			$listColumns["$model"] = \Schema::getColumnListing($tableName);
			$listColumnWithTypes["$model"] = $this->getTableColumnAndTypeList($tableName);
		}
		return view('builder.build-update')
			->with('models',$models)
			->with('relationshipIdentifiers',$relationshipIdentifiers)
			->with('dataTypes',$listColumns)
			->with('dataTypes',$listColumnWithTypes)
			->with('dataTypes',$dataTypes);
	}

	public function buildUpdate(BuilderRequest $request) {
		$builder = new BuilderUpdateService($request);
		$response = $builder->buildFromRequest($request);
		return response()->json(['route' => getBeUrl($response)]);
	}

	private function getTableColumnAndTypeList($tableName = 'users'){
		$fieldAndTypeList = [];
		foreach (DB::select( "describe $tableName")  as $field){
			$type = (!str_contains($field->Type, '('))? $field->Type: substr($field->Type, 0, strpos($field->Type, '('));
			$tmp['Type'] = $type;
			$type = (!str_contains($field->Null, '('))? $field->Null: substr($field->Null, 0, strpos($field->Null, '('));
			$tmp['Null'] = $type;
			$type = (!str_contains($field->Key, '('))? $field->Key: substr($field->Key, 0, strpos($field->Key, '('));
			$tmp['Key'] = $type;
			$type = (!str_contains($field->Default, '('))? $field->Default: substr($field->Default, 0, strpos($field->Default, '('));
			$tmp['Default'] = $type;
			$type = (!str_contains($field->Extra, '('))? $field->Extra: substr($field->Extra, 0, strpos($field->Extra, '('));
			$tmp['Extra'] = $type;



			$fieldAndTypeList[$field->Field] = $tmp;
		}
		return $fieldAndTypeList;
	}
}
