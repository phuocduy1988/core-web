<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Models\User;
use DateTime;
use Validator;
use Auth;

/**
* Backend Login Controller
* @author duylbp
* @date   2017-10-04
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $_loginPath           = '';
    protected $_redirectTo          = '';
    protected $_redirectAfterLogout = '';
    protected $_maxLoginAttempts    = 5;
    protected $_resetKeyDuration    = 24;  // 24 hours

    function __construct(Guard $auth, Request $request)
    {
        parent::__construct($request);
        // Define path URL
        $this->_redirectTo          = route('common.home');
        $this->_loginPath           = '/be/login';
        $this->_redirectAfterLogout = '/be/login';
    }

    /**
    * Override the username method used to validate login
    *
    * @return string
    */
    public function username()
    {
        return 'email';
    }

    /**
     * show login view
     * @author duylbp
     * @date   2017-10-04
     * @return view
     */
    public function showLoginForm()
    {
        return view('backend.users.login');
    }

    /**
     * Check login account
     * @author duylbp
     * @date   2017-10-04
     * @param  Request    $request
     * @return response
     */
    public function postLogin(Request $request)
	{
		try {
			$email  = $request->get('email', '');
			$password = $request->get('password', '');
			// Check validate email and password
			$validator = $this->validateLogin($request);

			if($validator->fails()) {
				return redirect()->to($this->_loginPath)
					->withErrors($validator->errors());
			}
			// Check email and password data
			if(Auth::attempt(['email' => $email, 'password' => $password])) {
				// If account is not activated will back with error
				// Update last login time
				Auth::user()->last_login = new DateTime();
				Auth::user()->save();
				// Redirect to home
				return redirect()->to($this->_redirectTo);
			}
			//redirect to login page and show error message
			return redirect()->to($this->_loginPath)
				->withErrors(array('login' => trans('auth.failed')));
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}

    /**
	 * Log the user out of the application.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
        // Update last logout time
		Auth::user()->save();
        // Clear session
		$this->guard()->logout();
		$request->session()->flush();
		$request->session()->regenerate();

		return redirect($this->_redirectAfterLogout);
	}

    /**
     * Check validation
     * @param  [type]     $request
     * @return [type]     $validator
     */
    public function validateLogin($request)
    {
        $validator = Validator::make($request->all(), $this->_loginRules());
        $validator->setAttributeNames($this->_setAttributeName());
        return $validator;
    }

    /**
     * Login rules
     * @param  array      $merge
     * @return array      $rule
     */
    private function _loginRules($merge = [])
    {
    	//email and password must require
        return array_merge(
            [
                'email'  => 'required',
                'password'  => 'required'
            ], $merge);
    }

    /**
     * Login rules
     * @param  array      $merge
     * @return array      $attribute
     */
    private function _setAttributeName($merge = [])
    {
        return array_merge(
            [
                'email'  => trans('label.email'),
                'password'  => trans('label.password')
            ], $merge);
    }

	/** Overide
	 * Get the failed login response instance.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function sendFailedLoginResponse(Request $request) {

		$errors = [$this->username() => trans('auth.failed')];
		if ($request->expectsJson()) {
			return response()->json($errors, 422);
		}

		return redirect()->back()
                            ->withInput($request->only($this->username(), 'remember'))
                            ->withErrors($errors);
	}
}
