<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-08-15
 * Time: 18:23:23
 * File: PageController.php
 */
namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $pages = Page::query();

			$pages = $pages->with('menu');

            if ($request->has('keyword')) {
                //Search via param key word
                $pages = $pages->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')


								->orWhere('description', 'LIKE', '%'.$request->get('keyword').'%')
								->orWhere('title', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $pages = $pages->orderBy($this->_orderBy, $this->_sortType);
	        $this->_gridPageSize = 50;

            // $pages = $pages->where('lang', $this->_lang);

            //Get data with pagination
            $pages = $pages
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.pages.index')->with('pagesData', $pages);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(Page $page = null)
    {
        try {
            //Get data for create new or update
            $page = $page ?: new Page;
            return view('backend.pages.form')->with('page', $page);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via PageRequest
     * @param  PageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(PageRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            $page = Page::firstOrNew(['id' => $request->get('id')]);

            if(isset($page->id)) {
                $isUpdate = true;
            }

            $content = $request->get('content');

            if($request->get('optimize_image', '')) {
				compressContentSEO($content);
			}

            $page->title = $request->get('title');

			$page->slug = $request->get('slug');
			$page->description = $request->get('description');
			$page->content = $content;
			$page->status = $request->get('status', 0);
			$page->lang = $request->get('lang');

			$page->is_html = $request->get('is_html', 0);

			$page->meta_title = $request->get('meta_title');
			$page->meta_description = $request->get('meta_description');
			$page->rating = $request->get('rating');
			$page->comment = $request->get('comment');

			$page->schema = $request->get('schema');

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
            $page->save();
            Helper::cacheClearAll();

			$message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
			return response()->json(['message' => $message, 'url' => route('page.form', [$page->id])], 200);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return response()->json(['message' => $e->getMessage()], 500);
		}
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Page $page)
    {
        try {
           $page->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($pageId)
    {
        try {
            $page = Page::withTrashed()->find($pageId);
            $page->restore();
            $message = 'page Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($pageId)
    {
        try {
            $page = Page::withTrashed()->find($pageId);
            $page->forceDelete();
            $message = 'page permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
