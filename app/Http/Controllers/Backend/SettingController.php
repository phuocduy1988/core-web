<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-08-03
 * Time: 18:19:15
 * File: MemberController.php
 */
namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\Models\Setting;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $settings = Setting::query();

			$settings = $settings->selectRaw("settings.id, settings.key, settings.value, settings.description");

            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
			$settings = $settings->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
			$settings = $settings->get();

			$jsonSettings = json_encode($settings->toArray(),true);

            return view('backend.settings.index')->with('jsonSettings', $jsonSettings);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }


    /**
     * Store a newly created resource in storage.
     * Check validation via MemberRequest
     * @param  SettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(SettingRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
			$id = $request->get('id');
			$setting = Setting::firstOrNew(['id' => $id]);

            if(isset($setting->id)) {
                $isUpdate = true;
            }

			$setting->key = $request->get('key');
			$setting->value = $request->get('value');
			$setting->description = $request->get('description');
			$setting->status = $request->get('status', 1);

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
			$setting->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return response()->json(['message' => $message, 'setting' => $setting], 200);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
			return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function clearCaches(Request $request) {
    	$cacheKey = $request->get('key', '');
    	if(strlen($cacheKey) > 0) {
    		Helper::forgetCache($cacheKey);
		}
		else {
    		Helper::cacheClearAll();
		}
		return redirect()->back()->with('alert', trans('message.clear_cache_success'));
	}

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
        	$id = $request->get('id');
			$setting = Setting::findOrFail($id);
			$setting->delete();
            $message = trans('message.deleted_success');
			return response()->json(['message' => $message], 200);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
