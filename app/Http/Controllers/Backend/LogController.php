<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-08-03
 * Time: 18:19:15
 * File: LogController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\LogActivity;
use Illuminate\Http\Request;

class LogController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function logActivity(Request $request)
	{
		try {
			return view('backend.logs.logActivity');
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}

	public function getLogActivity(Request $request) {
		try {
			//Get param page
			$page           = $request->get('page', 1);
			//Get data from position
			$offSet         = ($page - 1) * $this->_gridPageSize;

			$activities = LogActivity::query();

			$activities = LogActivity::with('user');

			//Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
			$activities = $activities->orderBy($this->_orderBy, $this->_sortType);

			//Get data with pagination
			$activities = $activities
				->skip($offSet)
				->take($this->_gridPageSize)
				->paginate($this->_gridPageSize);

			return $this->jsonOK($activities);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return $this->jsonNG($e->getMessage());
		}
	}
}
