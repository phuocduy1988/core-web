<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2019-05-07
 * Time: 18:19:09
 * File: PageTypeController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\PageType;
use Illuminate\Http\Request;
use App\Http\Requests\PageTypeRequest;
use App\Http\Controllers\Backend\Controller;

class PageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $pageTypes = PageType::query();

            if ($request->has('keyword')) {
                //Search via param key word
                $pageTypes = $pageTypes->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $pageTypes = $pageTypes->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            $pageTypes = $pageTypes
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.pageTypes.index')->with('pageTypesData', $pageTypes);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(PageType $pageType = null)
    {
        try {
            //Get data for create new or update
            $pageType = $pageType ?: new PageType;
            return view('backend.pageTypes.form')->with('pageType', $pageType);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via PageTypeRequest
     * @param  PageTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(PageTypeRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            $pageType = PageType::firstOrNew(['id' => $request->get('id')]);

            if(isset($pageType->id)) {
                $isUpdate = true;
            }

            $pageType->name = $request->get('name');
			$pageType->status = $request->get('status', 0);

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
            $pageType->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return redirect()->route('pageType.index')->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(PageType $pageType)
    {
        try {
           $pageType->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($pageTypeId)
    {
        try {
            $pageType = PageType::withTrashed()->find($pageTypeId);
            $pageType->restore();
            $message = 'pageType Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($pageTypeId)
    {
        try {
            $pageType = PageType::withTrashed()->find($pageTypeId);
            $pageType->forceDelete();
            $message = 'pageType permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
