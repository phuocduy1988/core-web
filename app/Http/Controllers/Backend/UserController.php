<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-23
 * Time: 16:45:03
 * File: MemberController.php
 */

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		try {
			//Get param page
			$page = $request->get('page', 1);
			//Get data from position
			$offSet = ($page - 1) * $this->_gridPageSize;
			
			$users = User::query();
			
			if ($request->has('keyword')) {
				//Search via param key word
				$users = $users->where(
					function ($query) use ($request) {
						/** @var $query Illuminate\Database\Query\Builder
						 * return $query->where()->orWhere()->orWhere()
						 * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
						 *
						 * @Please do not remove search id
						 */
						return $query
							->where('id', 'LIKE', '%' . $request->get('keyword') . '%')
							->orWhere('email', 'LIKE', '%' . $request->get('keyword') . '%');
					}
				);
			}
			//Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
			$users = $users->orderBy($this->_orderBy, $this->_sortType);
			
			//Get data with pagination
			$users = $users
				->skip($offSet)
				->take($this->_gridPageSize)
				->paginate($this->_gridPageSize);
			return view('backend.users.index')->with('usersData', $users);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function form(User $user = null)
	{
		try {
			//Get data for create new or update
			$user = $user ? : new User;
			return view('backend.users.form')->with('user', $user);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Store a newly created resource in storage.
	 * Check validation via MemberRequest
	 *
	 * @param MemberRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function post(UserRequest $request)
	{
		try {
			$isUpdate = false;
			//Get data for create new or update via id
			$user = User::firstOrNew(['id' => $request->get('id')]);
			
			if (isset($user->id)) {
				$isUpdate = true;
			}
			$password = $request->get('password', '');
			$user->first_name = $request->get('first_name');
			$user->last_name = $request->get('last_name');
			$user->address = $request->get('address');
			$user->phone = $request->get('phone');
			$user->image = $request->get('image');
			$user->email = $request->get('email');
			if (strlen($password) > 0) {
				$user->password = bcrypt($password);
			}
			$user->status = $request->get('status', 0);
			
			
			//--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
			$user->save();
			
			$message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
			return redirect()->route('user.index')->with('alert', $message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Remove the specified resource from database.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete(User $user)
	{
		try {
			$user->delete();
			$message = trans('message.deleted_success');
			return redirect()->back()->with('alert', $message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Restore the specified deleted resource to database.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function restore($userId)
	{
		try {
			$user = User::withTrashed()->find($userId);
			$user->restore();
			$message = 'User Restored.';
			return redirect()->back()->with('alert', $message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
		
	}
	
	/**
	 * Delete forever
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function forceDelete($userId)
	{
		try {
			$user = User::withTrashed()->find($userId);
			$user->forceDelete();
			$message = 'User permanently deleted.';
			return redirect()->back()->withMessage($message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
}
