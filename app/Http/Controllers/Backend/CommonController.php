<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-09
 * Time: 12:26:05
 * File: NewsController.php
 */
namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\Jobs\SendEmailTest;
use App\Models\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Analytics;
use Intervention\Image\Facades\Image;
use Softon\LaravelFaceDetect\Facades\FaceDetect;
use Spatie\Analytics\Period;
use Spatie\ImageOptimizer\OptimizerChainFactory;


class CommonController extends Controller
{

    public function home(Request $request)
    {
		try {
			return view('backend.home.index');

		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return redirect(route('page.index'));
		}

    }

	public function statMembers() {
		$cacheKey = 'statMemberRegisted';
		if(\App\Helpers\Helper::checkCache($cacheKey)) {
			//Get data from cache
			$data = \App\Helpers\Helper::getCache($cacheKey);
		}
		else {
			//Set cache
			$memberRegisterCount 	= \App\Models\User::count();
			$data = [
				'memberRegisterCount' 	=> $memberRegisterCount,
			];
			\App\Helpers\Helper::setCache($cacheKey, $data);
		}

		return $data;
	}

	/**
	 * Display url
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function iframe(Request $request)
	{
		$url = '';
		return view('backend._partials.iframe', compact($url));
	}
	/**
	 * changeBoolean
	 * @param  Request    $request
	 * @return flag
	 */
	public function changeBoolean(Request $request)
	{
		try {
			if($request->ajax()) {
				$id 		= $request->get('id', '');
				$table 		= $request->get('table', '');
				$boolean 	= $request->get('boolean', 1);
				$columName = $request->get('column_name', 1);
				$flag 		= 0;

				if(!empty($id) && !empty($table) &&  !empty($columName)) {
					$flag = \DB::table($table)->where('id', $id)
						->update(array(
							$columName 		=> $boolean == 1 ? 0 : 1,
							'updated_at'	=> Helper::dateNow()
						));
				}

				return $flag;
			}
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
}
