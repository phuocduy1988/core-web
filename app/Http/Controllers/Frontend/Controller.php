<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Helper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\AppTraits\SettingTrait;
use Illuminate\Http\Request;

abstract class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests, SettingTrait;
	
	protected $_gridPageSize;
	protected $_orderBy;
	protected $_sortType;
	protected $_actionDraft;
	protected $_actionConfirm;
	protected $locale;
	
	public function __construct(Request $request)
	{
		$this->_gridPageSize = config('site.grid_page_size', 25);
		$this->_orderBy = $request->get('sort_field', config('site.order_by', 'id'));
		$this->_sortType = $request->get('sort_type', config('site.sort_type', 'desc'));
		if (empty($this->_orderBy) || empty($this->_sortType)) {
			$this->_orderBy = 'id';
			$this->_sortType = 'desc';
		}
		$this->_actionDraft = config('site.action_draft', 'draft_save');
		$this->_actionConfirm = config('site.action_confirm', 'confirm');
		
		otSetLocale($request);
		$this->locale = locale();
	}
	
	/**
	 * @param     $errorMsg
	 * @param int $statusCode
	 * @param int $line
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @Author DuyLBP
	 * @Date   2018-06-27
	 */
	protected function jsonNG($errorMsg, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR, $line = 0)
	{
		return response()->json(
			[
				'status'  => $statusCode,
				'message' => $errorMsg,
				'line'    => $line,
			], $statusCode
		);
	}
	
	/**
	 * @param     $errorMsg
	 * @param int $statusCode
	 * @param int $line
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @Author DuyLBP
	 * @Date   2018-06-27
	 */
	protected function jsonOK($data = [], $statusCode = Response::HTTP_OK)
	{
		return response()->json($data, $statusCode);
		
	}
	
	public function checkFEAuth()
	{
		if (is_null(feAuth()->user())) {
			echo view('errors.500')->with('message', trans('auth.failed'));
			die();
		}
	}
	
	public function memberId()
	{
		$user = feAuth()->user();
		if ($user) {
			return $user->getAuthIdentifier();
		}
		return null;
	}
	
	public function page404($message = '')
	{
		redirect301();
		return view('frontend.errors.404')->with('message', $message);
	}
	
}
