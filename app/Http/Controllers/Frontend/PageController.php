<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Helper;
use App\Models\Page;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App;

/**
 * @Class   PageController
 * @package App\Http\Controllers\Frontend
 * @Description
 * @Author  DuyLBP
 * @Date    8/15/18
 */
class PageController extends Controller
{
	public function __construct(Request $request)
	{
		parent::__construct($request);
	}
	
	/**
	 * @Description
	 * @Author DuyLBP
	 * @Date   8/15/18
	 */
	public function home(Request $request)
	{
		try {
			dd('Permision denied!!!');
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @Description
	 * @Author DuyLBP
	 * @Date   8/15/18
	 */
	public function loadPage(Request $request)
	{
		try {
			$slug = $request->route('slug');
			return $this->getPage($slug);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	public function getPage($slug)
	{
		$data = setDataPage($slug);
		if (is_null($data)) {
			return $this->page404();
		}
		return view('frontend.pages.page')->with('data', $data);
	}
}
