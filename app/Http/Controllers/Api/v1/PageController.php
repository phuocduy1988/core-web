<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-08-15
 * Time: 18:23:23
 * File: PageController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\PageRequest;
use App\Http\Controllers\Api\v1\ApiBaseController;

class PageController extends ApiBaseController
{
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $pages = Page::query();


			$pages = $pages->with('menu');
            //Generate search option here
            if ($request->has('keyword')) {
                //Search via param key word
                $pages = $pages->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')





								->orWhere('description', 'LIKE', '%'.$request->get('keyword').'%')
								->orWhere('title', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Get data with pagination and order by(Default:: id desc)
            $pages = $pages
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($pages);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created pages in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'title' => ['required', 'max:191'],


			'slug' => ['required', 'max:191', Rule::unique('pages')->ignore($this->get('id'))],
			'description' => ['required'],
			'content' => ['required'],
			'status' => ['nullable', 'boolean'],

			'lang' => ['required', 'max:191'],

			'is_html' => ['nullable', 'boolean'],

			'meta_title' => ['nullable'],
			'meta_description' => ['nullable'],
			'rating' => ['nullable', 'max:191'],
			'comment' => ['nullable'],

			'schema' => ['nullable'],

                //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('title', '').
				$request->get('slug', '').$request->get('description', '').$request->get('content', '').$request->get('status', '').
				$request->get('lang', '').
				$request->get('menu_id', '').
				$request->get('is_html', '').
				$request->get('meta_title', '').$request->get('meta_description', '').$request->get('rating', '').$request->get('comment', '').
				$request->get('schema', '').
                //--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new Page
            $page = new Page();
            $page->title = $request->get('title');

			$page->slug = $request->get('slug');
			$page->description = $request->get('description');
			$page->content = $request->get('content');
			$page->status = $request->get('status', 0);

			$page->lang = $request->get('lang');

			$page->menu_id = $request->get('menu_id');
			$page->is_html = $request->get('is_html', 0);

			$page->meta_title = $request->get('meta_title');
			$page->meta_description = $request->get('meta_description');
			$page->rating = $request->get('rating');
			$page->comment = $request->get('comment');

			$page->schema = $request->get('schema');

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $page->save();

            //Check response data
            $responseCode = is_null($page->id) ? 201 : 200;
            return $this->jsonOK(['data' => $page], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update pages in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('title', '').

				$request->get('slug', '').$request->get('description', '').$request->get('content', '').$request->get('status', '').
				$request->get('lang', '').
				$request->get('menu_id', '').
				$request->get('is_html', '').
				$request->get('meta_title', '').$request->get('meta_description', '').$request->get('rating', '').$request->get('comment', '').
				$request->get('schema', '').
                //--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $page = Page::find($id);
            if(is_null($page)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('title')) {
				$page->title = $request->get('title');
			}


			if ($request->get('slug')) {
				$page->slug = $request->get('slug');
			}
			if ($request->get('description')) {
				$page->description = $request->get('description');
			}
			if ($request->get('content')) {
				$page->content = $request->get('content');
			}
			if ($request->get('status')) {
				$page->status = $request->get('status');
			}

			if ($request->get('lang')) {
				$page->lang = $request->get('lang');
			}

			if ($request->get('menu_id')) {
				$page->menu_id = $request->get('menu_id');
			}
			if ($request->get('is_html')) {
				$page->is_html = $request->get('is_html');
			}

			if ($request->get('meta_title')) {
				$page->meta_title = $request->get('meta_title');
			}
			if ($request->get('meta_description')) {
				$page->meta_description = $request->get('meta_description');
			}
			if ($request->get('rating')) {
				$page->rating = $request->get('rating');
			}
			if ($request->get('comment')) {
				$page->comment = $request->get('comment');
			}

			if ($request->get('schema')) {
				$page->schema = $request->get('schema');
			}

            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $page->save();

            return $this->jsonOK(['data' => $page]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified pages from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $page = Page::find($id);
            if(!is_null($page)) {
                $page->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $page = Page::withTrashed()->find($id);
            if(!is_null($page)) {
                $page->restore();
                return $this->jsonOK(['data' => $page]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $page = Page::withTrashed()->find($id);
            if(!is_null($page)) {
                $page->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
