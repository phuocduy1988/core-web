<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: UserController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Helpers\Helper;
use App\Models\ApiToken;
use App\Models\Member;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class UserController extends ApiBaseController
{
	/**
	 * ClinicUserController constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|void
	 */
	public function login(Request $request)
	{
		try {
			$param['email'] = $request->get('email', '');
			$param['password'] = $request->get('password', '');

			$rules = [
				'email' => ['required'],
				'password' => ['required'],
			];

			$validator = Validator::make($param, $rules, [], User::NAME_VALIDATOR);

			if (strlen($this->client->deviceId) <= 0) {
				return $this->jsonDefaultError(trans('error.client_invalid_data'));
			}

			if ($validator->fails()) {
				return $this->jsonNG($validator->errors());
			}

			$email = $param['email'];
			$password = $param['password'];


			$user = User::where('email', $email)->first();

			if (!is_null($user)) {
				if (\Hash::check($password, $user->password)) {
					$token = $user->updateToken($user, $this->client);

					return $this->jsonOK(array_merge(['user' => $user], ['token' => $token]));
				}
			}
			return $this->jsonNG(trans('auth.failed'), Response::HTTP_UNAUTHORIZED);
		}
		catch (\Exception $e) {
			Helper::writeLogException($e);
			return $this->jsonCatchException($e->getMessage());
		}

	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * Author: DuyLBP
	 * Logout from client
	 */
	public function logout(Request $request)
	{
		try {
			//get token want to remove
			$token = $request->get('token', '');
			//get api token via token from request header
			$apiToken = ApiToken::where('token', $token)->first();
			if (!is_null($apiToken)) {
				//if token existed -> remove token
				$apiToken->delete();
				//Clear all session
				Auth::logout();
				return $this->jsonOK(['message' => 'Logout Ok']);
			}
			//Return error message when api token is not exist
			return $this->jsonNG('Error!');
		}
		catch (\Exception $e) {
			Helper::writeLogException($e);
			return $this->jsonCatchException($e->getMessage());
		}

	}
}
