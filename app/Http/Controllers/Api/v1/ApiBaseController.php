<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: ApiBaseController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Helpers\ApiAuth;
use App\Helpers\Client;
use App\Helpers\Helper;
use Illuminate\Routing\Controller as BaseController;
use App\AppTraits\SettingTrait;
Use Illuminate\Validation\Validator;
use Illuminate\Http\Response;

/**
 * @Class ApiBaseController
 * @package App\Http\Controllers\Api\v1
 * @Description Base controller for all api controller
 * @Author DuyLBP
 * @Date 2018-06-27
 */
class ApiBaseController extends BaseController
{
	use SettingTrait;

	/**
	 *
	 */
	const GET_PARAM_PER_PAGE = 'limit';

	/**
	 * @var int
	 */
	protected $perPage = 100;

	/**
	 * @var bool
	 */
	protected $isValid = false;

	/**
	 * @var \Illuminate\Database\Eloquent\Model|null|object|static
	 */
	protected $user;

	/**
	 * @var string
	 */
	protected $temp = "";

	/**
	 * @var string
	 */
	protected $token = "";

	/**
	 * @var \App\Helpers\Client
	 */
	protected $client;

	/**
	 * ApiBaseController constructor.
	 */
	public function __construct()
	{
		$this->client = new Client();
		$apiAuth = new ApiAuth();
		$this->user = $apiAuth->auth($this->client);
	}

	/**
	 * @return mixed|null
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	public function getUserId()
	{
		if (is_null($this->user)) {
			return null;
		}
		return $this->user->id;
	}

	/**
	 * @return bool
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function checkuserNull()
	{
		if (is_null($this->user)) {
			return true;
		}

		return false;
		}

	/**
	 * @param     $errorMsg
	 * @param int $statusCode
	 * @param int $line
	 * @return \Illuminate\Http\JsonResponse
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonNG($errorMsg, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR, $line = 0)
	{
		return response()->json([
			'status' => $statusCode,
			'message' => $errorMsg,
			'line' => $line,
		], $statusCode);

	}

	/**
	 * @param      $clientKey
	 * @param      $serverKey
	 * @param null $hashString
	 * @Description verify data hash key
	 * @Author DuyLBP
	 * @Date 7/4/18
	 */
	protected function checkHashKey($clientKey, $serverKey, $hashString = null)
	{

		if($serverKey != $clientKey)
		{
			$header = [];
			if(Helper::debugMode())
			{
				$header =  [
					'hash_string'       => $hashString,
					'app_hash_key'      => $clientKey,
					'server_hash_key'   => $serverKey,
				];
			}
			$response =  response()->json([
				'err_code'  => Response::HTTP_INTERNAL_SERVER_ERROR,
				'err_msg'   => \Lang::get('error.hash_key_invalid'),
				'data' => [
					'header' => $header
				]
			], 200);


			$response->send();
			die();
		}
	}

	/**
	 * @param     $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @Description return json response data
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonOK($data, $statusCode = Response::HTTP_OK)
	{
		return response()->json([
			'result' => $data,
			'status' => $statusCode
		]);
	}

	/**
	 * @param     $data
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @Description return json without param result, status, message
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonRawOK($data, $statusCode = Response::HTTP_OK)
	{
		return response()->json($data, $statusCode);
	}

	/**
	 * @param     $data
	 * @param     $other
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @Description return json response data and more info
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonOKMore($data, $other, $statusCode = Response::HTTP_OK)
	{
		return response()->json([
			'status' => $statusCode,
			'result' => array_merge($data, $other),
		]);
	}

	/**
	 * @param     $errorMsg
	 * @param int $line
	 * @return \Illuminate\Http\JsonResponse
	 * @Description return json response error with default error code
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonDefaultError($errorMsg, $line = 0)
	{

		return response()->json([
			'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
			'message' => $errorMsg,
			'line' => $line,
		], Response::HTTP_INTERNAL_SERVER_ERROR);

	}

	/**
	 * @param     $errorMsg
	 * @param int $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @Description return json response error on try-> catch exception
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonCatchException($errorMsg, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR)
	{
		//if debug mode == true show message exception, else show default message
		if (Helper::debugMode()) {
			return response()->json([
				'status' => $statusCode,
				'message' => $errorMsg,
			], $statusCode);
		}

		return response()->json([
			'status' => $statusCode,
			'message' => \Lang::get('error.default_error'),
		], $statusCode);
	}

	/**
	 * @param \Illuminate\Validation\Validator $validator
	 * @param int                              $statusCode
	 * @return \Illuminate\Http\JsonResponse
	 * @Author DuyLBP
	 * @Date 2018-06-27
	 */
	protected function jsonValidateError(Validator $validator, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR)
	{
		$message = implode("\n", array_map(function ($entry) {
			return implode("\n", $entry);
		}, $validator->errors()->messages()));

		return response()->json([
			'status' => $statusCode,
			'message' => $message,
		], $statusCode);

	}
}
