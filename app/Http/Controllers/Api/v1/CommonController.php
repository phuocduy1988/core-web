<?php
/**
 * Created by: DuyLBP
 * Email: duy@onetech.vn
 * Date: 2018-06-27
 * Time: 16:31
 * File: CommonController.php
 */
namespace App\Http\Controllers\Api\v1;
/**
 * Class CommonController
 * @package App\Http\Controllers\Api\v1
 * @Author: DuyLBP
 * @Date: 2018-03-02
 */
class CommonController extends ApiBaseController
{
	/**
	 * CommonController constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * @return \Illuminate\Http\JsonResponse
	 * Author: DuyLBP
	 */
	public function startup()
	{
		//return common config info
		return $this->jsonOK([
			'member'  => $this->user,
			'setting' => $this->traitGetAllSetting()
		]);
	}

//	/**
//	 * @return array
//	 * Author: DuyLBP
//	 * Get info for table settings
//	 */
//	private function getSettingStartUp()
//	{
//		$traitSetting = $this->traitGetAllSetting();
//		return [
//			'maintenance_flag' => $this->traitGetSetting('maintenance_flag', 0, $traitSetting),
//			'force_update_flag' => $this->traitGetSetting('force_update_flag', 0, $traitSetting),
//			'force_update_target_version' => $this->traitGetSetting('force_update_target_version', '', $traitSetting),
//			'new_version_flag' => $this->traitGetSetting('new_version_flag', 0, $traitSetting),
//			'new_version_url' => 'https://itunes.apple.com/us/app/id1359592562?ls=1&mt=8',
//			'data_url' => $this->traitGetSetting('data_url_ios', '', $traitSetting),
//			'data_version' => $this->traitGetSetting('data_version', '', $traitSetting),
//			"ads" => "{\"admob\":{\"app_id\":\"ca-app-pub-2275814798528907~1062508658\",\"ads\":[{\"unit_id\":\"ca-app-pub-2275814798528907/2539241858\",\"unit_name\":\"banner\"},{\"unit_id\":\"ca-app-pub-2275814798528907/6216076062\",\"unit_name\":\"interstitial\",\"frequency\":0.5,\"delay_time\":1}]}}",
//		];
//	}
//	/**
//	 * @return array
//	 * Author: DuyLBP
//	 * Get info for table settings
//	 */
//	private function getSettingAndroidStartUp()
//	{
//		$traitSetting = $this->traitGetAllSetting();
//		return [
//			'maintenance_flag' => $this->traitGetSetting('maintenance_flag', 0, $traitSetting),
//			'force_update_flag' => $this->traitGetSetting('force_update_flag', 0, $traitSetting),
//			'force_update_target_version' => $this->traitGetSetting('force_update_target_version', '', $traitSetting),
//			'new_version_flag' => $this->traitGetSetting('new_version_flag', 0, $traitSetting),
//			'new_version_url' => 'https://play.google.com/store/apps/details?id=jp.onetech.nihonmannar',
//			'data_url' => $this->traitGetSetting('data_url_android', '', $traitSetting),
//			'data_version' => $this->traitGetSetting('data_version', '', $traitSetting),
//			"ads" => "{\"admob\":{\"app_id\":\"ca-app-pub-2275814798528907~1062508658\",\"ads\":[{\"unit_id\":\"ca-app-pub-2275814798528907/1949984843\",\"unit_name\":\"banner\"},{\"unit_id\":\"ca-app-pub-2275814798528907/9272272795\",\"unit_name\":\"interstitial\",\"frequency\":0.5,\"delay_time\":1}]}}",
//		];
//	}
}
