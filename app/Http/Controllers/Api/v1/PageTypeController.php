<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2019-05-07
 * Time: 18:19:09
 * File: PageTypeController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\PageType;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\PageTypeRequest;
use App\Http\Resources\PageTypeCollection;
use App\Http\Resources\PageType as PageTypeResource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class PageTypeController extends ApiBaseController
{
    /**
     * Display a listing of the pageTypes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $pageTypes = PageType::query();
            //Generate search option here
            if ($request->has('keyword')) {
                //Search via param key word
                $pageTypes = $pageTypes->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Get data with pagination and order by(Default:: id desc)
            $pageTypes = $pageTypes
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($pageTypes);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created pageTypes in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'name' => ['nullable', 'max:191'],
			'status' => ['nullable', 'boolean'],


                //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('name', '').$request->get('status', '').
                //--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new PageType
            $pageType = new PageType();
            $pageType->name = $request->get('name');
			$pageType->status = $request->get('status', 0);

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $pageType->save();

            //Check response data
            $responseCode = is_null($pageType->id) ? 201 : 200;
            return $this->jsonOK(['data' => $pageType], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update pageTypes in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('name', '').$request->get('status', '').
                //--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $pageType = PageType::find($id);
            if(is_null($pageType)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('name')) {
				$pageType->name = $request->get('name');
			}
			if ($request->get('status')) {
				$pageType->status = $request->get('status');
			}


            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $pageType->save();

            return $this->jsonOK(['data' => $pageType]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified pageTypes from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $pageType = PageType::find($id);
            if(!is_null($pageType)) {
                $pageType->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $pageType = PageType::withTrashed()->find($id);
            if(!is_null($pageType)) {
                $pageType->restore();
                return $this->jsonOK(['data' => $pageType]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $pageType = PageType::withTrashed()->find($id);
            if(!is_null($pageType)) {
                $pageType->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
