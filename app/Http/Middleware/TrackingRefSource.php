<?php

namespace App\Http\Middleware;

use App\Models\RefUrl;
use Closure;

class TrackingRefSource
{
    public function __construct()
    {

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$ref = $request->get('ref', '');
    	if(strlen($ref) > 0) {
			$refUrl = RefUrl::where('id', $ref)->first();
			if(!is_null($refUrl)) {
				//Count traffic
				$refUrl->traffic_count = $refUrl->traffic_count + 1;
				$refUrl->save();
				//Add to session
				session([refUrlKey() => $ref]);
			}
		}
        return $next($request);
    }
}
