<?php

namespace App\Http\Middleware;

use App\Helpers\Helper;
use Illuminate\Contracts\Auth\Guard;
use Closure;
use Illuminate\Support\Facades\Auth;

class FrontendAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {
        $this->auth = Auth::guard('frontend');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
            	if(str_contains($request->getUri(), '/startExam')) {
		            $url = asset('/thanh-vien/danh-sach-bai-thi/bai-thi/'. $request->route('examination'));
            		Helper::setSession('bai-thi', $url);
	            }
                return redirect()->guest(route('vn.login'));
            }
        }
	    return $next($request);
    }
}
