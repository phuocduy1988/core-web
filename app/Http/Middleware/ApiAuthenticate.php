<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class ApiAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {
		$this->auth = feAuth();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If middleware is api
        if ($this->auth->guest()) {
            $response =  \Response::json([
                'status'  => 401,
                'message'   => trans('error.client_login_failed'.'!!'),
                'line' =>  'Midleware'
            ]);

            $response->send();
            die();
        }

        return $next($request);
    }
}
