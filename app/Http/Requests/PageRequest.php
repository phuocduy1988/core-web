<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-08-15
 * Time: 18:23:23
 * File: PageRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:70'],

			'slug' => ['required', 'max:191', Rule::unique('pages')->ignore($this->get('id'))],
			'description' => ['required','min:20','max:300'],
			'content' => ['required'],
			'status' => ['nullable', 'boolean'],

			'lang' => ['required', 'max:191'],

			'is_html' => ['nullable', 'boolean'],

			'meta_title' => ['nullable'],
			'meta_description' => ['nullable'],
			'rating' => ['nullable', 'max:191'],
			'comment' => ['nullable'],

			'schema' => ['nullable'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => trans('validation.required', ['attribute' => trans('pages.title')]),
			
			'slug.required' => trans('validation.required', ['attribute' => trans('pages.slug')]),
						'description.required' => trans('validation.required', ['attribute' => trans('pages.description')]),
						'content.required' => trans('validation.required', ['attribute' => trans('pages.content')]),
			
			'lang.required' => trans('validation.required', ['attribute' => trans('pages.lang')]),
			



            //--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }
}
