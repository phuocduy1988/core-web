<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2019-05-07
 * Time: 18:19:09
 * File: PageTypeRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PageTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['nullable', 'max:191'],
			'status' => ['nullable', 'boolean'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            
            //--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }
}
