<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-23
 * Time: 16:45:03
 * File: MemberRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:191'],
            'last_name' => ['required', 'max:191'],
			'email' => ['required', 'email', 'max:191', 'unique:members,email,'. $this->get('id') .',id'],
			'password' => ['nullable', 'max:191'],
			'status' => ['nullable', 'boolean'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => trans('validation.required', ['attribute' => trans('members.name')]),
			'email.required' => trans('validation.required', ['attribute' => trans('members.email')]),
			'password.required' => trans('validation.required', ['attribute' => trans('members.password')]),

            //--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }
}
