/*------------------ google map START --------------------*/
emptyAddMsg = "住所を入力してください。";
errorAddMsg = "入力された住所から緯度・経度を取得できません。";
markersArray = [];

/**
 * Clear marker
 * @author duylbp
 * @date   2017-09-28
 * @return {[type]}   [description]
 */
function clearOverlays() {
    for (var i = 0; i < markersArray.length; i++ ) {
        markersArray[i].setMap(null);
    }
    markersArray.length = 0;
}

/**
 * Initial google map
 * @author duylbp
 * @date   2017-09-28
 * @return {[type]}   [description]
 */
function initMap() {
    map = new google.maps.Map(document.getElementById('js-map'), {
        zoom: parseInt(iniZoom),
        center: {
            lat: iniLatitude,
            lng: iniLongitude
        }
    });
    geocoder = new google.maps.Geocoder();
    var myCenter=new google.maps.LatLng(iniLatitude, iniLongitude);
    // Create marker
	var marker=new google.maps.Marker({
		position:myCenter,
	});
	marker.setMap(map);
	markersArray.push(marker);
    // Show current zoom value
    $('#js-map-zoom').val(map.getZoom());

    // autoLoadMap(map);

    // Search address button event
    document.getElementById('js-map-submit').addEventListener('click', function() {
        clearErrorMessage();
        var address = $('#js-map-address').val();
        address = address.trim();
        if(address == '') {
            // Show error message
            showErrorMessage(emptyAddMsg);
        }
        else {
            // Remove marker
            clearOverlays();
            // Load map with address
            geocodeAddress(geocoder, map);
        }
    });

    // Change address event
    document.getElementById('js-map-address').addEventListener('change', function() {
        $('#js-map-submit').trigger('click');
    });

    // Mouse click event in map
    google.maps.event.addListener(map, 'click', function (event) {
        var latitude = event.latLng.lat();
        var longitude = event.latLng.lng();
        $('#js-map-latitude').val(latitude);
        $('#js-map-longitude').val(longitude);
        clearErrorMessage();
        setMapByLocation(map);
    });

    // init load map when change param
    loadMapByChangeInput();
    // init zoom change
    zoomChange();
    // Clear error message
    clearErrorMessage();
}

function clearErrorMessage() {
    $('#js-map-message').addClass('hide');
    $('#js-map-message').empty();
}

function showErrorMessage(message) {
    $('#js-map-message').removeClass('hide');
    $('#js-map-message').empty();
    $('#js-map-message').text(message);
}

/**
 * Load map data with address
 * @author duylbp
 * @date   2017-09-28
 * @param  {[type]}   geocoder   [description]
 * @param  {[type]}   resultsMap [description]
 * @return {[type]}              [description]
 */
function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('js-map-address').value;
    address = address.trim();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });

            if(results[0].geometry.viewport)
                resultsMap.fitBounds(results[0].geometry.viewport);

            markersArray.push(marker);

            $('#js-map-latitude').val(results[0].geometry.location.lat());
            $('#js-map-longitude').val(results[0].geometry.location.lng());
            $('#js-map-zoom').val(resultsMap.getZoom());
        } else if(status == 'ZERO_RESULTS') {
            showErrorMessage(errorAddMsg);
        }
    });
}

/**
 * Load map with location
 * @author duylbp
 * @date   2017-09-28
 * @param  {[type]}   map [description]
 */
function setMapByLocation(map){
    var latitude = $('#js-map-latitude').val();
    var longitude = $('#js-map-longitude').val();

    clearOverlays(); // remove marker

    var myCenter=new google.maps.LatLng(latitude,longitude);
    var marker=new google.maps.Marker({
        position:myCenter,
    });
    marker.setMap(map);

    markersArray.push(marker);
    // Center of map
    map.panTo(new google.maps.LatLng(latitude,longitude));

    //set address
    geocoder.geocode({
        'latLng': myCenter
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                // Set data
                $('#js-map-address').val(results[0].formatted_address);
            }
        }
    });
}

/**
 * Auto load map
 * @author duylbp
 * @date   2017-09-28
 * @param  {[type]}   map [description]
 * @return {[type]}       [description]
 */
function autoLoadMap(map){
    var address = document.getElementById('js-map-address').value;
    address = address.trim();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            if(results[0].geometry.viewport)
                map.fitBounds(results[0].geometry.viewport);

            markersArray.push(marker);

            $('#js-map-latitude').val(results[0].geometry.location.lat());
            $('#js-map-longitude').val(results[0].geometry.location.lng());
            $('#js-map-zoom').val(map.getZoom());
        } else {
            var latitude = $("#js-map-latitude").val();
            var longitude = $("#js-map-longitude").val();

            clearOverlays(); //remove marker

            var myCenter=new google.maps.LatLng(latitude,longitude);
            var marker=new google.maps.Marker({
                position:myCenter,
            });
            marker.setMap(map);

            markersArray.push(marker);
            // Center of map
            map.panTo(new google.maps.LatLng(latitude,longitude));

            //set address
            geocoder.geocode({
                'latLng': myCenter
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        // Set zoom
                        if(results[0].geometry.viewport) {
                            map.fitBounds(results[0].geometry.viewport);
                        }

                        // Set data
                        $('#js-map-address').val(results[0].formatted_address);
                    }
                }else{
                    // showErrorMessage(errorAddMsg);
                }
            });
        }
    });
}

/**
 * Load map when change param: latitude, longitude, zoom
 * @author duylbp
 * @date   2017-09-28
 * @return {[type]}   [description]
 */
function loadMapByChangeInput() {
    $('#js-map-latitude, #js-map-longitude, #js-map-zoom').blur(function() {
		var iniLat = $('#js-map-latitude').val(),
			iniLong = $('#js-map-longitude').val();
			iniZoom = $('#js-map-zoom').val().length == 0 ? 10 : $('#js-map-zoom').val();

		clearOverlays(); //remove marker

		var latLng = new google.maps.LatLng(iniLat, iniLong);
		var marker = new google.maps.Marker({
			position: latLng,
		});
        // Setting map
		marker.setMap(map);
		markersArray.push(marker);
		// Center of map
		map.panTo(latLng);
        // Set map zoom
		map.setZoom(parseInt(iniZoom));
		// Set map address
		geocoder.geocode({
			'latLng': latLng
		}, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[1]) {
					$('#js-map-address').val(results[1].formatted_address);
				} else {
					// No data
				}
			} else {
				// console.log('Geocoder failed due to: ' + status);
			}
		});
	});
}

/**
 * zoom change when use ctrl + zoom
 * @author duylbp
 * @date   2017-09-28
 * @return {[type]}   [description]
 */
function zoomChange() {
	google.maps.event.addListener(map, 'zoom_changed', function() {
		$('#js-map-zoom').val(map.getZoom());
	});
}

/**
 * Reset map to default value
 * @author duylbp
 * @date   2017-09-28
 * @return {[type]}   [description]
 */
$('#js-map-reset').on('click', function(){
	$('#js-map-latitude').val('');
	$('#js-map-longitude').val('');
	$('#js-map-zoom').val(10);
	initMap();
});

$(document).ready(function() {
    initMap();
});

/*-------------------- google map END --------------------*/
