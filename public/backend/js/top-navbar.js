$(document).ready(function () {
	// prevent dropdown hide
	// $(document).on('click', '.dropdown-alerts', function (e) {
	// 	e.stopPropagation();
	// });

	$(document).on('click', '.js-notification-del', function() {
		// $('.loader-handle').html("<div class='loader'></div>");

		// Call Ajax
		var content    = $(this).parent().parent();
		var notification_id = $(this).attr('id-value');
		$('.keep-id-value').append(notification_id+', ');

		content.fadeOut('fast').queue(function() {
			$('#js-notification-count').text($('#js-notification-count').text()-1);
		});

	});

	// Delete Notify và Cập nhật lại dữ liệu
	$(this).on('click', function() {
		if(!$('#js-notification-block').hasClass('open'))
		{
			var notification_id = $('.keep-id-value').text();
			var _token = $('#notice_token').val();

			if(notification_id)
			{
				$.ajax({
					type: "POST",
					url: '/fe/client/notification/delete',
					data:
					{
						notification_id: notification_id,
						_token: _token,
					},
					success: function(result)
					{
						if(result == 1)
						{
							// alert('ok');
						}
						else
						{
							// alert('fail');
						}
					}
				});
			}
		}
	});

	$(window).on('beforeunload', function() {
		var notification_id = $('.keep-id-value').text();
		var _token = $('#notice_token').val();

		if(notification_id)
		{
			$.ajax({
				type: "POST",
				url: '/fe/client/notification/delete',
				data:
				{
					notification_id: notification_id,
					_token: _token,
				},
				success: function(result)
				{
					if(result == 1)
					{
						// alert('ok');
					}
					else
					{
						// alert('fail');
					}
				}
			});
		}
	});
	// Delete Notify và Cập nhật lại dữ liệu

	// AJAX LOAD MORE NOTIFY
	$(document).on('click', '.loadmore', function(){

		$('.show-load').addClass('loader-mini');
		$('.loadmore').hide();
		var _token = $('#notice_token').val();

		$.ajax({
			method: "POST",
			url: '/fe/load-more-notice',
			data:
			{
				_token: _token,
				// _token: '{{ csrf_token() }}',
			},
			success: function(result)
			{
				// console.log(result);
				if(result.indexOf("なし") !== 67)
				{
					$('.show-load').removeClass('loader-mini');
					$('.loadmore').show();
					$('li.position-notice').last().after(result);
				}
				else
				{
					$('.show-load').removeClass('loader-mini');
					$('li.position-notice').last().after(result);
				}
			}
		});
	});
	// AJAX LOAD MORE NOTIFY

	});
