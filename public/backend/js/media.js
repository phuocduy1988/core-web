function mediaAppendError(message, _this='.ibox-content') {
    $(_this).find('.alert').remove();
    $(_this).prepend('<div class="alert alert-danger validation-error">' + message + '</div>');
	$('html, body').animate({
		scrollTop: $("#page-wrapper").offset().top
	}, 800);
}

/*
	Function: image file type validation
	Author: duylbp
	Date: 2017-09-14
*/
function imageValidation(filePath) {

	//remove check validation
    return true;

	// var fileExtension = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx'];
	// if ($.inArray(filePath.split(".").pop().toLowerCase(), fileExtension) == -1) {
	// 	var message = '画像ファイルは、JPEG、PNGのいずれかのファイルを選択してください。';
	// 	mediaAppendError(message);
	// 	return false;
	// } else {
	// 	return true;
	// }
}

/*
	Function: image file size validation
	Author: duylbp
	Date: 2017-09-14
*/
function imageCheckFileSize(filePath, callback) {
	var xhr = new XMLHttpRequest();
	xhr.open("HEAD", filePath, true); // Notice "HEAD" instead of "GET",
	//  to get only the header
	xhr.onreadystatechange = function() {
		if (this.readyState == this.DONE) {
			callback(parseInt(xhr.getResponseHeader("Content-Length")));
		}
	};
	xhr.send();
}

/*
	Function: image check when load page
	Author: duylbp
	Date: 2017-09-14
*/
function imageLoad() {
	$(".js-img-change").each(function( index ) {
		if ($(this).attr("src") != "") {
			$(this).closest('.media-box').find('.js-media-delete').show();
			$(this).closest('.media-box').find('.popup_selector').hide();
			$(this).closest('.media-box').find('.select-img-desc').hide();
		}
        else {
			$(this).closest('.media-box').find('.js-media-delete').hide();
		}
	});
}

/**
 * Show preview audio when load page
 * @author duylbp
 * @date   2017-10-17
 * @return
 */
function audioLoad() {
    // Check all audio class
	$(".js-audio-preview").each(function( index ) {
        // If this has url path
		if ($(this).attr("href") != "#") {
            // Show audio name
            $(this).text(getMediaName($(this).attr('href')));
            // Show button delete and hide input button
			$(this).closest('.media-box').find('.js-media-delete').show();
			$(this).closest('.media-box').find('.popup_selector').hide();
			$(this).closest('.media-box').find('.select-img-desc').hide();
		}
        else {
			$(this).closest('.media-box').find('.js-media-delete').hide();
		}
    });
}

/*
	Function: movie check when load page
	Author: duylbp
	Date: 2017-09-14
*/
function movieLoad() {
	$(".js-movie-change").each(function( index ) {
		if ($(this).attr("href") != "") {
			$(this).closest('.media-box').find('.js-movie-delete').show();
			$(this).closest('.media-box').find('.popup_selector').hide();
			$(this).closest('.media-box').find('.select-img-desc').hide();
		}
        else {
			$(this).closest('.media-box').find('.js-movie-delete').hide();
		}
	});
}

/*
	Function: audio file type validation
	Author: duylbp
	Date: 2017-10-16
*/
function audioValidation(filePath) {
	var fileExtension = ['mp3'];
	if ($.inArray(filePath.split(".").pop().toLowerCase(), fileExtension) == -1) {
		var message = 'ボイスデータは、mp3のいずれかのファイルを選択してください。';
		mediaAppendError(message);
		return false;
	} else {
		return true;
	}
}

function videoValidation(filePath) {
	var fileExtension = ['mp4', 'MP4'];
	if ($.inArray(filePath.split('.').pop().toLowerCase(), fileExtension) == -1) {
		var message = '動画ファイルは、MP4のファイルを選択してください。';
		mediaAppendError(message);
		return false;
	} else {
		return true;
	}
}

function videoCheckFileSize(filePath, callback) {
	var xhr = new XMLHttpRequest();
	xhr.open("HEAD", filePath, true); // Notice "HEAD" instead of "GET",
	//  to get only the header
	xhr.onreadystatechange = function() {
		if (this.readyState == this.DONE) {
			callback(parseInt(xhr.getResponseHeader("Content-Length")));
		}
	};
	xhr.send();
}

function getMediaName(url) {
	return url.match(/[-_\w]+[.][\w]+$/i)[0];
}

$(document).ready(function() {
    // Use for upload image
	$(".media-change").on("change", function (argument) {
		var filePath = $(this).closest(".media-box").find(".media-change").attr("value");
		var target = $(this);
		if(!imageValidation(filePath)) {
			$(this).closest(".media-box").find(".media-change").attr("value", "");
			return false;
		}

		imageCheckFileSize(filePath, function (size) {
			var limitSize = 10485760; // 1MB
			if(size > limitSize) {
				// check file size fail
				var message = 'ファイルサイズが大きすぎます';
				mediaAppendError(message);
				return false;
			}
			else {
				$(".ibox-content").find('.alert').remove();
				target.closest(".media-box").find("#js-img-preview").addClass("img-preview");
				target.closest(".media-box").find("#js-img-preview >img").attr("src", filePath);
				target.closest(".media-box").find("#js-img-preview >img").addClass("display");
				target.closest(".media-box").find("#js-img-preview >img").removeClass("hide");

				target.closest(".media-box").find(".js-media-delete").show();
				target.closest(".media-box").find(".popup_selector").hide();
				target.closest(".media-box").find(".select-img-desc").hide();
			}
		});
	});

    // Use for upload audio
    $(".audio-change").on("change", function (argument) {
		var filePath = $(this).closest(".media-box").find(".audio-change").attr("value");
		var target = $(this);
        // Validate audio type
		if(!audioValidation(filePath)) {
			$(this).closest(".media-box").find(".audio-change").attr("value", "");
			return false;
		}
        // Check audio file size
		imageCheckFileSize(filePath, function (size) {
			var limitSize = 3145728; // 3MB
			if(size > limitSize) {
				// check file size fail
				var message = 'ファイルサイズが大きすぎます';
				mediaAppendError(message);
				return false;
			}
			else {
				$(".ibox-content").find('.alert').remove();
                // Load audio preview
				target.closest(".media-box").find(".js-audio-preview").attr("href", filePath);
				target.closest(".media-box").find(".js-audio-preview").addClass("audio-preview");
				target.closest(".media-box").find(".js-audio-preview").text(getMediaName(filePath));
                // Show delete button and hide description
				target.closest(".media-box").find(".js-media-delete").show();
				target.closest(".media-box").find(".popup_selector").hide();
				target.closest(".media-box").find(".select-img-desc").hide();
			}
		});
	});

	/*
		Function: media delete
		Author: duylbp
		Date: 2017-09-14
	*/
	$(".js-media-delete").on('click', function(e){
		$(this).closest('.media-box').find(".popup_selector").show();
		$(this).closest('.media-box').find(".select-img-desc").show();
        // Delete with audio case
		$(this).closest('.media-box').find(".js-audio-preview").text("");
		$(this).closest('.media-box').find(".js-audio-preview").attr("href", "");
		$(this).closest('.media-box').find(".audio-change").attr("value", "");
		$(this).closest('.media-box').find(".js-audio-preview").removeClass("audio-preview");
        // Delete with image case
		$(this).closest('.media-box').find("#js-img-preview").removeClass("img-preview");
		$(this).closest('.media-box').find(".media-change").attr("value", "");
		$(this).closest('.media-box').find("#js-img-preview > img").attr("src", "");
		$(this).closest('.media-box').find("#js-img-preview > img").removeClass("display");
		$(this).closest('.media-box').find("#js-img-preview > img").addClass("hide");
		$(this).hide();
	});

	// auto hide button choose image when image existed
	imageLoad();
	audioLoad();

	/*
		Function: movie change
		Author: duylbp
		Date: 2017-09-14
	*/
	$(".js-movie-change").on("change", function(e){
		e.preventDefault();
		var filePath = $(this).val();
		var target = $(this);
		/* media validation */
		if(!videoValidation(filePath)) {
			return false;
		}
		videoCheckFileSize(filePath, function (size) {
			var limitSize = 31457280; // 30MB
			if(size > limitSize) {
				// check file size fail
				var message = 'ファイルサイズが大きすぎます';
				mediaAppendError(message);
				return false;
			}
			else {
				target.closest(".media-box").find(".movie-name").attr('href', filePath);;
				target.closest(".media-box").find(".movie-name").text(getMediaName(filePath));
				target.closest(".media-box").find(".js-movie-delete").show();
				target.closest(".media-box").find(".popup_selector").hide();
				target.closest(".media-box").find(".select-img-desc").hide();
				$(".ibox-content").find('.alert').remove();
			}
		});
	});

	// movie remove
	$(".js-movie-delete").on('click', function(e){
		$(this).closest('.media-box').find(".popup_selector").show();
		$(this).closest('.media-box').find(".select-img-desc").show();
		$(this).closest(".media-box").find(".movie-name").attr('href', "");
		$(this).closest(".media-box").find(".movie-name").text("");
		$(this).closest(".media-box").find(".js-movie-change").attr('value', "");
		$(this).hide();
	});
});
