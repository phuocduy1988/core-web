.stitches-sprite(@x: 0, @y: 0, @width: 0, @height: 0) {
	background-position: @x @y;
	width: @width;
	height: @height;
}

.sprite {
	background-image: url(spritesheet.png);    background-repeat: no-repeat;
	display: block;

	&.sprite-betonamu {
		.stitches-sprite(-10px, -10px, 32px, 20px);
	}

	&.sprite-blog-icon {
		.stitches-sprite(-10px, -50px, 26px, 25px);
	}

	&.sprite-contact-phone-icon {
		.stitches-sprite(-10px, -95px, 30px, 30px);
	}

	&.sprite-facebook-icon-sidebar {
		.stitches-sprite(-10px, -145px, 48px, 48px);
	}

	&.sprite-google-icon-sidebar {
		.stitches-sprite(-10px, -213px, 48px, 48px);
	}

	&.sprite-icon-blog-32 {
		.stitches-sprite(-10px, -281px, 32px, 31px);
	}

	&.sprite-icon-office-32 {
		.stitches-sprite(-10px, -332px, 32px, 26px);
	}

	&.sprite-icon-staff-32 {
		.stitches-sprite(-10px, -378px, 32px, 21px);
	}

	&.sprite-icon-work {
		.stitches-sprite(-10px, -419px, 32px, 26px);
	}

	&.sprite-mail-icon-sidebar {
		.stitches-sprite(-10px, -465px, 48px, 48px);
	}

	&.sprite-next-icon {
		.stitches-sprite(-10px, -533px, 16px, 16px);
	}

	&.sprite-nihon {
		.stitches-sprite(-10px, -569px, 32px, 20px);
	}

	&.sprite-profile-icon {
		.stitches-sprite(-10px, -609px, 25px, 20px);
	}

	&.sprite-screen-icon {
		.stitches-sprite(-10px, -649px, 36px, 30px);
	}

	&.sprite-scroll-top-icon {
		.stitches-sprite(-10px, -699px, 32px, 32px);
	}

	&.sprite-service-icon-white {
		.stitches-sprite(-10px, -751px, 56px, 56px);
	}

	&.sprite-skype-icon-sidebar {
		.stitches-sprite(-10px, -827px, 48px, 48px);
	}
}
