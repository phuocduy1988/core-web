$(document).ready(function() {
    var itemBorder = 0

    function resetWaterLayout($step, itemBorder) {
        $heightMax = 0;
        $(".js-water-list").children().each(function(idx) {
            if (idx % $step < ($step - 1)) {
                $heightMax = Math.max($(this).height(), $heightMax);
            } else if (idx % $step === ($step - 1)) {
                for (var i = 0; i < $step; i++) {
                    $(".js-water-list").children().eq(idx - i).css({
                        "height": Math.max($(this).height(), $heightMax) + (itemBorder * 2) + "px"
                    }); // 6 = 3 + 3 = border
                }
                $heightMax = 0;
            }
        });
    }

    window.resetHeight = function() {
        $(".js-water-list").children().css({
            "height": "auto"
        });
        if ($(window).width() >= 993) {
            resetWaterLayout(4, itemBorder);
        } else if ($(window).width() >= 380 && $(window).width() <= 992) {
            resetWaterLayout(4, itemBorder);
        } else {
            $(".js-water-list").children().css({
                "height": "auto"
            });
        }
    }
    $(window).on("load", resetHeight);
    $(window).on("orientationchange", resetHeight);
    $(window).on("resize", resetHeight);
});