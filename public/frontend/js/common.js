$(document).ready(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 86) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
        /*
        if($(window).width() >= 768 && $(this).scrollTop() > 110) {
            $('.t-fixed .navbar-header').css('display', 'none');
        }
        else if($(window).width() >= 768 && $(this).scrollTop() < 110) {
            $('.t-fixed .navbar-header').css('display', 'table');
        }
        else {
            $('.t-fixed .navbar-header').css('display', 'block');
        }
         */
    });

    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
    
    $('.dropdown').hover(function() {
        if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
            if(!$(this).hasClass('open')) { // Keeps it open when hover it again
                $('.dropdown-toggle', this).trigger('click');
            }
        }
    }, function(){
        $(this).removeClass('open');
    });
	
	$('.js-social-sidebar-container').fadeIn();
	// var flagShowSlidebar = false;
	// $(window).scroll(function(){
	// 	if($(window).width() >= 768) {
	// 		if ($(this).scrollTop() > 128) {
	// 			if (flagShowSlidebar == false)
	// 			{
	// 				flagShowSlidebar = true;
	// 				$('.js-social-sidebar-container').fadeIn();
	// 			}
	// 		} else {
	// 			flagShowSlidebar = false;
	// 			$('.js-social-sidebar-container').fadeOut();
	// 		}
	// 	} else {
	// 		// TODO
	// 	}
	// });
});
function LoadHeader() {
	/*
    if($(window).width() >= 768 && $(window).scrollTop() > 86) {
        $('.t-fixed .navbar-header').css('display', 'none');
    }
    else if($(window).width() >= 768 && $(window).scrollTop() < 86) {
        $('.t-fixed .navbar-header').css('display', 'table');
    }
    else {
        $('.t-fixed .navbar-header').css('display', 'block');
    }
	 */
}

// You can use $(function(){stringToSlug();});
var stringToSlug = function(str, ext = '-') {
	if(!str) {
		return str;
	}
	str = str.replace(/^\s+|\s+$/g, ''); // trim
	str = str.toLowerCase();
	
	// remove accents, swap ñ for n, etc
	// var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
	// var to   = "aaaaeeeeiiiioooouuuunc------";
	var from = "ÁÀẢÃẠÄĂẮẰẲẴẶÂẤẦẬẨẪÃÅČÇĆĎÉĚËÈẺẼẸÊẾỀỂỄỆĔȆĞỊÍÌỈĨÎÏİŇÑÓÖÒỎÕỌÔỐỒỔỖỘƠỚỜỢỞỠØŘŔŠŞŤỤỦŨÚŮÜÙÛƯỰỨỪỬỮỴỶỸÝŸŽáàảãạäăắằặẳẵâấầậẩẫåčçćďẻẽẹếềệểễéěëèêĕȇğịíìỉĩîïıňñóöòỏõọôốồổỗộơớờợởỡøðřŕšşťụủũúůüùûưựứừửữỵỷỹýÿžþÞĐđßÆa·/_,:;";
	var to   = "AAAAAAAAAAAAAAAAAAAACCCDEEEEEEEEEEEEEEEGIIIIIIIINNOOOOOOOOOOOOOOOOOOORRSSTUUUUUUUUUUUUUUYYYYYZaaaaaaaaaaaaaaaaaaacccdeeeeeeeeeeeeeeegiiiiiiiinnoooooooooooooooooooorrsstuuuuuuuuuuuuuuyyyyyzbBDdBAa------";
	for (var i=0, l=from.length ; i<l ; i++) {
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	
	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		.replace(/\s+/g, ext) // collapse whitespace and replace by -
		.replace(/-+/g, ext); // collapse dashes
	
	return str;
};
$(window).on("load", LoadHeader);
$(window).on("orientationchange", LoadHeader);
$(window).on("resize", LoadHeader);