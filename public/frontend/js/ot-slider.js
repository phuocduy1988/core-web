$(document).ready(function() {
    $wSlider = $(".ot-container").width();
    $maxSl = 4;
    $mrSl = 24;
    $lrSl = 64;

    $maxSlImages = 3;
    $mrSlImages = 12;

    $maxSlKH = 3;

    function reloadRecruitSlider($maxSlides, $mr, $lr, $widthSlider) {
        // if (typeof($recruitSlider) != 'undefined') {
        //     $recruitSlider.reloadSlider({
        //         minSlides: 1,
        //         maxSlides: $maxSlides,
        //         moveSlides: 1,
        //         slideWidth: ($widthSlider - $lr - (($maxSlides - 1) * $mr)) / $maxSlides,
        //         slideMargin: $mr,
        //         autoHover: true,
        //         auto: true,
        //         pause: 3000,
        //         controls: false,
        //     });
        // } else {
        //     $recruitSlider = $(".js-recruit-slider").bxSlider({
        //         minSlides: 1,
        //         maxSlides: $maxSlides,
        //         moveSlides: 1,
        //         slideWidth: ($widthSlider - $lr - (($maxSlides - 1) * $mr)) / $maxSlides,
        //         slideMargin: $mr,
        //         autoHover: true,
        //         auto: true,
        //         pause: 3000,
        //     });
        // }
    }

    function reloadPublicImageSlider($maxSlImages, $mrSlImages, $lr, $widthSlider) {
        if (typeof($imagesSlider) != 'undefined' && typeof $imagesSlider.reloadSlider === "function") {
            $imagesSlider.reloadSlider({
                minSlides: 1,
                maxSlides: $maxSlImages,
                moveSlides: 1,
                slideWidth: ($widthSlider - $lr - (($maxSlImages - 1) * $mrSlImages)) / $maxSlImages,
                slideMargin: $mrSlImages,
                autoHover: true,
                auto: true,
                pause: 3000,
                controls: false,
            });
        } else {
            if (typeof $(".js-recruit-slider").bxSlider === "function") {
                $imagesSlider = $(".js-recruit-slider").bxSlider({
                    minSlides: 1,
                    maxSlides: $maxSlImages,
                    moveSlides: 1,
                    slideWidth: ($widthSlider - $lr - (($maxSlImages - 1) * $mrSlImages)) / $maxSlImages,
                    slideMargin: $mrSlImages,
                    autoHover: true,
                    auto: true,
                    pause: 3000,
                });
            }
        }
    }

    function reloadKHSlider($maxSlKH, $mrSl, $lr, $widthSlider) {
        // if (typeof($khSlider) != 'undefined') {
        //     $khSlider.reloadSlider({
        //         minSlides: 1,
        //         maxSlides: $maxSlKH,
        //         moveSlides: 1,
        //         slideWidth: ($widthSlider - $lr - (($maxSlKH - 1) * $mrSl)) / $maxSlKH,
        //         slideMargin: $mrSl,
        //         autoHover: true,
        //         auto: true,
        //         pause: 3000,
        //         controls: false,
        //         infiniteLoop: false,
        //     });
        // } else {
        //     $khSlider = $(".js-recruit-slider").bxSlider({
        //         minSlides: 1,
        //         maxSlides: $maxSlKH,
        //         moveSlides: 1,
        //         slideWidth: ($widthSlider - $lr - (($maxSlKH - 1) * $mrSl)) / $maxSlKH,
        //         slideMargin: $mrSl,
        //         autoHover: true,
        //         auto: true,
        //         pause: 3000,
        //         controls: false,
        //         infiniteLoop: false,
        //     });
        // }
    }

    function resetSlider() {
        $screenWidth = $(window).width();
        $widthSlider = $(".ot-container").width();
        if ($screenWidth <= (1024) && $screenWidth >= (768)) {
            reloadRecruitSlider(3, 16, 48, $widthSlider);
            reloadPublicImageSlider(3, 16, 48, $widthSlider);
            reloadKHSlider(3, 16, 48, $widthSlider);
        } else if ($screenWidth <= (767)) { // 15px === scroll width.
            if ($screenWidth <= (480)) {
                reloadRecruitSlider(1, 12, 48, $widthSlider);
                reloadPublicImageSlider(1, 12, 48, $widthSlider);
                reloadKHSlider(1, 12, 48, $widthSlider);
            } else {
                reloadRecruitSlider(2, 16, 48, $widthSlider);
                reloadPublicImageSlider(2, 16, 48, $widthSlider);
                reloadKHSlider(2, 16, 48, $widthSlider);
            }
        } else {
            reloadRecruitSlider(4, 24, 64, $widthSlider);
            reloadPublicImageSlider(3, 24, 64, $widthSlider);
            reloadKHSlider(3, 24, 64, $widthSlider);
        }
    }

    // if (typeof $(".js-public-images-slider").bxSlider === "function") {
    $imagesSlider = $('.js-public-images-slider').bxSlider({
        minSlides: 3,
        maxSlides: $maxSlImages,
        moveSlides: 1,
        slideWidth: ($wSlider - $lrSl - (($maxSlImages - 1) * $mrSlImages)) / $maxSlImages,
        slideMargin: $mrSlImages,
        autoHover: true,
        pause: 3000,
        controls: false,

        auto: true,
        adaptiveHeight: true,
        mode: 'fade',
    });
    // }


    // $recruitSlider = $(".js-recruit-slider").bxSlider({
    //     minSlides: 1,
    //     maxSlides: $maxSl,
    //     moveSlides: 1,
    //     slideWidth: ($wSlider - $lrSl - (($maxSl - 1) * $mrSl)) / $maxSl,
    //     slideMargin: $mrSl,
    //     autoHover: true,
    //     auto: true,
    //     pause: 3000,
    //     controls: false,
    // });

    // $khSlider = $(".js-kh-slider").bxSlider({
    //     minSlides: 1,
    //     maxSlides: $maxSlKH,
    //     moveSlides: 1,
    //     slideWidth: ($wSlider - $lrSl - (($maxSlKH - 1) * $mrSl)) / $maxSlKH,
    //     slideMargin: $mrSl,
    //     autoHover: true,
    //     auto: true,
    //     pause: 3000,
    //     controls: false,
    //     infiniteLoop: false,
    // });

    $(window).on("load", resetSlider);
    $(window).on("beforeunload", resetSlider);
    $(window).on("resize", resetSlider);
    $(window).on("orientationchange", resetSlider);
});