function getDataQuestion(url) {
        $.ajax({
            url: url + '/danh-sach-bai-thi',
            type: 'GET',
            data: {type: 'master'},
            success: function (res) {
                console.log(res);
                $.each(res, function (i, item) {
                    var i = Number(i) + Number(1);
                    // set title
                    var title = item.title;
                    /**
                     * @Question
                     */
                    var itemData = JSON.parse(item.data);
                    // debugger
                    var questionA = itemData.answer_a;
                    var questionB = itemData.answer_b;
                    var questionC = itemData.answer_c;
                    //
                    if (itemData.answer_to_right == 'answer_a') {
                        var answer_to_right = itemData.answer_a;
                    } else if (itemData.answer_to_right == 'answer_b') {
                        var answer_to_right = itemData.answer_b;
                    } else {
                        var answer_to_right = itemData.answer_c;
                    }
                    // get data
                    $(".javascript-html-data .one-tech-jbaa-box").removeAttr('class').attr('class', 'one-tech-jbaa-box inline-block');
                    var dataClass = "one-tech-jbaa-box-" + item.id;
                    $(".javascript-html-data .one-tech-jbaa-box").addClass(dataClass);
                    // get title
                    var strExplain = '';
                    if (item.explain) {
                        var strResult = item.explain;
                        strResult = strResult.replace(/\n/g, "<br />");
                        // strExplain = '<i><a class="btn-question" href="javascript:void(0)" data-toggle="popover" data-html="true" data-placement="top" data-content="' + strResult + '">' + 'Dịch Tiếng Việt</a></i>';
                        $(".javascript-html-data .js-popup-help").html(strExplain);
                    }
                    $strHTML = '<img src="' + url + item.thumbnail + '" class="sp-only-show">';
                    $(".javascript-html-data .one-tech-jbaa-box h4").html(item.title + $strHTML);
                    // set image
                    $(".javascript-html-data img").attr('src', url + item.thumbnail);


                    // because we use first item for title so will start from number two
                    // get question
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(2) span").html(questionA);
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(3) span").html(questionB);
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(4) span").html(questionC);
                    // remove event
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(2)").attr('onclick', "selectQuestion('" + item.id + "','" + questionA + "',2)");
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(3)").attr('onclick', "selectQuestion('" + item.id + "','" + questionB + "',3)");
                    $(".javascript-html-data .one-tech-jbaa-box ul.survey-list li:nth-child(4)").attr('onclick', "selectQuestion('" + item.id + "','" + questionC + "',4)");
                    // set result data
                    $("#oneTech-asia-survey-result li:nth-child(" + i + ") ").addClass('test-question-' + item.id);
                    // set question id
                    $("#oneTech-asia-survey-result li:nth-child(" + i + ") span#test-question-main-id").html(item.id);
                    // answer right
                    $("#oneTech-asia-survey-result li:nth-child(" + i + ") span#test-question-id").html(answer_to_right);
                    var newData = $(".javascript-html-data").html();
                    $(".one-tech-jbaa-page-body").append(newData);

                    $('[data-toggle="popover"]').popover({});
                    $('body').on('click', function (e) {
                        $('[data-toggle="popover"]').each(function () {
                            //the 'is' for buttons that trigger popups
                            //the 'has' for icons within a button that triggers a popup
                            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                                $(this).popover('hide');
                            }
                        });
                    });

                });
            },
            error: function (err) {
                console.log(err);
            }
        });
}


function selectQuestion(id,title,li_position){

    var the_question_before = $("#oneTech-asia-survey-result li.test-question-" + id + ' span#test-answer').html();

    $("#oneTech-asia-survey-result li.test-question-" + id + ' span#test-answer').html(title);
    // check answer to right
    var question = $("#oneTech-asia-survey-result li.test-question-" + id + ' span#test-question-id').html();
    var user_answer = $("#oneTech-asia-survey-result li.test-question-" + id + ' span#test-answer').html();
    var total_result = $("#oneTech-asia-survey-result li:nth-child(11) span#total_result").html();
    // unactive
    $(".one-tech-jbaa-box-" + id + " li").removeClass('active');
    // set active
    $(".one-tech-jbaa-box-" + id + " li:nth-child(" + li_position + ")").addClass('active');
}


/**
 *@author Le Minh Hoang
 *@time 16 / 05 / 2018
 */
function examFinish(url){
    var isCheck = confirm("Bạn có muốn kết thúc bài thi?");
    if (!isCheck) {
        return false;
    }
    $('body').addClass('ot-fr-loading');
    // question
    var question_1 = $('ul#oneTech-asia-survey-result li:nth-child(1) #test-question-main-id').html();
    var question_2 = $('ul#oneTech-asia-survey-result li:nth-child(2) #test-question-main-id').html();
    var question_3 = $('ul#oneTech-asia-survey-result li:nth-child(3) #test-question-main-id').html();
    var question_4 = $('ul#oneTech-asia-survey-result li:nth-child(4) #test-question-main-id').html();
    var question_5 = $('ul#oneTech-asia-survey-result li:nth-child(5) #test-question-main-id').html();
    var question_6 = $('ul#oneTech-asia-survey-result li:nth-child(6) #test-question-main-id').html();
    var question_7 = $('ul#oneTech-asia-survey-result li:nth-child(7) #test-question-main-id').html();
    var question_8 = $('ul#oneTech-asia-survey-result li:nth-child(8) #test-question-main-id').html();
    var question_9 = $('ul#oneTech-asia-survey-result li:nth-child(9) #test-question-main-id').html();
    var question_10 = $('ul#oneTech-asia-survey-result li:nth-child(10) #test-question-main-id').html();
    // question
    var answer_1 = $('ul#oneTech-asia-survey-result li:nth-child(1) #test-answer').html();
    var answer_2 = $('ul#oneTech-asia-survey-result li:nth-child(2) #test-answer').html();
    var answer_3 = $('ul#oneTech-asia-survey-result li:nth-child(3) #test-answer').html();
    var answer_4 = $('ul#oneTech-asia-survey-result li:nth-child(4) #test-answer').html();
    var answer_5 = $('ul#oneTech-asia-survey-result li:nth-child(5) #test-answer').html();
    var answer_6 = $('ul#oneTech-asia-survey-result li:nth-child(6) #test-answer').html();
    var answer_7 = $('ul#oneTech-asia-survey-result li:nth-child(7) #test-answer').html();
    var answer_8 = $('ul#oneTech-asia-survey-result li:nth-child(8) #test-answer').html();
    var answer_9 = $('ul#oneTech-asia-survey-result li:nth-child(9) #test-answer').html();
    var answer_10 = $('ul#oneTech-asia-survey-result li:nth-child(10) #test-answer').html();
    var _token = $('input[name="_token"]').val();
    //var total_result = $('ul#oneTech-asia-survey-result li:nth-child(11) span').html();
    var dataExam = {
        question_1:question_1,
        answer_1:answer_1,
        question_2:question_2,
        answer_2:answer_2,
        question_3:question_3,
        answer_3:answer_3,
        question_4:question_4,
        answer_4:answer_4,
        question_5:question_5,
        answer_5:answer_5,
        question_6:question_6,
        answer_6:answer_6,
        question_7:question_7,
        answer_7:answer_7,
        question_8:question_8,
        answer_8:answer_8,
        question_9:question_9,
        answer_9:answer_9,
        question_10:question_10,
        answer_10:answer_10,
        _token:_token
    };
    $.ajax({
        url:url + '/luu-bai-thi',
        type:'POST',
        data:dataExam,
        success: function(res){
            console.log(res);
            $('body').removeClass('ot-fr-loading');
            if(res.status == true ) {
                    // $(".popup-result-public .main-popup h4").html(total_result);
                    // $(".popup-result-public").removeClass('hide');
                    // $(".time-count").addClass('hide');
                window.location.href = url + '/ket-qua-thi-thu?id=' + res.exam_result_id;
            } else {
                    alert(res.message);
                $('body').removeClass('ot-fr-loading');
                    return false;
            }
        },
        error: function(err){
            $('body').removeClass('ot-fr-loading');
            return false;
        }
    });

}


/**
 *@timedown
 *@ W3c school
 */
function startTimeExam(url){
    $('body').addClass('ot-fr-loading');
    $("#start_time_exam").remove();
    getDataQuestion(url);
    $(".time-count").removeClass('hide');
    $(".start_time_survey").addClass('hide');
    $("#button-finish").removeClass('hide');
    $(".one-tech-jbaa-page-body").removeClass('hide');
    $('body').removeClass('ot-fr-loading');
    $(document).ready(function(){
        var _settime = new Date();
        var setime = _settime.setMinutes(_settime.getMinutes() + 10);
        // Set the date we're counting down to
        //var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();
        var countDownDate = setime;
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("time-count-down-text").innerHTML = minutes + ":" + seconds + "s ";

            // If the count down is over, write some text
            if (distance <= 0) {
                $("#button-finish").trigger('click');
            }
        }, 1000);
    });
}

function closePopup(){
    $(".popup-result-public").addClass('hide');
}

function loginPopup(){

    $("#button_onetech_popup_login").trigger('click');
}

function userAcceptJoinSurvey(){
    $(".box-intro").addClass('hide');
    $(".box-action").removeClass('hide');
}






