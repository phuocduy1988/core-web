function startSurveyGetGift(){
    // $(".box-intro").addClass('hide');
    $(".box-action").removeClass('hide');
    $(".alert-success").addClass('hide');
}

function loginPopup(){

        $("#button_onetech_popup_login").trigger('click');
}

function getDataSubscriber(baseUrl){

    // start loading
    $('body').addClass('ot-fr-loading');

    // token

    var _token = $("input[name='_token']").val();

    // ten

    var name = $("#name").val();

    var ngay_thang_nam_sinh = $("#birthday").val();

    var email = $("#email").val();

    // var phone = $("#phone").val();

    var chuyen_nghanh = $("#specialized").val();
    console.log(chuyen_nghanh);
    // sinh vien nam thu may

    var sinh_vien_nam_may = $(".form-subscriber > .form-group:nth-child(5) input[type='radio']:checked").val();
    console.log(sinh_vien_nam_may);
    // trinh do tieng nhat

    var trinh_do_tieng_nhat = $(".form-subscriber > .form-group:nth-child(6) input[type='radio']:checked").val();
    console.log(trinh_do_tieng_nhat);

    var co_phan_biet_dc_business_japanese = $(".form-subscriber > .form-group:nth-child(7) input[type='radio']:checked").val();

    if(co_phan_biet_dc_business_japanese == 'other') {
        var co_phan_biet_dc_business_japanese = $("#co_phan_biet_dc_business_japanese_other").val();
    }
    console.log(co_phan_biet_dc_business_japanese);

    var ban_co_biet_phong_cach_lam_viec_cua_nhat = $(".form-subscriber > .form-group:nth-child(8) input[type='radio']:checked").val();
    if(ban_co_biet_phong_cach_lam_viec_cua_nhat == 'other') {
        var ban_co_biet_phong_cach_lam_viec_cua_nhat = $("#ban_co_biet_phong_cach_lam_viec_cua_nhat_other").val();
    }
    console.log(ban_co_biet_phong_cach_lam_viec_cua_nhat);
    var tai_sao_hoc_tieng_nhat = $(".form-subscriber > .form-group:nth-child(9) input[type='radio']:checked").val();

    if(tai_sao_hoc_tieng_nhat == 'other') {
        var tai_sao_hoc_tieng_nhat = $("#tai_sao_hoc_tieng_nhat_other").val();
    }
    console.log(tai_sao_hoc_tieng_nhat);


    var ban_co_nghe_van_de_lien_quan_den_long_dong_nhat = $(".form-subscriber > .form-group:nth-child(10) input[type='radio']:checked").val();

    if(ban_co_nghe_van_de_lien_quan_den_long_dong_nhat == 'other') {
        var ban_co_nghe_van_de_lien_quan_den_long_dong_nhat = $("#ban_co_nghe_van_de_lien_quan_den_long_dong_nhat_other").val();
    }

    console.log(ban_co_nghe_van_de_lien_quan_den_long_dong_nhat);

    var nhung_van_de_lien_quan_sau_xa = $(".form-subscriber > .form-group:nth-child(11) input[type='radio']:checked").val();

    if(nhung_van_de_lien_quan_sau_xa == 'other') {
        var nhung_van_de_lien_quan_sau_xa = $("#nhung_van_de_lien_quan_sau_xa_other").val();
    }

    var neu_day_la_mon_hoc_tu_chon = $(".form-subscriber > .form-group:nth-child(12) input[type='radio']:checked").val();

    // if(neu_day_la_mon_hoc_tu_chon == 'other') {
    //     var neu_day_la_mon_hoc_tu_chon = $("#neu_day_la_mon_hoc_tu_chon_other").val();
    // }

    console.log(neu_day_la_mon_hoc_tu_chon);

    // neu da la mon hoc tu chon

    var theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc = $(".form-subscriber > .form-group:nth-child(13) input[type='radio']:checked").val();

    // if(theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc == 'other') {
    //     var theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc = $("#theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc_other").val();
    // }
    console.log(theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc);

    var ban_co_vui_long_nhan_thong_tin = $(".form-subscriber > .form-group:nth-child(14) input[type='radio']:checked").val();

    if(ban_co_vui_long_nhan_thong_tin == 'other') {
        var ban_co_vui_long_nhan_thong_tin = $("#ban_co_vui_long_nhan_thong_tin_other").val();
    }
    console.log(ban_co_vui_long_nhan_thong_tin);

    if( name == ''|| typeof name === "undefined" || chuyen_nghanh == ''
        || sinh_vien_nam_may == '' || typeof sinh_vien_nam_may === "undefined"
        || co_phan_biet_dc_business_japanese == '' || typeof co_phan_biet_dc_business_japanese === "undefined"
        || neu_day_la_mon_hoc_tu_chon == '' || typeof neu_day_la_mon_hoc_tu_chon === "undefined"
        || ban_co_nghe_van_de_lien_quan_den_long_dong_nhat == '' || typeof ban_co_nghe_van_de_lien_quan_den_long_dong_nhat === "undefined"
        || nhung_van_de_lien_quan_sau_xa == '' || typeof nhung_van_de_lien_quan_sau_xa === "undefined"
        || neu_day_la_mon_hoc_tu_chon == '' || typeof neu_day_la_mon_hoc_tu_chon === "undefined"
        || theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc == '' || typeof theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc === "undefined"
        || ban_co_vui_long_nhan_thong_tin == '' || typeof ban_co_vui_long_nhan_thong_tin === "undefined"
    ) {
        // end loading
        $('body').removeClass('ot-fr-loading');
        alert("Bạn cần điền đầy đủ thông tin!");
    } else {
        $.ajax({
            url:baseUrl + '/khao-sat-nhan-qua',
            type:'POST',
            data:{
                ngay_thang_nam_sinh:ngay_thang_nam_sinh,
                email:email,
                name:name,
                //phone:phone,
                chuyen_nghanh:chuyen_nghanh,
                sinh_vien_nam_may:sinh_vien_nam_may,
                trinh_do_tieng_nhat:trinh_do_tieng_nhat,
                co_phan_biet_dc_business_japanese:co_phan_biet_dc_business_japanese,
                ban_co_biet_phong_cach_lam_viec_cua_nhat:ban_co_biet_phong_cach_lam_viec_cua_nhat,
                tai_sao_hoc_tieng_nhat:tai_sao_hoc_tieng_nhat,
                ban_co_nghe_van_de_lien_quan_den_long_dong_nhat:ban_co_nghe_van_de_lien_quan_den_long_dong_nhat,
                nhung_van_de_lien_quan_sau_xa:nhung_van_de_lien_quan_sau_xa,
                neu_day_la_mon_hoc_tu_chon:neu_day_la_mon_hoc_tu_chon,
                theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc:theo_chi_phi_khoa_hoc_khoan_bao_nhieu_thi_se_tham_gia_hoc,
                ban_co_vui_long_nhan_thong_tin:ban_co_vui_long_nhan_thong_tin,
                _token:_token
            },
            success: function(res){
                if(res.status == true ) {
                    location.href = baseUrl + '/khao-sat-thanh-cong?ks_id=' + res.ks_id;
                }
            },
            error: function(err){
                // end loading
                $('body').removeClass('ot-fr-loading');
            }
        });
    }

}