jQuery(function($) {
	$(document).ready(function(){
		$(window).scroll(function(){
	        if ($(this).scrollTop() > 110) {
	            $('.scrollToTop').fadeIn();
	        } else {
	            $('.scrollToTop').fadeOut();
	        }
	        /*
	        if($(window).width() >= 768 && $(this).scrollTop() > 86) {
	            $('.t-fixed .navbar-header').css('display', 'none');
	        }
	        else if($(window).width() >= 768 && $(this).scrollTop() < 86) {
	            $('.t-fixed .navbar-header').css('display', 'table');
	        }
	        else {
	            $('.t-fixed .navbar-header').css('display', 'block');
	        }
	         */
	    });

	    $('.scrollToTop').click(function(){
	        $('html, body').animate({scrollTop : 0},800);
	        return false;
	    });
	});
	$(function(){
        $('.dropdown').hover(function() {
            if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
                if(!$(this).hasClass('open')) { // Keeps it open when hover it again
                    $('.dropdown-toggle', this).trigger('click');
                }
            }
        }, function(){
            $(this).removeClass('open');
        });
    });
    function LoadHeader() {
    	/*
        if($(window).width() >= 768 && $(window).scrollTop() > 86) {
            $('.t-fixed .navbar-header').css('display', 'none');
            $('.highlight-section').css('display', 'none');
        }
        else if($(window).width() >= 768 && $(window).scrollTop() < 86) {
            $('.t-fixed .navbar-header').css('display', 'table');
            $('.highlight-section').css('display', 'none');
        }
        else {
            $('.t-fixed .navbar-header').css('display', 'block');
            $('.highlight-section').css('display', 'block');
        }
    	 */
    }
    $(window).on("load", LoadHeader);
    $(window).on("orientationchange", LoadHeader);
    $(window).on("resize", LoadHeader);

    var owl = $('.ot-slide-list');
		owl.owlCarousel({
		  responsiveClass:true,
		  autoplay : false,
		  slideSpeed : 2000,
          margin:0,
		  dots: false,
		  loop: true,
		  nav:true,
          navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		  items:1
		});
    var windowSize = $(window).width();
    var tabslide = $('.tab-slide');
        tabslide.owlCarousel({
          responsiveClass:true,
          animateOut: 'fadeOut',
          autoplay : false,
          slideSpeed : 2000,
          margin:0,
          dots: true,
          loop: false,
          nav:true,
          navText:['<i class="angle-left" aria-hidden="true"></i>','<i class="angle-right" aria-hidden="true"></i>'],
          items:2,
          responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1000:{
                items:2
            }
        }
        });
    $('.ot-list-employers .list-icon').owlCarousel({
      responsiveClass:true,
      autoplay : false,
      slideSpeed : 2000,
      margin:0,
      dots: false,
      loop: true,
      nav:false,
      items:10,
       responsive:{
            0:{
                items:2
            },
            480:{
                items:3
            },
            768:{
                items:4
            },
            1000:{
                items:10
            }
        }
    });
    var h = 0;
    $('.ot-list-program .ot-box-item h2.title').each(function(i, desc){
        if($(desc).height() > h) h = $(desc).height();
    })
    $('.ot-list-program .ot-box-item h2.title').height(h);

    var total = $('.ot-job .owl-dots').width() + 120;
    $('.ot-job .owl-nav').width(total);
});