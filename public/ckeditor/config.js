/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'editing',     groups: [ 'find', 'selection' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'forms' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		// '/', Break line toolbar
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar. https://ckeditor.com/latest/samples/old/datafiltering.html
	//config.removeButtons = 'Underline,Subscript,Superscript,Image,Maximize,PasteFromWord,PasteText,Paste';
	// config.removeButtons = 'Underline,Subscript,Superscript,Maximize,PasteFromWord,PasteText,Paste';
    //

    config.extraPlugins = 'youtube,confighelper,colorbutton,panelbutton,colordialog,justify,image2,find,button,font,tableresize,tabletools,html5video,widget,widgetselection,clipboard,lineutils,autolink,pastefromexcel';

    config.filebrowserBrowseUrl = '/elfinder/ckeditor';
    config.width = '100%';
    config.height = 300;
    config.allowedContent = true;
    config.removeFormatAttributes = '';
    config.colorButton_enableAutomatic = true;
    config.colorButton_enableMore = true;

    // Set the most common block elements.
	//config.format_tags = 'p;h1;h2;h3;pre';

    config.resize_enabled = false;
    config.uiColor = '#FFFFFF';
    config.toolbarLocation = 'top';


    // Simplify the dialog windows.
	config.removeDialogTabs = 'link:advanced';
	// config.removeDialogTabs = 'image:advanced;link:advanced';

	config.language = 'en';
};
