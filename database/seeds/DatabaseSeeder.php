<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use \App\Models\Setting;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //Insert setting
        Setting::truncate();
        $dataSetting = [
            ['key' => 'maintenance_flag', 'value' => '0'],
            ['key' => 'url', 'value' => 'https://onetech.vn'],
            ['key' => 'security_key','value' => 'wt1U5MACWJFTXGenFoZoiLwQGrLgdbHA'],
            ['key' => 'is_debug', 'value' => '1'],
            ['key' => 'force_update_flag', 'value' => '0'],
            ['key' => 'force_update_target_version','value' => '1.0.0'],
            ['key' => 'new_version_flag','value' => '0']
        ];

        Setting::insert($dataSetting);

        $user = new User();
        if (config('permissible.first_last_name_migration', false) === true) {
            $user->email = 'supper';
            $user->first_name = 'Super';
            $user->last_name = 'User';
            $user->password = '123456';
        } else {
            $user->email = 'admin';
            $user->first_name = 'Admin';
            $user->last_name = 'User';
            $user->password = '123456';
        }
        $user->save();

        $user->roles()->sync([1]);

    }
}
