const mix = require('laravel-mix');

mix.js('resources/assets/frontend/frontend.js', 'public/js')
	.webpackConfig({
		resolve: {
			alias: {
				'@': path.resolve(__dirname, 'resources/assets/frontend')
			}
		}
	});
