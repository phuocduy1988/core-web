<?php
	return [
		'enable_routes' => false,
		'enable_user_management_routes' => false,
    'first_last_name_migration' => false,
    'default_fallback_route' => 'backend.home',
    'hierarchy' => true,
	];