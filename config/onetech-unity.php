<?php
//$redprintMenu = function_exists('redprintMenu') ? redprintMenu() : [];
//$permissibleMenu = function_exists('permissibleMenu') ? permissibleMenu() : [];
//$backendMenu = include(__DIR__.'/backend-menu.php');
//$backendMenu = array_merge(['Backend'], $backendMenu);

return [

    'title' => 'OneTech Generate CRUD',
    //'menu' => array_merge($redprintMenu, $permissibleMenu, $backendMenu),
    'skin' => 'purple', // available skins: blue, purple
    'dashboard_route' => 'backend.home',
    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */
];
