<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:22:42
 * File: ExaminationResultController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\ExaminationResult;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ExaminationResultRequest;
use App\Http\Resources\ExaminationResultCollection;
use App\Http\Resources\ExaminationResult as ExaminationResultResource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class ExaminationResultController extends ApiBaseController
{
    /**
     * Display a listing of the examinationResults.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $examinationResults = ExaminationResult::query();
            //Generate search option here
            //Get data with pagination and order by(Default:: id desc)
            $examinationResults = $examinationResults
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($examinationResults);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created examinationResults in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'result' => ['required', 'max:191'],
			'status' => ['nullable', 'boolean'],

            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('result', '').$request->get('status', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new ExaminationResult
            $examinationResult = new ExaminationResult();
            $examinationResult->result = $request->get('result');
			$examinationResult->status = $request->get('status', 0);

			$examinationResult->predict_participation = $request->get('predict_participation');

			$examinationResult->start_exam = $request->get('start_exam');
			$examinationResult->end_exam = $request->get('end_exam');

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationResult->save();

            //Check response data
            $responseCode = is_null($examinationResult->id) ? 201 : 200;
            return $this->jsonOK(['data' => $examinationResult], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update examinationResults in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('result', '').$request->get('status', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationResult = ExaminationResult::find($id);
            if(is_null($examinationResult)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('result')) {
				$examinationResult->result = $request->get('result');
			}
			if ($request->get('status')) {
				$examinationResult->status = $request->get('status');
			}

			if ($request->get('predict_participation')) {
				$examinationResult->predict_participation = $request->get('predict_participation');
			}

			if ($request->get('start_exam')) {
				$examinationResult->start_exam = $request->get('start_exam');
			}
			if ($request->get('end_exam')) {
				$examinationResult->end_exam = $request->get('end_exam');
			}

            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationResult->save();

            return $this->jsonOK(['data' => $examinationResult]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified examinationResults from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationResult = ExaminationResult::find($id);
            if(!is_null($examinationResult)) {
                $examinationResult->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationResult = ExaminationResult::withTrashed()->find($id);
            if(!is_null($examinationResult)) {
                $examinationResult->restore();
                return $this->jsonOK(['data' => $examinationResult]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationResult = ExaminationResult::withTrashed()->find($id);
            if(!is_null($examinationResult)) {
                $examinationResult->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
