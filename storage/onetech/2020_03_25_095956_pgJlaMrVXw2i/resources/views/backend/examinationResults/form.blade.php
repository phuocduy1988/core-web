@extends('backend.layouts.app')

@section('title') {{ trans('examinationResults.examinationResults') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

@include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('examinationResults.examinationResults'), 'link' => getBeUrl('examinationResults')]
    	)])

	<div class="wrapper wrapper-content animated fadeInRight">
		{!! Form::open(['method' => 'post', 'route' => 'examinationResult.save', 'files'=>true]) !!}
		{!! Form::hidden('id', $examinationResult->id) !!}
		<div class="row">
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
				    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationTypes.examinationTypes') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="examination_type_id">
                                            @foreach($examinationTypes as $examinationType)
                                                @if(old('examination_type_id', isset($examinationResult->examination_type_id) ? $examinationResult->examination_type_id : '' ) == $examinationType->id)
                                                    <option value ='{{ $examinationType->id }}' selected>
                                                        {{ isset($examinationType->name) ? $examinationType->name : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $examinationType->id }}'>
                                                        {{ isset($examinationType->name) ? $examinationType->name : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('members.members') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="member_id">
                                            @foreach($members as $member)
                                                @if(old('member_id', isset($examinationResult->member_id) ? $examinationResult->member_id : '' ) == $member->id)
                                                    <option value ='{{ $member->id }}' selected>
                                                        {{ isset($member->name) ? $member->name : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $member->id }}'>
                                                        {{ isset($member->name) ? $member->name : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.result') }}
                                        @if(true)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="result" type="text" value="{{ old('result', isset($examinationResult->result) ? $examinationResult->result : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.status') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" name="status" value="1" @if(isset($examinationResult->status)? $examinationResult->status : 0) checked @endif>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.predict_participation') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="predict_participation" type="text" value="{{ old('predict_participation', isset($examinationResult->predict_participation) ? $examinationResult->predict_participation : '' ) }}">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.start_exam') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-3">
                                        <div class="controls input-group date datepicker" id="js-viewdate">
                                            <input class="form-control" name="start_exam" type="text" value="{!! old('start_exam', isset($examinationResult->start_exam) ? $examinationResult->start_exam : '' ) !!}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.end_exam') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-3">
                                        <div class="controls input-group date datepicker" id="js-viewdate">
                                            <input class="form-control" name="end_exam" type="text" value="{!! old('end_exam', isset($examinationResult->end_exam) ? $examinationResult->end_exam : '' ) !!}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

{{-- --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                <hr />
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 text-center js-page-btn">
                                        <a href="{{ route('examinationResult.index') }}" class="btn btn-large btn-default">
                                        <i class="fa fa-caret-left"></i>
                                        {{ trans('button.cancel') }}
                                        </a>
                                        <button type="submit" action="confirm" class="btn btn-large btn-info">
                                        <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    <script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <!-- Use for insert media from file manager end -->
    <script src="{!! asset('backend/js/media.js') !!}"></script>
    <!-- Date time picker start -->
    <script src="{!! asset('backend/plugins/datetimepicker/moment-with-locales.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('backend/js/datetime-validation.js') !!}"></script>
    <!-- Date time picker end -->
    <script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('js-editor');
        });
        $('#js-viewdate').datetimepicker({
            format: 'YYYY/MM/DD HH:mm',
            locale: 'ja'
        });
    </script>
@stop
