<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update5w4xWsExaminationResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('examination_results', function (Blueprint $table) {
            $table->dateTime('start_exam')->nullable();
			$table->dateTime('end_exam')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examination_results', function (Blueprint $table) {
            $table->dropColumn('start_exam');
			$table->dropColumn('end_exam');
			
        });
    }
}
