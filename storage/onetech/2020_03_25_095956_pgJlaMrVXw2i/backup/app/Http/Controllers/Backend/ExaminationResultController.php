<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:22:42
 * File: ExaminationResultController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\ExaminationResult;
use Illuminate\Http\Request;
use App\Http\Requests\ExaminationResultRequest;
use App\Http\Controllers\Backend\Controller;

class ExaminationResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $examinationResults = ExaminationResult::query();

			$examinationResults = $examinationResults->with('examinationType');

			$examinationResults = $examinationResults->with('member');

			$examinationResults = $examinationResults->with('examination');

            if ($request->has('keyword')) {
                //Search via param key word
                $examinationResults = $examinationResults->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')

								->orWhere('result', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $examinationResults = $examinationResults->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            $examinationResults = $examinationResults
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.examinationResults.index')->with('examinationResultsData', $examinationResults);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ExaminationResult $examinationResult = null)
    {
        try {
            //Get data for create new or update
            $examinationResult = $examinationResult ?: new ExaminationResult;
            return view('backend.examinationResults.form')->with('examinationResult', $examinationResult)->with('examinationTypes', \App\Models\ExaminationType::get())->with('members', \App\Models\Member::get())->with('examinations', \App\Models\Examination::get());
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via ExaminationResultRequest
     * @param  ExaminationResultRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(ExaminationResultRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            $examinationResult = ExaminationResult::firstOrNew(['id' => $request->get('id')]);

            if(isset($examinationResult->id)) {
                $isUpdate = true;
            }

            $examinationResult->result = $request->get('result');
			$examinationResult->status = $request->get('status', 0);

			$examinationResult->predict_participation = $request->get('predict_participation');

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
			$examinationResult->member_id = $request->get('member_id');
			$examinationResult->examination_type_id = $request->get('examination_type_id');
            $examinationResult->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return redirect()->route('examinationResult.index')->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ExaminationResult $examinationResult)
    {
        try {
           $examinationResult->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($examinationResultId)
    {
        try {
            $examinationResult = ExaminationResult::withTrashed()->find($examinationResultId);
            $examinationResult->restore();
            $message = 'examinationResult Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($examinationResultId)
    {
        try {
            $examinationResult = ExaminationResult::withTrashed()->find($examinationResultId);
            $examinationResult->forceDelete();
            $message = 'examinationResult permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
