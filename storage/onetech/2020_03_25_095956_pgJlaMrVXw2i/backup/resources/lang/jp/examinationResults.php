<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationResults translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationResults' => 'Exam Results',
    'examinationResults_index' => 'Exam Results',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'examinationResults_index' => 'Exam Results',
    'id' => 'ID',
	'result' => 'Result',
	'status' => 'status',
	
	'predict_participation' => 'Dự đoán tham gia',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
