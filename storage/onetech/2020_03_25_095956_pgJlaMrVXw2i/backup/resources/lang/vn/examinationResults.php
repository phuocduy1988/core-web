<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationResults translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationResults' => 'Kết quả thi',
    'examinationResults_index' => 'Kết quả thi',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'result' => 'Result',
	'status' => 'status',
	
	'predict_participation' => 'Dự đoán tham gia',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
