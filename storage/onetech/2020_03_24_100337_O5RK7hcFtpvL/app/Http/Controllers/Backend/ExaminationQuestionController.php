<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2020-03-24
 * Time: 21:25:18
 * File: ExaminationQuestionController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\ExaminationQuestion;
use Illuminate\Http\Request;
use App\Http\Requests\ExaminationQuestionRequest;
use App\Http\Controllers\Backend\Controller;

class ExaminationQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $examinationQuestions = ExaminationQuestion::query();

            if ($request->has('keyword')) {
                //Search via param key word
                $examinationQuestions = $examinationQuestions->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')

								->orWhere('title', 'LIKE', '%'.$request->get('keyword').'%')
								->orWhere('description', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $examinationQuestions = $examinationQuestions->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            $examinationQuestions = $examinationQuestions
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.examinationQuestions.index')->with('examinationQuestionsData', $examinationQuestions);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ExaminationQuestion $examinationQuestion = null)
    {
        try {
            //Get data for create new or update
            $examinationQuestion = $examinationQuestion ?: new ExaminationQuestion;
            return view('backend.examinationQuestions.form')->with('examinationQuestion', $examinationQuestion);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via ExaminationQuestionRequest
     * @param  ExaminationQuestionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(ExaminationQuestionRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            $examinationQuestion = ExaminationQuestion::firstOrNew(['id' => $request->get('id')]);

            if(isset($examinationQuestion->id)) {
                $isUpdate = true;
            }

            $examinationQuestion->title = $request->get('title');
			$examinationQuestion->description = $request->get('description');
			$examinationQuestion->status = $request->get('status', 0);
			$examinationQuestion->multi_choices = $request->get('multi_choices');
			$examinationQuestion->image = $request->get('image');

			$examinationQuestion->answer_right = $request->get('answer_right');

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationQuestion->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return redirect()->route('examinationQuestion.index')->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ExaminationQuestion $examinationQuestion)
    {
        try {
           $examinationQuestion->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($examinationQuestionId)
    {
        try {
            $examinationQuestion = ExaminationQuestion::withTrashed()->find($examinationQuestionId);
            $examinationQuestion->restore();
            $message = 'examinationQuestion Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($examinationQuestionId)
    {
        try {
            $examinationQuestion = ExaminationQuestion::withTrashed()->find($examinationQuestionId);
            $examinationQuestion->forceDelete();
            $message = 'examinationQuestion permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
