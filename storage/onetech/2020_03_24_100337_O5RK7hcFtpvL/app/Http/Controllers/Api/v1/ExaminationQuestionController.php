<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2020-03-24
 * Time: 21:25:18
 * File: ExaminationQuestionController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\ExaminationQuestion;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ExaminationQuestionRequest;
use App\Http\Resources\ExaminationQuestionCollection;
use App\Http\Resources\ExaminationQuestion as ExaminationQuestionResource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class ExaminationQuestionController extends ApiBaseController
{
    /**
     * Display a listing of the examinationQuestions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $examinationQuestions = ExaminationQuestion::query();
            //Generate search option here
            if ($request->has('keyword')) {
                //Search via param key word
                $examinationQuestions = $examinationQuestions->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')

								->orWhere('title', 'LIKE', '%'.$request->get('keyword').'%')
								->orWhere('description', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Get data with pagination and order by(Default:: id desc)
            $examinationQuestions = $examinationQuestions
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($examinationQuestions);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created examinationQuestions in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'title' => ['nullable'],
			'description' => ['nullable'],
			'status' => ['nullable', 'boolean'],
			'multi_choices' => ['nullable'],
			'image' => ['nullable', 'max:191'],


			'answer_right' => ['required', 'max:191'],

                //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('title', '').$request->get('description', '').$request->get('status', '').$request->get('multi_choices', '').$request->get('image', '').
				$request->get('answer_right', '').
                //--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new ExaminationQuestion
            $examinationQuestion = new ExaminationQuestion();
            $examinationQuestion->title = $request->get('title');
			$examinationQuestion->description = $request->get('description');
			$examinationQuestion->status = $request->get('status', 0);
			$examinationQuestion->multi_choices = $request->get('multi_choices');
			$examinationQuestion->image = $request->get('image');

			$examinationQuestion->answer_right = $request->get('answer_right');

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationQuestion->save();

            //Check response data
            $responseCode = is_null($examinationQuestion->id) ? 201 : 200;
            return $this->jsonOK(['data' => $examinationQuestion], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update examinationQuestions in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('title', '').$request->get('description', '').$request->get('status', '').$request->get('multi_choices', '').$request->get('image', '').

				$request->get('answer_right', '').
                //--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationQuestion = ExaminationQuestion::find($id);
            if(is_null($examinationQuestion)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('title')) {
				$examinationQuestion->title = $request->get('title');
			}
			if ($request->get('description')) {
				$examinationQuestion->description = $request->get('description');
			}
			if ($request->get('status')) {
				$examinationQuestion->status = $request->get('status');
			}
			if ($request->get('multi_choices')) {
				$examinationQuestion->multi_choices = $request->get('multi_choices');
			}
			if ($request->get('image')) {
				$examinationQuestion->image = $request->get('image');
			}


			if ($request->get('answer_right')) {
				$examinationQuestion->answer_right = $request->get('answer_right');
			}

            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationQuestion->save();

            return $this->jsonOK(['data' => $examinationQuestion]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified examinationQuestions from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationQuestion = ExaminationQuestion::find($id);
            if(!is_null($examinationQuestion)) {
                $examinationQuestion->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationQuestion = ExaminationQuestion::withTrashed()->find($id);
            if(!is_null($examinationQuestion)) {
                $examinationQuestion->restore();
                return $this->jsonOK(['data' => $examinationQuestion]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationQuestion = ExaminationQuestion::withTrashed()->find($id);
            if(!is_null($examinationQuestion)) {
                $examinationQuestion->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
