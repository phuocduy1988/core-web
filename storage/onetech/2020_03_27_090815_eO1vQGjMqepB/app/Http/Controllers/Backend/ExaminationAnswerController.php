<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:24:34
 * File: ExaminationAnswerController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\ExaminationAnswer;
use Illuminate\Http\Request;
use App\Http\Requests\ExaminationAnswerRequest;
use App\Http\Controllers\Backend\Controller;

class ExaminationAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $examinationAnswers = ExaminationAnswer::query();

			$examinationAnswers = $examinationAnswers->with('examinationQuestion');

			$examinationAnswers = $examinationAnswers->with('examinationType');

			$examinationAnswers = $examinationAnswers->with('member');

			$examinationAnswers = $examinationAnswers->with('examination');

			$examinationAnswers = $examinationAnswers->with('examinationResult');

            if ($request->has('keyword')) {
                //Search via param key word
                $examinationAnswers = $examinationAnswers->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')


								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $examinationAnswers = $examinationAnswers->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            $examinationAnswers = $examinationAnswers
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.examinationAnswers.index')->with('examinationAnswersData', $examinationAnswers);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ExaminationAnswer $examinationAnswer = null)
    {
        try {
            //Get data for create new or update
            $examinationAnswer = $examinationAnswer ?: new ExaminationAnswer;
            return view('backend.examinationAnswers.form')->with('examinationAnswer', $examinationAnswer)->with('examinationQuestions', \App\Models\ExaminationQuestion::get())->with('examinationTypes', \App\Models\ExaminationType::get())->with('members', \App\Models\Member::get())->with('examinations', \App\Models\Examination::get())->with('examinationResults', \App\Models\ExaminationResult::get());
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via ExaminationAnswerRequest
     * @param  ExaminationAnswerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(ExaminationAnswerRequest $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            $examinationAnswer = ExaminationAnswer::firstOrNew(['id' => $request->get('id')]);

            if(isset($examinationAnswer->id)) {
                $isUpdate = true;
            }

            $examinationAnswer->answer = $request->get('answer');

			$examinationAnswer->is_correct = $request->get('is_correct', 0);
			$examinationAnswer->answer_right = $request->get('answer_right');

			$examinationAnswer->order = $request->get('order');

            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
			$examinationAnswer->examination_result_id = $request->get('examination_result_id');
			$examinationAnswer->examination_id = $request->get('examination_id');
			$examinationAnswer->member_id = $request->get('member_id');
			$examinationAnswer->examination_type_id = $request->get('examination_type_id');
			$examinationAnswer->examination_question_id = $request->get('examination_question_id');
            $examinationAnswer->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return redirect()->route('examinationAnswer.index')->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ExaminationAnswer $examinationAnswer)
    {
        try {
           $examinationAnswer->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($examinationAnswerId)
    {
        try {
            $examinationAnswer = ExaminationAnswer::withTrashed()->find($examinationAnswerId);
            $examinationAnswer->restore();
            $message = 'examinationAnswer Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($examinationAnswerId)
    {
        try {
            $examinationAnswer = ExaminationAnswer::withTrashed()->find($examinationAnswerId);
            $examinationAnswer->forceDelete();
            $message = 'examinationAnswer permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
