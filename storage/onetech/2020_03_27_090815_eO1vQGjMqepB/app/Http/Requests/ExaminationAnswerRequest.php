<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:24:34
 * File: ExaminationAnswerRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExaminationAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer' => ['required', 'max:191'],

			'is_correct' => ['nullable', 'boolean'],
			'answer_right' => ['nullable'],

			'order' => ['nullable', 'integer'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'answer.required' => trans('validation.required', ['attribute' => trans('examinationAnswers.answer')]),
			


            //--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }
}
