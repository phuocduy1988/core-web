<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationAnswers translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationAnswers' => 'Exam Answers',
    'examinationAnswers_index' => 'Exam Answers',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'examinationAnswers_index' => 'Exam Answers',
    'id' => 'ID',
	'answer' => 'Answer',
	
	'is_correct' => 'Đúng',
	'answer_right' => 'Đáp án đúng',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
