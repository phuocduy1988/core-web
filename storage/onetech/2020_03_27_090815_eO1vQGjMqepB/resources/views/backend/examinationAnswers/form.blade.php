@extends('backend.layouts.app')

@section('title') {{ trans('examinationAnswers.examinationAnswers') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

@include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('examinationAnswers.examinationAnswers'), 'link' => getBeUrl('examinationAnswers')]
    	)])

	<div class="wrapper wrapper-content animated fadeInRight">
		{!! Form::open(['method' => 'post', 'route' => 'examinationAnswer.save', 'files'=>true]) !!}
		{!! Form::hidden('id', $examinationAnswer->id) !!}
		<div class="row">
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
				    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.examinationQuestions') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="examination_question_id">
                                            @foreach($examinationQuestions as $examinationQuestion)
                                                @if(old('examination_question_id', isset($examinationAnswer->examination_question_id) ? $examinationAnswer->examination_question_id : '' ) == $examinationQuestion->id)
                                                    <option value ='{{ $examinationQuestion->id }}' selected>
                                                        {{ isset($examinationQuestion->title) ? $examinationQuestion->title : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $examinationQuestion->id }}'>
                                                        {{ isset($examinationQuestion->title) ? $examinationQuestion->title : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationTypes.examinationTypes') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="examination_type_id">
                                            @foreach($examinationTypes as $examinationType)
                                                @if(old('examination_type_id', isset($examinationAnswer->examination_type_id) ? $examinationAnswer->examination_type_id : '' ) == $examinationType->id)
                                                    <option value ='{{ $examinationType->id }}' selected>
                                                        {{ isset($examinationType->name) ? $examinationType->name : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $examinationType->id }}'>
                                                        {{ isset($examinationType->name) ? $examinationType->name : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('members.members') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="member_id">
                                            @foreach($members as $member)
                                                @if(old('member_id', isset($examinationAnswer->member_id) ? $examinationAnswer->member_id : '' ) == $member->id)
                                                    <option value ='{{ $member->id }}' selected>
                                                        {{ isset($member->name) ? $member->name : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $member->id }}'>
                                                        {{ isset($member->name) ? $member->name : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinations.examinations') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="examination_id">
                                            @foreach($examinations as $examination)
                                                @if(old('examination_id', isset($examinationAnswer->examination_id) ? $examinationAnswer->examination_id : '' ) == $examination->id)
                                                    <option value ='{{ $examination->id }}' selected>
                                                        {{ isset($examination->title) ? $examination->title : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $examination->id }}'>
                                                        {{ isset($examination->title) ? $examination->title : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


								                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationResults.examinationResults') }}
                                    </label>
                                    <div class="col-sm-6">
                                        <select class="form-control select2" name="examination_result_id">
                                            @foreach($examinationResults as $examinationResult)
                                                @if(old('examination_result_id', isset($examinationAnswer->examination_result_id) ? $examinationAnswer->examination_result_id : '' ) == $examinationResult->id)
                                                    <option value ='{{ $examinationResult->id }}' selected>
                                                        {{ isset($examinationResult->result) ? $examinationResult->result : '' }}
                                                    </option>
                                                @else
                                                    <option value ='{{ $examinationResult->id }}'>
                                                        {{ isset($examinationResult->result) ? $examinationResult->result : '' }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationAnswers.answer') }}
                                        @if(true)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="answer" type="text" value="{{ old('answer', isset($examinationAnswer->answer) ? $examinationAnswer->answer : '' ) }}">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationAnswers.is_correct') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" name="is_correct" value="1" @if(isset($examinationAnswer->is_correct)? $examinationAnswer->is_correct : 0) checked @endif>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationAnswers.answer_right') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea name="answer_right" class="form-control">{!! old('answer_right', isset($examinationAnswer->answer_right) ? $examinationAnswer->answer_right : '' ) !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationAnswers.order') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="number" id="" class="form-control" name="order" min="-4294967295" max="4294967295" step="1" value="{{ old('order', isset($examinationAnswer->order) ? $examinationAnswer->order : '' ) }}" maxlength="9">
                                    </div>
                                </div>

{{-- --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                <hr />
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 text-center js-page-btn">
                                        <a href="{{ route('examinationAnswer.index') }}" class="btn btn-large btn-default">
                                        <i class="fa fa-caret-left"></i>
                                        {{ trans('button.cancel') }}
                                        </a>
                                        <button type="submit" action="confirm" class="btn btn-large btn-info">
                                        <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    <script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <!-- Use for insert media from file manager end -->
    <script src="{!! asset('backend/js/media.js') !!}"></script>
    <!-- Date time picker start -->
    <script src="{!! asset('backend/plugins/datetimepicker/moment-with-locales.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('backend/js/datetime-validation.js') !!}"></script>
    <!-- Date time picker end -->
    <script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('js-editor');
        });
        $('#js-viewdate').datetimepicker({
            format: 'YYYY/MM/DD HH:mm',
            locale: 'ja'
        });
    </script>
@stop
