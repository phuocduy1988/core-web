@extends('backend.layouts.app')

@section('title') {{ trans('examinations.examinations') }} @stop

@section('css')
	<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
@stop

@section('content')

	@include('backend._partials.breadcrumb',
			['page_list' => array(
				['title' => trans('examinations.examinations'), 'link' => route('gallery.index')]
			)])

	<div id="app" class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@include('backend._partials.notifications')
						@include('backend._partials.vue-notifications')
						<div class="box-content">
							<p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
								<div class="col-sm-7">
									<div class="form-group clearfix">
										<label class="text-right control-label col-sm-4 col-md-3">
											{{ trans('examinationTypes.examinationTypes') }}
											@if(true)
												<span class="red-color">{{ trans('table.required') }}</span>
											@endif
										</label>
										<div class="col-sm-6">
											<select class="form-control" v-model="form.examination_type_id">
												<option
													v-for="item in examinationTypes"
													:key="item.id"
													:label="item.name"
													:value="item.id">
												</option>
											</select>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="text-right control-label col-sm-4 col-md-3">
											{{ trans('examinations.title') }}
											@if(true)
												<span class="red-color">{{ trans('table.required') }}</span>
											@endif
										</label>
										<div class="col-sm-6">
											<textarea rows="1" v-model="form.title" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="text-right control-label col-sm-4 col-md-3">
											{{ trans('examinations.explain') }}
											@if(true)
												<span class="red-color">{{ trans('table.required') }}</span>
											@endif
										</label>
										<div class="col-sm-6">
											<textarea rows="2" v-model="form.explain" class="form-control"></textarea>
										</div>
									</div>
{{--									<div class="form-group clearfix">--}}
{{--										<label class="text-right control-label col-sm-4 col-md-3">--}}
{{--											{{ trans('examinations.thumbnail') }}--}}
{{--											@if(true)--}}
{{--												<span class="red-color">{{ trans('table.required') }}</span>--}}
{{--											@endif--}}
{{--										</label>--}}
{{--										<div class="media-box col-sm-6">--}}
{{--											<div v-if="form.thumbnail" id="js-img-preview" class="img-preview">--}}
{{--												<img class="js-img-change img-responsive" :src="form.thumbnail" />--}}
{{--											</div>--}}
{{--											<div class="media-action">--}}
{{--												<a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">--}}
{{--													<i class="fa fa-warning"></i>--}}
{{--													{{ trans('button.delete') }}--}}
{{--												</a>--}}
{{--											</div>--}}
{{--											<input type="hidden" class="media-change" name="thumbnail" value="" v-model="form.thumbnail" />--}}
{{--											<button type="button" @click.prevent="showMediaPop(0)" class="popup_selector">--}}
{{--												{{ trans('message.select_image') }}--}}
{{--											</button>--}}
{{--										</div>--}}
{{--									</div>--}}
									<div v-if="form.id">
										<div class="col-lg-10">
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalQuestions">
												<i class="fa fa-plus"></i> Thêm câu hỏi
											</button>
											<table class="table table-bordered table-hover">
												<thead>
{{--												<th width="10"><input type="checkbox" v-model="selectAll2" ></th>--}}
												<th class="text-center">{{trans('examinationQuestions.title')}}</th>
												<th class="text-center">{{trans('examinationQuestions.description')}}</th>
												<th class="text-center">{{trans('Số thứ tự')}}</th>
												<th class="text-center">{{trans('Chỉnh sửa')}}</th>
												<th class="text-center">{{trans('Xoá')}}</th>
												</thead>
												<tbody>
												<tr v-for="question in questionSelected">
{{--													<td class="text-center">--}}
{{--														<input type="checkbox" name="checkbox[]" v-model="selected2" :value="question.id">--}}
{{--													</td>--}}
													<td>@{{ question.title }}</td>
													<td class="">@{{ question.description }}</td>
													<td class="text-center">
														<input type="text" v-model="question.order">
													</td>
													<td class="text-center" style="max-width: 20px;">
														<a :href="'/be/examination-questions/' + question.id" class="btn btn-sm btn-success" target="_blank">Sửa</a>
													</td>
													<td class="text-center" style="max-width: 20px;">
														<a href="javascript:void(0)" class="btn btn-sm btn-success" @click.prevent="deleteQuestionSelected(question.id)">Xoá</a>
													</td>
												</tr>
												</tbody>
											</table>
										</div>
										<!-- Modal -->
										<div class="modal fade" id="addModalQuestions" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
											<div class="modal-dialog pms-width-90" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h3 class="modal-title text-center">Câu hỏi</h3>
													</div>
													<div class="modal-body row">
														<div class="col-lg-10" style="margin: 10px">
															<input type="text" class="form-control" v-model="keyword" placeholder="Tìm kiếm">
														</div>
														<div class="col-lg-12">
															<table class="table table-bordered table-hover">
																<thead>
																<th width="10"><input type="checkbox" v-model="selectAll" ></th>
																<th>{{trans('examinationQuestions.title')}}</th>
																<th>{{trans('examinationQuestions.description')}}</th>
																</thead>
																<tbody>
																<tr v-for="question in questions">
																	<td class="text-center">
																		<input type="checkbox" name="checkbox[]" v-model="selected" :value="question.id">
																	</td>
																	<td>@{{ question.title }}</td>
																	<td class="">@{{ question.description }}</td>
																</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="modal-footer">
														<div style="margin-top: 10px">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															<button style="margin-bottom: 5px;"  class="btn btn-primary" @click.prevent="addQuestions">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<hr>
								</div>

								<div class="control-group form-group">
									<div class="col-sm-2"></div>
									<div class="col-sm-6 text-center js-page-btn">
										<a href="{{ route('examination.index') }}"
										   class="btn btn-large btn-default">
											<i class="fa fa-caret-left"></i>
											{{ trans('button.cancel') }}
										</a>
										<a v-if="form.id" href="javascript:void(0)" class="btn btn-large btn-info" @click.prevent="saveChanges">
											<i class="fa fa-location-arrow"></i>
											{{ trans('button.save') }}
										</a>
										<a v-else href="javascript:void(0)" class="btn btn-large btn-info" @click.prevent="saveChanges">
											<i class="fa fa-location-arrow"></i>
											{{ trans('Lưu & chọn câu hỏi') }}
										</a>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<!-- Use for insert media from file manager start -->
	{{--	<script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>--}}
	<script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
	<script src="{!! asset('backend/js/vue.js') !!}"></script>
	<script src="{!! asset('backend/js/axios.min.js') !!}"></script>
	<script src="https://unpkg.com/element-ui/lib/index.js"></script>
	<script type="text/javascript">
		var app = new Vue({
			el: '#app',
			data: {
				error: false, // check isset Error
				errors: {}, // object error (all error)
				message: '',
				errorMessage: '',
				examinationTypes: {!! json_encode($examinationTypes) !!},
				dataQuestions: {!! json_encode($questions) !!},
				questions: [],
				questionSelected: [],
				selected: [],
				selected2: [],
				keyword: "",
				form: {
					id: 0,
					examination_type_id: 1,
					title: "",
					explain: "",
					status: 1,
					thumbnail: "",
					data: [],
				},
			},
			mounted: function () {
				this.questions = this.dataQuestions;
				let examination = {!! json_encode($examination) !!};
				if (examination && typeof examination.id != "undefined") {
					this.form = {
						id: examination.id,
						title: examination.title,
						explain: examination.explain,
						status: examination.status,
						thumbnail: examination.thumbnail,
						examination_type_id: examination.examination_type_id,
						data: JSON.parse(examination.data),
					};
					this.modifyQuestionSelected();
				}
			},
			computed: {
				selectAll: {
					get: function () {
						return this.questions ? this.selected.length == this.questions.length : false;
					},
					set: function (value) {
						var selected = [];
						if (value) {
							this.questions.forEach(function (data) {
								selected.push(data.id);
							});
						}
						this.selected = selected;
					}
				},
				selectAll2: {
					get: function () {
						return this.questionSelected ? this.selected2.length == this.questionSelected.length : false;
					},
					set: function (value) {
						var selected2 = [];
						if (value) {
							this.questionSelected.forEach(function (data) {
								selected2.push(data.id);
							});
						}
						this.selected2 = selected2;
					}
				}
			},
			methods: {
				findQuestionIndex: function(id) {
					return this.questions.findIndex(function (question) {
						return question.id === id
					})
				},
				findDataIndex: function(id) {
					return this.form.data.findIndex(function (item) {
						return item.id === id
					})
				},
				findQuestionSelectedIndex: function(id) {
					return this.questionSelected.findIndex(function (item) {
						return item.id === id
					})
				},
				deleteQuestionSelected: function(id) {
					var result = confirm("Bạn muốn xoá?");
					if (!result) {
						return false;
					}
					let index = this.findQuestionSelectedIndex(id);
					this.questionSelected.splice(index, 1);
				},
				modifyQuestionSelected: function() {
					if(this.form.data.length) {
						this.form.data.forEach((item) => {
							let index = this.findQuestionIndex(item.id);
							this.questionSelected.push({
								id: this.questions[index].id,
								title: this.questions[index].title,
								description: this.questions[index].description,
								order: item.order,
							});
							this.questions.splice(index, 1);
						});
						console.log(this.questions);
						// Sort by order
						this.questionSelected.sort(function (a, b) {
							return a.order - b.order;
						});
					}
				},
				modifyFormData() {
					this.questionSelected.forEach((item) => {
						let index = this.findDataIndex(item.id);
						this.form.data[index] = {
							id: item.id,
							order: item.order,
						};
						this.questions.splice(index, 1);
					});
				},
				saveChanges: function () {
					$(".ot-loading").show()
					this.modifyFormData();
					console.log(this.form);
					axios.post("{{ route('examination.save') }}", this.form)
						.then((response) => {
							this.error = false
							this.errors = {}
							this.message = response.data.message
							$(".ot-loading").hide()
							location.replace(response.data.url_update)
						}).catch((error) => {
						$('body,html').animate({
							scrollTop: 0
						}, 500)
						$(".ot-loading").hide()
						this.error = true
						var errors = error.response.data.errors
						if (typeof (errors) === 'object') {
							this.errors = errors
							this.errorMessage = ''
						} else {
							this.errors = {}
							this.errorMessage = errors.message
						}
					})
				},
				showMediaPop: function (index) {
					var updateID = index; // Btn id clicked
					var elfinderUrl = '/elfinder/popup/';

					// trigger the reveal modal with elfinder inside
					var triggerUrl = elfinderUrl + updateID;
					$.colorbox({
						href: triggerUrl,
						fastIframe: true,
						iframe: true,
						width: '70%',
						height: '80%'
					});
				},
				searchQuestions: function(){
					let keyword = $("[name='keyword']").val();
					let params = {
						keyword: keyword,
					};
					$(".ot-loading").show();
					axios.get("{{route('examinationQuestion.search')}}", {params: params})
						.then((response)=>{
							console.log(response.data.questions);
							this.questions = response.data.questions;
							$(".ot-loading").hide();
						}).catch((err)=>{
						console.log(err);
					});
				},
				addQuestions: function(){
					if(!this.selected.length) {
						alert('Hãy chọn cậu hỏi');
					}
					let data = [];
					this.selected.forEach((id, index) =>{
						this.form.data.push({
							id:id,
							order:this.form.data.length + 1
						});
					});
					this.saveChanges();
				}
			},
			watch: {
				'keyword': function() {
					if(this.keyword) {
						this.questions = this.questions.filter((row) => {
							return row.title.includes(this.keyword);
						});
					}
					else {
						this.questions = this.dataQuestions;
					}
				}
			}
		});

		// function to update the file selected by elfinder
		function processSelectedFile(filePath, requestingField) {
			$path = '/' + filePath;
			$path = $path.replace('\\', '/'); // format file path
			$('#' + requestingField).val(filePath);
			app.form.thumbnail = $path;
		}

	</script>
@stop
