<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinations translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinations' => 'Đề thi',
    'examinations_index' => 'Đề thi',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'title' => 'Tiêu đề',
	'data' => 'Câu hỏi',
	'status' => 'Trạng thái',
	'thumbnail' => 'Hình ảnh',
	'explain' => 'Giải thích',

	'lang' => 'Lang',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
