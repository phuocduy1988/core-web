<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinations translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinations' => 'Examinations',
    'examinations_index' => 'Examinations',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'title' => 'Title',
	'data' => 'Data',
	'status' => 'Status',
	'thumbnail' => 'Thumbnail',

	'explain' => 'Explain',

	'lang' => 'lang',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
