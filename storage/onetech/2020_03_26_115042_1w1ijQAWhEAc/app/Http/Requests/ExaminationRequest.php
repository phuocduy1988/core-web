<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-25
 * Time: 23:15:16
 * File: ExaminationRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExaminationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:191'],
			'status' => ['nullable', 'boolean'],
			'thumbnail' => ['nullable', 'max:191'],

			'explain' => ['required'],

			'lang' => ['nullable', 'max:191'],

			'exam_time' => ['nullable', 'max:191'],

			'show_suggest' => ['nullable', 'boolean'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => trans('validation.required', ['attribute' => trans('examinations.title')]),
			'explain.required' => trans('validation.required', ['attribute' => trans('examinations.explain')]),
			'thumbnail.required' => trans('validation.required', ['attribute' => trans('examinations.thumbnail')]),
        ];
    }
}
