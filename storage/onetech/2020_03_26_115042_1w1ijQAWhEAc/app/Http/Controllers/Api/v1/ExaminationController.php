<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-25
 * Time: 23:15:16
 * File: ExaminationController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\Examination;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ExaminationRequest;
use App\Http\Resources\ExaminationCollection;
use App\Http\Resources\Examination as ExaminationResource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class ExaminationController extends ApiBaseController
{
    /**
     * Display a listing of the examinations.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $examinations = Examination::query();
            //Generate search option here
            //Get data with pagination and order by(Default:: id desc)
            $examinations = $examinations
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($examinations);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created examinations in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'title' => ['required', 'max:191'],
			'data' => ['required'],
			'status' => ['nullable', 'boolean'],
			'thumbnail' => ['required', 'max:191'],

            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('title', '').$request->get('data', '').$request->get('status', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new Examination
            $examination = new Examination();
            $examination->title = $request->get('title');
			$examination->data = $request->get('data');
			$examination->status = $request->get('status', 0);
			if ($request->hasFile('thumbnail')) {
				$examination->thumbnail = MediaHelper::uploadFileToServer($request->file('thumbnail'), '/examinations/');
			} else {
				$examination->thumbnail = $request->get('thumbnail');
			}

			$examination->explain = $request->get('explain');

			$examination->lang = $request->get('lang');

			$examination->exam_time = $request->get('exam_time');

			$examination->show_suggest = $request->get('show_suggest', 0);

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examination->save();

            //Check response data
            $responseCode = is_null($examination->id) ? 201 : 200;
            return $this->jsonOK(['data' => $examination], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update examinations in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('title', '').$request->get('data', '').$request->get('status', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examination = Examination::find($id);
            if(is_null($examination)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('title')) {
				$examination->title = $request->get('title');
			}
			if ($request->get('data')) {
				$examination->data = $request->get('data');
			}
			if ($request->get('status')) {
				$examination->status = $request->get('status');
			}
			if ($request->hasFile('thumbnail')) {
				$examination->thumbnail = MediaHelper::uploadFileToServer($request->file('thumbnail'), '/examinations/');
			} else {
				if ($request->get('thumbnail')) {
					$examination->thumbnail = $request->get('thumbnail');
				}
			}

			if ($request->get('explain')) {
				$examination->explain = $request->get('explain');
			}

			if ($request->get('lang')) {
				$examination->lang = $request->get('lang');
			}

			if ($request->get('exam_time')) {
				$examination->exam_time = $request->get('exam_time');
			}

			if ($request->get('show_suggest')) {
				$examination->show_suggest = $request->get('show_suggest');
			}

            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examination->save();

            return $this->jsonOK(['data' => $examination]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified examinations from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examination = Examination::find($id);
            if(!is_null($examination)) {
                $examination->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examination = Examination::withTrashed()->find($id);
            if(!is_null($examination)) {
                $examination->restore();
                return $this->jsonOK(['data' => $examination]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examination = Examination::withTrashed()->find($id);
            if(!is_null($examination)) {
                $examination->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
