<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-25
 * Time: 23:15:16
 * File: ExaminationController.php
 */

namespace App\Http\Controllers\Backend;

use App\Models\Examination;
use App\Models\ExaminationQuestion;
use Illuminate\Http\Request;
use App\Http\Requests\ExaminationRequest;
use App\Http\Controllers\Backend\Controller;

class ExaminationController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		try {
			//Get param page
			$page = $request->get('page', 1);
			//Get data from position
			$offSet = ($page - 1) * $this->_gridPageSize;
			
			$examinations = Examination::query();
			
			$examinations = $examinations->with('examinationType');
			
			if ($request->has('keyword')) {
				//Search via param key word
				$examinations = $examinations->where(
					function ($query) use ($request) {
						/** @var $query Illuminate\Database\Query\Builder
						 * return $query->where()->orWhere()->orWhere()
						 * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
						 *
						 * @Please do not remove search id
						 */
						return $query
							->where('id', 'LIKE', '%' . $request->get('keyword') . '%')

							->orWhere('exam_time', 'LIKE', '%' . $request->get('keyword') . '%')
							->orWhere('title', 'LIKE', '%' . $request->get('keyword') . '%');
					}
				);
			}
			//Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
			$examinations = $examinations->orderBy($this->_orderBy, $this->_sortType);
			
			//Get data with pagination
			$examinations = $examinations
				->skip($offSet)
				->take($this->_gridPageSize)
				->paginate($this->_gridPageSize);
			return view('backend.examinations.index')->with('examinationsData', $examinations);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function form(Examination $examination = null)
	{
		try {
			//Get data for create new or update
			$examination = $examination ? : new Examination;
			return view('backend.examinations.form')
				->with('examination', $examination)
				->with('questions', ExaminationQuestion::where('status', 1)->get())
				->with('examinationTypes', \App\Models\ExaminationType::where('status', 1)->get());
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Store a newly created resource in storage.
	 * Check validation via ExaminationRequest
	 *
	 * @param ExaminationRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function post(ExaminationRequest $request)
	{
		try {
			$isUpdate = false;
			//Get data for create new or update via id
			$examination = Examination::firstOrNew(['id' => $request->get('id')]);
			
			if (isset($examination->id)) {
				$isUpdate = true;
			}
			$data = $request->get('data', []);
			$examination->title = $request->get('title');
			$examination->data = json_encode($data, JSON_UNESCAPED_UNICODE);
			$examination->status = $request->get('status', 0);
			$examination->thumbnail = $request->get('thumbnail');
			$examination->explain = $request->get('explain');
			$examination->lang = $request->get('lang');
			$examination->examination_type_id = $request->get('examination_type_id');
			$examination->exam_time = $request->get('exam_time');
			$examination->save();
			return $this->jsonOK(['is_update' => $isUpdate, 'url_update' => route('examination.form', [$examination->id])]);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return $this->jsonNG($e->getMessage());
		}
	}
	
	/**
	 * Remove the specified resource from database.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete(Examination $examination)
	{
		try {
			$examination->delete();
			$message = trans('message.deleted_success');
			return redirect()->back()->with('alert', $message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
	
	/**
	 * Restore the specified deleted resource to database.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function restore($examinationId)
	{
		try {
			$examination = Examination::withTrashed()->find($examinationId);
			$examination->restore();
			$message = 'examination Restored.';
			return redirect()->back()->with('alert', $message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
		
	}
	
	/**
	 * Delete forever
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function forceDelete($examinationId)
	{
		try {
			$examination = Examination::withTrashed()->find($examinationId);
			$examination->forceDelete();
			$message = 'examination permanently deleted.';
			return redirect()->back()->withMessage($message);
		}
		catch (\Exception $e) {
			\Helper::writeLogException($e);
			return view('errors.500')->withErrors($e->getMessage());
		}
	}
}
