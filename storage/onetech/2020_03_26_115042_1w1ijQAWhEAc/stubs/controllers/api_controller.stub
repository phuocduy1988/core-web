<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: {{DATE}}
 * Time: {{TIME}}
 * File: {{CONTROLLER_CLASS}}.php
 */
namespace App\Http\Controllers\Api\v1{{GENERAL_NAMESPACE}};

use App\Models\{{MODEL_CLASS}};
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\{{GENERAL_NAMESPACE}}{{REQUEST_CLASS}};
use App\Http\Resources\{{GENERAL_NAMESPACE}}{{MODEL_CLASS}}Collection;
use App\Http\Resources\{{GENERAL_NAMESPACE}}{{MODEL_CLASS}} as {{MODEL_CLASS}}Resource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class {{CONTROLLER_CLASS}} extends ApiBaseController
{
    /**
     * Display a listing of the {{MODEL_ENTITIES}}.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            ${{MODEL_ENTITIES}} = {{MODEL_CLASS}}::query();
            //Generate search option here
            if ($request->has('keyword')) {
                //Search via param key word
                ${{MODEL_ENTITIES}} = ${{MODEL_ENTITIES}}->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            //SEARCH_STATEMENTS ;
                });
            }
            //Get data with pagination and order by(Default:: id desc)
            ${{MODEL_ENTITIES}} = ${{MODEL_ENTITIES}}
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK(${{MODEL_ENTITIES}});
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created {{MODEL_ENTITIES}} in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                //API_RULES

                //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                //HASH_STRING
                //--UPDATE_HASH_STRING_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new {{MODEL_CLASS}}
            ${{MODEL_ENTITY}} = new {{MODEL_CLASS}}();
            //POST_STATEMENTS

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ${{MODEL_ENTITY}}->save();

            //Check response data
            $responseCode = is_null(${{MODEL_ENTITY}}->id) ? 201 : 200;
            return $this->jsonOK(['data' => ${{MODEL_ENTITY}}], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update {{MODEL_ENTITIES}} in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                //HASH_STRING
                //--UPDATE_HASH_STRING_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            ${{MODEL_ENTITY}} = {{MODEL_CLASS}}::find($id);
            if(is_null(${{MODEL_ENTITY}})) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            //API_UPDATE_POST_STATEMENTS
            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            ${{MODEL_ENTITY}}->save();

            return $this->jsonOK(['data' => ${{MODEL_ENTITY}}]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified {{MODEL_ENTITIES}} from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            ${{MODEL_ENTITY}} = {{MODEL_CLASS}}::find($id);
            if(!is_null(${{MODEL_ENTITY}})) {
                ${{MODEL_ENTITY}}->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }
}
