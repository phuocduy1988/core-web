<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: {{DATE}}
 * Time: {{TIME}}
 * File: {{CONTROLLER_CLASS}}.php
 */
namespace App\Http\Controllers\Backend{{GENERAL_NAMESPACE}};

use App\Models\{{MODEL_CLASS}};
use Illuminate\Http\Request;
use App\Http\Requests\{{GENERAL_NAMESPACE}}{{REQUEST_CLASS}};
use App\Http\Controllers\Backend\Controller;

class {{CONTROLLER_CLASS}} extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            ${{MODEL_ENTITIES}} = {{MODEL_CLASS}}::query();

            if ($request->has('keyword')) {
                //Search via param key word
                ${{MODEL_ENTITIES}} = ${{MODEL_ENTITIES}}->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            //SEARCH_STATEMENTS ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            ${{MODEL_ENTITIES}} = ${{MODEL_ENTITIES}}->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            ${{MODEL_ENTITIES}} = ${{MODEL_ENTITIES}}
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.{{MODEL_ENTITIES}}.index')->with('{{MODEL_ENTITIES}}Data', ${{MODEL_ENTITIES}});
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form({{MODEL_CLASS}} ${{MODEL_ENTITY}} = null)
    {
        try {
            //Get data for create new or update
            ${{MODEL_ENTITY}} = ${{MODEL_ENTITY}} ?: new {{MODEL_CLASS}};
            return view('backend.{{MODEL_ENTITIES}}.form')->with('{{MODEL_ENTITY}}', ${{MODEL_ENTITY}});
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via {{REQUEST_CLASS}}
     * @param  {{REQUEST_CLASS}}  $request
     * @return \Illuminate\Http\Response
     */
    public function post({{REQUEST_CLASS}} $request)
    {
        try {
            $isUpdate = false;
            //Get data for create new or update via id
            ${{MODEL_ENTITY}} = {{MODEL_CLASS}}::firstOrNew(['id' => $request->get('id')]);

            if(isset(${{MODEL_ENTITY}}->id)) {
                $isUpdate = true;
            }

            //POST_STATEMENTS
            //--UPDATE_GENCODE_POST_DO_NOT_DELETE_OR_CHANGE_THIS--
            ${{MODEL_ENTITY}}->save();

            $message = $isUpdate ? trans('message.updated_success') : trans('message.created_success');
            return redirect()->route('{{MODEL_ENTITY}}.index')->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete({{MODEL_CLASS}} ${{MODEL_ENTITY}})
    {
        try {
           ${{MODEL_ENTITY}}->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(${{MODEL_ENTITY}}Id)
    {
        try {
            ${{MODEL_ENTITY}} = {{MODEL_CLASS}}::withTrashed()->find(${{MODEL_ENTITY}}Id);
            ${{MODEL_ENTITY}}->restore();
            $message = '{{MODEL_ENTITY}} Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(${{MODEL_ENTITY}}Id)
    {
        try {
            ${{MODEL_ENTITY}} = {{MODEL_CLASS}}::withTrashed()->find(${{MODEL_ENTITY}}Id);
            ${{MODEL_ENTITY}}->forceDelete();
            $message = '{{MODEL_ENTITY}} permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
