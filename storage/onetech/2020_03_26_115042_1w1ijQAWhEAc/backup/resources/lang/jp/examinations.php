<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinations translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinations' => 'Examinations',
    'examinations_index' => 'Examinations',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'examinations_index' => 'Examinations',
    'id' => 'ID',
	'title' => 'Title',
	'data' => 'Data',
	'status' => 'Status',
	'thumbnail' => 'Thumbnail',

	'explain' => 'Giải thích',

	'lang' => 'lang',

	'exam_time' => 'Thời gian',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
