<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationQuestions translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationQuestions' => 'Câu hỏi',
    'examinationQuestions_index' => 'Câu hỏi',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'title' => 'Tiêu đề',
	'description' => 'Mô tả',
	'status' => 'Trạng thái',
	'multi_choices' => 'Trắc nghiệm',
	'image' => 'Hình ảnh',
	
    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
