@extends('backend.layouts.app')

@section('title') {{ trans('examinationQuestions.examinationQuestions') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

@include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('examinationQuestions.examinationQuestions'), 'link' => route('examinationQuestion.index')]
    	)])

	<div class="wrapper wrapper-content animated fadeInRight clearfix">
		{!! Form::open(['method' => 'post', 'route' => 'examinationQuestion.save', 'files'=>true]) !!}
		{!! Form::hidden('id', $examinationQuestion->id) !!}
		<div class="row">
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
				    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.title') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea name="title" class="form-control">{!! old('title', isset($examinationQuestion->title) ? $examinationQuestion->title : '' ) !!}</textarea>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.description') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea name="description" class="form-control">{!! old('description', isset($examinationQuestion->description) ? $examinationQuestion->description : '' ) !!}</textarea>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.status') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" name="status" value="1" @if(isset($examinationQuestion->status)? $examinationQuestion->status : 0) checked @endif>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.multi_choices') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea name="multi_choices" class="form-control">{!! old('multi_choices', isset($examinationQuestion->multi_choices) ? $examinationQuestion->multi_choices : '' ) !!}</textarea>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('examinationQuestions.image') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="image" type="text" value="{{ old('image', isset($examinationQuestion->image) ? $examinationQuestion->image : '' ) }}">
                                    </div>
                                </div>

{{-- --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                <hr />
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 text-center js-page-btn">
                                        <a href="{{ route('examinationQuestion.index') }}" class="btn btn-large btn-default">
                                        <i class="fa fa-caret-left"></i>
                                        {{ trans('button.cancel') }}
                                        </a>
                                        <button type="submit" action="confirm" class="btn btn-large btn-info">
                                        <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    <script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <!-- Use for insert media from file manager end -->
    <script src="{!! asset('backend/js/media.js') !!}"></script>
    <!-- Date time picker start -->
    <script src="{!! asset('backend/plugins/datetimepicker/moment-with-locales.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('backend/js/datetime-validation.js') !!}"></script>
    <!-- Date time picker end -->
    <script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('js-editor');
        });
        $('#js-viewdate').datetimepicker({
            format: 'YYYY/MM/DD HH:mm',
            locale: 'ja'
        });
    </script>
@stop
