<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo 'You do not have permission!';
});

// Login
Route::get('login',['uses' => 'LoginController@showLoginForm'])->name('backend.login');
Route::post('login',['uses' => 'LoginController@postLogin']);
Route::any('logout',['uses' => 'LoginController@logout'])->name('backend.logout');


Route::group(['middleware' => 'auth.admin'], function ()
{
	//Home
	Route::any('/home', 'CommonController@home')->name('common.home');
	//Load iframe
	Route::any('/load-iframe', 'CommonController@iframe')->name('common.iframe');
	//Change status
	Route::any('/change-boolean', 'CommonController@changeBoolean')->name('common.change-boolean');

	/*================================================
	   Export
	================================================*/
	Route::any('/export/members', 'ExportController@exportMembers')->name('export.exportMembers');
	Route::any('/export/members/exam', 'ExportController@exportMembersExam')->name('export.exportMembersExam');


	//Auto generation here

	/*================================================
		   Activity Log
		================================================*/
	Route::any('/logs/activities', 'LogController@logActivity')->name('log.logActivity');
	Route::any('/logs/activities/list', 'LogController@getLogActivity')->name('log.getLogActivity');

	/*================================================
        Setting Route
    ================================================*/
	Route::any('/settings', 'SettingController@index')->name('setting.index');
	Route::get('/clear-caches', 'SettingController@clearCaches')->name('setting.clearCaches');
	Route::post('/settings/save', 'SettingController@post')->name('setting.save');
	Route::post('/settings/delete', 'SettingController@delete')->name('setting.delete');

	/*================================================
        User Route
    ================================================*/
	Route::any('/users', 'UserController@index')->name('user.index');

	Route::get('/users/new', 'UserController@form')->name('user.new');
	Route::get('/users/{user}', 'UserController@form')->name('user.form');
	Route::post('/users/save', 'UserController@post')->name('user.save');
	Route::get('/users/delete/{user}', 'UserController@delete')->name('user.delete');
	Route::get('/users/restore/{user}', 'UserController@restore')->name('user.restore');
	Route::get('/users/force-delete/{user}', 'UserController@forceDelete')->name('user.force-delete');

    /*================================================
        Member Route
    ================================================*/
    Route::any('/members', 'MemberController@index')->name('member.index');

    Route::get('/members/new', 'MemberController@form')->name('member.new');
    Route::get('/members/{member}', 'MemberController@form')->name('member.form');
    Route::post('/members/save', 'MemberController@post')->name('member.save');
    Route::get('/members/delete/{member}', 'MemberController@delete')->name('member.delete');
    Route::get('/members/restore/{member}', 'MemberController@restore')->name('member.restore');
    Route::get('/members/force-delete/{member}', 'MemberController@forceDelete')->name('member.force-delete');
	
	/*================================================
	 MemberExamInfo Route
 ================================================*/
	Route::any('/member-exam-infos', 'MemberExamInfoController@index')->name('memberExamInfo.index');
	Route::any('/member-exam-infos/update-data', 'MemberExamInfoController@updateData')->name('memberExamInfo.updateData');
	
	Route::get('/member-exam-infos/new', 'MemberExamInfoController@form')->name('memberExamInfo.new');
	Route::get('/member-exam-infos/{memberExamInfo}', 'MemberExamInfoController@form')->name('memberExamInfo.form');
	Route::post('/member-exam-infos/save', 'MemberExamInfoController@post')->name('memberExamInfo.save');
	Route::get('/member-exam-infos/delete/{memberExamInfo}', 'MemberExamInfoController@delete')->name('memberExamInfo.delete');
	Route::get('/member-exam-infos/restore/{memberExamInfo}', 'MemberExamInfoController@restore')->name('memberExamInfo.restore');
	Route::get('/member-exam-infos/force-delete/{memberExamInfo}', 'MemberExamInfoController@forceDelete')->name('memberExamInfo.force-delete');
	
	
	/*================================================
		Examination Route
	================================================*/
    Route::any('/examinations', 'ExaminationController@index')->name('examination.index');

    Route::get('/examinations/new', 'ExaminationController@form')->name('examination.new');
    Route::get('/examinations/{examination}', 'ExaminationController@form')->name('examination.form');
    Route::post('/examinations/save', 'ExaminationController@post')->name('examination.save');
    Route::get('/examinations/delete/{examination}', 'ExaminationController@delete')->name('examination.delete');
    Route::get('/examinations/restore/{examination}', 'ExaminationController@restore')->name('examination.restore');
    Route::get('/examinations/force-delete/{examination}', 'ExaminationController@forceDelete')->name('examination.force-delete');
    
    


    /*================================================
        ExaminationType Route
    ================================================*/
    Route::any('/examination-types', 'ExaminationTypeController@index')->name('examinationType.index');

    Route::get('/examination-types/new', 'ExaminationTypeController@form')->name('examinationType.new');
    Route::get('/examination-types/{examinationType}', 'ExaminationTypeController@form')->name('examinationType.form');
    Route::post('/examination-types/save', 'ExaminationTypeController@post')->name('examinationType.save');
    Route::get('/examination-types/delete/{examinationType}', 'ExaminationTypeController@delete')->name('examinationType.delete');
    Route::get('/examination-types/restore/{examinationType}', 'ExaminationTypeController@restore')->name('examinationType.restore');
    Route::get('/examination-types/force-delete/{examinationType}', 'ExaminationTypeController@forceDelete')->name('examinationType.force-delete');


    /*================================================
        ExaminationResult Route
    ================================================*/
    Route::any('/examination-results', 'ExaminationResultController@index')->name('examinationResult.index');

    Route::get('/examination-results/new', 'ExaminationResultController@form')->name('examinationResult.new');
    Route::get('/examination-results/{examinationResult}', 'ExaminationResultController@form')->name('examinationResult.form');
    Route::post('/examination-results/save', 'ExaminationResultController@post')->name('examinationResult.save');
    Route::get('/examination-results/delete/{examinationResult}', 'ExaminationResultController@delete')->name('examinationResult.delete');
    Route::get('/examination-results/restore/{examinationResult}', 'ExaminationResultController@restore')->name('examinationResult.restore');
    Route::get('/examination-results/force-delete/{examinationResult}', 'ExaminationResultController@forceDelete')->name('examinationResult.force-delete');


    /*================================================
        ExaminationAnswer Route
    ================================================*/
    Route::any('/examination-answers', 'ExaminationAnswerController@index')->name('examinationAnswer.index');

    Route::get('/examination-answers/new', 'ExaminationAnswerController@form')->name('examinationAnswer.new');
    Route::get('/examination-answers/{examinationAnswer}', 'ExaminationAnswerController@form')->name('examinationAnswer.form');
    Route::post('/examination-answers/save', 'ExaminationAnswerController@post')->name('examinationAnswer.save');
    Route::get('/examination-answers/delete/{examinationAnswer}', 'ExaminationAnswerController@delete')->name('examinationAnswer.delete');
    Route::get('/examination-answers/restore/{examinationAnswer}', 'ExaminationAnswerController@restore')->name('examinationAnswer.restore');
    Route::get('/examination-answers/force-delete/{examinationAnswer}', 'ExaminationAnswerController@forceDelete')->name('examinationAnswer.force-delete');


    /*================================================
        News Route
    ================================================*/
    Route::any('/news', 'NewsController@index')->name('news.index');

    Route::get('/news/new', 'NewsController@form')->name('news.new');
    Route::get('/news/{news}', 'NewsController@form')->name('news.form');
    Route::post('/news/save', 'NewsController@post')->name('news.save');
    Route::get('/news/delete/{news}', 'NewsController@delete')->name('news.delete');
    Route::get('/news/restore/{news}', 'NewsController@restore')->name('news.restore');
    Route::get('/news/force-delete/{news}', 'NewsController@forceDelete')->name('news.force-delete');


    /*================================================
        NewsCategory Route
    ================================================*/
    Route::any('/news-categories', 'NewsCategoryController@index')->name('newsCategory.index');

    Route::get('/news-categories/new', 'NewsCategoryController@form')->name('newsCategory.new');
    Route::get('/news-categories/{newsCategory}', 'NewsCategoryController@form')->name('newsCategory.form');
    Route::post('/news-categories/save', 'NewsCategoryController@post')->name('newsCategory.save');
    Route::get('/news-categories/delete/{newsCategory}', 'NewsCategoryController@delete')->name('newsCategory.delete');
    Route::get('/news-categories/restore/{newsCategory}', 'NewsCategoryController@restore')->name('newsCategory.restore');
    Route::get('/news-categories/force-delete/{newsCategory}', 'NewsCategoryController@forceDelete')->name('newsCategory.force-delete');

    /*================================================
        Page Route
    ================================================*/
    Route::any('/pages', 'PageController@index')->name('page.index');

    Route::get('/pages/new', 'PageController@form')->name('page.new');
    Route::get('/pages/{page}', 'PageController@form')->name('page.form');
    Route::post('/pages/save', 'PageController@post')->name('page.save');
    Route::get('/pages/delete/{page}', 'PageController@delete')->name('page.delete');
    Route::get('/pages/restore/{page}', 'PageController@restore')->name('page.restore');
    Route::get('/pages/force-delete/{page}', 'PageController@forceDelete')->name('page.force-delete');


    /*================================================
        Menu Route
    ================================================*/
    Route::any('/menus', 'MenuController@index')->name('menu.index');

    Route::get('/menus/new', 'MenuController@form')->name('menu.new');
    Route::get('/menus/{menu}', 'MenuController@form')->name('menu.form');
    Route::post('/menus/save', 'MenuController@post')->name('menu.save');
    Route::get('/menus/delete/{menu}', 'MenuController@delete')->name('menu.delete');
    Route::get('/menus/restore/{menu}', 'MenuController@restore')->name('menu.restore');
    Route::get('/menus/force-delete/{menu}', 'MenuController@forceDelete')->name('menu.force-delete');


    /*================================================
        MenuType Route
    ================================================*/
    Route::any('/menu-types', 'MenuTypeController@index')->name('menuType.index');

    Route::get('/menu-types/new', 'MenuTypeController@form')->name('menuType.new');
    Route::get('/menu-types/{menuType}', 'MenuTypeController@menu')->name('menuType.menu');
    Route::post('/menu-types/update-menu', 'MenuTypeController@updateMenu')->name('menuType.updateMenu');
    Route::post('/menu-types/update-menu-tree', 'MenuTypeController@updateMenuTree')->name('menuType.updateMenuTree');
    Route::post('/menu-types/save', 'MenuTypeController@post')->name('menuType.save');
    Route::get('/menu-types/delete/{menuType}', 'MenuTypeController@delete')->name('menuType.delete');
    Route::get('/menu-types/restore/{menuType}', 'MenuTypeController@restore')->name('menuType.restore');
    Route::get('/menu-types/force-delete/{menuType}', 'MenuTypeController@forceDelete')->name('menuType.force-delete');


    /*================================================
        Banner Route
    ================================================*/
    Route::any('/banners', 'BannerController@index')->name('banner.index');

    Route::get('/banners/new', 'BannerController@form')->name('banner.new');
    Route::get('/banners/{banner}', 'BannerController@form')->name('banner.form');
    Route::post('/banners/save', 'BannerController@post')->name('banner.save');
    Route::get('/banners/delete/{banner}', 'BannerController@delete')->name('banner.delete');
    Route::get('/banners/restore/{banner}', 'BannerController@restore')->name('banner.restore');
    Route::get('/banners/force-delete/{banner}', 'BannerController@forceDelete')->name('banner.force-delete');


    /*================================================
        SampleHtmlType Route
    ================================================*/
    Route::any('/sample-html-types', 'SampleHtmlTypeController@index')->name('sampleHtmlType.index');

    Route::get('/sample-html-types/new', 'SampleHtmlTypeController@form')->name('sampleHtmlType.new');
    Route::get('/sample-html-types/{sampleHtmlType}', 'SampleHtmlTypeController@form')->name('sampleHtmlType.form');
    Route::post('/sample-html-types/save', 'SampleHtmlTypeController@post')->name('sampleHtmlType.save');
    Route::get('/sample-html-types/delete/{sampleHtmlType}', 'SampleHtmlTypeController@delete')->name('sampleHtmlType.delete');
    Route::get('/sample-html-types/restore/{sampleHtmlType}', 'SampleHtmlTypeController@restore')->name('sampleHtmlType.restore');
    Route::get('/sample-html-types/force-delete/{sampleHtmlType}', 'SampleHtmlTypeController@forceDelete')->name('sampleHtmlType.force-delete');
	
	
	/*================================================
		SampleHtml Route
	================================================*/
	Route::any('/sample-htmls', 'SampleHtmlController@index')->name('sampleHtml.index');
	
	Route::get('/sample-htmls/new', 'SampleHtmlController@form')->name('sampleHtml.new');
	Route::get('/sample-htmls/{sampleHtml}', 'SampleHtmlController@form')->name('sampleHtml.form');
	Route::post('/sample-htmls/save', 'SampleHtmlController@post')->name('sampleHtml.save');
	Route::get('/sample-htmls/delete/{sampleHtml}', 'SampleHtmlController@delete')->name('sampleHtml.delete');
	Route::get('/sample-htmls/restore/{sampleHtml}', 'SampleHtmlController@restore')->name('sampleHtml.restore');
	Route::get('/sample-htmls/force-delete/{sampleHtml}', 'SampleHtmlController@forceDelete')->name('sampleHtml.force-delete');
	
	
	/*================================================
		MemberGiveBook Route
	================================================*/
    Route::any('/member-give-books', 'MemberGiveBookController@index')->name('memberGiveBook.index');

    Route::get('/member-give-books/new', 'MemberGiveBookController@form')->name('memberGiveBook.new');
    Route::get('/member-give-books/{memberGiveBook}', 'MemberGiveBookController@form')->name('memberGiveBook.form');
    Route::post('/member-give-books/save', 'MemberGiveBookController@post')->name('memberGiveBook.save');
    Route::get('/member-give-books/delete/{memberGiveBook}', 'MemberGiveBookController@delete')->name('memberGiveBook.delete');
    Route::get('/member-give-books/restore/{memberGiveBook}', 'MemberGiveBookController@restore')->name('memberGiveBook.restore');
    Route::get('/member-give-books/force-delete/{memberGiveBook}', 'MemberGiveBookController@forceDelete')->name('memberGiveBook.force-delete');

    /*================================================
        Country Route
    ================================================*/
    Route::any('/countries', 'CountryController@index')->name('country.index');

    Route::get('/countries/new', 'CountryController@form')->name('country.new');
    Route::get('/countries/{country}', 'CountryController@form')->name('country.form');
    Route::post('/countries/save', 'CountryController@post')->name('country.save');
    Route::get('/countries/delete/{country}', 'CountryController@delete')->name('country.delete');
    Route::get('/countries/restore/{country}', 'CountryController@restore')->name('country.restore');
    Route::get('/countries/force-delete/{country}', 'CountryController@forceDelete')->name('country.force-delete');


    /*================================================
        Province Route
    ================================================*/
    Route::any('/provinces', 'ProvinceController@index')->name('province.index');

    Route::get('/provinces/new', 'ProvinceController@form')->name('province.new');
    Route::get('/provinces/{province}', 'ProvinceController@form')->name('province.form');
    Route::post('/provinces/save', 'ProvinceController@post')->name('province.save');
    Route::get('/provinces/delete/{province}', 'ProvinceController@delete')->name('province.delete');
    Route::get('/provinces/restore/{province}', 'ProvinceController@restore')->name('province.restore');
    Route::get('/provinces/force-delete/{province}', 'ProvinceController@forceDelete')->name('province.force-delete');

    /*================================================
        RefUrl Route
    ================================================*/
    Route::any('/ref-urls', 'RefUrlController@index')->name('refUrl.index');
    Route::any('/ref-urls/short-url', 'RefUrlController@shortUrl')->name('refUrl.shortUrl');

    Route::get('/ref-urls/new', 'RefUrlController@form')->name('refUrl.new');
    Route::get('/ref-urls/{refUrl}', 'RefUrlController@form')->name('refUrl.form');
    Route::post('/ref-urls/save', 'RefUrlController@post')->name('refUrl.save');
    Route::post('/ref-urls/delete', 'RefUrlController@delete')->name('refUrl.delete');
    Route::get('/ref-urls/restore/{refUrl}', 'RefUrlController@restore')->name('refUrl.restore');
    Route::get('/ref-urls/force-delete/{refUrl}', 'RefUrlController@forceDelete')->name('refUrl.force-delete');
	
	
	/*================================================
		MemberRefSource Route
	================================================*/
	Route::any('/member-ref-sources', 'MemberRefSourceController@index')->name('memberRefSource.index');
	
	Route::get('/member-ref-sources/new', 'MemberRefSourceController@form')->name('memberRefSource.new');
	Route::get('/member-ref-sources/{memberRefSource}', 'MemberRefSourceController@form')->name('memberRefSource.form');
	Route::post('/member-ref-sources/save', 'MemberRefSourceController@post')->name('memberRefSource.save');
	Route::get('/member-ref-sources/delete/{memberRefSource}', 'MemberRefSourceController@delete')->name('memberRefSource.delete');
	Route::get('/member-ref-sources/restore/{memberRefSource}', 'MemberRefSourceController@restore')->name('memberRefSource.restore');
	Route::get('/member-ref-sources/force-delete/{memberRefSource}', 'MemberRefSourceController@forceDelete')->name('memberRefSource.force-delete');
	
	
	/*================================================
		RefSource Route
	================================================*/
    Route::any('/ref-sources', 'RefSourceController@index')->name('refSource.index');
    Route::any('/ref-sources/first', 'RefSourceController@getRefSource')->name('refSource.first');

    Route::get('/ref-sources/new', 'RefSourceController@form')->name('refSource.new');
    Route::get('/ref-sources/{refSource}', 'RefSourceController@form')->name('refSource.form');
    Route::post('/ref-sources/save', 'RefSourceController@post')->name('refSource.save');
    Route::get('/ref-sources/delete/{refSource}', 'RefSourceController@delete')->name('refSource.delete');
    Route::get('/ref-sources/restore/{refSource}', 'RefSourceController@restore')->name('refSource.restore');
    Route::get('/ref-sources/force-delete/{refSource}', 'RefSourceController@forceDelete')->name('refSource.force-delete');


    /*================================================
        MemberConsulting Route
    ================================================*/
    Route::any('/member-consultings', 'MemberConsultingController@index')->name('memberConsulting.index');

    Route::get('/member-consultings/new', 'MemberConsultingController@form')->name('memberConsulting.new');
    Route::get('/member-consultings/{memberConsulting}', 'MemberConsultingController@form')->name('memberConsulting.form');
    Route::post('/member-consultings/save', 'MemberConsultingController@post')->name('memberConsulting.save');
    Route::get('/member-consultings/delete/{memberConsulting}', 'MemberConsultingController@delete')->name('memberConsulting.delete');
    Route::get('/member-consultings/restore/{memberConsulting}', 'MemberConsultingController@restore')->name('memberConsulting.restore');
    Route::get('/member-consultings/force-delete/{memberConsulting}', 'MemberConsultingController@forceDelete')->name('memberConsulting.force-delete');

    /*================================================
        MailTemplate Route
    ================================================*/
    Route::any('/mail-templates', 'MailTemplateController@index')->name('mailTemplate.index');
    Route::any('/mail-templates/send-mail', 'MailTemplateController@sendMail')->name('mailTemplate.sendMail');
    Route::any('/mail-templates/send-test', 'MailTemplateController@sendTest')->name('mailTemplate.sendTest');

    Route::get('/mail-templates/new', 'MailTemplateController@form')->name('mailTemplate.new');
    Route::get('/mail-templates/{mailTemplate}', 'MailTemplateController@form')->name('mailTemplate.form');
    Route::post('/mail-templates/save', 'MailTemplateController@post')->name('mailTemplate.save');
    Route::get('/mail-templates/delete/{mailTemplate}', 'MailTemplateController@delete')->name('mailTemplate.delete');
    Route::get('/mail-templates/restore/{mailTemplate}', 'MailTemplateController@restore')->name('mailTemplate.restore');
    Route::get('/mail-templates/force-delete/{mailTemplate}', 'MailTemplateController@forceDelete')->name('mailTemplate.force-delete');

    /*================================================
        Contact Route
    ================================================*/
    Route::any('/contacts', 'ContactController@index')->name('contact.index');

    Route::get('/contacts/new', 'ContactController@form')->name('contact.new');
    Route::get('/contacts/{contact}', 'ContactController@form')->name('contact.form');
    Route::post('/contacts/save', 'ContactController@post')->name('contact.save');
    Route::get('/contacts/delete/{contact}', 'ContactController@delete')->name('contact.delete');
    Route::get('/contacts/restore/{contact}', 'ContactController@restore')->name('contact.restore');
    Route::get('/contacts/force-delete/{contact}', 'ContactController@forceDelete')->name('contact.force-delete');


    /*================================================
        InquiryType Route
    ================================================*/
    Route::any('/inquiry-types', 'InquiryTypeController@index')->name('inquiryType.index');

    Route::get('/inquiry-types/new', 'InquiryTypeController@form')->name('inquiryType.new');
    Route::get('/inquiry-types/{inquiryType}', 'InquiryTypeController@form')->name('inquiryType.form');
    Route::post('/inquiry-types/save', 'InquiryTypeController@post')->name('inquiryType.save');
    Route::get('/inquiry-types/delete/{inquiryType}', 'InquiryTypeController@delete')->name('inquiryType.delete');
    Route::get('/inquiry-types/restore/{inquiryType}', 'InquiryTypeController@restore')->name('inquiryType.restore');
    Route::get('/inquiry-types/force-delete/{inquiryType}', 'InquiryTypeController@forceDelete')->name('inquiryType.force-delete');

    /*================================================
        PageType Route
    ================================================*/
    Route::any('/page-types', 'PageTypeController@index')->name('pageType.index');

    Route::get('/page-types/new', 'PageTypeController@form')->name('pageType.new');
    Route::get('/page-types/{pageType}', 'PageTypeController@form')->name('pageType.form');
    Route::post('/page-types/save', 'PageTypeController@post')->name('pageType.save');
    Route::get('/page-types/delete/{pageType}', 'PageTypeController@delete')->name('pageType.delete');
    Route::get('/page-types/restore/{pageType}', 'PageTypeController@restore')->name('pageType.restore');
    Route::get('/page-types/force-delete/{pageType}', 'PageTypeController@forceDelete')->name('pageType.force-delete');


    /*================================================
        Tag Route
    ================================================*/
    Route::any('/tags', 'TagController@index')->name('tag.index');

    Route::get('/tags/new', 'TagController@form')->name('tag.new');
    Route::get('/tags/{tag}', 'TagController@form')->name('tag.form');
    Route::post('/tags/save', 'TagController@post')->name('tag.save');
    Route::get('/tags/delete/{tag}', 'TagController@delete')->name('tag.delete');
    Route::get('/tags/restore/{tag}', 'TagController@restore')->name('tag.restore');
    Route::get('/tags/force-delete/{tag}', 'TagController@forceDelete')->name('tag.force-delete');

    /*================================================
        UrlRedirect Route
    ================================================*/
    Route::any('/url-redirects', 'UrlRedirectController@index')->name('urlRedirect.index');

    Route::get('/url-redirects/new', 'UrlRedirectController@form')->name('urlRedirect.new');
    Route::get('/url-redirects/{urlRedirect}', 'UrlRedirectController@form')->name('urlRedirect.form');
    Route::post('/url-redirects/save', 'UrlRedirectController@post')->name('urlRedirect.save');
    Route::get('/url-redirects/delete/{urlRedirect}', 'UrlRedirectController@delete')->name('urlRedirect.delete');
    Route::get('/url-redirects/restore/{urlRedirect}', 'UrlRedirectController@restore')->name('urlRedirect.restore');
    Route::get('/url-redirects/force-delete/{urlRedirect}', 'UrlRedirectController@forceDelete')->name('urlRedirect.force-delete');

    /*================================================
        RefSourceParent Route
    ================================================*/
    Route::any('/ref-source-parents', 'RefSourceParentController@index')->name('refSourceParent.index');
    Route::any('/ref-source-parents/get-ref-sources', 'RefSourceParentController@getRefSources')->name('refSourceParent.getRefSources');

    Route::get('/ref-source-parents/new', 'RefSourceParentController@form')->name('refSourceParent.new');
    Route::get('/ref-source-parents/{refSourceParent}', 'RefSourceParentController@form')->name('refSourceParent.form');
    Route::post('/ref-source-parents/save', 'RefSourceParentController@post')->name('refSourceParent.save');
    Route::get('/ref-source-parents/delete/{refSourceParent}', 'RefSourceParentController@delete')->name('refSourceParent.delete');
    Route::get('/ref-source-parents/restore/{refSourceParent}', 'RefSourceParentController@restore')->name('refSourceParent.restore');
    Route::get('/ref-source-parents/force-delete/{refSourceParent}', 'RefSourceParentController@forceDelete')->name('refSourceParent.force-delete');

    /*================================================
        Gallery Route
    ================================================*/
    Route::any('/galleries', 'GalleryController@index')->name('gallery.index');

    Route::get('/galleries/new', 'GalleryController@form')->name('gallery.new');
    Route::get('/galleries/{gallery}', 'GalleryController@form')->name('gallery.form');
    Route::post('/galleries/save', 'GalleryController@post')->name('gallery.save');
    Route::get('/galleries/delete/{gallery}', 'GalleryController@delete')->name('gallery.delete');
    Route::get('/galleries/restore/{gallery}', 'GalleryController@restore')->name('gallery.restore');
    Route::get('/galleries/force-delete/{gallery}', 'GalleryController@forceDelete')->name('gallery.force-delete');


    /*================================================
        RecruitmentJob Route
    ================================================*/
    Route::any('/recruitment-jobs', 'RecruitmentJobController@index')->name('recruitmentJob.index');

    Route::get('/recruitment-jobs/new', 'RecruitmentJobController@form')->name('recruitmentJob.new');
    Route::get('/recruitment-jobs/{recruitmentJob}', 'RecruitmentJobController@form')->name('recruitmentJob.form');
    Route::post('/recruitment-jobs/save', 'RecruitmentJobController@post')->name('recruitmentJob.save');
    Route::get('/recruitment-jobs/delete/{recruitmentJob}', 'RecruitmentJobController@delete')->name('recruitmentJob.delete');
    Route::get('/recruitment-jobs/restore/{recruitmentJob}', 'RecruitmentJobController@restore')->name('recruitmentJob.restore');
    Route::get('/recruitment-jobs/force-delete/{recruitmentJob}', 'RecruitmentJobController@forceDelete')->name('recruitmentJob.force-delete');


    /*================================================
        RecruitmentJobApply Route
    ================================================*/
    Route::any('/recruitment-job-applies', 'RecruitmentJobApplyController@index')->name('recruitmentJobApply.index');

    Route::get('/recruitment-job-applies/new', 'RecruitmentJobApplyController@form')->name('recruitmentJobApply.new');
    Route::get('/recruitment-job-applies/{recruitmentJobApply}', 'RecruitmentJobApplyController@form')->name('recruitmentJobApply.form');
    Route::post('/recruitment-job-applies/save', 'RecruitmentJobApplyController@post')->name('recruitmentJobApply.save');
    Route::get('/recruitment-job-applies/delete/{recruitmentJobApply}', 'RecruitmentJobApplyController@delete')->name('recruitmentJobApply.delete');
    Route::get('/recruitment-job-applies/restore/{recruitmentJobApply}', 'RecruitmentJobApplyController@restore')->name('recruitmentJobApply.restore');
    Route::get('/recruitment-job-applies/force-delete/{recruitmentJobApply}', 'RecruitmentJobApplyController@forceDelete')->name('recruitmentJobApply.force-delete');


    /*================================================
        MemberCv Route
    ================================================*/
    Route::any('/member-cvs', 'MemberCvController@index')->name('memberCv.index');

    Route::get('/member-cvs/new', 'MemberCvController@form')->name('memberCv.new');
    Route::get('/member-cvs/{memberCv}', 'MemberCvController@form')->name('memberCv.form');
    Route::post('/member-cvs/save', 'MemberCvController@post')->name('memberCv.save');
    Route::get('/member-cvs/delete/{memberCv}', 'MemberCvController@delete')->name('memberCv.delete');
    Route::get('/member-cvs/restore/{memberCv}', 'MemberCvController@restore')->name('memberCv.restore');
    Route::get('/member-cvs/force-delete/{memberCv}', 'MemberCvController@forceDelete')->name('memberCv.force-delete');


    /*================================================
        ExaminationQuestion Route
    ================================================*/
    Route::any('/examination-questions', 'ExaminationQuestionController@index')->name('examinationQuestion.index');

    Route::get('/examination-questions/new', 'ExaminationQuestionController@form')->name('examinationQuestion.new');
    Route::get('/examination-questions/{examinationQuestion}', 'ExaminationQuestionController@form')->name('examinationQuestion.form');
    Route::post('/examination-questions/save', 'ExaminationQuestionController@post')->name('examinationQuestion.save');
    Route::get('/examination-questions/delete/{examinationQuestion}', 'ExaminationQuestionController@delete')->name('examinationQuestion.delete');
    Route::get('/examination-questions/restore/{examinationQuestion}', 'ExaminationQuestionController@restore')->name('examinationQuestion.restore');
    Route::get('/examination-questions/force-delete/{examinationQuestion}', 'ExaminationQuestionController@forceDelete')->name('examinationQuestion.force-delete');

});
