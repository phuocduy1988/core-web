<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login',       ['uses' => 'UserController@login']);
Route::post('/logout',      ['uses' => 'UserController@logout']);
//API get all config setting for application
Route::get('/startup',      ['uses' => 'CommonController@startup']);
//Login with facebook
Route::post('/login-facebook',      ['uses' => 'MemberController@facebookLogin']);

/*================================================
	CrawlSource Route
================================================*/
Route::any('/crawl-sources', 'CrawlSourceController@index')->name('api.crawlSource.index');

Route::post('/crawl-sources/create', 'CrawlSourceController@create')->name('api.crawlSource.create');
Route::post('/crawl-sources/update', 'CrawlSourceController@update')->name('api.crawlSource.update');
Route::post('/crawl-sources/delete', 'CrawlSourceController@delete')->name('api.crawlSource.delete');
Route::post('/crawl-sources/restore', 'CrawlSourceController@restore')->name('api.crawlSource.restore');
Route::post('/crawl-sources/force-delete', 'CrawlSourceController@forceDelete')->name('api.crawlSource.force-delete');


/*================================================
	CrawlContent Route
================================================*/
Route::any('/crawl-contents', 'CrawlContentController@index')->name('api.crawlContent.index');

Route::post('/crawl-contents/check-exist', 'CrawlContentController@checkExist')->name('api.crawlContent.checkExist');
Route::post('/crawl-contents/create', 'CrawlContentController@create')->name('api.crawlContent.create');
Route::post('/crawl-contents/update', 'CrawlContentController@update')->name('api.crawlContent.update');
Route::post('/crawl-contents/delete', 'CrawlContentController@delete')->name('api.crawlContent.delete');
Route::post('/crawl-contents/restore', 'CrawlContentController@restore')->name('api.crawlContent.restore');
Route::post('/crawl-contents/force-delete', 'CrawlContentController@forceDelete')->name('api.crawlContent.force-delete');

//crawl data
Route::post('/crawl-contents/create-or-update', 'CrawlContentController@createOrUpdate')->name('api.crawlContent.create-or-update');
Route::post('/crawl-jobs/create-or-update', 'CrawlJobController@createOrUpdate');
Route::any('/crawl-jobs/get-job/{id}', 'CrawlJobController@getJob');

// end crawl data

Route::group(['middleware' => 'auth.api'], function() {


	//Auto generation here


    /*================================================
        Member Route
    ================================================*/
    Route::any('/members', 'MemberController@index')->name('api.member.index');

    Route::post('/members/create', 'MemberController@create')->name('api.member.create');
    Route::post('/members/update', 'MemberController@update')->name('api.member.update');
    Route::post('/members/delete', 'MemberController@delete')->name('api.member.delete');
    Route::post('/members/restore', 'MemberController@restore')->name('api.member.restore');
    Route::post('/members/force-delete', 'MemberController@forceDelete')->name('api.member.force-delete');
	Route::post('/user-data',      ['uses' => 'MemberController@userData']);


    /*================================================
        Survey Route
    ================================================*/
    Route::any('/surveys', 'SurveyController@index')->name('api.survey.index');

    Route::post('/surveys/create', 'SurveyController@create')->name('api.survey.create');
    Route::post('/surveys/update', 'SurveyController@update')->name('api.survey.update');
    Route::post('/surveys/delete', 'SurveyController@delete')->name('api.survey.delete');
    Route::post('/surveys/restore', 'SurveyController@restore')->name('api.survey.restore');
    Route::post('/surveys/force-delete', 'SurveyController@forceDelete')->name('api.survey.force-delete');


    /*================================================
        SurveyType Route
    ================================================*/
    Route::any('/survey-types', 'SurveyTypeController@index')->name('api.surveyType.index');

    Route::post('/survey-types/create', 'SurveyTypeController@create')->name('api.surveyType.create');
    Route::post('/survey-types/update', 'SurveyTypeController@update')->name('api.surveyType.update');
    Route::post('/survey-types/delete', 'SurveyTypeController@delete')->name('api.surveyType.delete');
    Route::post('/survey-types/restore', 'SurveyTypeController@restore')->name('api.surveyType.restore');
    Route::post('/survey-types/force-delete', 'SurveyTypeController@forceDelete')->name('api.surveyType.force-delete');


    /*================================================
        MemberSurvey Route
    ================================================*/
    Route::any('/member-surveys', 'MemberSurveyController@index')->name('api.memberSurvey.index');

    Route::post('/member-surveys/create', 'MemberSurveyController@create')->name('api.memberSurvey.create');
    Route::post('/member-surveys/update', 'MemberSurveyController@update')->name('api.memberSurvey.update');
    Route::post('/member-surveys/delete', 'MemberSurveyController@delete')->name('api.memberSurvey.delete');
    Route::post('/member-surveys/restore', 'MemberSurveyController@restore')->name('api.memberSurvey.restore');
    Route::post('/member-surveys/force-delete', 'MemberSurveyController@forceDelete')->name('api.memberSurvey.force-delete');


    /*================================================
        Examination Route
    ================================================*/
    Route::any('/examinations', 'ExaminationController@index')->name('api.examination.index');

    Route::post('/examinations/create', 'ExaminationController@create')->name('api.examination.create');
    Route::post('/examinations/update', 'ExaminationController@update')->name('api.examination.update');
    Route::post('/examinations/delete', 'ExaminationController@delete')->name('api.examination.delete');
    Route::post('/examinations/restore', 'ExaminationController@restore')->name('api.examination.restore');
    Route::post('/examinations/force-delete', 'ExaminationController@forceDelete')->name('api.examination.force-delete');


    /*================================================
        ExaminationType Route
    ================================================*/
    Route::any('/examination-types', 'ExaminationTypeController@index')->name('api.examinationType.index');

    Route::post('/examination-types/create', 'ExaminationTypeController@create')->name('api.examinationType.create');
    Route::post('/examination-types/update', 'ExaminationTypeController@update')->name('api.examinationType.update');
    Route::post('/examination-types/delete', 'ExaminationTypeController@delete')->name('api.examinationType.delete');
    Route::post('/examination-types/restore', 'ExaminationTypeController@restore')->name('api.examinationType.restore');
    Route::post('/examination-types/force-delete', 'ExaminationTypeController@forceDelete')->name('api.examinationType.force-delete');


    /*================================================
        ExaminationResult Route
    ================================================*/
    Route::any('/examination-results', 'ExaminationResultController@index')->name('api.examinationResult.index');

    Route::post('/examination-results/create', 'ExaminationResultController@create')->name('api.examinationResult.create');
    Route::post('/examination-results/update', 'ExaminationResultController@update')->name('api.examinationResult.update');
    Route::post('/examination-results/delete', 'ExaminationResultController@delete')->name('api.examinationResult.delete');
    Route::post('/examination-results/restore', 'ExaminationResultController@restore')->name('api.examinationResult.restore');
    Route::post('/examination-results/force-delete', 'ExaminationResultController@forceDelete')->name('api.examinationResult.force-delete');


    /*================================================
        ExaminationAnswer Route
    ================================================*/
    Route::any('/examination-answers', 'ExaminationAnswerController@index')->name('api.examinationAnswer.index');

    Route::post('/examination-answers/create', 'ExaminationAnswerController@create')->name('api.examinationAnswer.create');
    Route::post('/examination-answers/update', 'ExaminationAnswerController@update')->name('api.examinationAnswer.update');
    Route::post('/examination-answers/delete', 'ExaminationAnswerController@delete')->name('api.examinationAnswer.delete');
    Route::post('/examination-answers/restore', 'ExaminationAnswerController@restore')->name('api.examinationAnswer.restore');
    Route::post('/examination-answers/force-delete', 'ExaminationAnswerController@forceDelete')->name('api.examinationAnswer.force-delete');


    /*================================================
        News Route
    ================================================*/
    Route::any('/news', 'NewsController@index')->name('api.news.index');

    Route::post('/news/create', 'NewsController@create')->name('api.news.create');
    Route::post('/news/update', 'NewsController@update')->name('api.news.update');
    Route::post('/news/delete', 'NewsController@delete')->name('api.news.delete');
    Route::post('/news/restore', 'NewsController@restore')->name('api.news.restore');
    Route::post('/news/force-delete', 'NewsController@forceDelete')->name('api.news.force-delete');


    /*================================================
        NewsCategory Route
    ================================================*/
    Route::any('/news-categories', 'NewsCategoryController@index')->name('api.newsCategory.index');

    Route::post('/news-categories/create', 'NewsCategoryController@create')->name('api.newsCategory.create');
    Route::post('/news-categories/update', 'NewsCategoryController@update')->name('api.newsCategory.update');
    Route::post('/news-categories/delete', 'NewsCategoryController@delete')->name('api.newsCategory.delete');
    Route::post('/news-categories/restore', 'NewsCategoryController@restore')->name('api.newsCategory.restore');
    Route::post('/news-categories/force-delete', 'NewsCategoryController@forceDelete')->name('api.newsCategory.force-delete');


    /*================================================
        ExamRegistation Route
    ================================================*/
    Route::any('/exam-registations', 'ExamRegistationController@index')->name('api.examRegistation.index');

    Route::post('/exam-registations/create', 'ExamRegistationController@create')->name('api.examRegistation.create');
    Route::post('/exam-registations/update', 'ExamRegistationController@update')->name('api.examRegistation.update');
    Route::post('/exam-registations/delete', 'ExamRegistationController@delete')->name('api.examRegistation.delete');
    Route::post('/exam-registations/restore', 'ExamRegistationController@restore')->name('api.examRegistation.restore');
    Route::post('/exam-registations/force-delete', 'ExamRegistationController@forceDelete')->name('api.examRegistation.force-delete');


    /*================================================
        Page Route
    ================================================*/
    Route::any('/pages', 'PageController@index')->name('api.page.index');

    Route::post('/pages/create', 'PageController@create')->name('api.page.create');
    Route::post('/pages/update', 'PageController@update')->name('api.page.update');
    Route::post('/pages/delete', 'PageController@delete')->name('api.page.delete');
    Route::post('/pages/restore', 'PageController@restore')->name('api.page.restore');
    Route::post('/pages/force-delete', 'PageController@forceDelete')->name('api.page.force-delete');


    /*================================================
        Menu Route
    ================================================*/
    Route::any('/menus', 'MenuController@index')->name('api.menu.index');

    Route::post('/menus/create', 'MenuController@create')->name('api.menu.create');
    Route::post('/menus/update', 'MenuController@update')->name('api.menu.update');
    Route::post('/menus/delete', 'MenuController@delete')->name('api.menu.delete');
    Route::post('/menus/restore', 'MenuController@restore')->name('api.menu.restore');
    Route::post('/menus/force-delete', 'MenuController@forceDelete')->name('api.menu.force-delete');


    /*================================================
        MenuType Route
    ================================================*/
    Route::any('/menu-types', 'MenuTypeController@index')->name('api.menuType.index');

    Route::post('/menu-types/create', 'MenuTypeController@create')->name('api.menuType.create');
    Route::post('/menu-types/update', 'MenuTypeController@update')->name('api.menuType.update');
    Route::post('/menu-types/delete', 'MenuTypeController@delete')->name('api.menuType.delete');
    Route::post('/menu-types/restore', 'MenuTypeController@restore')->name('api.menuType.restore');
    Route::post('/menu-types/force-delete', 'MenuTypeController@forceDelete')->name('api.menuType.force-delete');


    /*================================================
        Banner Route
    ================================================*/
    Route::any('/banners', 'BannerController@index')->name('api.banner.index');

    Route::post('/banners/create', 'BannerController@create')->name('api.banner.create');
    Route::post('/banners/update', 'BannerController@update')->name('api.banner.update');
    Route::post('/banners/delete', 'BannerController@delete')->name('api.banner.delete');
    Route::post('/banners/restore', 'BannerController@restore')->name('api.banner.restore');
    Route::post('/banners/force-delete', 'BannerController@forceDelete')->name('api.banner.force-delete');


    /*================================================
        SampleHtmlType Route
    ================================================*/
    Route::any('/sample-html-types', 'SampleHtmlTypeController@index')->name('api.sampleHtmlType.index');

    Route::post('/sample-html-types/create', 'SampleHtmlTypeController@create')->name('api.sampleHtmlType.create');
    Route::post('/sample-html-types/update', 'SampleHtmlTypeController@update')->name('api.sampleHtmlType.update');
    Route::post('/sample-html-types/delete', 'SampleHtmlTypeController@delete')->name('api.sampleHtmlType.delete');
    Route::post('/sample-html-types/restore', 'SampleHtmlTypeController@restore')->name('api.sampleHtmlType.restore');
    Route::post('/sample-html-types/force-delete', 'SampleHtmlTypeController@forceDelete')->name('api.sampleHtmlType.force-delete');


    /*================================================
        SampleHtml Route
    ================================================*/
    Route::any('/sample-htmls', 'SampleHtmlController@index')->name('api.sampleHtml.index');

    Route::post('/sample-htmls/create', 'SampleHtmlController@create')->name('api.sampleHtml.create');
    Route::post('/sample-htmls/update', 'SampleHtmlController@update')->name('api.sampleHtml.update');
    Route::post('/sample-htmls/delete', 'SampleHtmlController@delete')->name('api.sampleHtml.delete');
    Route::post('/sample-htmls/restore', 'SampleHtmlController@restore')->name('api.sampleHtml.restore');
    Route::post('/sample-htmls/force-delete', 'SampleHtmlController@forceDelete')->name('api.sampleHtml.force-delete');


    /*================================================
        MemberGiveBook Route
    ================================================*/
    Route::any('/member-give-books', 'MemberGiveBookController@index')->name('api.memberGiveBook.index');

    Route::post('/member-give-books/create', 'MemberGiveBookController@create')->name('api.memberGiveBook.create');
    Route::post('/member-give-books/update', 'MemberGiveBookController@update')->name('api.memberGiveBook.update');
    Route::post('/member-give-books/delete', 'MemberGiveBookController@delete')->name('api.memberGiveBook.delete');
    Route::post('/member-give-books/restore', 'MemberGiveBookController@restore')->name('api.memberGiveBook.restore');
    Route::post('/member-give-books/force-delete', 'MemberGiveBookController@forceDelete')->name('api.memberGiveBook.force-delete');


    /*================================================
        PageSpeed Route
    ================================================*/
    Route::any('/page-speeds', 'PageSpeedController@index')->name('api.pageSpeed.index');

    Route::post('/page-speeds/create', 'PageSpeedController@create')->name('api.pageSpeed.create');
    Route::post('/page-speeds/update', 'PageSpeedController@update')->name('api.pageSpeed.update');
    Route::post('/page-speeds/delete', 'PageSpeedController@delete')->name('api.pageSpeed.delete');
    Route::post('/page-speeds/restore', 'PageSpeedController@restore')->name('api.pageSpeed.restore');
    Route::post('/page-speeds/force-delete', 'PageSpeedController@forceDelete')->name('api.pageSpeed.force-delete');


    /*================================================
        Recruitment Route
    ================================================*/
    Route::any('/recruitments', 'RecruitmentController@index')->name('api.recruitment.index');

    Route::post('/recruitments/create', 'RecruitmentController@create')->name('api.recruitment.create');
    Route::post('/recruitments/update', 'RecruitmentController@update')->name('api.recruitment.update');
    Route::post('/recruitments/delete', 'RecruitmentController@delete')->name('api.recruitment.delete');
    Route::post('/recruitments/restore', 'RecruitmentController@restore')->name('api.recruitment.restore');
    Route::post('/recruitments/force-delete', 'RecruitmentController@forceDelete')->name('api.recruitment.force-delete');


    /*================================================
        JapaneseLevel Route
    ================================================*/
    Route::any('/japanese-levels', 'JapaneseLevelController@index')->name('api.japaneseLevel.index');

    Route::post('/japanese-levels/create', 'JapaneseLevelController@create')->name('api.japaneseLevel.create');
    Route::post('/japanese-levels/update', 'JapaneseLevelController@update')->name('api.japaneseLevel.update');
    Route::post('/japanese-levels/delete', 'JapaneseLevelController@delete')->name('api.japaneseLevel.delete');
    Route::post('/japanese-levels/restore', 'JapaneseLevelController@restore')->name('api.japaneseLevel.restore');
    Route::post('/japanese-levels/force-delete', 'JapaneseLevelController@forceDelete')->name('api.japaneseLevel.force-delete');


    /*================================================
        JapaneseQualification Route
    ================================================*/
    Route::any('/japanese-qualifications', 'JapaneseQualificationController@index')->name('api.japaneseQualification.index');

    Route::post('/japanese-qualifications/create', 'JapaneseQualificationController@create')->name('api.japaneseQualification.create');
    Route::post('/japanese-qualifications/update', 'JapaneseQualificationController@update')->name('api.japaneseQualification.update');
    Route::post('/japanese-qualifications/delete', 'JapaneseQualificationController@delete')->name('api.japaneseQualification.delete');
    Route::post('/japanese-qualifications/restore', 'JapaneseQualificationController@restore')->name('api.japaneseQualification.restore');
    Route::post('/japanese-qualifications/force-delete', 'JapaneseQualificationController@forceDelete')->name('api.japaneseQualification.force-delete');


    /*================================================
        EnglishLevel Route
    ================================================*/
    Route::any('/english-levels', 'EnglishLevelController@index')->name('api.englishLevel.index');

    Route::post('/english-levels/create', 'EnglishLevelController@create')->name('api.englishLevel.create');
    Route::post('/english-levels/update', 'EnglishLevelController@update')->name('api.englishLevel.update');
    Route::post('/english-levels/delete', 'EnglishLevelController@delete')->name('api.englishLevel.delete');
    Route::post('/english-levels/restore', 'EnglishLevelController@restore')->name('api.englishLevel.restore');
    Route::post('/english-levels/force-delete', 'EnglishLevelController@forceDelete')->name('api.englishLevel.force-delete');


    /*================================================
        JobDesire Route
    ================================================*/
    Route::any('/job-desires', 'JobDesireController@index')->name('api.jobDesire.index');

    Route::post('/job-desires/create', 'JobDesireController@create')->name('api.jobDesire.create');
    Route::post('/job-desires/update', 'JobDesireController@update')->name('api.jobDesire.update');
    Route::post('/job-desires/delete', 'JobDesireController@delete')->name('api.jobDesire.delete');
    Route::post('/job-desires/restore', 'JobDesireController@restore')->name('api.jobDesire.restore');
    Route::post('/job-desires/force-delete', 'JobDesireController@forceDelete')->name('api.jobDesire.force-delete');


    /*================================================
        JobDesireCategory Route
    ================================================*/
    Route::any('/job-desire-categories', 'JobDesireCategoryController@index')->name('api.jobDesireCategory.index');

    Route::post('/job-desire-categories/create', 'JobDesireCategoryController@create')->name('api.jobDesireCategory.create');
    Route::post('/job-desire-categories/update', 'JobDesireCategoryController@update')->name('api.jobDesireCategory.update');
    Route::post('/job-desire-categories/delete', 'JobDesireCategoryController@delete')->name('api.jobDesireCategory.delete');
    Route::post('/job-desire-categories/restore', 'JobDesireCategoryController@restore')->name('api.jobDesireCategory.restore');
    Route::post('/job-desire-categories/force-delete', 'JobDesireCategoryController@forceDelete')->name('api.jobDesireCategory.force-delete');


    /*================================================
        HowToKnow Route
    ================================================*/
    Route::any('/how-to-knows', 'HowToKnowController@index')->name('api.howToKnow.index');

    Route::post('/how-to-knows/create', 'HowToKnowController@create')->name('api.howToKnow.create');
    Route::post('/how-to-knows/update', 'HowToKnowController@update')->name('api.howToKnow.update');
    Route::post('/how-to-knows/delete', 'HowToKnowController@delete')->name('api.howToKnow.delete');
    Route::post('/how-to-knows/restore', 'HowToKnowController@restore')->name('api.howToKnow.restore');
    Route::post('/how-to-knows/force-delete', 'HowToKnowController@forceDelete')->name('api.howToKnow.force-delete');


    /*================================================
        ClientUser Route
    ================================================*/
    Route::any('/client-users', 'ClientUserController@index')->name('api.clientUser.index');

    Route::post('/client-users/create', 'ClientUserController@create')->name('api.clientUser.create');
    Route::post('/client-users/update', 'ClientUserController@update')->name('api.clientUser.update');
    Route::post('/client-users/delete', 'ClientUserController@delete')->name('api.clientUser.delete');
    Route::post('/client-users/restore', 'ClientUserController@restore')->name('api.clientUser.restore');
    Route::post('/client-users/force-delete', 'ClientUserController@forceDelete')->name('api.clientUser.force-delete');


    /*================================================
        Client Route
    ================================================*/
    Route::any('/clients', 'ClientController@index')->name('api.client.index');

    Route::post('/clients/create', 'ClientController@create')->name('api.client.create');
    Route::post('/clients/update', 'ClientController@update')->name('api.client.update');
    Route::post('/clients/delete', 'ClientController@delete')->name('api.client.delete');
    Route::post('/clients/restore', 'ClientController@restore')->name('api.client.restore');
    Route::post('/clients/force-delete', 'ClientController@forceDelete')->name('api.client.force-delete');


    /*================================================
        ClientJob Route
    ================================================*/
    Route::any('/client-jobs', 'ClientJobController@index')->name('api.clientJob.index');

    Route::post('/client-jobs/create', 'ClientJobController@create')->name('api.clientJob.create');
    Route::post('/client-jobs/update', 'ClientJobController@update')->name('api.clientJob.update');
    Route::post('/client-jobs/delete', 'ClientJobController@delete')->name('api.clientJob.delete');
    Route::post('/client-jobs/restore', 'ClientJobController@restore')->name('api.clientJob.restore');
    Route::post('/client-jobs/force-delete', 'ClientJobController@forceDelete')->name('api.clientJob.force-delete');


    /*================================================
        ClientJobCategory Route
    ================================================*/
    Route::any('/client-job-categories', 'ClientJobCategoryController@index')->name('api.clientJobCategory.index');

    Route::post('/client-job-categories/create', 'ClientJobCategoryController@create')->name('api.clientJobCategory.create');
    Route::post('/client-job-categories/update', 'ClientJobCategoryController@update')->name('api.clientJobCategory.update');
    Route::post('/client-job-categories/delete', 'ClientJobCategoryController@delete')->name('api.clientJobCategory.delete');
    Route::post('/client-job-categories/restore', 'ClientJobCategoryController@restore')->name('api.clientJobCategory.restore');
    Route::post('/client-job-categories/force-delete', 'ClientJobCategoryController@forceDelete')->name('api.clientJobCategory.force-delete');


    /*================================================
        ClientJobPositionType Route
    ================================================*/
    Route::any('/client-job-position-types', 'ClientJobPositionTypeController@index')->name('api.clientJobPositionType.index');

    Route::post('/client-job-position-types/create', 'ClientJobPositionTypeController@create')->name('api.clientJobPositionType.create');
    Route::post('/client-job-position-types/update', 'ClientJobPositionTypeController@update')->name('api.clientJobPositionType.update');
    Route::post('/client-job-position-types/delete', 'ClientJobPositionTypeController@delete')->name('api.clientJobPositionType.delete');
    Route::post('/client-job-position-types/restore', 'ClientJobPositionTypeController@restore')->name('api.clientJobPositionType.restore');
    Route::post('/client-job-position-types/force-delete', 'ClientJobPositionTypeController@forceDelete')->name('api.clientJobPositionType.force-delete');


    /*================================================
        Country Route
    ================================================*/
    Route::any('/countries', 'CountryController@index')->name('api.country.index');

    Route::post('/countries/create', 'CountryController@create')->name('api.country.create');
    Route::post('/countries/update', 'CountryController@update')->name('api.country.update');
    Route::post('/countries/delete', 'CountryController@delete')->name('api.country.delete');
    Route::post('/countries/restore', 'CountryController@restore')->name('api.country.restore');
    Route::post('/countries/force-delete', 'CountryController@forceDelete')->name('api.country.force-delete');


    /*================================================
        Province Route
    ================================================*/
    Route::any('/provinces', 'ProvinceController@index')->name('api.province.index');

    Route::post('/provinces/create', 'ProvinceController@create')->name('api.province.create');
    Route::post('/provinces/update', 'ProvinceController@update')->name('api.province.update');
    Route::post('/provinces/delete', 'ProvinceController@delete')->name('api.province.delete');
    Route::post('/provinces/restore', 'ProvinceController@restore')->name('api.province.restore');
    Route::post('/provinces/force-delete', 'ProvinceController@forceDelete')->name('api.province.force-delete');


    /*================================================
        MemberClientJob Route
    ================================================*/
    Route::any('/member-client-jobs', 'MemberClientJobController@index')->name('api.memberClientJob.index');

    Route::post('/member-client-jobs/create', 'MemberClientJobController@create')->name('api.memberClientJob.create');
    Route::post('/member-client-jobs/update', 'MemberClientJobController@update')->name('api.memberClientJob.update');
    Route::post('/member-client-jobs/delete', 'MemberClientJobController@delete')->name('api.memberClientJob.delete');
    Route::post('/member-client-jobs/restore', 'MemberClientJobController@restore')->name('api.memberClientJob.restore');
    Route::post('/member-client-jobs/force-delete', 'MemberClientJobController@forceDelete')->name('api.memberClientJob.force-delete');


    /*================================================
        RefUrl Route
    ================================================*/
    Route::any('/ref-urls', 'RefUrlController@index')->name('api.refUrl.index');

    Route::post('/ref-urls/create', 'RefUrlController@create')->name('api.refUrl.create');
    Route::post('/ref-urls/update', 'RefUrlController@update')->name('api.refUrl.update');
    Route::post('/ref-urls/delete', 'RefUrlController@delete')->name('api.refUrl.delete');
    Route::post('/ref-urls/restore', 'RefUrlController@restore')->name('api.refUrl.restore');
    Route::post('/ref-urls/force-delete', 'RefUrlController@forceDelete')->name('api.refUrl.force-delete');


    /*================================================
        RefSource Route
    ================================================*/
    Route::any('/ref-sources', 'RefSourceController@index')->name('api.refSource.index');

    Route::post('/ref-sources/create', 'RefSourceController@create')->name('api.refSource.create');
    Route::post('/ref-sources/update', 'RefSourceController@update')->name('api.refSource.update');
    Route::post('/ref-sources/delete', 'RefSourceController@delete')->name('api.refSource.delete');
    Route::post('/ref-sources/restore', 'RefSourceController@restore')->name('api.refSource.restore');
    Route::post('/ref-sources/force-delete', 'RefSourceController@forceDelete')->name('api.refSource.force-delete');


    /*================================================
        MemberConsulting Route
    ================================================*/
    Route::any('/member-consultings', 'MemberConsultingController@index')->name('api.memberConsulting.index');

    Route::post('/member-consultings/create', 'MemberConsultingController@create')->name('api.memberConsulting.create');
    Route::post('/member-consultings/update', 'MemberConsultingController@update')->name('api.memberConsulting.update');
    Route::post('/member-consultings/delete', 'MemberConsultingController@delete')->name('api.memberConsulting.delete');
    Route::post('/member-consultings/restore', 'MemberConsultingController@restore')->name('api.memberConsulting.restore');
    Route::post('/member-consultings/force-delete', 'MemberConsultingController@forceDelete')->name('api.memberConsulting.force-delete');


    /*================================================
        EntryForm Route
    ================================================*/
    Route::any('/entry-forms', 'EntryFormController@index')->name('api.entryForm.index');

    Route::post('/entry-forms/create', 'EntryFormController@create')->name('api.entryForm.create');
    Route::post('/entry-forms/update', 'EntryFormController@update')->name('api.entryForm.update');
    Route::post('/entry-forms/delete', 'EntryFormController@delete')->name('api.entryForm.delete');
    Route::post('/entry-forms/restore', 'EntryFormController@restore')->name('api.entryForm.restore');
    Route::post('/entry-forms/force-delete', 'EntryFormController@forceDelete')->name('api.entryForm.force-delete');


    /*================================================
        MailTemplate Route
    ================================================*/
    Route::any('/mail-templates', 'MailTemplateController@index')->name('api.mailTemplate.index');

    Route::post('/mail-templates/create', 'MailTemplateController@create')->name('api.mailTemplate.create');
    Route::post('/mail-templates/update', 'MailTemplateController@update')->name('api.mailTemplate.update');
    Route::post('/mail-templates/delete', 'MailTemplateController@delete')->name('api.mailTemplate.delete');
    Route::post('/mail-templates/restore', 'MailTemplateController@restore')->name('api.mailTemplate.restore');
    Route::post('/mail-templates/force-delete', 'MailTemplateController@forceDelete')->name('api.mailTemplate.force-delete');


    /*================================================
        PointType Route
    ================================================*/
    Route::any('/point-types', 'PointTypeController@index')->name('api.pointType.index');

    Route::post('/point-types/create', 'PointTypeController@create')->name('api.pointType.create');
    Route::post('/point-types/update', 'PointTypeController@update')->name('api.pointType.update');
    Route::post('/point-types/delete', 'PointTypeController@delete')->name('api.pointType.delete');
    Route::post('/point-types/restore', 'PointTypeController@restore')->name('api.pointType.restore');
    Route::post('/point-types/force-delete', 'PointTypeController@forceDelete')->name('api.pointType.force-delete');


    /*================================================
        MemberPoint Route
    ================================================*/
    Route::any('/member-points', 'MemberPointController@index')->name('api.memberPoint.index');

    Route::post('/member-points/create', 'MemberPointController@create')->name('api.memberPoint.create');
    Route::post('/member-points/update', 'MemberPointController@update')->name('api.memberPoint.update');
    Route::post('/member-points/delete', 'MemberPointController@delete')->name('api.memberPoint.delete');
    Route::post('/member-points/restore', 'MemberPointController@restore')->name('api.memberPoint.restore');
    Route::post('/member-points/force-delete', 'MemberPointController@forceDelete')->name('api.memberPoint.force-delete');


    /*================================================
        ExchangeType Route
    ================================================*/
    Route::any('/exchange-types', 'ExchangeTypeController@index')->name('api.exchangeType.index');

    Route::post('/exchange-types/create', 'ExchangeTypeController@create')->name('api.exchangeType.create');
    Route::post('/exchange-types/update', 'ExchangeTypeController@update')->name('api.exchangeType.update');
    Route::post('/exchange-types/delete', 'ExchangeTypeController@delete')->name('api.exchangeType.delete');
    Route::post('/exchange-types/restore', 'ExchangeTypeController@restore')->name('api.exchangeType.restore');
    Route::post('/exchange-types/force-delete', 'ExchangeTypeController@forceDelete')->name('api.exchangeType.force-delete');


    /*================================================
        ExchangePoint Route
    ================================================*/
    Route::any('/exchange-points', 'ExchangePointController@index')->name('api.exchangePoint.index');

    Route::post('/exchange-points/create', 'ExchangePointController@create')->name('api.exchangePoint.create');
    Route::post('/exchange-points/update', 'ExchangePointController@update')->name('api.exchangePoint.update');
    Route::post('/exchange-points/delete', 'ExchangePointController@delete')->name('api.exchangePoint.delete');
    Route::post('/exchange-points/restore', 'ExchangePointController@restore')->name('api.exchangePoint.restore');
    Route::post('/exchange-points/force-delete', 'ExchangePointController@forceDelete')->name('api.exchangePoint.force-delete');


    /*================================================
        MemberBank Route
    ================================================*/
    Route::any('/member-banks', 'MemberBankController@index')->name('api.memberBank.index');

    Route::post('/member-banks/create', 'MemberBankController@create')->name('api.memberBank.create');
    Route::post('/member-banks/update', 'MemberBankController@update')->name('api.memberBank.update');
    Route::post('/member-banks/delete', 'MemberBankController@delete')->name('api.memberBank.delete');
    Route::post('/member-banks/restore', 'MemberBankController@restore')->name('api.memberBank.restore');
    Route::post('/member-banks/force-delete', 'MemberBankController@forceDelete')->name('api.memberBank.force-delete');


    /*================================================
        CompanyInvitation Route
    ================================================*/
    Route::any('/company-invitations', 'CompanyInvitationController@index')->name('api.companyInvitation.index');

    Route::post('/company-invitations/create', 'CompanyInvitationController@create')->name('api.companyInvitation.create');
    Route::post('/company-invitations/update', 'CompanyInvitationController@update')->name('api.companyInvitation.update');
    Route::post('/company-invitations/delete', 'CompanyInvitationController@delete')->name('api.companyInvitation.delete');
    Route::post('/company-invitations/restore', 'CompanyInvitationController@restore')->name('api.companyInvitation.restore');
    Route::post('/company-invitations/force-delete', 'CompanyInvitationController@forceDelete')->name('api.companyInvitation.force-delete');


    /*================================================
        MemberCv Route
    ================================================*/
    Route::any('/member-cvs', 'MemberCvController@index')->name('api.memberCv.index');

    Route::post('/member-cvs/create', 'MemberCvController@create')->name('api.memberCv.create');
    Route::post('/member-cvs/update', 'MemberCvController@update')->name('api.memberCv.update');
    Route::post('/member-cvs/delete', 'MemberCvController@delete')->name('api.memberCv.delete');
    Route::post('/member-cvs/restore', 'MemberCvController@restore')->name('api.memberCv.restore');
    Route::post('/member-cvs/force-delete', 'MemberCvController@forceDelete')->name('api.memberCv.force-delete');


    /*================================================
        MemberExamInfo Route
    ================================================*/
    Route::any('/member-exam-infos', 'MemberExamInfoController@index')->name('api.memberExamInfo.index');

    Route::post('/member-exam-infos/create', 'MemberExamInfoController@create')->name('api.memberExamInfo.create');
    Route::post('/member-exam-infos/update', 'MemberExamInfoController@update')->name('api.memberExamInfo.update');
    Route::post('/member-exam-infos/delete', 'MemberExamInfoController@delete')->name('api.memberExamInfo.delete');
    Route::post('/member-exam-infos/restore', 'MemberExamInfoController@restore')->name('api.memberExamInfo.restore');
    Route::post('/member-exam-infos/force-delete', 'MemberExamInfoController@forceDelete')->name('api.memberExamInfo.force-delete');


    /*================================================
        JbaaLevel Route
    ================================================*/
    Route::any('/jbaa-levels', 'JbaaLevelController@index')->name('api.jbaaLevel.index');

    Route::post('/jbaa-levels/create', 'JbaaLevelController@create')->name('api.jbaaLevel.create');
    Route::post('/jbaa-levels/update', 'JbaaLevelController@update')->name('api.jbaaLevel.update');
    Route::post('/jbaa-levels/delete', 'JbaaLevelController@delete')->name('api.jbaaLevel.delete');
    Route::post('/jbaa-levels/restore', 'JbaaLevelController@restore')->name('api.jbaaLevel.restore');
    Route::post('/jbaa-levels/force-delete', 'JbaaLevelController@forceDelete')->name('api.jbaaLevel.force-delete');


    /*================================================
        WorkExp Route
    ================================================*/
    Route::any('/work-exps', 'WorkExpController@index')->name('api.workExp.index');

    Route::post('/work-exps/create', 'WorkExpController@create')->name('api.workExp.create');
    Route::post('/work-exps/update', 'WorkExpController@update')->name('api.workExp.update');
    Route::post('/work-exps/delete', 'WorkExpController@delete')->name('api.workExp.delete');
    Route::post('/work-exps/restore', 'WorkExpController@restore')->name('api.workExp.restore');
    Route::post('/work-exps/force-delete', 'WorkExpController@forceDelete')->name('api.workExp.force-delete');


    /*================================================
        ClientScheduleInterview Route
    ================================================*/
    Route::any('/client-schedule-interviews', 'ClientScheduleInterviewController@index')->name('api.clientScheduleInterview.index');

    Route::post('/client-schedule-interviews/create', 'ClientScheduleInterviewController@create')->name('api.clientScheduleInterview.create');
    Route::post('/client-schedule-interviews/update', 'ClientScheduleInterviewController@update')->name('api.clientScheduleInterview.update');
    Route::post('/client-schedule-interviews/delete', 'ClientScheduleInterviewController@delete')->name('api.clientScheduleInterview.delete');
    Route::post('/client-schedule-interviews/restore', 'ClientScheduleInterviewController@restore')->name('api.clientScheduleInterview.restore');
    Route::post('/client-schedule-interviews/force-delete', 'ClientScheduleInterviewController@forceDelete')->name('api.clientScheduleInterview.force-delete');


    /*================================================
        ClientMemberInterview Route
    ================================================*/
    Route::any('/client-member-interviews', 'ClientMemberInterviewController@index')->name('api.clientMemberInterview.index');

    Route::post('/client-member-interviews/create', 'ClientMemberInterviewController@create')->name('api.clientMemberInterview.create');
    Route::post('/client-member-interviews/update', 'ClientMemberInterviewController@update')->name('api.clientMemberInterview.update');
    Route::post('/client-member-interviews/delete', 'ClientMemberInterviewController@delete')->name('api.clientMemberInterview.delete');
    Route::post('/client-member-interviews/restore', 'ClientMemberInterviewController@restore')->name('api.clientMemberInterview.restore');
    Route::post('/client-member-interviews/force-delete', 'ClientMemberInterviewController@forceDelete')->name('api.clientMemberInterview.force-delete');


    /*================================================
        MemberPointReview Route
    ================================================*/
    Route::any('/member-point-reviews', 'MemberPointReviewController@index')->name('api.memberPointReview.index');

    Route::post('/member-point-reviews/create', 'MemberPointReviewController@create')->name('api.memberPointReview.create');
    Route::post('/member-point-reviews/update', 'MemberPointReviewController@update')->name('api.memberPointReview.update');
    Route::post('/member-point-reviews/delete', 'MemberPointReviewController@delete')->name('api.memberPointReview.delete');
    Route::post('/member-point-reviews/restore', 'MemberPointReviewController@restore')->name('api.memberPointReview.restore');
    Route::post('/member-point-reviews/force-delete', 'MemberPointReviewController@forceDelete')->name('api.memberPointReview.force-delete');


    /*================================================
        MemberClientJobAffiliateUrl Route
    ================================================*/
    Route::any('/member-client-job-affiliate-urls', 'MemberClientJobAffiliateUrlController@index')->name('api.memberClientJobAffiliateUrl.index');

    Route::post('/member-client-job-affiliate-urls/create', 'MemberClientJobAffiliateUrlController@create')->name('api.memberClientJobAffiliateUrl.create');
    Route::post('/member-client-job-affiliate-urls/update', 'MemberClientJobAffiliateUrlController@update')->name('api.memberClientJobAffiliateUrl.update');
    Route::post('/member-client-job-affiliate-urls/delete', 'MemberClientJobAffiliateUrlController@delete')->name('api.memberClientJobAffiliateUrl.delete');
    Route::post('/member-client-job-affiliate-urls/restore', 'MemberClientJobAffiliateUrlController@restore')->name('api.memberClientJobAffiliateUrl.restore');
    Route::post('/member-client-job-affiliate-urls/force-delete', 'MemberClientJobAffiliateUrlController@forceDelete')->name('api.memberClientJobAffiliateUrl.force-delete');


    /*================================================
        ClientMessageGroup Route
    ================================================*/
    Route::any('/client-message-groups', 'ClientMessageGroupController@index')->name('api.clientMessageGroup.index');

    Route::post('/client-message-groups/create', 'ClientMessageGroupController@create')->name('api.clientMessageGroup.create');
    Route::post('/client-message-groups/update', 'ClientMessageGroupController@update')->name('api.clientMessageGroup.update');
    Route::post('/client-message-groups/delete', 'ClientMessageGroupController@delete')->name('api.clientMessageGroup.delete');
    Route::post('/client-message-groups/restore', 'ClientMessageGroupController@restore')->name('api.clientMessageGroup.restore');
    Route::post('/client-message-groups/force-delete', 'ClientMessageGroupController@forceDelete')->name('api.clientMessageGroup.force-delete');


    /*================================================
        ClientMessage Route
    ================================================*/
    Route::any('/client-messages', 'ClientMessageController@index')->name('api.clientMessage.index');

    Route::post('/client-messages/create', 'ClientMessageController@create')->name('api.clientMessage.create');
    Route::post('/client-messages/update', 'ClientMessageController@update')->name('api.clientMessage.update');
    Route::post('/client-messages/delete', 'ClientMessageController@delete')->name('api.clientMessage.delete');
    Route::post('/client-messages/restore', 'ClientMessageController@restore')->name('api.clientMessage.restore');
    Route::post('/client-messages/force-delete', 'ClientMessageController@forceDelete')->name('api.clientMessage.force-delete');
    Route::any('/client-member-messages', 'ClientMemberMessageController@index')->name('api.clientMemberMessage.index');

    Route::post('/client-member-messages/create', 'ClientMemberMessageController@create')->name('api.clientMemberMessage.create');
    Route::post('/client-member-messages/update', 'ClientMemberMessageController@update')->name('api.clientMemberMessage.update');
    Route::post('/client-member-messages/delete', 'ClientMemberMessageController@delete')->name('api.clientMemberMessage.delete');
    Route::post('/client-member-messages/restore', 'ClientMemberMessageController@restore')->name('api.clientMemberMessage.restore');
    Route::post('/client-member-messages/force-delete', 'ClientMemberMessageController@forceDelete')->name('api.clientMemberMessage.force-delete');


    /*================================================
        StatSale Route
    ================================================*/
    Route::any('/stat-sales', 'StatSaleController@index')->name('api.statSale.index');

    Route::post('/stat-sales/create', 'StatSaleController@create')->name('api.statSale.create');
    Route::post('/stat-sales/update', 'StatSaleController@update')->name('api.statSale.update');
    Route::post('/stat-sales/delete', 'StatSaleController@delete')->name('api.statSale.delete');
    Route::post('/stat-sales/restore', 'StatSaleController@restore')->name('api.statSale.restore');
    Route::post('/stat-sales/force-delete', 'StatSaleController@forceDelete')->name('api.statSale.force-delete');


    /*================================================
        Contact Route
    ================================================*/
    Route::any('/contacts', 'ContactController@index')->name('api.contact.index');

    Route::post('/contacts/create', 'ContactController@create')->name('api.contact.create');
    Route::post('/contacts/update', 'ContactController@update')->name('api.contact.update');
    Route::post('/contacts/delete', 'ContactController@delete')->name('api.contact.delete');
    Route::post('/contacts/restore', 'ContactController@restore')->name('api.contact.restore');
    Route::post('/contacts/force-delete', 'ContactController@forceDelete')->name('api.contact.force-delete');


    /*================================================
        InquiryType Route
    ================================================*/
    Route::any('/inquiry-types', 'InquiryTypeController@index')->name('api.inquiryType.index');

    Route::post('/inquiry-types/create', 'InquiryTypeController@create')->name('api.inquiryType.create');
    Route::post('/inquiry-types/update', 'InquiryTypeController@update')->name('api.inquiryType.update');
    Route::post('/inquiry-types/delete', 'InquiryTypeController@delete')->name('api.inquiryType.delete');
    Route::post('/inquiry-types/restore', 'InquiryTypeController@restore')->name('api.inquiryType.restore');
    Route::post('/inquiry-types/force-delete', 'InquiryTypeController@forceDelete')->name('api.inquiryType.force-delete');


    /*================================================
        JobFair Route
    ================================================*/
    Route::any('/job-fairs', 'JobFairController@index')->name('api.jobFair.index');

    Route::post('/job-fairs/create', 'JobFairController@create')->name('api.jobFair.create');
    Route::post('/job-fairs/update', 'JobFairController@update')->name('api.jobFair.update');
    Route::post('/job-fairs/delete', 'JobFairController@delete')->name('api.jobFair.delete');
    Route::post('/job-fairs/restore', 'JobFairController@restore')->name('api.jobFair.restore');
    Route::post('/job-fairs/force-delete', 'JobFairController@forceDelete')->name('api.jobFair.force-delete');


    /*================================================
        MemberJobFair Route
    ================================================*/
    Route::any('/member-job-fairs', 'MemberJobFairController@index')->name('api.memberJobFair.index');

    Route::post('/member-job-fairs/create', 'MemberJobFairController@create')->name('api.memberJobFair.create');
    Route::post('/member-job-fairs/update', 'MemberJobFairController@update')->name('api.memberJobFair.update');
    Route::post('/member-job-fairs/delete', 'MemberJobFairController@delete')->name('api.memberJobFair.delete');
    Route::post('/member-job-fairs/restore', 'MemberJobFairController@restore')->name('api.memberJobFair.restore');
    Route::post('/member-job-fairs/force-delete', 'MemberJobFairController@forceDelete')->name('api.memberJobFair.force-delete');


    /*================================================
        PageType Route
    ================================================*/
    Route::any('/page-types', 'PageTypeController@index')->name('api.pageType.index');

    Route::post('/page-types/create', 'PageTypeController@create')->name('api.pageType.create');
    Route::post('/page-types/update', 'PageTypeController@update')->name('api.pageType.update');
    Route::post('/page-types/delete', 'PageTypeController@delete')->name('api.pageType.delete');
    Route::post('/page-types/restore', 'PageTypeController@restore')->name('api.pageType.restore');
    Route::post('/page-types/force-delete', 'PageTypeController@forceDelete')->name('api.pageType.force-delete');


    /*================================================
        Tag Route
    ================================================*/
    Route::any('/tags', 'TagController@index')->name('api.tag.index');

    Route::post('/tags/create', 'TagController@create')->name('api.tag.create');
    Route::post('/tags/update', 'TagController@update')->name('api.tag.update');
    Route::post('/tags/delete', 'TagController@delete')->name('api.tag.delete');
    Route::post('/tags/restore', 'TagController@restore')->name('api.tag.restore');
    Route::post('/tags/force-delete', 'TagController@forceDelete')->name('api.tag.force-delete');


    /*================================================
        MemberCvUnique Route
    ================================================*/
    Route::any('/member-cv-uniques', 'MemberCvUniqueController@index')->name('api.memberCvUnique.index');

    Route::post('/member-cv-uniques/create', 'MemberCvUniqueController@create')->name('api.memberCvUnique.create');
    Route::post('/member-cv-uniques/update', 'MemberCvUniqueController@update')->name('api.memberCvUnique.update');
    Route::post('/member-cv-uniques/delete', 'MemberCvUniqueController@delete')->name('api.memberCvUnique.delete');
    Route::post('/member-cv-uniques/restore', 'MemberCvUniqueController@restore')->name('api.memberCvUnique.restore');
    Route::post('/member-cv-uniques/force-delete', 'MemberCvUniqueController@forceDelete')->name('api.memberCvUnique.force-delete');


    /*================================================
        MemberClientJobSave Route
    ================================================*/
    Route::any('/member-client-job-saves', 'MemberClientJobSaveController@index')->name('api.memberClientJobSave.index');

    Route::post('/member-client-job-saves/create', 'MemberClientJobSaveController@create')->name('api.memberClientJobSave.create');
    Route::post('/member-client-job-saves/update', 'MemberClientJobSaveController@update')->name('api.memberClientJobSave.update');
    Route::post('/member-client-job-saves/delete', 'MemberClientJobSaveController@delete')->name('api.memberClientJobSave.delete');
    Route::post('/member-client-job-saves/restore', 'MemberClientJobSaveController@restore')->name('api.memberClientJobSave.restore');
    Route::post('/member-client-job-saves/force-delete', 'MemberClientJobSaveController@forceDelete')->name('api.memberClientJobSave.force-delete');


    /*================================================
        UrlRedirect Route
    ================================================*/
    Route::any('/url-redirects', 'UrlRedirectController@index')->name('api.urlRedirect.index');

    Route::post('/url-redirects/create', 'UrlRedirectController@create')->name('api.urlRedirect.create');
    Route::post('/url-redirects/update', 'UrlRedirectController@update')->name('api.urlRedirect.update');
    Route::post('/url-redirects/delete', 'UrlRedirectController@delete')->name('api.urlRedirect.delete');
    Route::post('/url-redirects/restore', 'UrlRedirectController@restore')->name('api.urlRedirect.restore');
    Route::post('/url-redirects/force-delete', 'UrlRedirectController@forceDelete')->name('api.urlRedirect.force-delete');


    /*================================================
        RefSourceParent Route
    ================================================*/
    Route::any('/ref-source-parents', 'RefSourceParentController@index')->name('api.refSourceParent.index');

    Route::post('/ref-source-parents/create', 'RefSourceParentController@create')->name('api.refSourceParent.create');
    Route::post('/ref-source-parents/update', 'RefSourceParentController@update')->name('api.refSourceParent.update');
    Route::post('/ref-source-parents/delete', 'RefSourceParentController@delete')->name('api.refSourceParent.delete');
    Route::post('/ref-source-parents/restore', 'RefSourceParentController@restore')->name('api.refSourceParent.restore');
    Route::post('/ref-source-parents/force-delete', 'RefSourceParentController@forceDelete')->name('api.refSourceParent.force-delete');


    /*================================================
        MemberRefSource Route
    ================================================*/
    Route::any('/member-ref-sources', 'MemberRefSourceController@index')->name('api.memberRefSource.index');

    Route::post('/member-ref-sources/create', 'MemberRefSourceController@create')->name('api.memberRefSource.create');
    Route::post('/member-ref-sources/update', 'MemberRefSourceController@update')->name('api.memberRefSource.update');
    Route::post('/member-ref-sources/delete', 'MemberRefSourceController@delete')->name('api.memberRefSource.delete');
    Route::post('/member-ref-sources/restore', 'MemberRefSourceController@restore')->name('api.memberRefSource.restore');
    Route::post('/member-ref-sources/force-delete', 'MemberRefSourceController@forceDelete')->name('api.memberRefSource.force-delete');


    /*================================================
        StatReferral Route
    ================================================*/
    Route::any('/stat-referrals', 'StatReferralController@index')->name('api.statReferral.index');

    Route::post('/stat-referrals/create', 'StatReferralController@create')->name('api.statReferral.create');
    Route::post('/stat-referrals/update', 'StatReferralController@update')->name('api.statReferral.update');
    Route::post('/stat-referrals/delete', 'StatReferralController@delete')->name('api.statReferral.delete');
    Route::post('/stat-referrals/restore', 'StatReferralController@restore')->name('api.statReferral.restore');
    Route::post('/stat-referrals/force-delete', 'StatReferralController@forceDelete')->name('api.statReferral.force-delete');


    /*================================================
        StatJobCategory Route
    ================================================*/
    Route::any('/stat-job-categories', 'StatJobCategoryController@index')->name('api.statJobCategory.index');

    Route::post('/stat-job-categories/create', 'StatJobCategoryController@create')->name('api.statJobCategory.create');
    Route::post('/stat-job-categories/update', 'StatJobCategoryController@update')->name('api.statJobCategory.update');
    Route::post('/stat-job-categories/delete', 'StatJobCategoryController@delete')->name('api.statJobCategory.delete');
    Route::post('/stat-job-categories/restore', 'StatJobCategoryController@restore')->name('api.statJobCategory.restore');
    Route::post('/stat-job-categories/force-delete', 'StatJobCategoryController@forceDelete')->name('api.statJobCategory.force-delete');

    /*================================================
        CrawlKeyword Route
    ================================================*/
    Route::any('/crawl-keywords', 'CrawlKeywordController@index')->name('api.crawlKeyword.index');

    Route::post('/crawl-keywords/create', 'CrawlKeywordController@create')->name('api.crawlKeyword.create');
    Route::post('/crawl-keywords/update', 'CrawlKeywordController@update')->name('api.crawlKeyword.update');
    Route::post('/crawl-keywords/delete', 'CrawlKeywordController@delete')->name('api.crawlKeyword.delete');
    Route::post('/crawl-keywords/restore', 'CrawlKeywordController@restore')->name('api.crawlKeyword.restore');
    Route::post('/crawl-keywords/force-delete', 'CrawlKeywordController@forceDelete')->name('api.crawlKeyword.force-delete');


    /*================================================
        CrawlJob Route
    ================================================*/
    Route::any('/crawl-jobs', 'CrawlJobController@index')->name('api.crawlJob.index');

    Route::post('/crawl-jobs/create', 'CrawlJobController@create')->name('api.crawlJob.create');
    Route::post('/crawl-jobs/update', 'CrawlJobController@update')->name('api.crawlJob.update');
    Route::post('/crawl-jobs/delete', 'CrawlJobController@delete')->name('api.crawlJob.delete');
    Route::post('/crawl-jobs/restore', 'CrawlJobController@restore')->name('api.crawlJob.restore');
    Route::post('/crawl-jobs/force-delete', 'CrawlJobController@forceDelete')->name('api.crawlJob.force-delete');

    /*================================================
        ScheduleSetting Route
    ================================================*/
    Route::any('/schedule-settings', 'ScheduleSettingController@index')->name('api.scheduleSetting.index');

    Route::post('/schedule-settings/create', 'ScheduleSettingController@create')->name('api.scheduleSetting.create');
    Route::post('/schedule-settings/update', 'ScheduleSettingController@update')->name('api.scheduleSetting.update');
    Route::post('/schedule-settings/delete', 'ScheduleSettingController@delete')->name('api.scheduleSetting.delete');
    Route::post('/schedule-settings/restore', 'ScheduleSettingController@restore')->name('api.scheduleSetting.restore');
    Route::post('/schedule-settings/force-delete', 'ScheduleSettingController@forceDelete')->name('api.scheduleSetting.force-delete');


    /*================================================
        MemberClientFollow Route
    ================================================*/
    Route::any('/member-client-follows', 'MemberClientFollowController@index')->name('api.memberClientFollow.index');

    Route::post('/member-client-follows/create', 'MemberClientFollowController@create')->name('api.memberClientFollow.create');
    Route::post('/member-client-follows/update', 'MemberClientFollowController@update')->name('api.memberClientFollow.update');
    Route::post('/member-client-follows/delete', 'MemberClientFollowController@delete')->name('api.memberClientFollow.delete');
    Route::post('/member-client-follows/restore', 'MemberClientFollowController@restore')->name('api.memberClientFollow.restore');
    Route::post('/member-client-follows/force-delete', 'MemberClientFollowController@forceDelete')->name('api.memberClientFollow.force-delete');


    /*================================================
        Gallery Route
    ================================================*/
    Route::any('/galleries', 'GalleryController@index')->name('api.gallery.index');

    Route::post('/galleries/create', 'GalleryController@create')->name('api.gallery.create');
    Route::post('/galleries/update', 'GalleryController@update')->name('api.gallery.update');
    Route::post('/galleries/delete', 'GalleryController@delete')->name('api.gallery.delete');
    Route::post('/galleries/restore', 'GalleryController@restore')->name('api.gallery.restore');
    Route::post('/galleries/force-delete', 'GalleryController@forceDelete')->name('api.gallery.force-delete');


    /*================================================
        UniversityMajor Route
    ================================================*/
    Route::any('/university-majors', 'UniversityMajorController@index')->name('api.universityMajor.index');

    Route::post('/university-majors/create', 'UniversityMajorController@create')->name('api.universityMajor.create');
    Route::post('/university-majors/update', 'UniversityMajorController@update')->name('api.universityMajor.update');
    Route::post('/university-majors/delete', 'UniversityMajorController@delete')->name('api.universityMajor.delete');
    Route::post('/university-majors/restore', 'UniversityMajorController@restore')->name('api.universityMajor.restore');
    Route::post('/university-majors/force-delete', 'UniversityMajorController@forceDelete')->name('api.universityMajor.force-delete');


    /*================================================
        UniversityMajorMapping Route
    ================================================*/
    Route::any('/university-major-mappings', 'UniversityMajorMappingController@index')->name('api.universityMajorMapping.index');

    Route::post('/university-major-mappings/create', 'UniversityMajorMappingController@create')->name('api.universityMajorMapping.create');
    Route::post('/university-major-mappings/update', 'UniversityMajorMappingController@update')->name('api.universityMajorMapping.update');
    Route::post('/university-major-mappings/delete', 'UniversityMajorMappingController@delete')->name('api.universityMajorMapping.delete');
    Route::post('/university-major-mappings/restore', 'UniversityMajorMappingController@restore')->name('api.universityMajorMapping.restore');
    Route::post('/university-major-mappings/force-delete', 'UniversityMajorMappingController@forceDelete')->name('api.universityMajorMapping.force-delete');


    /*================================================
        RecruitmentJob Route
    ================================================*/
    Route::any('/recruitment-jobs', 'RecruitmentJobController@index')->name('api.recruitmentJob.index');

    Route::post('/recruitment-jobs/create', 'RecruitmentJobController@create')->name('api.recruitmentJob.create');
    Route::post('/recruitment-jobs/update', 'RecruitmentJobController@update')->name('api.recruitmentJob.update');
    Route::post('/recruitment-jobs/delete', 'RecruitmentJobController@delete')->name('api.recruitmentJob.delete');
    Route::post('/recruitment-jobs/restore', 'RecruitmentJobController@restore')->name('api.recruitmentJob.restore');
    Route::post('/recruitment-jobs/force-delete', 'RecruitmentJobController@forceDelete')->name('api.recruitmentJob.force-delete');


    /*================================================
        RecruitmentJobApply Route
    ================================================*/
    Route::any('/recruitment-job-applies', 'RecruitmentJobApplyController@index')->name('api.recruitmentJobApply.index');

    Route::post('/recruitment-job-applies/create', 'RecruitmentJobApplyController@create')->name('api.recruitmentJobApply.create');
    Route::post('/recruitment-job-applies/update', 'RecruitmentJobApplyController@update')->name('api.recruitmentJobApply.update');
    Route::post('/recruitment-job-applies/delete', 'RecruitmentJobApplyController@delete')->name('api.recruitmentJobApply.delete');
    Route::post('/recruitment-job-applies/restore', 'RecruitmentJobApplyController@restore')->name('api.recruitmentJobApply.restore');
    Route::post('/recruitment-job-applies/force-delete', 'RecruitmentJobApplyController@forceDelete')->name('api.recruitmentJobApply.force-delete');


    /*================================================
        MemberCv Route
    ================================================*/
    Route::any('/member-cvs', 'MemberCvController@index')->name('api.memberCv.index');

    Route::post('/member-cvs/create', 'MemberCvController@create')->name('api.memberCv.create');
    Route::post('/member-cvs/update', 'MemberCvController@update')->name('api.memberCv.update');
    Route::post('/member-cvs/delete', 'MemberCvController@delete')->name('api.memberCv.delete');
    Route::post('/member-cvs/restore', 'MemberCvController@restore')->name('api.memberCv.restore');
    Route::post('/member-cvs/force-delete', 'MemberCvController@forceDelete')->name('api.memberCv.force-delete');


    /*================================================
        ExaminationQuestion Route
    ================================================*/
    Route::any('/examination-questions', 'ExaminationQuestionController@index')->name('api.examinationQuestion.index');

    Route::post('/examination-questions/create', 'ExaminationQuestionController@create')->name('api.examinationQuestion.create');
    Route::post('/examination-questions/update', 'ExaminationQuestionController@update')->name('api.examinationQuestion.update');
    Route::post('/examination-questions/delete', 'ExaminationQuestionController@delete')->name('api.examinationQuestion.delete');
    Route::post('/examination-questions/restore', 'ExaminationQuestionController@restore')->name('api.examinationQuestion.restore');
    Route::post('/examination-questions/force-delete', 'ExaminationQuestionController@forceDelete')->name('api.examinationQuestion.force-delete');

});
