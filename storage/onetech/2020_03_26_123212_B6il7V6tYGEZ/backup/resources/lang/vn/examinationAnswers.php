<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationAnswers translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationAnswers' => 'Xem phần đáp án',
    'examinationAnswers_index' => 'Xem phần đáp án',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'answer' => 'Trả lời',
	
    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
