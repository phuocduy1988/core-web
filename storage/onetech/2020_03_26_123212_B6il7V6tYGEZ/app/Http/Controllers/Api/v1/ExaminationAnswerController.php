<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:24:34
 * File: ExaminationAnswerController.php
 */
namespace App\Http\Controllers\Api\v1;

use App\Models\ExaminationAnswer;
use Illuminate\Http\Request;
use App\Helpers\MediaHelper;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ExaminationAnswerRequest;
use App\Http\Resources\ExaminationAnswerCollection;
use App\Http\Resources\ExaminationAnswer as ExaminationAnswerResource;
use App\Http\Controllers\Api\v1\ApiBaseController;

class ExaminationAnswerController extends ApiBaseController
{
    /**
     * Display a listing of the examinationAnswers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order          = (int)$request->get('order', 1);
            $orderColumn    = $request->get('order_culumn', 'id');
            $limit          = (int)$request->get('limit', 1);
            $page           = (int)$request->get('page', 1);

            if ($page == 0) {
                $page = 1;
            }

            $strOrder = 'DESC';
            if ($order == 0) {
                $strOrder = 'ASC';
            }
            //Pagination
            $offset = ($page - 1) * $limit;

            //Declare Model
            $examinationAnswers = ExaminationAnswer::query();

			$examinationAnswers = $examinationAnswers->with('examinationQuestion');
            //Generate search option here
            //Get data with pagination and order by(Default:: id desc)
            $examinationAnswers = $examinationAnswers
                                ->offset($offset)
                                ->limit($limit)
                                ->orderBy($orderColumn, $strOrder)
                                ->get();

            return $this->jsonOK($examinationAnswers);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Store a newly created examinationAnswers in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            //check validator
            $rules = [
                'answer' => ['required', 'max:191'],

            ];
            $nameValidators = [
                //API_NAME_VALIDATORS
            ];
            $validator = Validator::make($request->all(), $rules, [], $nameValidators);

            if ($validator->fails()) {
                return $this->jsonNG($validator->errors());
            }

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = ''.
                $request->get('answer', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            //create new ExaminationAnswer
            $examinationAnswer = new ExaminationAnswer();
            $examinationAnswer->answer = $request->get('answer');

			$examinationAnswer->examination_question_id = $request->get('examination_question_id');
			$examinationAnswer->is_correct = $request->get('is_correct', 0);
			$examinationAnswer->answer_right = $request->get('answer_right');

            //--UPDATE_GENCODE_CREATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationAnswer->save();

            //Check response data
            $responseCode = is_null($examinationAnswer->id) ? 201 : 200;
            return $this->jsonOK(['data' => $examinationAnswer], $responseCode);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Update examinationAnswers in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.
                $request->get('answer', '').
                env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationAnswer = ExaminationAnswer::find($id);
            if(is_null($examinationAnswer)) {
                return $this->jsonNG(trans('error.client_invalid_data'));
            }
            if ($request->get('answer')) {
				$examinationAnswer->answer = $request->get('answer');
			}

			if ($request->get('examination_question_id')) {
				$examinationAnswer->examination_question_id = $request->get('examination_question_id');
			}
			if ($request->get('is_correct')) {
				$examinationAnswer->is_correct = $request->get('is_correct');
			}
			if ($request->get('answer_right')) {
				$examinationAnswer->answer_right = $request->get('answer_right');
			}

            //--UPDATE_GENCODE_UPDATE_DO_NOT_DELETE_OR_CHANGE_THIS--
            $examinationAnswer->save();

            return $this->jsonOK(['data' => $examinationAnswer]);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }

    /**
     * Remove the specified examinationAnswers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationAnswer = ExaminationAnswer::find($id);
            if(!is_null($examinationAnswer)) {
                $examinationAnswer->delete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }

    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationAnswer = ExaminationAnswer::withTrashed()->find($id);
            if(!is_null($examinationAnswer)) {
                $examinationAnswer->restore();
                return $this->jsonOK(['data' => $examinationAnswer]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
             return $this->jsonCatchException($e->getMessage());
        }
    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete(Request $request)
    {
        try {
            $id = (int)$request->get('id');

            //Check POST hashkey
            $hashKey = $request->get('hash_key', '');
            $hashString = $id.env('HASH_KEY', 'OT2018ASIA_TM');
            $hashKeyServer = Helper::encryptMd5($hashString);
            $this->checkHashKey($hashKey, $hashKeyServer, $hashString);

            $examinationAnswer = ExaminationAnswer::withTrashed()->find($id);
            if(!is_null($examinationAnswer)) {
                $examinationAnswer->forceDelete();
                return $this->jsonOK(['no_content' => true]);
            }
            return $this->jsonNG(trans('error.client_invalid_data'));
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonCatchException($e->getMessage());
        }
    }
}
