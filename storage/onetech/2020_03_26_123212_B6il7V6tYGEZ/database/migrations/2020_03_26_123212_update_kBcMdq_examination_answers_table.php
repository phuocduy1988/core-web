<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKBcMdqExaminationAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('examination_answers', function (Blueprint $table) {
            $table->boolean('is_correct')->nullable();
			$table->text('answer_right')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examination_answers', function (Blueprint $table) {
            $table->dropColumn('is_correct');
			$table->dropColumn('answer_right');
			
        });
    }
}
