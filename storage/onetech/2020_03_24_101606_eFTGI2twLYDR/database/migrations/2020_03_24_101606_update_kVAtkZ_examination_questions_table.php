<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKVAtkZExaminationQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('examination_questions', function (Blueprint $table) {
            $table->text('answer_a');
			$table->text('answer_b');
			$table->text('answer_c');
			$table->text('answer_d');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examination_questions', function (Blueprint $table) {
            $table->dropColumn('answer_a');
			$table->dropColumn('answer_b');
			$table->dropColumn('answer_c');
			$table->dropColumn('answer_d');
			
        });
    }
}
