<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2020-03-24
 * Time: 21:25:18
 * File: ExaminationQuestionRequest.php
 */
namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ExaminationQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['nullable'],
			'description' => ['nullable'],
			'status' => ['nullable', 'boolean'],
			'multi_choices' => ['nullable'],
			'image' => ['nullable', 'max:191'],

			'answer_right' => ['required', 'max:191'],

			'answer_a' => ['required'],
			'answer_b' => ['required'],
			'answer_c' => ['required'],
			'answer_d' => ['required'],

            //--UPDATE_GENCODE_RULE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            
			'answer_right.required' => trans('validation.required', ['attribute' => trans('examinationQuestions.answer_right')]),
			
			'answer_a.required' => trans('validation.required', ['attribute' => trans('examinationQuestions.answer_a')]),
						'answer_b.required' => trans('validation.required', ['attribute' => trans('examinationQuestions.answer_b')]),
						'answer_c.required' => trans('validation.required', ['attribute' => trans('examinationQuestions.answer_c')]),
						'answer_d.required' => trans('validation.required', ['attribute' => trans('examinationQuestions.answer_d')]),
			
            //--UPDATE_GENCODE_MESSAGE_DO_NOT_DELETE_OR_CHANGE_THIS--
        ];
    }
}
