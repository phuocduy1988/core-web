@extends('backend.layouts.app')

@section('title') {{ trans('examinationQuestions.examinationQuestions') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

    @include('backend._partials.breadcrumb',
			['page_list' => array(
				['title' => trans('examinationQuestions.examinationQuestions'), 'link' => '#']
			)])

    <div id="app" class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        @include('backend._partials.vue-notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
                            <fieldset>
                                <div class="col-sm-7">
                                    <div v-for="(item, index) in questions">
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('galleries.photos') }}
                                                @if(true)
                                                    <span class="red-color">{{ trans('table.required') }}</span>
                                                @endif
                                            </label>
                                            <div class="media-box col-sm-6">
                                                <div v-if="item.image" id="js-img-preview" class="img-preview">
                                                    <img class="js-img-change img-responsive" :src="item.image" />
                                                </div>
                                                <div class="media-action">
                                                    <a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">
                                                        <i class="fa fa-warning"></i>
                                                        {{ trans('button.delete') }}
                                                    </a>
                                                </div>
                                                <input type="hidden" class="media-change" name="thumbnail" value="" v-model="item.image" />
                                                <button type="button" @click.prevent="showMediaPop(index)" class="popup_selector">
                                                    {{ trans('message.select_image') }}
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('galleries.title') }}
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea rows="1" v-model="item.title" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('galleries.url') }}
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea rows="1" v-model="item.url" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('galleries.description') }}
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea rows="2" v-model="item.description" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                            </label>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="btn-sm btn-warning" @click.prevent="remove(index)" v-show="index || ( !index && form.photos.length > 1)">Remove</a>
                                                <a href="javascript:void(0)" class="btn-sm btn-success" @click.prevent="add(index)">Add more</a>
                                            </div>
                                        </div>
                                        <hr>
                                        <br>
                                    </div>
                                </div>

                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-6 text-center js-page-btn">
                                        <a href="{{ route('gallery.index') }}"
                                           class="btn btn-large btn-default">
                                            <i class="fa fa-caret-left"></i>
                                            {{ trans('button.cancel') }}
                                        </a>
                                        <a href="#" class="btn btn-large btn-info" @click.prevent="saveChanges">
                                            <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    {{--	<script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>--}}
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <script src="{!! asset('backend/js/vue.js') !!}"></script>
    <script src="{!! asset('backend/js/axios.min.js') !!}"></script>
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                error: false, // check isset Error
                errors: {}, // object error (all error)
                message: '',
                errorMessage: '',
                question: {
                    id: 0,
                    title: "",
                    description: "",
                    multi_choices: [],
                    answer_right: "",
                    image: "",
                },
                questions: [],
            },
            mounted: function () {
                let question = {!! json_encode($question) !!};
                if (question && question != []) {
                    this.questions.push(question);
                }
                else {
                    this.questions.push(this.question);
                }
            },
            methods: {
                saveChanges: function () {
                    $(".ot-loading").show()
                    axios.post("{{ route('examinationQuestion.save') }}", this.form)
                        .then((response) => {
                            this.error = false
                            this.errors = {}
                            this.message = response.data.message
                            alert('Tạo thành công!!');
                            location.replace('{{ route('examinationQuestion.index') }}');
                        }).catch((error) => {
                        $('body,html').animate({
                            scrollTop: 0
                        }, 500)
                        $(".ot-loading").hide()
                        this.error = true
                        var errors = error.response.data.errors
                        if (typeof (errors) === 'object') {
                            this.errors = errors
                            this.errorMessage = ''
                        } else {
                            this.errors = {}
                            this.errorMessage = errors.message
                        }
                    })
                },
                add: function (index) {
                    this.questions.push(this.question);
                },
                remove: function (index) {
                    this.form.photos.splice(index, 1);
                },
                showMediaPop: function (index) {
                    var updateID = index; // Btn id clicked
                    var elfinderUrl = '/elfinder/popup/';

                    // trigger the reveal modal with elfinder inside
                    var triggerUrl = elfinderUrl + updateID;
                    $.colorbox({
                        href: triggerUrl,
                        fastIframe: true,
                        iframe: true,
                        width: '70%',
                        height: '80%'
                    });
                }
            },
        });

        // function to update the file selected by elfinder
        function processSelectedFile(filePath, requestingField) {
            $path = '/' + filePath;
            $path = $path.replace('\\', '/'); // format file path
            $('#' + requestingField).val(filePath);
            app.questions[requestingField].image = $path;
        }

    </script>
@stop
