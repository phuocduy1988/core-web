<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2020-03-24
 * Time: 21:25:18
 * File: ExaminationQuestionController.php
 */
namespace App\Http\Controllers\Backend;

use App\Models\ExaminationQuestion;
use Illuminate\Http\Request;
use App\Http\Requests\ExaminationQuestionRequest;
use App\Http\Controllers\Backend\Controller;

class ExaminationQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            //Get param page
            $page           = $request->get('page', 1);
            //Get data from position
            $offSet         = ($page - 1) * $this->_gridPageSize;

            $examinationQuestions = ExaminationQuestion::query();

            if ($request->has('keyword')) {
                //Search via param key word
                $examinationQuestions = $examinationQuestions->where(function($query) use ($request) {
                     /** @var $query Illuminate\Database\Query\Builder
                     * return $query->where()->orWhere()->orWhere()
                     * Query sample: select * from sample where a = a and (b=b or c=b or d = b)
                     * @Please do not remove search id
                     */
                    return $query
                            ->where('id', 'LIKE', '%'.$request->get('keyword').'%')


								->orWhere('title', 'LIKE', '%'.$request->get('keyword').'%')
								->orWhere('description', 'LIKE', '%'.$request->get('keyword').'%')
								 ;
                });
            }
            //Order via param _orderBy(Default:: id), _sortType(Default:: DESC)
            $examinationQuestions = $examinationQuestions->orderBy($this->_orderBy, $this->_sortType);

            //Get data with pagination
            $examinationQuestions = $examinationQuestions
                            ->skip($offSet)
                            ->take($this->_gridPageSize)
                            ->paginate($this->_gridPageSize);
            return view('backend.examinationQuestions.index')->with('examinationQuestionsData', $examinationQuestions);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(ExaminationQuestion $examinationQuestion = null)
    {
        try {
            //Get data for create new or update
            $examinationQuestion = $examinationQuestion ?: new ExaminationQuestion;
            return view('backend.examinationQuestions.form')->with('examinationQuestion', $examinationQuestion);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * Check validation via ExaminationQuestionRequest
     * @param  ExaminationQuestionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
        try {
        	$data = $request->get('form', []);
        	if(is_array($data)) {
        		foreach ($data as $item) {
        			try {
				        //Get data for create new or update via id
				        $examinationQuestion = ExaminationQuestion::firstOrNew(['id' => $item['id']]);
				        $examinationQuestion->title = $item['title'];
				        $examinationQuestion->description = $item['description'];
				        $examinationQuestion->image = $item['image'];
				        $examinationQuestion->answer_right = trim($item['answer_right']);
				        $examinationQuestion->answer_a = trim($item['answer_a']);
				        $examinationQuestion->answer_b = trim($item['answer_b']);
				        $examinationQuestion->answer_c = trim($item['answer_c']);
				        $examinationQuestion->answer_d = trim($item['answer_d']);
				        if(
				        	$examinationQuestion->title
					        && $examinationQuestion->answer_right
					        && $examinationQuestion->answer_a
					        && $examinationQuestion->answer_b
					        && $examinationQuestion->answer_c
					        && $examinationQuestion->answer_d
				        ) {
					        $examinationQuestion->status = 1;
				        }
				        $examinationQuestion->save();
			        }
			        catch (\Exception $e) {
				        \Helper::writeLogException($e);
				        continue;
			        }
		        }
        		return $this->jsonOK(['message'=> 'Nhập dữ liệu thành công']);
	        }

	        return $this->jsonNG('Dữ liệu nhập vào bị lỗi');
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return $this->jsonNG($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ExaminationQuestion $examinationQuestion)
    {
        try {
           $examinationQuestion->delete();
           $message = trans('message.deleted_success');
           return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }

    /**
     * Restore the specified deleted resource to database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($examinationQuestionId)
    {
        try {
            $examinationQuestion = ExaminationQuestion::withTrashed()->find($examinationQuestionId);
            $examinationQuestion->restore();
            $message = 'examinationQuestion Restored.';
            return redirect()->back()->with('alert', $message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }

    }
    /**
    * Delete forever
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function forceDelete($examinationQuestionId)
    {
        try {
            $examinationQuestion = ExaminationQuestion::withTrashed()->find($examinationQuestionId);
            $examinationQuestion->forceDelete();
            $message = 'examinationQuestion permanently deleted.';
            return redirect()->back()->withMessage($message);
        }
        catch (\Exception $e) {
            \Helper::writeLogException($e);
            return view('errors.500')->withErrors($e->getMessage());
        }
    }
}
