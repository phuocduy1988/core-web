@extends('backend.layouts.app')

@section('title') {{ trans('examinationQuestions.examinationQuestions') }} @stop

@section('content')
    @include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('examinationQuestions.examinationQuestions'), 'link' => route('examinationQuestion.index')]
    	)])

    <div class="wrapper wrapper-content animated fadeInRight clearfix">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <!-- Search block START -->
                        {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => 'examinationQuestion.index']) !!}
                            <table class="table table-bordered m-b-md">
                                <tr>
                                    <td class="text-right"><strong>{{ trans('label.keyword') }}</strong></td>
                                    <td colspan="3">
                                        <div class="form-group">
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm " name="keyword" type="text" value="{{ Request::get('keyword') }}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="row m-b-md">
                                <div class="col-sm-12 text-right">
                                    <button id="btn-search" type="submit" action="confirm" class="btn btn-large btn-default">
                                        <i class="fa fa fa-search"></i> {{ trans('button.search') }}
                                    </button>
                                    <a  href="{{ route('examinationQuestion.new') }}" class="btn btn-large btn-info">
                                        <i class="fa fa-plus-circle"></i> {{ trans('button.create') }}
                                    </a>
                                </div>
                            </div>
                        <!-- Search block END -->
                        <!-- List content START -->
                        @include('backend._partials.sort-field')
                        <div class="table-responsive">
                            <table class="table table-bordered js-table">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ trans('examinationQuestions.id') }} @include('backend._partials.controls.sort-icon', ['field' => 'id'])</th>
										<th>{{ trans('examinationQuestions.title') }} </th>
										<th>{{ trans('examinationQuestions.description') }} </th>
										<th>{{ trans('examinationQuestions.answer_a') }} </th>
										<th>{{ trans('examinationQuestions.answer_b') }} </th>
										<th>{{ trans('examinationQuestions.answer_c') }} </th>
										<th>{{ trans('examinationQuestions.answer_d') }} </th>
	                                    <th>{{ trans('examinationQuestions.answer_right') }} </th>
	                                    <th>{{ trans('examinationQuestions.status') }} </th>
	                                    <th>{{ trans('examinationQuestions.image') }} </th>

                                        {{-- --UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                        <th>{{ trans('label.edit') }}</th>
                                        <th>{{ trans('label.delete') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($examinationQuestionsData) > 0)
                                        @foreach($examinationQuestionsData as $examinationQuestion)
                                            <tr>
                                                <td class="text-center"> {!! $examinationQuestion->id !!}</td>
												<td class="text-left"> {!! $examinationQuestion->title !!}</td>
												<td class="text-left"> {!! $examinationQuestion->description !!}</td>
												<td class="text-left"> {!! $examinationQuestion->answer_a !!}</td>
												<td class="text-left"> {!! $examinationQuestion->answer_b !!}</td>
												<td class="text-left"> {!! $examinationQuestion->answer_c !!}</td>
												<td class="text-left"> {!! $examinationQuestion->answer_d !!}</td>
	                                            <td class="text-center"> {!! $examinationQuestion->answer_right !!}</td>
	                                            <td class="text-center"><a href="javascript:void(0);" table="{{ $examinationQuestion->getTable() }}" id-value="{{ isset($examinationQuestion->id) ? $examinationQuestion->id : '' }}" column-name="status" boolean="{{ $examinationQuestion->status }}" class="js-table-change-boolean label @if($examinationQuestion->status == 1) label-success @else label-warning @endif"> {{ showBoolean($examinationQuestion->status) }} </a></td>
	                                            <td class="text-center"><img src="{!! $examinationQuestion->image !!}" width="70px"></td>
	                                            {{-- --UPDATE_GENCODE_ROW_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                                <td class="text-center add-relation">
                                                    <a title="Edit" href="{{ route('examinationQuestion.form', [$examinationQuestion->id]) }}"><i class="fa fa-edit"></i></a>
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" data-href="{{ route('examinationQuestion.delete', [$examinationQuestion->id]) }}" class="js-delete" data-message="{{ trans('message.alert_delete') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="11">{{ trans('message.no_record') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <!-- Show pagination -->
                            {!! $examinationQuestionsData->appends(array(
                                                'grid_page_size'    => Request::get('grid_page_size'),
                                                'keyword'           => Request::get('keyword'),
                                                'sort_field'        => Request::get('sort_field'),
                                                'sort_type'         => Request::get('sort_type')
                                              ))->links('backend._partials.pagination'); !!}
                        </div>
                        {{ Form::close() }}
                    <!-- List content END -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('backend._partials.js.js-popup')
    <script>
		$('#btn-search').on('click', function(e) {
			e.preventDefault();
			// Get current URL
			 var url = window.location.href;

			var keyword = $("input[name='keyword']").val();

			var removeQueryString = url.split("?");
			if(removeQueryString.length == 2) {
				url = removeQueryString[0];
			}

			if(keyword) {
				url = url + '?keyword=' + keyword;
			}

			$('form').prop('action', url);
			$('form').submit();
		});
	</script>
@stop
