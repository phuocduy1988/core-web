<?php

return [

    /*
    |--------------------------------------------------------------------------
    | examinationQuestions translate for column fields
    |--------------------------------------------------------------------------
    */
    'examinationQuestions' => 'Câu hỏi',
    'examinationQuestions_index' => 'Câu hỏi',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'title' => 'Tiêu đề',
	'description' => 'Mô tả',
	'status' => 'Trạng thái',
	'multi_choices' => 'Trắc nghiệm',
	'image' => 'Hình ảnh',
	
	'answer_right' => 'Đáp án đúng',

	'answer_a' => 'Đáp án A',
	'answer_b' => 'Đáp án B',
	'answer_c' => 'Đáp án C',
	'answer_d' => 'Đáp án D',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
