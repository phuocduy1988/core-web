@extends('backend.layouts.app')

@section('title') {{ trans('examinationQuestions.examinationQuestions') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

    @include('backend._partials.breadcrumb',
			['page_list' => array(
				['title' => trans('examinationQuestions.examinationQuestions'), 'link' => '#']
			)])

    <div id="app" class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        @include('backend._partials.vue-notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
                            <fieldset>
                                <div class="col-sm-7">
                                    <div v-for="(item, index) in questions">
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.title') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea rows="1" v-model="item.title" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.description') }}
                                            </label>
                                            <div class="col-sm-6">
                                                <textarea rows="2" v-model="item.description" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.answer_a') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" v-model="item.answer_a" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.answer_b') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" v-model="item.answer_b" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.answer_c') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" v-model="item.answer_c" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.answer_d') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" v-model="item.answer_d" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.answer_right') }}
                                                <span class="label label-danger">{{ trans('table.required') }}</span>
                                            </label>
                                            <div class="col-sm-6">
                                                <input type="text" v-model="item.answer_right" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                                {{ trans('examinationQuestions.image') }}
                                                @if(true)
                                                    <span class="red-color">{{ trans('table.required') }}</span>
                                                @endif
                                            </label>
                                            <div class="media-box col-sm-6">
                                                <div v-if="item.image" id="js-img-preview" class="img-preview">
                                                    <img class="js-img-change img-responsive" :src="item.image" />
                                                </div>
                                                <div class="media-action">
                                                    <a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">
                                                        <i class="fa fa-warning"></i>
                                                        {{ trans('button.delete') }}
                                                    </a>
                                                </div>
                                                <input type="hidden" class="media-change" name="thumbnail" value="" v-model="item.image" />
                                                <button type="button" @click.prevent="showMediaPop(index)" class="popup_selector">
                                                    {{ trans('message.select_image') }}
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="text-right control-label col-sm-4 col-md-3">
                                            </label>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="btn-sm btn-warning" @click.prevent="remove(index)" v-show="index || ( !index && questions.length > 1)">Xoá</a>
                                                <a href="javascript:void(0)" class="btn-sm btn-success" @click.prevent="add(index)">Thêm câu hỏi</a>
                                            </div>
                                        </div>
                                        <hr>
                                        <br>
                                    </div>
                                </div>
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-6 text-center js-page-btn">
                                        <a href="{{ route('examinationQuestion.index') }}"
                                           class="btn btn-large btn-default">
                                            <i class="fa fa-caret-left"></i>
                                            {{ trans('button.cancel') }}
                                        </a>
                                        <a href="#" class="btn btn-large btn-info" @click.prevent="saveChanges">
                                            <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    {{--	<script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>--}}
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <script src="{!! asset('backend/js/vue.js') !!}"></script>
    <script src="{!! asset('backend/js/axios.min.js') !!}"></script>
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                error: false, // check isset Error
                errors: {}, // object error (all error)
                message: '',
                errorMessage: '',
                questions: [
                    {
                        id: 0,
                        title: "",
                        description: "",
                        answer_right: "",
                        answer_a: "",
                        answer_b: "",
                        answer_c: "",
                        answer_d: "",
                        image: "",
                    }
                ],
            },
            mounted: function () {
                let question = {!! json_encode($examinationQuestion) !!};
                if (question && typeof question.id != "undefined") {
                    this.questions[0].id = question.id;
                    this.questions[0].title = question.title;
                    this.questions[0].description = question.description;
                    this.questions[0].answer_right = question.answer_right;
                    this.questions[0].answer_a = question.answer_a;
                    this.questions[0].answer_b = question.answer_b;
                    this.questions[0].answer_c = question.answer_c;
                    this.questions[0].answer_d = question.answer_d;
                    this.questions[0].image = question.image;
                }
            },
            methods: {
                saveChanges: function () {
                    $(".ot-loading").show()
                    console.log(this.questions);
                    axios.post("{{ route('examinationQuestion.save') }}", {form: this.questions})
                        .then((response) => {
                            this.error = false
                            this.errors = {}
                            this.message = response.data.message
                            alert(this.message);
                            location.replace('{{ route('examinationQuestion.index') }}');
                        }).catch((error) => {
                        $('body,html').animate({
                            scrollTop: 0
                        }, 500)
                        $(".ot-loading").hide()
                        this.error = true
                        var errors = error.response.data.errors
                        if (typeof (errors) === 'object') {
                            this.errors = errors
                            this.errorMessage = ''
                        } else {
                            this.errors = {}
                            this.errorMessage = errors.message
                        }
                    })
                },
                add: function (index) {
                    this.questions.splice(index + 1, 0, {
                        id: 0,
                        title: "",
                        description: "",
                        multi_choices: "",
                        answer_right: "",
                        answer_a: "",
                        answer_b: "",
                        answer_c: "",
                        answer_d: "",
                        image: "",
                    });
                },
                remove: function (index) {
                    this.questions.splice(index, 1);
                },
                showMediaPop: function (index) {
                    var updateID = index; // Btn id clicked
                    var elfinderUrl = '/elfinder/popup/';

                    // trigger the reveal modal with elfinder inside
                    var triggerUrl = elfinderUrl + updateID;
                    $.colorbox({
                        href: triggerUrl,
                        fastIframe: true,
                        iframe: true,
                        width: '70%',
                        height: '80%'
                    });
                }
            },
        });

        // function to update the file selected by elfinder
        function processSelectedFile(filePath, requestingField) {
            console.log(requestingField);
            $path = '/' + filePath;
            $path = $path.replace('\\', '/'); // format file path
            $('#' + requestingField).val(filePath);
            app.questions[requestingField].image = $path;
        }

    </script>
@stop
