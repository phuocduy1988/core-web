<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2020-03-24
 * Time: 21:25:18
 * File: ExaminationQuestion.php
 */
namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExaminationQuestion extends BaseModel
{
    //Declare table name
    protected $table = 'examination_questions';
    use SoftDeletes;
    protected $fillable = ['id'];

   //Write activity log
    public static function boot() {

    		parent::boot();

    		// create a event to happen on updating
    		static::created(function($table)  {
    			if(isset($table) && !is_null($table)) {
    				$userId = \Auth::id();
    				$createData = json_encode($table->getAttributes());
    				$dataInsert = [
    					'table_model' => $table->getTable().'_'.$table->id,
    					'type' => 'Created',
    					'user_agent' => \Request::header('User-Agent'),
    					'original' => null,
    					'attribute' => $createData,
    					'user_id' => strlen($userId) > 0 ? $userId : 1,
    					'user_type' => 1,
    					'created_at' => date('Y-m-d H:i:s'),
    				];

    				$logActivity = new LogActivity();
    				$logActivity->writeActivityLog($dataInsert);
    			}
    		});

    		static::updated(function($table)  {
    			if(isset($table) && !is_null($table)) {
    				$userId = \Auth::id();
    				$originalData = json_encode($table->getOriginal());
    				$changeData = json_encode($table->getAttributes());
    				$dataInsert = [
    					'table_model' => $table->getTable().'_'.$table->id,
    					'type' => 'Updated',
    					'user_agent' => \Request::header('User-Agent'),
    					'original' => $originalData,
    					'attribute' => $changeData,
    					'user_id' => strlen($userId) > 0 ? $userId : 1,
    					'user_type' => 1,
    					'created_at' => date('Y-m-d H:i:s'),
    				];
    				$logActivity = new LogActivity();
    				$logActivity->writeActivityLog($dataInsert);
    			}
    		});

    		// create a event to happen on deleting
    		static::deleted(function($table)  {
    			if(isset($table) && !is_null($table)) {
    				$userId = \Auth::id();
    				$deleteData = json_encode($table->getAttributes());
    				$dataInsert = [
    					'table_model' => $table->getTable().'_'.$table->id,
    					'type' => 'Deleted',
    					'user_agent' => \Request::header('User-Agent'),
    					'original' => $deleteData,
    					'attribute' => null,
    					'user_id' => strlen($userId) > 0 ? $userId : 1,
    					'user_type' => 1,
    					'created_at' => date('Y-m-d H:i:s'),
    				];
    				$logActivity = new LogActivity();
    				$logActivity->writeActivityLog($dataInsert);
    			}
    		});

    		// create a event to happen on retrieved
    		static::retrieved(function($table)  {
    			//
    		});
    	}
    /**
     * examinationAnswer hasMany examinationAnswers
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * */
    public function examinationAnswers()
    {
        return $this->hasMany(\App\Models\ExaminationAnswer::class);
    }
 }