<?php
/**
 * Created by: OneTech Builder
 * Email: dev@onetech.vn
 * Date: 2018-07-26
 * Time: 00:24:34
 * File: ExaminationAnswer.php
 */
namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExaminationAnswer extends BaseModel
{
    //Declare table name
    protected $table = 'examination_answers';
//    use SoftDeletes;
    protected $fillable = ['id'];
    /**
     * examinationResult belongsTo examinationResult
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * */
    public function examinationResult()
    {
        return $this->belongsTo(\App\Models\ExaminationResult::class);
    }
    /**
     * examination belongsTo examination
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * */
    public function examination()
    {
        return $this->belongsTo(\App\Models\Examination::class);
    }
    /**
     * member belongsTo member
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * */
    public function member()
    {
        return $this->belongsTo(\App\Models\Member::class);
    }
    /**
     * examinationType belongsTo examinationType
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * */
    public function examinationType()
    {
        return $this->belongsTo(\App\Models\ExaminationType::class);
    }
    /**
     * examinationQuestion belongsTo examinationQuestion
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * */
    public function examinationQuestion()
    {
        return $this->belongsTo(\App\Models\ExaminationQuestion::class);
    }
 }