<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Menu Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the menu library to build
	| the sidebar menu. You are free to change them to anythingaaa
	| you want to customize your views to better match your application.
	|
	*/
	'menu' 					=> 'メニュー',
	'list'					=> '登録一覧',
	'site'					=> 'サイト',
	'create'				=> '新規登録',
	'edit'					=> '編集',
	'detail'				=> '詳細',
	// Users
	'user'					=> 'ユーザ管理',
	'login'					=> 'ログイン',
	'logout'				=> 'ログアウト',
	// Notice
	'notice'				=> 'お知らせ管理',
	'builder'				=> 'Builder',
	'admin'				=> 'Administration',
	'builder_update'				=> 'Builder Update',
	'relationship'				=> 'Relationship',
	'back'				=> 'back',
	'log'				=> 'Logs',
	'access_log'				=> 'Access Logs',
	'request_log'				=> 'Request Logs',
	'activity_log'				=> 'Activity Logs',
	'system_log'				=> 'System Logs',
	'command_tool'			=> 'Command tools',
	'api_postman'			=> 'API Postman',
	'create_project'	    => 'Create Project',
	'update_file'     	    => 'Update File',
	'gen_code'				=> 'Generation code',
	'system'				=> 'Systems',
	'make-tools'			=> 'Make tools',
];
