<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the button name text.
    |
    */

    'create'                => 'Create',
    'continue_create'       => 'Continue create',
    'register'              => 'Register',
    'edit'                  => 'Edit',
    'save'                  => 'Save',
    'update'                => 'Update',
    'draft_save'            => 'Draff save',
    'confirm'               => 'Confirm',
    'delete'                => 'Delete',
    'cancel'                => 'Cancel',
    'add'                   => 'Add',
    'return'                => 'Return',
    'reset'                 => 'Reset',
    'yes'                   => 'Yes',
    'no'                    => 'No',
    'search'                => 'Search',
];
