<?php

return [
	'display-button'    => 'Display button',
	'group-data-button' => 'Group data',
	
	'dashboard' => 'Dashboard',
	
	'save-display-field' => 'Save display',
	
	'add-to'           => 'Add to',
	
	// Term of Use
	'change_status'    => 'Change status',
	'create_new'       => 'Create new',
	'approve_setting'  => 'Approve setting',
	
	// Page Name
	'home'             => 'Home',
	'iframe'           => 'Iframe',
	'logout'           => 'Logout',
	'file-manager'     => 'File manager',
	
	// Login page
	'email'            => 'Email',
	'confirm-email'    => 'confirm-email',
	'password'         => 'Password',
	'confirm-password' => 'Confirm password',
	'update-password'  => 'Update password',
	'login'            => 'Login',
	'forgot_password'  => 'Forgot password',
	'have_an_account'  => 'Do you have account yet?',
	'create_account'   => 'Create account',
	'media'            => 'Media',
	'mapping'            => 'Mapping',
];
