<?php

return [
	
	/*
	|--------------------------------------------------------------------------
	| members translate for column fields
	|--------------------------------------------------------------------------
	*/
	'settings'       => 'Settings',
	'settings_index' => 'Settings',
	'key'            => 'Key',
	'value'          => 'Value',
	'id'             => 'ID',
	'description'    => 'Description',
	'type'           => 'Type',
	'status'         => 'Status',
	'remember_token' => 'Remember Token',
	'clear_cache' => 'Clear cache',
	
	
	//--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
