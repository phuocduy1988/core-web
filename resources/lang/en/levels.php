<?php

return [
    'all'       => 'All',
    'emergency' => 'Emergency',
    'alert'     => 'Alert',
    'critical'  => 'Critical',
    'error'     => 'error',
    'warning'   => 'Warning',
    'notice'    => 'Notice',
    'info'      => 'Info',
    'debug'     => 'Debug',
];
