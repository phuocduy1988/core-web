<?php

return [
	
	/*
	|--------------------------------------------------------------------------
	| Labels Language Lines
	|--------------------------------------------------------------------------
	*/
	
	'text'						=> 'Text',
	'true'						=> 'True',
	'false'						=> 'False',
	'textarea'					=> 'Text Area',
	'number'					=> 'Number',
	'select'					=> 'Select',
	'checkbox'					=> 'Checkbox',
	'multi_checkbox'			=> 'Multi Checkbox',
	'radio'						=> 'Radio',
	'upload'					=> 'Upload',
	'image'						=> 'Image',
	'date'						=> 'Date',
	'date_time'					=> 'Date Time',
	'start_date'				=> 'Start Date',
	'end_date'					=> 'End Date',
	'startdate_end_date'		=> 'Public Date',
	'map_address'				=> 'Map Address',
	'map_latitude'				=> 'Latitude',
	'map_longitude'				=> 'Longitude',
	'map_zoom'					=> 'Zoom',
	'map_marker'				=> 'Marker',
	'google_map'				=> 'Google Map',
	'viewdate_nolimit' 			=> 'No end date specified',
	'require'					=> 'Required',
	'created_at'				=> 'Created date',
	
	// Status
	'private'					=> 'Private',
	'public'					=> 'Public',
	'previous'					=> 'Previous',
	'next' 						=> 'Next',
	'page_total'				=> 'Total',
	'page_from'					=> 'From',
	'page_end'					=> '',
	
	// Login Page
	'email' 					=> 'Email',
	'login_id' 					=> 'Log in ID',
	'user_id' 					=> 'User ID',
	'password'					=> 'Password',
	'password_confirm' 			=> 'Password confirm',
	
	'edit'						=> 'Edit',
	'view'						=> 'View',
	'delete'					=> 'Delete',
	'keyword'					=> 'Keyword',
	
	// Notice
	'comment'					=> 'Comment',
	'url'						=> 'URL',
	'picture'					=> 'Picture',
	'viewdate'					=> 'View date',
	
	// User
	'full_name'					=> 'Full name',
	'user_friend'				=> 'User friend',
	'user_call_history'			=> 'User call history',
	'bookmark'					=> 'Bookmark',
	'select_folder'				=> 'Select folder',
	'move'						=> 'Move',
	'create_folder'				=> 'Create folder',
	'rename_folder'				=> 'Rename folder',
	'delete_folder'				=> 'Delete folder',
	
	//Sound
	'sound_record'				=> 'During recording',
	
	//Tag
	'tag_name'					=> 'Tag name',
	'tag_type'					=> 'Tag type',
	
	//Publishing
	'publishing_publish'		=> 'Already',
	'publishing_unpublish'		=> 'Not',

];
