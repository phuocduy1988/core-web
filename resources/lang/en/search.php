<?php

return [
	
	/*
	|--------------------------------------------------------------------------
	| Search Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the menu library to build
	| the sidebar menu. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	
	'keyword'        => 'Keyword',
	'sort'           => 'Sort',
	'date'           => 'Date',
	'search'         => 'Search',
	'create'         => 'Create',
	'asc'            => 'ASC',
	'desc'           => 'DESC',
	'delete_message' => 'Are you sure to delete this field?',
	'submit'         => 'Submit',
	'cancel'         => 'Cancel',
	'ascending'      => 'Ascending',
	'descending'     => 'Descending',
];