<?php

return [
	
	/*
	|--------------------------------------------------------------------------
	| pages translate for column fields
	|--------------------------------------------------------------------------
	*/
	'pages'            => 'Pages',
	'pages_index'      => 'Pages',
	'created_at'       => 'Created at',
	'updated_at'       => 'Updated at',
	'deleted_at'       => 'Deleted at',
	'id'               => 'ID',
	'title'            => 'Title',
	'slug'             => 'Slug',
	'description'      => 'Description',
	'content'          => 'Content',
	'status'           => 'Status',
	'is_html'          => 'HTML',
	'lang'             => 'Lang',
	'meta_title'       => 'Meta title',
	'meta_description' => 'Meta description',
	'rating'           => 'rating',
	'comment'          => 'Comment',
	
	'schema' => 'Schema',

	//--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
