<?php

return [

    /*
    |--------------------------------------------------------------------------
    | statSales translate for column fields
    |--------------------------------------------------------------------------
    */
    'statSales' => 'Stat Sales',
    'statSales_index' => 'Stat Sales',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'date' => 'Date',
	'new_member' => 'New Member',
	'new_cv' => 'New CV',
	'new_job' => 'New Job',
	'new_client' => 'New Client',
	'new_news' => 'New News',
	'total_member' => 'Total Member',
	'total_cv' => 'Total CV',
	'total_job' => 'Total Job',
	'total_client' => 'Total client',
	'total_news' => 'Total news',
	
	'job_available' => 'Job Available',

	'it_cv' => 'IT',
	'comtor_cv' => 'Comtor',
	'total_it_cv' => 'Total IT',
	'total_comtor_cv' => 'Total comtor',
	'it_job' => 'IT',
	'total_it_job' => 'total_it_job',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
