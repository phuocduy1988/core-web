<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードは8文字以上入力して下さい。',
    'reset' => 'パスワードがリセットされました。',
    'sent' => 'パスワード再設定用のURLを入力のメールアドレスに送信しました。',
    'token' => 'パスワードの再設定は無効になりました',
    'user' => "該当するデータがありません。",

];
