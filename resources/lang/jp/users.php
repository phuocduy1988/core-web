<?php

return [

    /*
    |--------------------------------------------------------------------------
    | members translate for column fields
    |--------------------------------------------------------------------------
    */
    'users' => 'Admin Users',
    'users_index' => 'Admin Users',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'name' => 'Name',
	'email' => 'Email',
	'password' => 'Password',
	'status' => 'Status',
	'remember_token' => 'Remember Token',
	
	'user_data' => 'User Data',

	'phone' => 'Phone',
	'address' => 'Address',

	'remark' => 'Remark',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
