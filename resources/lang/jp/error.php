<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines ---- API & FRONTEND
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'default_error' => 'エラーが発生しました。
    該当のデータが存在しません。',
    'data_null' => '該当するデータがありません。',
    'request_empty' => 'リクエストがありません。もう一度お試しください。',
    'email_exist' => 'このメールアドレスが既に登録されております。ログインして下さい。',
    'banknumber_exist' => 'バンク番号が存在していましたさ',
    'email_not_exist' => 'そのメールアドレスは登録されていません。
    メールアドレスの登録のない方はトップページより新規登録をお願い致します。',
    'insert_error' => 'エラーが発生しました。登録できません。',
    'key_expired' => 'このキーの有効期限が切れています。他のキーで試してください。',
    'email_required' => 'メールアドレスを入力してください。',
    'phone_required' => '電話を入力してください。',
    'password_required' => 'パスワードが確認できませんでした。
    再度パスワードを入力してください。',
    //'error_nickname' => 'ニックネームは１０文字以内で入力してください。',
    'email_format' => 'メールアドレスが正しくありません。',
    'profile_email' => 'このメールアドレースはまだ登録していません。',
    'profile_required' => 'ニックネームとパスワードが未入力です。',
    'phone' => 'こちらの電話は既に登録されています。',

    'old_password' => '旧パスワードが正しくありません。',
    'send_mail' => 'メールを送信できませんでした。再度お試しください。',

    'auth_key' => '認証キーが確認できません。',
    'password_confirm' => 'パスワード確認が不一致しません',
    'social_login' => 'SNSでログインできません。',
    'exchange_null' => '交換履歴のデータがありません。',
    'exchange_failed'=>'ギフトコードの交換が出来ません。
        大変申し訳ございませんが、ギフトコードの在庫が切れております。
        土日祝祭日を除いた、４８時間以内に対応させていただきます。
        万が一４８時間以内に対応されない場合は、大変お手数ですがお問い合わせください。',
    'wait_mail' => 'メールを送信しました。メールを確認をお願いします。または30分を待ち、再度試ください。',
    'no_friend' => '友達がいません。',
    'mail_limit' =>'時間内に送信メールが多すぎて、メール送信が拒否されました。１時間を待ち、再度試ください。',
    'all_required' => 'すべて項目は必須項目になっています。',
    'my_todo_existed' => '既にMy案件に追加されております',
    'lengh_min_required' => ':nameは :num文字以上を入力してください。',
    'lengh_max_required' => ':nameは :num文字以下を入力してください。',
    'lengh_exactly_required' => ':nameは :num文字で入力してください。',
    'point_not_enough' => 'ポイントが足りません。',
    'bank_account_existed' => 'こちらの口座番号は既に登録されています。',

    'bank_account_not_found' => '銀行口座が見つかりません！',
    'send_verify_code_pending' => '多くのコードを送信しました。この電話番後は約60分ブロックされています。',
    'send_verify_code_sms_failed' => '携帯電話にメッセージを送信することができません。もう一度お試しください！',
    'client_invalid_data' => 'クライアントの無効なデータ！',
    'client_invalid_param' => 'クライアントの無効なパラメータ！',
    'client_login_failed' => 'クライアントのログインは失敗したか、または見つかりませんでした。',
    'member_not_found' => 'メンバーが見つかりませんでした！',
    'member_exsit' => 'この電話番号は既に登録されています。',
    'verify_type_not_support' => 'この認証タイプがサポートされていませんでした！',
    'verify_registation_failed' => '登録の認証コードが正しくないです！',
    'verify_code_incorrect' => '認証コードが間違いました！',
    'verify_registation_expired' => '登録の確認が期限切れました。',
    'verify_code_expired' => '認証コードの有効期限が切れています。',

    'todo_error' => 'この案件が見つからないか登録が失効しました',
    'todo_registed' => 'この案件が登録されました。他の案件を試してください！',
    'upload_image_failed' => '画像をサーバーにアップロードできません。もう一度確認してください！',
    'hash_key_invalid' => 'ハッシュキーが無効です！',
    'please_verify_code' => 'コードを確認してください！',
    'registed_failed' => '案件の登録が失敗しました！',
    'exchange_bank_failed' => '銀行口座振込に失敗しました！',
    'exchange_bank_limit_amout' => 'You amount should be at least :amount円!',
    'exchange_giftcard_limit_amout' => 'You amount should be at least :amount円!',
    'member_bank_not_found' => '会員銀行が見つかりません！',
    'bank_exchange_token_not_match' => '銀行為替証拠は一致していません！',
    'error_request_unaccepted' => 'Your request was denied about 30 minutes because your request unaccepted!',

    'pex_account_invalid'   => '無効な番号もしくはすでにこの番号は他のアカウントで登録されています。',
    'pex_amount_invalid'   => '合計額が所持⾦額を上回っております。',
    'member_ban_phone'            => 'This phone was banned',

	'limit-send-sms'=>'送信に失敗しました。しばらく時間をあけて再度お試しください。',
	'email-not-exist'   => 'メールアドレスを存在しません。',
];
