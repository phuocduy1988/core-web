<?php

return [

    /*
    |--------------------------------------------------------------------------
    | pageTypes translate for column fields
    |--------------------------------------------------------------------------
    */
    'pageTypes' => 'Page types',
    'pageTypes_index' => 'Page types',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'name' => 'Name',
	'status' => 'Status',
	
    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
