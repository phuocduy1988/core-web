<?php

return [

    /*
    |--------------------------------------------------------------------------
    | pages translate for column fields
    |--------------------------------------------------------------------------
    */
    'pages' => 'Pages',
    'pages_index' => 'Pages',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'id' => 'ID',
	'title' => 'Title',
	
	'slug' => 'Slug',
	'description' => 'Description',
	'content' => 'Content',
	'status' => 'Status',

	'lang' => 'Lang',

	'is_html' => 'HTML',

	'meta_title' => 'Meta title',
	'meta_description' => 'Meta description',
	'rating' => 'rating',
	'comment' => 'Comment',

	'schema' => 'Schema',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
