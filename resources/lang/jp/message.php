<?php

return [

	'created_success' 			=> '登録完了しました。',
	'send_mail_success' 		=> 'ありがとうございます。 送信は正常に完了しました。',
	'created_failed' 			=> '更新中にエラーが発生しました。',
	'add_success' 				=> '追加完了しました。',
	'updated_success' 			=> '更新完了しました。',
	'updated_failed' 			=> '更新中にエラーが発生しました。',
	'deleted_success' 			=> '削除完了しました。',
	'deleted_failed' 			=> '削除できません。',
	'duplicate_success' 		=> '複製が完了しました。',
	'duplicate_failed' 			=> '複製ができませんでした。',
	'no_record' 				=> '該当するデータがありません。',
	'please_select' 			=> 'を選択して下さい。',
	'send_mail_reset' 			=> 'パスワードリセットメールを送信しました。ご確認ください。',
	'platform_changed' 			=> '端末情報を変更するために、確認メールを送信いたしました。届いたメールに記載されているURLをアクセスしてください。',
	'exchange_success' 			=> '交換成功！',
	'ok' 						=> 'OK',
	'cancel_edit_confirm' 		=> ' 編集した内容は破棄されます',

	// Confirm
	'is_delete'					=> 'を削除しますが、よろしいですか？',
	'is_public'					=> 'を公開しますか？',
	'is_private'				=> 'を非公開にしますか？',
	'is_boolean'				=> 'を非にしますか？',
	'is_save'					=> '保存します。よろしいですか？',

	'effect_used'  				=> 'このエフェクトを削除することはできません。',
	'post_used'    				=> 'このPOSTを削除することはできません。',
	'page_used'    				=> 'このページを削除することはできません。',
	'username_email_exist' 		=> 'このユーザ名/メールは既に登録されています。',
	'username_no_exist'    		=> '子アカウントIDが存在しません',
	'not_edit'     				=> 'これ通知を編集できません',
	'data_error'   				=> 'データエラー',
	'data_exist'   				=> 'データは既に登録されています。',
	'no_amount'    				=> '入金額を入力してください。',
	'not_enough_age'  			=> '年齢制限は1以上でなければなりません。', //'1歳以上を入力してください。',
	'min_age_1'       			=> '1歳以上を入力してください。',
	'empty_staff_name' 			=> '営業担当名を入力してください。',
	'empty_reply_message' 		=> '返信メッセージは空であってはなりません',
	'no_result_found' 			=> '結果が見つかりません',

	'empty_string'  			=> '各項目を入力してください。',
	'empty_file'    			=> '必須のファイルをアップロードしてください。',
	'confirm_page_desc'			=> '下記の入力内容を確認してください。よろしければ、登録するボタンを押してください。',
	'input_data_require'		=> '下記の項目を入力してください。',
	'detail_page_desc'			=> '登録情報',

	// Length require
	'maximum_length'			=> '* 最大全角:length文字まで',
	'minimum_length'			=> '* 最小全角:length文字',

	// Image message
	'select_image' 				=> 'ファイルを選択',
	'select_image_desc' 		=> '選択されていません',
	'image_type_require' 		=> '＊JPEG、PNG、GIFのいずれか',
	//'image_type_require' 		=> '＊JPEG、PNG、GIFのいずれか、最大5MBまで',

	// Audio message
	'select_audio' 				=> 'ファイルを選択',
	'select_audio_desc' 		=> '選択されていません',
	'audio_type_require' 		=> '＊mp3のみ、最大3MBまで ',

	// Google maps
	'google_map_require'		=> '住所または施設名を入力してください。※施設名から検索できない場合もあります。',

	// Login Page
	'login_require'				=> 'メールアドレスとパスワードを入力してください。',

	// User
	'invalid'					=> '無効',
	'character_not_found'		=> 'キャラクターが見つかりません。',
	'select_character_require'	=> 'キャラクタを選択してください。',
	'alert_delete' 				=> ':idに削除しますか？',
];
