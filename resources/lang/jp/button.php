<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the button name text.
    |
    */

    'create'                => '新規登録',
    'continue_create'       => '続けて登録する',
    'register'              => '登録する',
    'edit'                  => '編集する',
    'save'                  => '保存する',
    'update'                => '更新する',
    'draft_save'            => '下書き保存',
    'confirm'               => '確認する',
    'delete'                => '削除',
    'cancel'                => 'キャンセル',
    'add'                   => '追加する',
    'return'                => '戻る',
    'reset'                 => 'リセット',
    'map_search'            => '緯度・経度の取得',
    'yes'                   => 'はい',
    'no'                    => 'いいえ',
    'search'                => '検索',
];
