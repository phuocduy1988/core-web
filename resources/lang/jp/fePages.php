<?php

return [

	/*
	|--------------------------------------------------------------------------
	| pages translate for column fields
	|--------------------------------------------------------------------------
	*/
	'company_name' => env("CONFIG_COMPANY_NAME", "GrowUpWork"),
	'pages' => 'Pages',
	'title' => 'GrowUpWorkは日本語、ビジネス、ネットワーキングの３本柱をテーマに展開してまいります。',
	'keyword' => 'ビジネスマナー、人材紹介、インターンシップ紹介、ビジネススキル、ITエンジニアの人材紹介、日本語学校',
	'description' => 'GrowUpWorkは日本とベトナムとのビジネスやコミュニケーションに15年以上の経験を生かし、両国の発展に貢献したいと考えています。財務能力、技術、ビジネススキルなどを持つことで、GrowUp JVが日越のますます発展していく経済に貢献したい若いベトナム人にスキル（日本語、ビジネス）を教育する大手企業になりたいと思います。',
	'404' => 'ページが見つかりません',
	'logout' => 'ログアウト',
	'login' => 'ログイン',
	'registation' => '新規会員登録',
	'hotline' => 'ホットライン:',
	'phone' => '(+81)3-6403-0814 ',
	'str_phone' => '(+81)3-6403-0814',
	'menu' => 'メニュー',
	'share' => 'シェア',
	'news_relation' => '関連ニュース',
	'icon_address' => '住所：',
	'icon_tel' => 'Tel:',
	'icon_email' => 'Email:',
	'address' => ' 503 - 504 5F Waseco Building 10 Pho Quang Street, Ward 2, Tan Binh District, Ho Chi Minh City, Vietnam',
	'email' => 'info@growupwork.com',
	'email_contact' => 'info@growupwork.com',
	'skype' => 'kawamotomeister',
	'goole_plus' => '110663246602949147612',
	'facebook_fanpage' => 'https://www.facebook.com/growupjv',
	'email_exist' => 'メールアドレスが既に存在します。ログイン機能を使用してください。',
	'registation_account' => '新規会員登録',
	'registation_success' => '新規登録が成功しました',
	'login_success' => 'ログインは成功しました',
	'email_pass_wrong' => 'メールアドレスまたはパスワードが違います',
	'email_login' => 'メールアドレス',
	'password' => 'パスワード',
	'confirm_password' => 'パスワード再入力',
	'name' => 'お名前',
	'info_contact' => '連絡先',
	'introduce' => 'GrowUpWorkについて',
	'introduce_desc' => '日本は世界の中で最も少子高齢化が進んでおり2017年には1000万人以上の労働者が減少すると言われています。。世界第3位の経済を誇る日本は良質な教育で、経済力、ビジネス力や経営スキルが高いと言われています。一方、ベトナムはゴールデンエイジと言われる若くて活発な人材が豊富です。さらにベトナム人は、特に新しい技術に関しての学習に強い関心を持っています。この両国の資源をうまく活用すると、両国の経済発展と人材育成を促進することができます。...',
	'readmore' => 'もっと見る',
	'activity_img' => '活動写真',
	'unsubscribe_success' => 'メールの受信を中止しました。',


	'student_feelings' => 'JBAAの受講者の感想',
	'student_name1' => 'Đoàn Quỳnh Anh',
	'student_name2' => 'Trần Thuỷ Tiên',
	'student_name3' => 'Lê Thế Ngân',
	'student_name1_desc' => 'こんにちは。
人文社会科学大学日本学部のアインと申します。井下田先生の授業に参加しました。井下田先生が教えてくれたことは大学の科目だけではなく、日本で働きたい人にとって、役にたち、大切なことだと思います。',
	'student_name2_desc' => 'こんにちは。私はチャン・トゥイ・ティエンと申します。今は人文社会科学大学の日本学部4年生です。今日は最後の授業ですが、この日本ビジネスマナーの授業は私にとって、とても面白いと思います。私はいろいろなことを勉強しましたが、その中で一番印象に残ったのは日本の会社で挨拶し方もたくさんあって、たとえば場合と相手により違うのがとても面白かったと思います。この授業のおかげで将来日系企業に入って、自分の夢を叶えて、もっともっと頑張ります。',
	'student_name3_desc' => 'こんにちは。今学期先生の授業に参加させていただき誠にありがとうございました。実際に私が日本人の先生の授業に参加するのがとても好きですが、この大学はそのようなチャンスがあまりないから、せっかく先生がここに来て私たちに面白く、役にたった授業を教えてくれました。私は心の底からうれしく思います。先生の授業では日本人の特徴や日本人の生活、日本人と働くときどのような点を注意すべきかということはたくさんたくさん面白いことを教えてくれました。たくさん知識を身につけて、うれしかったです。井下田先生の授業は将来の仕事に役に立って、とてもいい授業だと思います。もう一度心を込めて、先生に感謝したいです。',
	'activity_action1'	=> ['さんは会員の登録を完了しました', 'さんは履歴書の登録が完了しました', 'さんは面接に合格しました', 'さんの履歴書が確認されました', 'さんは面接に招待されました'],
	'activity_action2'	=> ['さんは会員の登録を完了しました', 'さんは履歴書の登録が完了しました'],
	'activity_action3'	=> ['1時間前', '２時間前', '3時間前', '4時間前'],
	'employee_list'	=> '最新登録した人',
	'self_intro'	=> '自己PR',
	'register_now'	=> '今すぐ登録',
	'link_to_client'	=> '採用担当者様はこちら',
	'link_to_user'	=> '求職者の方はこちら',
	'office_at_vietnam'=>'ベトナム本社の連絡先',
	'office_at_japan'=>'日本営業オフィスの連絡先',
	'connect_with_growupwork'=>'GrowUpWork公式SNS',
	'support_center'=>'サポートセンター',
	'url' => 'URL: <a href="https://growupwork.com">https://growupwork.com</a>'
];
