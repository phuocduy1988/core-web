<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Search Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the menu library to build
	| the sidebar menu. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'keyword' => 'キーワード',
	'sort' => '表示順序',
	'date' => '日付',
	'search' => '検索',
	'create' => '作成',
	'asc' => '昇順',
	'desc' => '降順',
	'delete_message' => '本当にこのフィールドを削除しますか。',
	'submit' => '確認',
	'cancel' => 'キャンセル',
	'ascending' => '昇順',
	'descending' => '降順',
];