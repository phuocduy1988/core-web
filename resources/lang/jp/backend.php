<?php

return [
	'display-button'  => '表示項目',
	'group-data-button'    => '分割',

	'dashboard' => '計器盤',

	'save-display-field'    => 'この設定を保存',

	'add-to'    => '追加',

	// Term of Use
	'term_service_setting'  => '利用規約設定',
	'change_status'  		=> 'ステータス変更',
	'create_new'  			=> '新規作成',
	'current_reflect'		=> '現在反映中',
	'new_reflect'			=> '新規反映',
	'reflect'				=> '反映',
	'approve_setting'		=> '検閲設定管理',

	// Page Name
	'home'					=> 'ホーム',
	'iframe'				=> 'Iframe',
	'logout'				=> 'ログアウト',
	'file-manager'			=> 'ファイルマネージャー',

	// Login page
	'email' 				=> 'メール',
	'confirm-email' 		=> 'confirm-email',
	'password' 				=> 'パスワード',
	'confirm-password' 		=> '確認パスワード',
	'update-password' 		=> 'パスワード更新',
	'login' 				=> 'ログイン',
	'forgot_password' 		=> 'パスワードをお忘れの方',
	'have_an_account' 		=> 'まだアカウントをお持ちでない場合は',
	'create_account' 		=> 'アカウントを作成する',
	'media'            => 'Media',
	'mapping'            => 'Mapping',
];
