<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Labels Language Lines
	|--------------------------------------------------------------------------
	*/

	'text'						=> 'Text',
	'true'						=> 'True',
	'false'						=> 'False',
	'textarea'					=> 'Text Area',
	'number'					=> 'Number',
	'password'					=> 'Password',
	'select'					=> '選択',
	'checkbox'					=> 'Checkbox',
	'multi_checkbox'			=> 'Multi Checkbox',
	'radio'						=> 'Radio',
	'upload'					=> 'Upload',
	'image'						=> 'Image',
	'date'						=> 'Date',
	'date_time'					=> 'Date Time',
	'start_date'				=> 'Start Date',
	'end_date'					=> 'End Date',
	'startdate_end_date'		=> '公開期間',
	'map_address'				=> 'Map Address',
	'map_latitude'				=> '緯度',
	'map_longitude'				=> '経度',
	'map_zoom'					=> 'ズーム',
	'map_marker'				=> 'マーカー',
	'google_map'				=> 'Google Map',
	'viewdate_nolimit' 			=> '終了日時指定なし',
	'require'					=> '必須項目',
	'created_at'				=> '登録日時',

	// Status
	'private'					=> '非公開',
	'public'					=> '公開',
	'previous'					=> '前へ',
	'next' 						=> '次へ',
	'page_total'				=> '全',
	'page_from'					=> '件中',
	'page_end'					=> '件目',

	// Login Page
	'email' 					=> 'メール',
	'login_id' 					=> 'ログインID',
	'user_id' 					=> 'ユーザーID',
	'password' 					=> 'パスワード',
	'password_confirm' 			=> 'パスワード確認',

	// Character
	'character'					=> 'キャラクタ',
	'no'						=> '記事No.',
	'char_name'					=> 'キャラクタ名',
	'char_id'					=> 'キャラクタ',
	'icon_img'					=> 'アイコン画像',
	'status'					=> '状態',
	'viewdate_start'			=> '公開日時',
	'viewdate_end'				=> '公開終了日時',
	'edit'						=> '編集',
	'delete'					=> '削除',
	'keyword'					=> 'キーワード',
	'keyword_flag'				=> 'キーワード開放フラグ',
	'has_keyword_status'		=> 'キーワード開放あり',
	'not_has_keyword_status'	=> 'キーワード開放無し',
	'keyword_select'			=> 'キーワードを選択',
	'chat_status'				=> 'チャット可否フラグ',
	'chat_status_select'		=> 'チャット可否フラグを選択',
	'chat_available'			=> 'チャット可能',
	'chat_not_available'		=> 'チャット不可能',
	'chat_miss_message'			=> 'チャット該当無しメッセージ',
	'alert_set_message'			=> 'アラート設定時メッセージ',
	'alert_set_miss_message'	=> 'アラート設定失敗メッセージ',
	'alert_cancel_message'		=> 'アラート解除メッセージ',
	'first_chat_text'			=> '初回チャットテキスト:number',

	// Character Chat
	'char_tel_name'				=> '着電コード',
	'char_chat'					=> 'チャット',
	'class'						=> '応答区分',
	'class_select'				=> '応答区分を選択',
	'reaction_word'				=> '反応単語:number',
	'response_tel_flag'			=> '着電応答フラグ',
	'response_tel_flag_select'	=> '着電応答フラグを選択',
	'response_img'				=> '応答画像',
	'response_text'				=> '応答単語:number',
	'response_normal'			=> '通常応答',
	'response_question'			=> '疑問応答',
	'response_reward'			=> '特典応答',
	'response_tel_yes'			=> '有り',
	'response_tel_no'			=> '無し',

	// Character Tel
	'tel_name'					=> '着電名称',
	'char_img'					=> '着電画像',
	'voice_data'				=> 'ボイスデータ',

	// Notice
	'comment'					=> 'テキスト',
	'url'						=> 'URL',
	'picture'					=> '画像',
	'viewdate'					=> '公開日時',

	// User
	'full_name'					=> 'ニックネーム',
	'user_friend'				=> 'ユーザ友達一覧',
	'user_call_history'			=> 'ユーザ着電一覧',
	'bookmark'					=> 'ブックマーク',
	'select_folder'				=> 'フォルダの選択',
	'move'						=> '移動',
	'create_folder'				=> 'フォルダの作成',
	'rename_folder'				=> 'フォルダの編集',
	'delete_folder'				=> 'フォルダの削除',

	//Sound
	'sound_record'				=> '録音中',

	//Tag
	'tag_name'					=> 'タグの名前',
	'tag_type'					=> 'タイプ',

	//Publishing
	'publishing_publish'		=> '済み',
	'publishing_unpublish'		=> '未',

];
