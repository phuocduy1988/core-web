<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Labels Language Lines
	|--------------------------------------------------------------------------
	*/

	'text'						=> 'Text',
	'true'						=> 'True',
	'false'						=> 'False',
	'textarea'					=> 'Text Area',
	'number'					=> 'Number',
	'select'					=> 'Chọn',
	'select_location'			=> 'Tất cả địa điểm',
	'checkbox'					=> 'Checkbox',
	'multi_checkbox'			=> 'Multi Checkbox',
	'radio'						=> 'Radio',
	'upload'					=> 'Upload',
	'image'						=> 'Image',
	'date'						=> 'Ngày',
	'date_time'					=> 'Ngày giờ',
	'start_date'				=> 'Ngày bắt đầu',
	'end_date'					=> 'Ngày kết thúc',
	'start_date_end_date'		=> 'Ngày bắt đầu và kết thúc',
	'require'					=> 'Bắt buộc',
	'created_at'				=> 'Ngày tạo',

	// Status
	'private'					=> 'Riêng tư',
	'public'					=> 'Công khai',
	'previous'					=> 'Trước',
	'next' 						=> 'Sau',
	'page_total'				=> 'Tổng',
	'page_from'					=> ' Hiển thị:',
	'page_end'					=> '',

	// Login Page
	'email' 					=> 'Mail',
	'user_id' 					=> 'ID Người dùng',
	'password' 					=> 'Mật khẩu',
	'password_confirm' 			=> 'Xác nhận mật khẩu',

	'keyword'					=> 'Từ khoá',
	'edit'						=> 'Sửa',
	'delete'					=> 'Xoá',
];
