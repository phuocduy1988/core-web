<?php

return [
	'display'  => 'Hiển thị',

	'dashboard' => 'Bảng điều khiển',

	'change_status'  		=> 'Thay đổi trạng thái',
	'create_new'  			=> 'Tạo mới',

	// Page Name
	'home'					=> 'Trang chủ',
	'iframe'				=> 'Tích hợp HTML',
	'logout'				=> 'Đăng xuất',
	'file-manager'			=> 'Quản lý tập tin',

	// Login page
	'email' 				=> 'Email',
	'confirm_email' 		=> 'Xác nhận email',
	'password' 				=> 'Mật khẩu',
	'confirm-password' 		=> 'Xác nhận mật khẩu',
	'update-password' 		=> 'Cập nhật mật khẩu',
	'login' 				=> 'Đăng nhập',
	'forgot_password' 		=> 'Quên mật khẩu',
	'create_account' 		=> 'Đăng ký tài khoản',
	'media' 		=> 'Media',
	'mapping'            => 'Mapping',
];
