<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Table Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the menu library to build
	| the sidebar menu. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	// Common
	'active'	=> '✓',
	'inactive'	=> '𐄂',
	'required' => '※',
	'export' => 'Xuất dữ liệu',
];
