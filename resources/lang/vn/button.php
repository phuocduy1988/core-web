<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the button name text.
    |
    */

    'create'                => 'Tạo mới',
    'continue_create'       => 'Tiếp tục tạo mới',
    'register'              => 'Đăng ký',
    'edit'                  => 'Chỉnh sửa',
    'save'                  => 'Lưu',
    'update'                => 'Cập nhật',
    'draft_save'            => 'Lưu nháp',
    'confirm'               => 'Xác nhận',
    'delete'                => 'Xoá',
    'cancel'                => 'Đóng',
    'add'                   => 'Thêm',
    'return'                => 'Dừng',
    'reset'                 => 'Làm mới',
    'yes'                   => 'Có',
    'no'                    => 'Không',
    'search'                => 'Tìm kiếm',
];
