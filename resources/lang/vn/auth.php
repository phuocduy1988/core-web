<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    | Auth
    */

    'failed' => 'Email và mật khẩu không đúng',
    'throttle' => 'Bạn đã đăng nhập sai nhiều lần. Xin hãy thử lại sau :seconds giây',

];