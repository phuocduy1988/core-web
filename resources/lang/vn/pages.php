<?php

return [

    /*
    |--------------------------------------------------------------------------
    | pages translate for column fields
    |--------------------------------------------------------------------------
    */
    'pages' => 'Trang',
    'pages_index' => 'Trang',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày sửa',
    'deleted_at' => 'Ngày xoá',
    'id' => 'ID',
	'title' => 'Tiêu đề',
	
	'slug' => 'Slug',
	'description' => 'Mô tả',
	'content' => 'Nội dung',
	'status' => 'Trạng thái',
	'is_html' => 'CKEditor',

	'lang' => 'Ngôn ngữ',

	'meta_title' => 'Meta title',
	'meta_description' => 'Meta description',
	'rating' => 'rating',
	'comment' => 'Comment',

	'schema' => 'Schema',

    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
