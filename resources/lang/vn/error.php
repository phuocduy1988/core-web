<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines ---- API & FRONTEND
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'default_error' => 'Đã có lỗi xảy ra, dữ liệu tương ứng không tồn tại',
    'data_null' => 'Không có dữ liệu tương ứng',
    'member_cv_null' => 'Không có dữ liệu CV tương ứng',
    'request_empty' => 'Không có yêu cầu vui lòng thử lại',
    'email_exist' => 'Địa chỉ email này đã được đăng ký xin vui lòng đăng nhập',
    'email_not_exist' => 'Địa chỉ email chưa được đăng ký',
    'insert_error' => 'Đã có lỗi xảy ra trong quá trình tạo mới',
    'key_expired' => 'Khoá chính này đã hết hạn xin hãy thử lại',
    'email_required' => 'Vui lòng nhật địa chỉ email',
    'phone_required' => 'Vui lòng nhập số điện thoại',
    'password_required' => 'Không thể xác nhận mật khẩu. Vui lòng nhập lại mật khẩu!',
    'email_format' => 'Lỗi định dạng email',
    'phone_exist' => 'Điện thoại này đã được đăng ký',

    'old_password' => 'Mật khẩu cũ không đúng',
    'send_mail' => 'Không thể gửi thư, vui lòng thử lại',

    'auth_key' => 'Không thể xác thực',
    'password_confirm' => 'Xác nhận mật khẩu không khớp',
    'social_login' => 'Không thể đăng nhập bằng SNS',
    'exchange_null' => 'Không có dữ liệu trao đổi',
    'exchange_failed'=>'ギフトコードの交換が出来ません。
        大変申し訳ございませんが、ギフトコードの在庫が切れております。
        土日祝祭日を除いた、４８時間以内に対応させていただきます。
        万が一４８時間以内に対応されない場合は、大変お手数ですがお問い合わせください。',
    'wait_mail' => 'メールを送信しました。メールを確認をお願いします。または30分を待ち、再度試ください。',
    'no_friend' => '友達がいません。',
    'mail_limit' =>'時間内に送信メールが多すぎて、メール送信が拒否されました。１時間を待ち、再度試ください。',
    'all_required' => 'すべて項目は必須項目になっています。',
    'my_todo_existed' => '既にMy案件に追加されております',
    'lengh_min_required' => ':nameは :num文字以上を入力してください。',
    'lengh_max_required' => ':nameは :num文字以下を入力してください。',
    'lengh_exactly_required' => ':nameは :num文字で入力してください。',
    'point_not_enough' => 'ポイントが足りません。',
    'bank_account_existed' => 'こちらの口座番号は既に登録されています。',

    'bank_account_not_found' => '銀行口座が見つかりません！',
    'send_verify_code_pending' => '多くのコードを送信しました。この電話番後は約60分ブロックされています。',
    'send_verify_code_sms_failed' => '携帯電話にメッセージを送信することができません。もう一度お試しください！',
    'client_invalid_data' => 'Không có dữ liệu.',
    'client_invalid_param' => 'クライアントの無効なパラメータ！',
    'please_login' => 'Vui lòng đăng nhập để thực hiện chức năng này!',
    'client_login_failed' => 'Đăng nhập không thành công. Xin thử lại!',
    'member_not_found' => 'メンバーが見つかりませんでした！',
    'member_exsit' => 'この電話番号は既に登録されています。',
    'verify_type_not_support' => 'この認証タイプがサポートされていませんでした！',
    'verify_registation_failed' => '登録の認証コードが正しくないです！',
    'verify_code_incorrect' => '認証コードが間違いました！',
    'verify_registation_expired' => '登録の確認が期限切れました。',
    'verify_code_expired' => '認証コードの有効期限が切れています。',

    'upload_image_failed' => '画像をサーバーにアップロードできません。もう一度確認してください！',
    'hash_key_invalid' => 'ハッシュキーが無効です！',
    'please_verify_code' => 'コードを確認してください！',
    'registed_failed' => '案件の登録が失敗しました！',
    'exchange_bank_failed' => '銀行口座振込に失敗しました！',
    'member_bank_not_found' => '会員銀行が見つかりません！',
    'bank_exchange_token_not_match' => '銀行為替証拠は一致していません！',

    'pex_account_invalid'   => '無効な番号もしくはすでにこの番号は他のアカウントで登録されています。',
    'pex_amount_invalid'   => '合計額が所持⾦額を上回っております。',

	'limit-send-sms'=>'送信に失敗しました。しばらく時間をあけて再度お試しください。',
	'email-not-exist'   => 'メールアドレスを存在しません。',
	'client_login_failed' => 'クライアントのログインは失敗したか、または見つかりませんでした。',

];
