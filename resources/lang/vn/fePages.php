<?php

return [

    /*
    |--------------------------------------------------------------------------
    | pages translate for column fields
    |--------------------------------------------------------------------------
    */
    'company_name' => env("CONFIG_COMPANY_NAME", "GrowUpWork") . ' Co., Ltd',
    'pages' => 'Trang',
    'title' => env("CONFIG_COMPANY_NAME", "GrowUpWork") . ' – Tìm việc làm tiếng Nhật miễn phí!',
    'keyword' => 'Trung tâm giới thiệu việc làm GrowUpWork, giới thiệu việc làm sang Nhật, đào tạo kỹ năng làm việc',
    'description' => env("CONFIG_COMPANY_NAME", "GrowUpWork") . ' hy vọng sẽ đóng góp vào sự phát triển của cả hai quốc gia Việt Nam và Nhật Bản, bằng cách đào tạo kỹ năng và giới thiệu việc làm tại Nhật Bản.',
    '404' => 'Không tìm thấy trang',
    'logout' => 'Đăng xuất',
    'login' => 'Đăng nhập',
    'registation' => 'Đăng ký',
	'phone_name' => 'Điện thoại ',
	'hotline' => 'Hotline:',
	'phone' => '84353253373',
	'str_phone' => '(+84)353-253-373',
	'share' => 'Chia sẻ',
	'news_relation' => 'Tin tức liên quan',
    'email_contact' => 'contact@growupwork.com',
	'menu' => 'Menu',
	'icon_address' => 'Add:',
	'icon_tel' => 'Tel:',
	'icon_email' => 'Email:',
	'address' => 'Phòng 504 - Khu B - Waseco Building - 10 Phổ Quang, P.2, Q.Tân Bình, TP.HCM',
	'email' => 'contact@growupwork.com',
	'skype' => 'nguyenlamthao',
	'goole_plus' => '110663246602949147612',
	'facebook_fanpage' => 'https://www.facebook.com/growupjv',
	'email_exist' => 'Email đã tồn tại, xin hãy sử dụng chức năng đăng nhập',
	'registation_account' => 'Đăng ký tài khoản',
	'registation_success' => 'Bạn đã đăng ký thành công',
	'login_success' => 'Bạn đã đăng nhập thành công',
	'email_pass_wrong' => 'Email hoặc mật khẩu không đúng',
	'email_login' => 'Email',
	'password' => 'Mật khẩu',
	'confirm_password' => 'Lặp lại mật khẩu',
	'name' => 'Họ tên',
	'invite_code' => 'Mã bạn bè',
	'info_contact' => 'Thông tin liên lạc',
	'introduce' => 'Giới thiệu',
	'introduce_desc' => 'Nhật Bản là một trong những nước có dân số già nhất thế giới, hằng năm thiếu hụt khoảng 5 triệu lao động.
                Với nền kinh tế đứng thứ 3 thế giới, Nhật Bản có thế mạnh về tiềm lực tài chính, người Nhật Bản có nền tảng giáo dục tốt, có kỹ năng quản lý, kinh doanh cao.
                Trong khi đó người Việt Nam có nhân lực trẻ, dồi dào đang trong giai đoạn thế hệ vàng. Người Việt Nam học hỏi nhanh, cầu tiến, giỏi kỹ thuật đặc biệt là công nghệ mới.
                Nếu tận dụng nguồn lực 2 bên sẽ tạo thành sức mạnh to lớn...',
	'readmore' => 'Đọc tiếp',
	'activity_img' => 'Hình ảnh hoạt động',
	'unsubscribe_success' => 'Huỷ nhận mail thành công',


	'student_feelings' => 'Cảm nghĩ của học viên',
	'student_name1' => 'Đoàn Quỳnh Anh',
	'student_name2' => 'Trần Thuỷ Tiên',
	'student_name3' => 'Lê Thế Ngân',
	'student_name1_desc' => 'Xin chào, mình là Quỳnh Anh hiện đang học tại trường Đại học KHXH&NV. Mình đã tham gia khóa học Business Manner của cô Igeta. Mình nghĩ rằng những điều cô Igeta dạy không chỉ đơn thuần là nội dung của một môn hoc ở trường mà nó còn rất có ích và cực kì cần thiết đối với những bạn muốn làm việc ở Nhật sau này',
	'student_name2_desc' => 'Xin chào các bạn. Mình tên là Trần Thủy Tiên. Mình đang là sinh viên năm 4 trường ĐH KHXH&NV. Hôm nay đã là buổi học Business Manner cuối cùng rồi và mình cảm thấy khóa này rất thú vị. Ở khóa học này, mình đã học được rất nhiều điều nhưng trong đó mình ấn tượng nhất là chủ đề "Chào hỏi". Có rất nhiều cách chào hỏi trong công ty, tùy vào hoàn cảnh và đối tượng mà có các kiểu chào khác nhau khiến mình thấy chủ đề này rất thú vị. Nhờ có giờ học này mà em có thể cố gắng hơn nữa để tương lai sẽ vào công ty Nhật làm việc và thực hiện ước mơ của mình',
	'student_name3_desc' => 'Xin chào các bạn. Mình cảm thấy rất may mắn vì được tham gia khóa học Business Manner lần này. Vốn mình rất thích các tiết học có giảng viên người Nhật đứng lớp, nhưng ở trường, mình không có nhiều cơ hội như vậy cho lắm nên mình thật sư rất vui khi Cô Igeta đã lặn lội đến tận Thủ Đức dạy cho chúng mình những giờ học rất thú vị và cực kí bổ ích. Tận đáy lòng mình thật sự cảm thấy rất vui. Trong khóa học này, cô đã dạy về đặc trưng, cuộc sống sinh hoạt của người Nhật cũng như những điểm cần chú ý khi làm việc với người Nhật. Mình rất vui mừng vì được trang bị nhiều kiến thức bổ ích. Khóa học với nội dung rất hay chắc hẳn sẽ giúp ích rất nhiều cho công việc sau này. Một lần nữa em gửi tới cô lời cảm ơn chân thành nhất',
	'activity_few_seconds_ago'	=> 'vài giây trước',
	'activity_action1'	=> [' vừa đăng ký', ' vừa nộp hồ sơ', ' vừa đậu phỏng vấn', ' vừa được duyệt hồ sơ', ' vừa được hẹn phỏng vấn'],
	'activity_action2'	=> [' vừa đăng ký', ' vừa nộp hồ sơ'],
	'activity_action3'	=> ['1 giờ trước', '2 giờ trước', '3 giờ trước', '4 giờ trước'],
	'employee_list'	=> '応募者のリスト',
	'register_now'	=> 'Đăng ký ngay',
	'link_to_client'	=> 'Nhà tuyển dụng',
	'link_to_user'	=> 'Người tìm việc',
	'copyright'	=> 'Copyright © 2019 ' . env("CONFIG_COMPANY_NAME", "GrowUpWork") . '. All Rights Reserved',
	'read_term_of_use'	=> 'Đọc và đồng ý <a href="/policy">chính sách bảo mật</a>, <a href="/terms-of-use">điều khoản sử dụng</a>',
    'office_at_vietnam'=>'LIÊN HỆ',
    'office_at_japan'=>'TẠI NHẬT BẢN',
    'connect_with_growupwork'=>'Kết nối với GrowUpWork',
    'call_action_message' => '<p>Chúng tôi sẽ đồng hành và hỗ trợ bạn trên con đường chinh phục Tiếng Anh.</p>
    <p>Bạn có muốn được chúng tôi tư vấn?</p>',
    'consulting' => 'Tôi muốn được tư vấn',
    'message' => 'Lời nhắn',
    'consulting_success' => 'Yêu cầu tư vấn thành công, chúng tôi sẽ liên hệ lại với bạn sớm.',
    'send' => 'Gửi',
    'send_again' => 'Gửi lại',
];
