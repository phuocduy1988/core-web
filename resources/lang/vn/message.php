<?php

return [

	'created_success' 			=> 'Tạo mới thành công',
	'access_denied' 			=> 'Lỗi không có quyền truy cập. Xin hãy đăng nhập.',
	'send_mail_success' 		=> 'Gửi nội dung liên hệ thành công. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.',
	'created_failed' 			=> 'Tạo mới thất bại',
	'add_success' 				=> 'Thêm mới thành công',
	'updated_success' 			=> 'Cập nhật thành công',
	'clear_cache_success' 		=> 'Xoá tất cả bộ nhớ tạm thành công',
	'updated_failed' 			=> 'Cập nhật thất bại',
	'deleted_success' 			=> 'Xoá thành công',
	'deleted_failed' 			=> 'Xoá thất bại',
	'duplicate_success' 		=> 'Nhân bản thành công',
	'duplicate_failed' 			=> 'Nhân bản thất bại',
	'no_record' 				=> 'Không có dữ liệu',
	'send_mail_reset' 			=> 'Hãy kiểm tra hòm thư để đặt lại mật khẩu',
	'ok' 						=> 'OK',

	// Confirm
	'is_delete'					=> ' "Bạn thật sự muốn xoá"',
	'is_public'					=> ' "Bạn thật sự muốn công khai"',
	'is_private'				=> ' "Bạn thật sự muốn riêng tư"',
	'is_boolean'				=> ' "Bạn có chắc không"',
	'is_save'					=> 'Lưu lại. Bạn có chắc không?',

	'username_email_exist' 		=> 'Tài khoản và email không tồn tại',
	'username_no_exist'    		=> 'Tài khoản không tồn tại',
	'not_edit'     				=> 'Không thể chỉnh sửa',
	'data_error'   				=> 'Lỗi dữ liệu',
	'data_exist'   				=> 'Dữ liệu đã tồn tại',
	'no_amount'    				=> 'Số lượng không đủ',
	'not_enough_age'  			=> 'Bạn không đủ :age tuổi',
	'no_result_found' 			=> 'Không tìm kiếm được kết quả',

	'empty_string'  			=> 'Chuỗi ký tự rỗng',
	'empty_file'    			=> 'Tập tin rỗng',
	'input_data_require'		=> 'Hãy nhập thông tin bên dưới ▼',


	// Image message
	'select_image' 				=> 'Chọn tập tin',
	'select_image_desc' 		=> '＊JPEG、PNG、GIF',

	// Login Page
	'login_require'				=> 'Hãy nhập email và mật khẩu',

	// User
	'alert_delete' 				=> 'Bạn thật sự muốn xoá id=:id？',
	'platform_changed' 			=> '端末情報を変更するために、確認メールを送信いたしました。届いたメールに記載されているURLをアクセスしてください。',
	'exchange_success' 			=> '交換成功！',
	'cancel_edit_confirm' 		=> ' 編集した内容は破棄されます',


	'effect_used'  				=> 'このエフェクトを削除することはできません。',
	'post_used'    				=> 'このPOSTを削除することはできません。',
	'page_used'    				=> 'このページを削除することはできません。',
	'min_age_1'       			=> '1歳以上を入力してください。',
	'empty_staff_name' 			=> '営業担当名を入力してください。',
	'empty_reply_message' 		=> '返信メッセージは空であってはなりません',

	'confirm_page_desc'			=> '下記の入力内容を確認してください。よろしければ、登録するボタンを押してください。',
	'detail_page_desc'			=> '登録情報',

	// Length require
	'maximum_length'			=> '* 最大全角:length文字まで',
	'minimum_length'			=> '* 最小全角:length文字',

	// Image message
	'image_type_require' 		=> '＊JPEG、PNG、GIFのいずれか',
	//'image_type_require' 		=> '＊JPEG、PNG、GIFのいずれか、最大5MBまで',

	// Audio message
	'select_audio' 				=> 'ファイルを選択',
	'select_audio_desc' 		=> '選択されていません',
	'audio_type_require' 		=> '＊mp3のみ、最大3MBまで ',

	// Google maps
	'google_map_require'		=> '住所または施設名を入力してください。※施設名から検索できない場合もあります。',

	// Login Page

	// User
	'invalid'					=> '無効',
];
