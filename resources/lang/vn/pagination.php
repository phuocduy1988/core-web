<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Trước',
    'next' => 'Sau &raquo;',
	// page size and pagination
	'show_page_size' => 'Số lượng dòng',
	'total_page' => 'Tổng',
	'first_page' => 'Trang đầu',
	'last_page' => 'Trang cuối',
	'previous_page' => 'Trang trước',
	'next_page' => 'Trang sau',

];
