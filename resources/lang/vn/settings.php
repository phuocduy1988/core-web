<?php

return [

    /*
    |--------------------------------------------------------------------------
    | members translate for column fields
    |--------------------------------------------------------------------------
    */
    'settings' => 'Cài đặt',
    'settings_index' => 'Cài đặt',
    'key' => 'Từ khoá',
    'value' => 'Giá trị',
    'id' => 'ID',
	'description' => 'Mô tả',
	'type' => 'Loại',
	'status' => 'Trạng thái',
	'remember_token' => 'Remember token',
	'clear_cache' => 'Xoá bộ nhớ tạm',


    //--UPDATE_GENCODE_LANG_DO_NOT_DELETE_OR_CHANGE_THIS--
];
