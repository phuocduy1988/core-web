<?php

return [

	/**
	 *
	 * Shared translations.
	 *
	 */
	'title' => 'Trình cài đặt',
	'next' => 'Bước tiếp theo',
	'back' => 'Trước',
	'finish' => 'Cài đặt',
	'forms' => [
		'errorTitle' => 'Các lỗi sau xảy ra:',
	],

	/**
	 *
	 * Home page translations.
	 *
	 */
	'welcome' => [
		'templateTitle' => 'Chào mừng',
		'title'   => 'Trình cài đặt ứng dụng',
		'message' => 'Dễ dàng cài đặt và thiết lập Wizard cho Redprint.',
		'next'    => 'Yêu cầu kiểm tra',
	],

	/**
	 *
	 * Requirements page translations.
	 *
	 */
	'requirements' => [
		'templateTitle' => 'Bước 1 | Yêu cầu máy chủ',
		'title' => 'Yêu cầu máy chủ',
		'next'    => 'Kiểm tra quyền',
	],

	/**
	 *
	 * Permissions page translations.
	 *
	 */
	'permissions' => [
		'templateTitle' => 'Bước 2 | Phân quyền',
		'title' => 'Phân quyền',
		'next' => 'Cấu hình môi trường',
	],

	/**
	 *
	 * Environment page translations.
	 *
	 */
	'environment' => [
		'menu' => [
			'templateTitle' => 'Bước 3 | Cài đặt môi trường',
			'title' => 'Cài đặt môi trường',
			'desc' => 'Vui lòng chọn cách bạn muốn định cấu hình tệp <code> .env </ code> của ứng dụng.',
			'wizard-button' => 'Thiết lập Wizard Form',
			'classic-button' => 'Trình soạn thảo văn bản cổ điển',
		],
		'wizard' => [
			'templateTitle' => 'Bước 3 | Cài đặt môi trường | Wizard hướng dẫn',
			'title' => 'Hướng dẫn <code> .env </code>',
			'tabs' => [
				'environment' => 'Môi trường',
				'database' => 'Cơ sở dữ liệu',
				'application' => 'Mẫu đăng ký
'
			],
			'form' => [
				'name_required' => 'Tên môi trường là bắt buộc.',
				'app_name_label' => 'Tên ứng dụng',
				'app_name_placeholder' => 'Tên ứng dụng',
				'app_environment_label' => 'Môi trường ứng dụng',
				'app_environment_label_local' => 'Local',
				'app_environment_label_onetech' => 'OneTech',
				'app_environment_label_developement' => 'Development',
				'app_environment_label_qa' => 'Qa',
				'app_environment_label_production' => 'Production',
				'app_environment_label_other' => 'Khác',
				'app_environment_placeholder_other' => 'Nhập môi trường của bạn',
				'app_debug_label' => 'Gỡ lỗi ứng dụng',
				'app_debug_label_true' => 'True',
				'app_debug_label_false' => 'False',
				'app_log_level_label' => 'Cấp độ nhật ký ứng dụng',
				'app_log_level_label_debug' => 'Gỡ lỗi',
				'app_log_level_label_info' => 'Thông tin',
				'app_log_level_label_notice' => 'Để ý',
				'app_log_level_label_warning' => 'Cảnh báo',
				'app_log_level_label_error' => 'Lỗi',
				'app_log_level_label_critical' => 'Chỉ trích',
				'app_log_level_label_alert' => 'Thông báo',
				'app_log_level_label_emergency' => 'Trường hợp khẩn cấp',
				'app_url_label' => 'App Url',
				'app_url_placeholder' => 'App Url',
				'db_connection_label' => 'Kết nối cơ sở dữ liệu',
				'db_connection_label_mysql' => 'mysql',
				'db_connection_label_sqlite' => 'sqlite',
				'db_connection_label_pgsql' => 'pgsql',
				'db_connection_label_sqlsrv' => 'sqlsrv',
				'db_host_label' => 'Máy chủ cơ sở dữ liệu',
				'db_host_placeholder' => 'Máy chủ cơ sở dữ liệu',
				'db_port_label' => 'Cổng cơ sở dữ liệu',
				'db_port_placeholder' => 'Cổng cơ sở dữ liệu',
				'db_name_label' => 'Tên cơ sở dữ liệu
',
				'db_name_placeholder' => 'Tên cơ sở dữ liệu
',
				'db_username_label' => 'Tên người dùng cơ sở dữ liệu',
				'db_username_placeholder' => 'Tên người dùng cơ sở dữ liệu',
				'db_password_label' => 'Mật khẩu cơ sở dữ liệu',
				'db_password_placeholder' => 'Mật khẩu cơ sở dữ liệu',

				'app_tabs' => [
					'more_info' => 'Thêm thông tin',
					'broadcasting_title' => 'Broadcasting, Caching, Session, &amp; Queue',
					'broadcasting_label' => 'Trình điều khiển phát sóng',
					'broadcasting_placeholder' => 'Trình điều khiển phát sóng',
					'cache_label' => 'Trình điều khiển bộ nhớ cache',
					'cache_placeholder' => 'Trình điều khiển bộ nhớ cache',
					'session_label' => 'Session Driver',
					'session_placeholder' => 'Session Driver',
					'queue_label' => 'Queue Driver',
					'queue_placeholder' => 'Queue Driver',
					'redis_label' => 'Redis Driver',
					'redis_host' => 'Redis Host',
					'redis_password' => 'Redis Password',
					'redis_port' => 'Redis Port',

					'mail_label' => 'Mail',
					'mail_driver_label' => 'Mail Driver',
					'mail_driver_placeholder' => 'Mail Driver',
					'mail_host_label' => 'Mail Host',
					'mail_host_placeholder' => 'Mail Host',
					'mail_port_label' => 'Mail Port',
					'mail_port_placeholder' => 'Mail Port',
					'mail_username_label' => 'Mail Username',
					'mail_username_placeholder' => 'Mail Username',
					'mail_password_label' => 'Mail Password',
					'mail_password_placeholder' => 'Mail Password',
					'mail_encryption_label' => 'Mail Encryption',
					'mail_encryption_placeholder' => 'Mail Encryption',

					'pusher_label' => 'Pusher',
					'pusher_app_id_label' => 'Pusher App Id',
					'pusher_app_id_palceholder' => 'Pusher App Id',
					'pusher_app_key_label' => 'Pusher App Key',
					'pusher_app_key_palceholder' => 'Pusher App Key',
					'pusher_app_secret_label' => 'Pusher App Secret',
					'pusher_app_secret_palceholder' => 'Pusher App Secret',
				],
				'buttons' => [
					'setup_database' => 'Setup Database',
					'setup_application' => 'Setup Application',
					'install' => 'Install',
				],
			],
		],
		'classic' => [
			'templateTitle' => 'Step 3 | Environment Settings | Classic Editor',
			'title' => 'Classic Environment Editor',
			'save' => 'Save .env',
			'back' => 'Use Form Wizard',
			'install' => 'Save and Install',
		],
		'success' => 'Your .env file settings have been saved.',
		'errors' => 'Unable to save the .env file, Please create it manually.',
	],

	'install' => 'Install',

	/**
	 *
	 * Installed Log translations.
	 *
	 */
	'installed' => [
		'success_log_message' => 'Installer successfully INSTALLED on ',
	],

	/**
	 *
	 * Final page translations.
	 *
	 */
	'final' => [
		'title' => 'Installation Finished',
		'templateTitle' => 'Installation Finished',
		'finished' => 'Application has been successfully installed.',
		'migration' => 'Migration &amp; Seed Console Output:',
		'console' => 'Application Console Output:',
		'user' => 'User Admin Info:',
		'log' => 'Installation Log Entry:',
		'env' => 'Final .env File:',
		'exit' => 'Click here to exit',
	],

	/**
	 *
	 * Update specific translations
	 *
	 */
	'updater' => [
		/**
		 *
		 * Shared translations.
		 *
		 */
		'title' => 'Laravel Updater',

		/**
		 *
		 * Welcome page translations for update feature.
		 *
		 */
		'welcome' => [
			'title'   => 'Welcome To The Updater',
			'message' => 'Welcome to the update wizard.',
		],

		/**
		 *
		 * Welcome page translations for update feature.
		 *
		 */
		'overview' => [
			'title'   => 'Overview',
			'message' => 'There is 1 update.|There are :number updates.',
			'install_updates' => "Install Updates"
		],

		/**
		 *
		 * Final page translations.
		 *
		 */
		'final' => [
			'title' => 'Finished',
			'finished' => 'Application\'s database has been successfully updated.',
			'exit' => 'Click here to exit',
		],

		'log' => [
			'success_message' => 'Installer successfully UPDATED on ',
		],
	],
];
