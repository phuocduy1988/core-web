<?php

return [
    'all'       => 'Tổng',
    'emergency' => 'Khẩn cấp',
    'alert'     => 'Thông báo',
    'critical'  => 'Nguy hiểm',
    'error'     => 'Lỗi',
    'warning'   => 'Cảnh báo',
    'notice'    => 'Thông báo',
    'info'      => 'Thông tin',
    'debug'     => 'Debug',
];
