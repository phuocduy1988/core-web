<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>500</title>

    <link href="{!! asset('inspinia/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('inspinia/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
    <link href="{!! asset('inspinia/css/animate.css') !!}" rel="stylesheet">
    <link href="{!! asset('inspinia/css/style.css') !!}" rel="stylesheet">
	<link href="{!! asset('css/custom.css') !!}" rel="stylesheet">

</head>
<body>

	<div id="app">

		<div class="middle-box text-center animated fadeInDown" style="max-width: 450px">
			<h1 style="color: white"> 500 </h1>
			<h3 style="color: red" class="font-bold">ページが見つかりません。</h3>

			<div class="error-desc" style="color: white">
				トップページから再度アクセスしてください。
				<br>

				@if(isset($message) && strlen($message) > 0)
					{!! $message !!}
					<br>
				@endif

				@if(env('APP_DEBUG'))
					@include('backend._partials.notifications')
				@endif
			</div>
		</div>
	</div>

</body>
</html>


