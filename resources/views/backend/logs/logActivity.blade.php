@extends('backend.layouts.app')

@section('title')
	{{ trans('menu.activity_log') }}
@stop

@section('css')
    <link href="{{ asset('inspinia/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <!-- Date range picker -->
    <link href="{!! asset('backend/plugins/daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop
@section('content')
    @include('backend._partials.breadcrumb',
    ['page_list' => array(
        ['title' => trans('menu.activity_log'), 'link' => '#']
    )])

    <div id="app" class="wrapper wrapper-content  animated fadeInRight clearfix">
        <div v-if="showData" class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ trans('menu.activity_log') }}</h5>
                </div>
                <div class="ibox-content clearfix">

                    <div v-for="(data, index) in datas" class="stream-small">
                        <span class="label label-primary"> @{{ data.type }} </span>
                        <span class="text-muted"> @{{ data.created_at }} </span> / <a href="#">@{{ data.user.first_name }}</a> /  Table: @{{ data.table_model }} / <a href="#" @click.prevent="showLog(index)">{{ trans('menu.detail') }}</a>
                    </div>
                    <hr>

                    @include('backend._partials.vue-pagination')

                </div>
            </div>
        </div>
        {{--Show Log Detail--}}
        <div v-if="isLogDetail" class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5 v-html="logDetailTitle"></h5>
                </div>
                <div class="ibox-content clearfix">
                    <p v-if="logDetailOriginal" style="overflow-wrap: break-word;">Original: @{{ logDetailOriginal }}</p>
                    <p v-if="logDetailAttribute" style="overflow-wrap: break-word;">Update: @{{ logDetailAttribute }}</p>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="{!! asset('backend/js/vue.js') !!}"></script>
    <script src="{!! asset('backend/js/axios.min.js') !!}"></script>
    <script src="{!! asset('backend/js/moment.js') !!}"></script>
    <script>
        var app = new Vue({
            el: "#app",
            data:{
                datas: {}, // get data
                showData: false, // get data
                error: false, // check isset Error
                errors: {}, // object error (all error)
                message: '',
                setting: {},
                pagination: {
                    current_page: 0,
                    total: 0,
                    last_page: 0,
                },
                isLogDetail: false,
                logDetailOriginal: {},
                logDetailAttribute: {},
                logDetailTitle: '',
            },
            created: function () {
                $(".ot-loading").show()
                this.getDatas(1).then(datas => {
                    var resDatas = Object.keys(datas.data).map((key) => {
                        return datas.data[key]
                    })
                    resDatas.map(function (data) {
                        data.is_show = false
                    })
                    this.datas = resDatas
                    console.log(datas)
                    this.showData = true
                    this.setPagination(datas)
                });
            },
            methods: {
                setPagination: function(datas) {
                    this.pagination.current_page = datas.current_page
                    this.pagination.first_page_url = datas.first_page_url
                    this.pagination.from = datas.from
                    this.pagination.last_page = datas.last_page
                    this.pagination.last_page_url = datas.last_page_url
                    this.pagination.next_page_url = datas.next_page_url
                    this.pagination.url = datas.path
                    this.pagination.per_page = datas.per_page
                    this.pagination.prev_page_url = datas.prev_page_url
                    this.pagination.total = datas.total
                },
                getDatas: function(page) {
                    let d = $.Deferred(), resDatas = {}
                    setTimeout(function() {
                        axios.get('{{ route('log.getLogActivity') }}?page=' + page)
                        .then(function (response) {
                            d.resolve(response.data);
                            $(".ot-loading").hide()
                        })
                        .catch(function (error) {
                            $(".ot-loading").hide()
                        })
                    }, 100)
                    return d.promise()
                },
                changePage: function(page) {
                    $(".ot-loading").show()
                    this.getDatas(page).then(datas => {
                        var resDatas = Object.keys(datas.data).map((key) => {
                            return datas.data[key]
                        })
                        resDatas.map(function (data) {
                            data.is_show = false
                        })
                        this.datas = resDatas
                        console.log(datas)
                        this.setPagination(datas)
                    });

                },
                findIndex: function(id) {
                    return this.datas.findIndex(function (data) {
                        return data.id === id
                    })
                },
                showLog: function(index) {
                    this.isLogDetail = true
                    let data = this.datas[index]
                    this.logDetailOriginal = data.original
                    this.logDetailAttribute = data.attribute
                    this.logDetailTitle = "<span class=\"label label-primary\"> "+ data.type +" </span>\n" +
                        "                        <span class=\"text-muted\"> " + data.created_at + " </span> / <a href=\"#\">"+data.user.first_name+"</a> /  Table: "+ data.table_model
                },
            },
            watch:{
            }
        });
    </script>
@stop

