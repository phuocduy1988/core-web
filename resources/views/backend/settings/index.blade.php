@extends('backend.layouts.app')

@section('title') {{ trans('settings.settings') }} @stop

@section('css')
@stop

@section('content')
    @include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('settings.settings'), 'link' => route('setting.index')]
    	)])

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="setting">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div id="app" class="ibox-content">
                        @include('backend._partials.notifications')
                        <div v-if="error === true" class="alert alert-danger validation-error">
                            <p v-for="err in errors" v-html="err[0]"></p>
                        </div>
                        <div v-if="message" class="alert alert-success alert-dismissible">
                            <p v-html="message"></p>
                        </div>
                        <br>
                        <div class="clearfix">
                            <div class="col-lg-3 form-group">
                                <input type="text" v-model="setting.key" class="form-control" placeholder="Key">
                            </div>
                            <div class="col-lg-3 form-group">
                                <input type="text" v-model="setting.value" class="form-control" placeholder="Value">
                            </div>
                            <div class="col-lg-5 form-group">
                                <input type="text" v-model="setting.description" class="form-control" placeholder="Description">
                            </div>
                            <div class="col-lg-1 form-group ">
                                <button class="btn btn-success" @click.prevent="saveSetting(0)">Add</button>
                            </div>
                        </div>
                        <!-- Search block START -->
                        {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => 'setting.index']) !!}
                        <!-- Search block END -->
                        <!-- List content START -->
                        @include('backend._partials.sort-field')
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ trans('settings.id') }}</th>
										<th>{{ trans('settings.key') }} </th>
										<th>{{ trans('settings.value') }} </th>
										<th>{{ trans('settings.description') }} </th>


                                        {{-- --UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                        <th>{{ trans('label.edit') }}</th>
                                        {{--<th>{{ trans('label.delete') }}</th>--}}
                                    </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(setting, index) in settings" :id="setting.id">
                                    <td class="text-center">
                                        @{{ setting.id }}
                                    </td>
                                    <td>
                                        @{{ setting.key }}
                                    </td>
                                    <td v-if="!setting.is_edit">
                                        @{{ setting.value }}
                                    </td>
                                    <td v-else>
                                        <textarea type="text" v-model="setting.value"></textarea>
                                    </td>
                                    <td v-if="!setting.is_edit">
                                        @{{ setting.description }}
                                    </td>
                                    <td v-else>
                                        <textarea type="text" v-model="setting.description"></textarea>
                                    </td>
                                    <td class="text-center"  v-if="!setting.is_edit">
                                        <a href="javascript:void(0)" @click="editSetting(setting.id)"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td class="text-center" v-else>
                                        <button class="btn-xs btn-success" @click.prevent="saveSetting(setting.id)"><i class="fa fa-check"></i></button>
                                        <button class="btn-xs btn-danger" @click.prevent="cancelEdit(setting.id)"><i class="fa fa-times"></i></button>
                                    </td>
                                    {{--<td class="text-center">--}}
                                        {{--<a href="javascript:void(0)" @click="deleteSetting(setting.id)"><i class="text-danger fa fa-trash"></i></a>--}}
                                    {{--</td>--}}
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        {{ Form::close() }}
                    <!-- List content END -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('backend._partials.js.js-popup')
    <script src="{!! asset('backend/js/vue.js') !!}"></script>
    <script src="{!! asset('backend/js/axios.min.js') !!}"></script>
    <script src="{!! asset('backend/js/moment.js') !!}"></script>
    <script>

        var app = new Vue({
            el: "#app",
            data:{
                settings: {}, // get data
                error: false, // check isset Error
                errors: {}, // object error (all error)
                message: '',
                setting: {},
            },
            created: function () {
                this.getSettings().then(data => {
                    this.settings = data;
                });
            },
            methods: {
                getSettings: function() {

                    var d = $.Deferred();

                    setTimeout(function() {

                        var resSetting = {!! $jsonSettings !!}
                        resSetting.map(function (setting) {
                            setting.is_edit = false
                        })
                        d.resolve(resSetting);

                    }, 0)

                    return d.promise()
                },
                editSetting: function(id){
                    let index = this.findIndex(id)
                    this.settings[index].is_edit = true
                },
                cancelEdit: function(id){
                    let index = this.findIndex(id)
                    this.settings[index].is_edit = false
                },
                findIndex: function(id) {
                    return this.settings.findIndex(function (setting) {
                        return setting.id === id
                    })
                },
                deleteSetting: function(id){
                    var r = confirm("{{ trans('message.alert_delete') }}")
                    if (r == true) {
                        $(".ot-loading").show()
                        var params = {
                            id: id,
                        }
                        axios.post("{{ route('setting.delete') }}", params)
                            .then((response) => {
                                $(".ot-loading").hide()
                                console.log(response)
                                this.error = false
                                this.errors = {}
                                var index = this.findIndex(id)
                                this.settings.splice(index, 1)
                                this.message = response.data.message
                            }).catch((error) => {
                            $(".ot-loading").hide()
                            this.error = true
                            this.errors = error.response.data.errors
                        });
                    }
                    else {
                        return false
                    }
                },
                saveSetting: function(id){
                    let index = this.findIndex(id)
                    let params = {}
                    if(index >= 0) {
                        params = this.settings[index]
                    }
                    else {
                        params = this.setting
                    }

                    if(params.key === undefined || this.setting.key === '') {
                        alert('{{ trans('error.data_null') }}')
                        return false
                    }

                    $(".ot-loading").show()
                    axios.post("{{ route('setting.save') }}", params)
                        .then((response) => {
                            $(".ot-loading").hide()
                            this.error = false
                            this.errors = {}
                            this.message = response.data.message
                            if(index >= 0) {
                                this.settings[index].is_edit = false
                            }
                            else {
                                let resSetting = response.data.setting
                                resSetting = {
                                    id: resSetting.id,
                                    key: resSetting.key,
                                    value: resSetting.value,
                                    description: resSetting.description,
                                    is_edit: false,
                                }
                                this.settings.unshift(resSetting)
                                this.setting = {}
                            }

                        }).catch((error) => {
                            console.log(error)
                            $(".ot-loading").hide()
                            this.error = true
                            this.errors = error.response.data.errors
                    });
                },
            },
            watch:{
                'setting.key': function () {
                    this.setting.key = stringToSlug(this.setting.key, '_')
                },
            }
        });
	</script>
@stop