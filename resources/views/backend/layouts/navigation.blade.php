<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse" id="js-side-bar-height">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <a href="{{ route('common.home') }}" class="transparent">
                        <img src="{{ asset('frontend/img/lg-main-vertical-white.png') }}" width="60%" alt="LOGO" />
                    </a>

                    <a data-toggle="dropdown" class="dropdown-toggle transparent" href="#">
						<span class="clear">
							<span class="block m-t-xs">
								<strong class="font-bold">{{ isset(Auth::user()->first_name) ? Auth::user()->first_name : null }}</strong>
							</span>
						</span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('backend.logout') }}">{{ trans('menu.logout') }}</a></li>
                    </ul>
                </div>
            <li class="{{ isActiveUrl('/home') }}">
                <a href="{{ route('common.home') }}">
                    <i class="fa fa-circle"></i><span class="nav-label">{{ trans('backend.dashboard') }}</span>
                </a>
            </li>
            <li class="{{ isActiveUrl('/pages') }}">
                <a href="{{ route('page.index') }}">
                    <i class="fa fa-inbox"></i><span class="nav-label">{{ trans('pages.pages') }}</span>
                </a>
            </li>
            <li class="{{ isActiveUrl('/settings') }}">
                <a href="{{ route('setting.index') }}">
                    <i class="fa fa-gear"></i><span class="nav-label">{{ trans('settings.settings') }}</span>
                </a>
            </li>
            {{--Dynamic menu--}}
            <li class="{{ activeAdministrationMenu() }}">
                <a href="#">
                    <i class="fa fa-address-book"></i><span class="nav-label">{{ trans('menu.admin') }}</span><span class="fa arrow"></span>
                </a>
                @foreach(config('backend-menu') as $item)
					<?php
					$active = isset($item['url']) ? $item['url'] : '';
					?>

                    <ul class="nav nav-second-level collapse">
                        <li class="{{ isActiveUrl('/'.$active) }}">
                            <a href="{{ route($item['route']) }}"><i class="fa fa-list-alt"></i>{{ trans($item['text'].'.'.$item['text'].'_index') }}</a>
                        </li>
                    </ul>
                @endforeach

            </li>

            <li class="{{ isActiveUrl('/system*') }}">
                <a href="#">
                    <i class="fa fa-cogs"></i><span class="nav-label">{{ trans('menu.system') }}</span><span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/builder') }}">
                        <a href="{{ asset(getBeUrl('builder')) }}"><i class="fa fa-adjust"></i>{{ trans('menu.builder') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/builder/update') }}">
                        <a href="{{ asset(getBeUrl('builder/update')) }}"><i class="fa fa-edit"></i>{{ trans('menu.builder_update') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/relationships') }}">
                        <a href="{{ asset(getBeUrl('relationships')) }}"><i class="fa fa-arrow-up"></i>{{ trans('menu.relationship') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/update-file') }}">
                        <a href="{{ route('builder.update-file') }}"><i class="fa fa-paperclip"></i>{{ trans('menu.update_file') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/make-tools') }}">
                        <a href="{{ route('builder.make-tools') }}"><i class="fa fa-dashboard"></i>{{ trans('menu.command_tool') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/create-project') }}">
                        <a href="{{ route('builder.create-project') }}"><i class="fa fa-creative-commons"></i>{{ trans('menu.create_project') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/postman') }}">
                        <a target="_blank" href="/builder/postman/api_postman_collection.json"><i class="fa fa-plug"></i>{{ trans('menu.api_postman') }}</a>
                    </li>
                </ul>
            </li>
            <li class="{{ isActiveUrl('/logs*') }}">
                <a href="#">
                    <i class="fa fa-bug"></i><span class="nav-label">{{ trans('menu.log') }}</span><span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/log-viewer') }}">
                        <a href="{{ route('log-viewer::logs.list') }}"><i class="fa fa-bug"></i>{{ trans('menu.system_log') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/beanstalkd/tubes') }}">
                        <a href="/beanstalkd/tubes"><i class="fa fa-bug"></i>{{ trans('Beanstalkd') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/activity-log') }}">
                        <a href="{{ route('log.logActivity') }}"><i class="fa fa-plus-circle"></i>{{ trans('menu.activity_log') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/request-log') }}">
                        <a href="#"><i class="fa fa-toggle-on"></i>{{ trans('menu.request_log') }}</a>
                    </li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveUrl('/access-log') }}">
                        <a href="#"><i class="fa fa-apple"></i>{{ trans('menu.access_log') }}</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('setting.clearCaches') }}">
                    <i class="fa fa-remove"></i><span class="nav-label">{{ trans('settings.clear_cache') }}</span>
                </a>
            </li>
            <li>
                <a href="/elfinder/ckeditor?CKEditor=js-editor&CKEditorFuncNum=0&langCode=en#elf_l1_dHJhaW5pbmc">
                    <i class="fa fa-remove"></i><span class="nav-label">{{ trans('backend.file-manager') }}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
