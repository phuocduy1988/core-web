<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:void(0);"><i class="fa fa-bars"></i> </a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li class="logo-sp hidden-pc hidden-tb">
                <img src="/frontend/img/growup-logo.png" class="img-responsive" alt=""/>
            </li>

            <li class="logout">
                <a href="{{ getBeUrl('logout') }}">
                    <i class="fa fa-sign-out"></i>
                    {{ trans('backend.logout') }}
                </a>
            </li>
        </ul>

    </nav>
</div>
