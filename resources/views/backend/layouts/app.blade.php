<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> @yield('title') </title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">


	<link rel="shortcut icon" href="" type="image/x-icon" />

	<link href="{!! asset('inspinia/css/bootstrap.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<link href="{!! asset('inspinia/font-awesome/css/font-awesome.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<link href="{!! asset('inspinia/css/animate.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<link href="{!! asset('inspinia/css/style.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">

	<!-- Sweet Alert -->
	<link href="{!! asset('inspinia/css/sweetalert.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">

	<!-- Loader -->
	<link href="{!! asset('inspinia/css/loader.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">

	<link href="{!! asset('backend/css/custom.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">

	<link href="{!! asset('backend/css/common.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<!-- Selectbox auto complete -->
	<link href="{!! asset('backend/plugins/select2/select2.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<link href="{!! asset('backend/plugins/select2/select2-bootstrap.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


	@yield('css')
</head>
<body class="pace-done">
<!-- Wrapper-->
<div id="wrapper">

	<!-- Navigation -->
@include('backend.layouts.navigation')

<!-- Page wraper -->
	<div id="page-wrapper" class="gray-bg">

		<!-- Page wrapper -->
	@include('backend.layouts.topnavbar')
	@include('backend._partials.loading')
	<!-- Main view  -->
	@yield('content')

	<!-- Footer -->
		@include('backend.layouts.footer')

	</div>
	<!-- End page wrapper-->

</div>
<!-- End wrapper-->

<!-- <script src="{!! asset('inspinia/js/app.js') !!}" type="text/javascript"></script> -->

<!-- Mainly scripts -->
<script src="{!! asset('inspinia/js/jquery-3.1.1.min.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<script src="{!! asset('inspinia/js/bootstrap.min.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<script src="{!! asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<script src="{!! asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') !!}"></script>

<!-- Date range -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="{!! asset('inspinia/js/inspinia.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<script src="{!! asset('inspinia/js/plugins/pace/pace.min.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<script type="text/javascript" src="{!! asset('backend/js/bootbox.min.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<!-- Sweet alert -->

<!-- Sweet alert -->
<script src="{!! asset('inspinia/js/sweetalert.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>

<!-- Fixed bottom scroll -->
<script type="text/javascript" src="{!! asset('backend/js/jquery.ba-floatingscrollbar.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>

<script type="text/javascript" src="{!! asset('backend/plugins/select2/select2.min.js').'?v='. env('CSS_VERSION', '0.0.0') !!}"></script>
<!-- Custom common javascript -->
<script src="{!! asset('backend/js/common.js').'?v='. env('CSS_VERSION', '0.0.0') !!}" type="text/javascript"></script>
<script>
    $('.select2').select2({
        theme: 'bootstrap',
        placeholder: "{{ trans('label.select') }}",
        allClear: true
    });
</script>
<script>
    $(document).ready(function() {
        moment.locale('jp');
        $('input[name="daterange"]').daterangepicker({
            opens: 'center',
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD',
                "separator": " 〜 ",
                "applyLabel": "OK",
                "cancelLabel": "キャンセル",
                "fromLabel": "期間",
                "toLabel": "〜",
                "customRangeLabel": "期間選択",
                "weekLabel": "週",
                "daysOfWeek": [
                    "日",
                    "月",
                    "火",
                    "水",
                    "木",
                    "金",
                    "土"
                ],
                "monthNames": [
                    "一月",
                    "二月",
                    "三月",
                    "四月",
                    "五月",
                    "六月",
                    "七月",
                    "八月",
                    "九月",
                    "十月",
                    "十一月",
                    "十二月"
                ],
                "firstDay": 1
            },
            ranges: {
                '今日': [moment(), moment()],
                '昨日': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '過去７日間': [moment().subtract(6, 'days'), moment()],
                '過去30日間': [moment().subtract(29, 'days'), moment()],
                '今月': [moment().startOf('month'), moment().endOf('month')],
                '先月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "alwaysShowCalendars": true,
			{{--"startDate": "{{ \App\Helpers\Helper::dateAddDay(-30, 'Y-m-d') }}",--}}
			{{--"endDate": "{{ \App\Helpers\Helper::dateAddDay(-1, 'Y-m-d') }}"--}}
            },
            function(start, end, label) {
             console.log('New date range selected: ' + start.format('YYYY-MM-DD') + '〜' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                $('input[name="daterange"]').val(start.format('YYYY-MM-DD') + '〜' + end.format('YYYY-MM-DD'));

            },
        );

    });
</script>
@section('scripts')
@show
</body>
</html>
