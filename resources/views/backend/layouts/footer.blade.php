<div class="footer">
    <div class="text-center">
        Copyright &copy; <strong>{{ config('site.name') }}</strong> {{ date('Y') }} All Rights Reserved.
        <br />
        <strong>{!! config('site.copyright') !!}</strong>
    </div>
</div>
