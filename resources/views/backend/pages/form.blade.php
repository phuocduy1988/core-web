@extends('backend.layouts.app')

@section('title') {{ trans('pages.pages') }} @stop

@section('css')
	<link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
	<link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
	<link href="{!! asset('ace_ed') !!}" rel="stylesheet">
	<style type="text/css">
		#ace-editor {
			position: relative;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			min-height: 500px;
			min-width: 100%;
		}

		.cd-panel {
			position: fixed;
			top: 0;
			left: 0;
			height: 100%;
			width: 100%;
			visibility: hidden;
			-webkit-transition: visibility 0s 0.6s;
			transition: visibility 0s 0.6s;
		}

		.cd-panel::after {
			/* overlay layer */
			content: '';
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: transparent;
			cursor: pointer;
			-webkit-transition: background 0.3s 0.3s;
			transition: background 0.3s 0.3s;
		}

		.cd-panel.cd-panel--is-visible {
			visibility: visible;
			-webkit-transition: visibility 0s 0s;
			transition: visibility 0s 0s;
		}

		.cd-panel.cd-panel--is-visible::after {
			background: rgba(0, 0, 0, 0.6);
			-webkit-transition: background 0.3s 0s;
			transition: background 0.3s 0s;
		}

		@-webkit-keyframes cd-close-1 {
			0%, 50% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(45deg);
				transform: rotate(45deg);
			}
		}

		@keyframes cd-close-1 {
			0%, 50% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(45deg);
				transform: rotate(45deg);
			}
		}

		@-webkit-keyframes cd-close-2 {
			0%, 50% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(-45deg);
				transform: rotate(-45deg);
			}
		}

		@keyframes cd-close-2 {
			0%, 50% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(-45deg);
				transform: rotate(-45deg);
			}
		}

		.cd-panel__container {
			position: fixed;
			width: 90%;
			height: 100%;
			top: 0;
			background: #dbe2e9;
			z-index: 1;
			-webkit-transition: -webkit-transform 0.3s 0.3s;
			transition: -webkit-transform 0.3s 0.3s;
			transition: transform 0.3s 0.3s;
			transition: transform 0.3s 0.3s, -webkit-transform 0.3s 0.3s;
		}

		.cd-panel--from-right .cd-panel__container {
			right: 0;
			-webkit-transform: translate3d(100%, 0, 0);
			transform: translate3d(100%, 0, 0);
		}

		.cd-panel--from-left .cd-panel__container {
			left: 0;
			-webkit-transform: translate3d(-100%, 0, 0);
			transform: translate3d(-100%, 0, 0);
		}

		.cd-panel--is-visible .cd-panel__container {
			-webkit-transform: translate3d(0, 0, 0);
			transform: translate3d(0, 0, 0);
			-webkit-transition-delay: 0s;
			transition-delay: 0s;
		}

		@media only screen and (min-width: 768px) {
			.cd-panel__container {
				width: 30%;
			}
		}

		@media only screen and (min-width: 1170px) {
			.cd-panel__container {
				width: 30%;
			}
		}

		.cd-panel__content {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			overflow: auto;
			/* smooth scrolling on touch devices */
			-webkit-overflow-scrolling: touch;
		}

		.cd-panel__content p:first-of-type {
			margin-top: 0;
		}

	</style>
@stop

@section('content')
	@include('backend._partials.breadcrumb',
			['page_list' => array(
				['title' => trans('pages.pages'), 'link' => getBeUrl('pages')]
			)])

	<div id="app" class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						@include('backend._partials.notifications')
						@include('backend._partials.vue-notifications')
						<div class="box-content">
							<p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.title') }}
										@if(true)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<input v-model="form.title" class="form-control input-sm" type="text">
									</div>
									<span v-html="form.count_title"></span> /<span> 70</span>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.slug') }}
									</label>
									<div class="col-sm-6">
										<input class="form-control input-sm" v-model="form.slug">
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.description') }}
										@if(true)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<textarea rows="5" v-model="form.description" class="form-control"></textarea>
									</div>
									<span v-html="form.count_desc"></span> /<span> 160 ~ 300 </span>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
									</label>
									<div class="col-sm-6">
										<a href="javascript:void(0)" class="btn btn-success js-cd-panel-trigger" data-panel="main">Sample HTML</a>

									</div>
								</div>
								<div v-show="!form.is_html" class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.content') }}
										@if(true)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-12">
												<ul class="nav nav-pills">
													<li class="nav-item ">
														<a :class="themeNavLink('xcode')" href="#" @click.prevent="changeTheme('xcode')">Xcode</a>
													</li>
													<!-- Light -->
													<li class="nav-item">
														<a :class="themeNavLink('kuroir')" href="#" @click.prevent="changeTheme('kuroir')">Kuroir</a>
													</li>

													<!-- Dark -->
													<li class="nav-item">
														<a :class="themeNavLink('monokai')" href="#" @click.prevent="changeTheme('monokai')">Monokai</a>
													</li>

													<li class="nav-item">
														<a :class="themeNavLink('twilight')" href="#" @click.prevent="changeTheme('twilight')">Twilight</a>
													</li>

													<li class="nav-item">
														<a :class="themeNavLink('tomorrow_night')" href="#" @click.prevent="changeTheme('tomorrow_night')">Tomorrow Night</a>
													</li>

												</ul>
											</div>

										</div>

										<div class="row">
											<div class="col-md-12">
												<div id="ace-editor"></div>
											</div>
										</div>
									</div>

								</div>
								<div v-show="form.is_html" class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.content') }}
										@if(true)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-10">
										<textarea id="js-editor" class="form-control"></textarea>
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.meta_title') }}
										@if(false)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<input v-model="form.meta_title" class="form-control input-sm" type="text">
									</div>
									<span v-html="form.count_meta_title"></span> /<span> 70</span>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.meta_description') }}
										@if(false)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<textarea rows="5" v-model="form.meta_description" class="form-control"></textarea>
									</div>
									<span v-html="form.count_meta_description"></span> /<span> 160 ~ 300 </span>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.schema') }}
										@if(false)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<textarea rows="5" v-model="form.schema" class="form-control"></textarea>
										<a target="_blank" href="https://technicalseo.com/tools/schema-markup-generator/">Gen schema</a>
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('pages.status') }}
										@if(false)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<input type="checkbox" v-model="form.status">
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="text-right control-label col-sm-3 col-md-2">
										{{ trans('Optimize image') }}
										@if(false)
											<span class="label label-danger">{{ trans('table.required') }}</span>
										@endif
									</label>
									<div class="col-sm-6">
										<input type="checkbox" v-model="form.optimize_image">
									</div>
								</div>
								<hr />
								<div class="control-group form-group">
									<div class="col-sm-2"></div>
									<div class="col-sm-8 text-center js-page-btn">
										<a href="{{ route('page.index') }}" class="btn btn-large btn-default">
											<i class="fa fa-caret-left"></i>
											{{ trans('button.cancel') }}
										</a>
										<a href="#" class="btn btn-large btn-info" @click.prevent="saveChanges">
											<i class="fa fa-location-arrow"></i>
											{{ trans('button.save') }}
										</a>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<!-- Use for insert media from file manager start -->
	<script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
	<script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
	<script src="{!! asset('backend/js/vue.js') !!}"></script>
	<script src="{!! asset('backend/js/axios.min.js') !!}"></script>
	<script src="{!! asset('builder/onetech/vendor/ace/src-min/ace.js') !!}" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
	<script type="text/javascript">
		var app = new Vue({
			el: '#app',
			data: {
				error: false, // check isset Error
				errors: {}, // object error (all error)
				message: '',
				errorMessage: '',
				editorTheme: 'monokai',
				editor: '',
				ckeditorId: 'js-editor',
				form: {
					id: 0,
					lang: 'vn',
					title: '',
					slug: '',
					description: '',
					content: '',
					status: 1,
					is_html: 1,
					count_title: 0,
					count_desc: 0,
					count_meta_title: 0,
					count_meta_description: 0,
					optimize_image: 0,
					meta_title: '',
					meta_description: '',
					schema: '',
				},
			},
			watch: {
				'form.title': function () {
					this.form.count_title = this.form.title.length
					if (this.form.id === 0) {
						this.form.slug = this.changeToSlug(this.form.title)
					}
				},
				'form.description': function () {
					this.form.count_desc = this.form.description.length
				},
				'form.meta_title': function () {
					this.form.count_meta_title = this.form.meta_title.length
				},
				'form.meta_description': function () {
					this.form.count_meta_description = this.form.meta_description.length
				},
				'form.is_html': function () {
					if (this.form.is_html) {
						CKEDITOR.instances[this.ckeditorId].setData(this.editor.getValue())
					} else {
						this.editor.setValue(CKEDITOR.instances[this.ckeditorId].getData())
					}
				},
			},
			mounted: function () {
				this.initEditor()
				CKEDITOR.replace(this.ckeditorId)
				CKEDITOR.config.contentsCss = [
					"{{ asset('frontend/css/font-awesome.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/bootstrap.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/common.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/default.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/smart-phone.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/common.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/style.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/growup.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/growup-v2.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/jquery.mCustomScrollbar.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/jquery.bxslider.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('frontend/css/client.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
					"{{ asset('backend/css/custom-ckeditor.css').'?v='. env('CSS_VERSION', '0.0.0') }}",
				];

				this.getPage().then(data => {
					this.page = data
					if (Object.keys(data).length > 0) {
						this.form.id = data.id
						this.form.lang = data.lang
						this.form.title = data.title
						this.form.slug = data.slug
						this.form.description = data.description
						this.form.content = data.content
						this.form.is_html = data.is_html
						this.form.status = data.status
						this.form.meta_title = data.meta_title
						this.form.meta_description = data.meta_description
						this.form.schema = data.schema
						this.editor.setValue(this.form.content)
						this.editor.session.setMode('ace/mode/html')
						CKEDITOR.instances[this.ckeditorId].setData(this.form.content)
					}
				})
			},
			methods: {
				addHTML: function (index) {
				},
				themeNavLink: function (theme) {
					return this.editorTheme === theme ? 'nav-link active' : 'nav-link'
				},
				fileListItemClass: function (path) {
					return this.filePath === path ? 'list-group-item active' : 'list-group-item'
				},
				initEditor: function () {
					this.editor = ace.edit('ace-editor')
					this.editor.setTheme('ace/theme/monokai')
					this.editor.session.setMode('ace/mode/html')
				},
				changeTheme: function (theme) {
					this.editorTheme = theme
					this.editor.setTheme('ace/theme/' + theme)
				},
				getPage: function () {
					let d = $.Deferred();
					setTimeout(function () {
						let page = {!! json_encode($page->toArray(),true) !!}
						d.resolve(page);
					}, 0)
					return d.promise()
				},
				saveChanges: function () {
					// Make sure there are no syntax errors
					var hasError = false
					var annotations = this.editor.getSession().getAnnotations()
					for (var i = 0; i < annotations.length; i++) {
						if (annotations[i]['type'] === 'error') {
							hasError = true
						}
					}
					if (hasError) {
						this.error = true
						this.errorMessage = 'You have syntax errors in your code! Please fix them and try again.'
						return false
					}
					// Save changes
					if (!this.form.is_html) {
						this.form.content = this.editor.getValue()
					} else {
						this.form.content = CKEDITOR.instances[this.ckeditorId].getData()
					}
					console.log(this.form)
					$(".ot-loading").show()
					axios.post("{{ route('page.save') }}", this.form)
						.then((response) => {
							this.error = false
							this.errors = {}
							this.message = response.data.message
							if (this.form.id <= 0) {
								window.location.replace(response.data.url)
							}
							$(".ot-loading").hide()
							$('body,html').animate({
								scrollTop: 0
							}, 500)
						}).catch((error) => {
						console.log(error)
						$(".ot-loading").hide()
						this.error = true
						var errors = error.response.data.errors
						if (typeof (errors) === 'object') {
							this.errors = errors
							this.errorMessage = ''
						} else {
							this.errors = {}
							this.errorMessage = errors.message
						}
					})
				},
				changeToSlug: function (title) {
					var slug;
					//Đổi chữ hoa thành chữ thường
					slug = title.toLowerCase();
					//Đổi ký tự có dấu thành không dấu
					slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
					slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
					slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
					slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
					slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
					slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
					slug = slug.replace(/đ/gi, 'd');
					//Xóa các ký tự đặt biệt
					slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
					//Đổi khoảng trắng thành ký tự gạch ngang
					slug = slug.replace(/ /gi, "-");
					//Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
					//Phòng trường hợp người nhập vào quá nhiều ký tự trắng
					slug = slug.replace(/\-\-\-\-\-/gi, '-');
					slug = slug.replace(/\-\-\-\-/gi, '-');
					slug = slug.replace(/\-\-\-/gi, '-');
					slug = slug.replace(/\-\-/gi, '-');
					//Xóa các ký tự gạch ngang ở đầu và cuối
					slug = '@' + slug + '@';
					slug = slug.replace(/\@\-|\-\@|\@/gi, '');
					//In slug ra textbox có id “slug”
					return slug;
				}
			},
		});
		(function () {
			// Slide In Panel - by CodyHouse.co
			var panelTriggers = document.getElementsByClassName('js-cd-panel-trigger');
			if (panelTriggers.length > 0) {
				for (var i = 0; i < panelTriggers.length; i++) {
					(function (i) {
						var panelClass = 'js-cd-panel-' + panelTriggers[i].getAttribute('data-panel'),
							panel = document.getElementsByClassName(panelClass)[0];
						// open panel when clicking on trigger btn
						panelTriggers[i].addEventListener('click', function (event) {
							event.preventDefault();
							addClass(panel, 'cd-panel--is-visible');
						});
						//close panel when clicking on 'x' or outside the panel
						panel.addEventListener('click', function (event) {
							if (hasClass(event.target, 'js-cd-close') || hasClass(event.target, panelClass)) {
								event.preventDefault();
								removeClass(panel, 'cd-panel--is-visible');
							}
						});
					})(i);
				}
			}
			//class manipulations - needed if classList is not supported
			//https://jaketrent.com/post/addremove-classes-raw-javascript/
			function hasClass(el, className) {
				if (el.classList) return el.classList.contains(className);
				else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
			}

			function addClass(el, className) {
				if (el.classList) el.classList.add(className);
				else if (!hasClass(el, className)) el.className += " " + className;
			}

			function removeClass(el, className) {
				if (el.classList) el.classList.remove(className);
				else if (hasClass(el, className)) {
					var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
					el.className = el.className.replace(reg, ' ');
				}
			}
		})();
	</script>
@stop
