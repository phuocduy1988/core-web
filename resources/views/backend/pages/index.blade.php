@extends('backend.layouts.app')

@section('title') {{ trans('pages.pages') }} @stop

@section('content')
    @include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('pages.pages'), 'link' => route('page.index')]
    	)])

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <!-- Search block START -->
                        {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => 'page.index']) !!}
                            <table class="table table-bordered m-b-md">
                                <tr>
                                    <td class="text-right"><strong>{{ trans('label.keyword') }}</strong></td>
                                    <td colspan="3">
                                        <div class="form-group">
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm " name="keyword" type="text" value="{{ Request::get('keyword') }}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="row m-b-md">
                                <div class="col-sm-12 text-right">
                                    <button id="btn-search" type="submit" action="confirm" class="btn btn-large btn-default">
                                        <i class="fa fa fa-search"></i> {{ trans('button.search') }}
                                    </button>
                                    <a  href="{{ route('page.new') }}" class="btn btn-large btn-info">
                                        <i class="fa fa-plus-circle"></i> {{ trans('button.create') }}
                                    </a>
                                </div>
                            </div>
                        <!-- Search block END -->
                        <!-- List content START -->
                        @include('backend._partials.sort-field')
                        <div class="table-responsive">
                            <table class="table table-bordered js-table">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ trans('pages.id') }} @include('backend._partials.controls.sort-icon', ['field' => 'id'])</th>
										<th>{{ trans('pages.title') }} </th>
										<th>{{ trans('pages.slug') }} </th>
										<th>{{ trans('pages.description') }} </th>
                                        <th>{{ trans('pages.lang') }}@include('backend._partials.controls.sort-icon', ['field' => 'lang'])</th>
                                        <th>{{ trans('pages.status') }} @include('backend._partials.controls.sort-icon', ['field' => 'status'])</th>


										{{--<th>{{ trans('pages.meta_title') }} </th>--}}
										{{--<th>{{ trans('pages.meta_description') }} </th>--}}
										{{--<th>{{ trans('pages.rating') }} </th>--}}
										{{--<th>{{ trans('pages.comment') }} </th>--}}

{{--										<th>{{ trans('pages.schema') }} </th>--}}

                                        {{-- --UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
										<th>{{ trans('label.edit') }}</th>
                                        <th>{{ trans('label.delete') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($pagesData) > 0)
                                        @foreach($pagesData as $page)
                                            <tr>
                                                <td class="text-center"> {!! $page->id !!}</td>
												<td class="text-left"> {!! $page->title !!}</td>
												<td class="text-left"> {!! $page->slug !!}</td>
												<td class="text-left"> {!! $page->description !!}</td>
                                                <td class="text-center"> {!! $page->lang !!}</td>
                                                <td class="text-center"><a href="javascript:void(0);" table="{{ $page->getTable() }}" id-value="{{ isset($page->id) ? $page->id : '' }}" column-name="status" boolean="{{ $page->status }}" class="js-table-change-boolean label @if($page->status == 1) label-success @else label-warning @endif"> {{ showBoolean($page->status) }} </a></td>


												{{--<td class="text-left"> {!! $page->meta_title !!}</td>--}}
												{{--<td class="text-left"> {!! $page->meta_description !!}</td>--}}
												{{--<td class="text-left"> {!! $page->rating !!}</td>--}}
												{{--<td class="text-left"> {!! $page->comment !!}</td>--}}

{{--												<td class="text-left"> {!! $page->schema !!}</td>--}}

                                                {{-- --UPDATE_GENCODE_ROW_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
													<td class="text-center add-relation">


                                                    <a title="Edit" href="{{ route('page.form', [$page->id]) }}"><i class="fa fa-edit"></i></a>
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" data-href="{{ route('page.delete', [$page->id]) }}" class="js-delete" data-message="{{ trans('message.alert_delete') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="11">{{ trans('message.no_record') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <!-- Show pagination -->
                            {!! $pagesData->appends(array(
                                                'grid_page_size'    => Request::get('grid_page_size'),
                                                'keyword'           => Request::get('keyword'),
                                                'sort_field'        => Request::get('sort_field'),
                                                'sort_type'         => Request::get('sort_type')
                                              ))->links('backend._partials.pagination'); !!}
                        </div>
                        {{ Form::close() }}
                    <!-- List content END -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('backend._partials.js.js-popup')
    <script>
		$('#btn-search').on('click', function(e) {
			e.preventDefault();
			// Get current URL
			 var url = window.location.href;

			var keyword = $("input[name='keyword']").val();

			var removeQueryString = url.split("?");
			if(removeQueryString.length == 2) {
				url = removeQueryString[0];
			}

			if(keyword) {
				url = url + '?keyword=' + keyword;
			}

			$('form').prop('action', url);
			$('form').submit();
		});
	</script>
@stop