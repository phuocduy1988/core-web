<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ trans('menu.login') }}</title>
	<link rel="shortcut icon" href="" type="image/x-icon" />
	<link href="{!! asset('inspinia/css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! asset('inspinia/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
	<link href="{!! asset('inspinia/css/style.css') !!}" rel="stylesheet">
	<link href="{!! asset('backend/css/custom.min.css') !!}" rel="stylesheet">
	@yield('css')
</head>

<body class="login-bg">
	<div id="app">
		<div class="middle-box text-center loginscreen animated fadeInDown">
			<div>
				{{ Form::open(array('method' => 'post' , 'class' => 'login-form')) }}
					<img src="{!! asset('frontend/img/new/lg-main-horizontal.png') !!}" alt="" style="width: 256px;margin-bottom: 16px;">
					@include('backend._partials.notifications')
{{--					<p><strong>{{ trans('message.login_require') }}</strong></p>--}}
					<div class="form-group">
						<input type="text" name="email" class="form-control" placeholder="{{ trans('label.email') }}" required />
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="{{ trans('label.password') }}" required />
					</div>
					<button type="submit" class="btn btn-primary full-width">{{ trans('menu.login') }}</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>

	<script src="{!! asset('inspinia/js/app.js') !!}" type="text/javascript"></script>
</body>
</html>
