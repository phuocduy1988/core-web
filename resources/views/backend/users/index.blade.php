@extends('backend.layouts.app')

@section('title') {{ trans('users.users') }} @stop

@section('content')
    @include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('users.users'), 'link' => route('user.index')]
    	)])

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <!-- Search block START -->
                        {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => 'user.index']) !!}
                            <table class="table table-bordered m-b-md">
                                <tr>
                                    <td class="text-right"><strong>{{ trans('label.keyword') }}</strong></td>
                                    <td colspan="3">
                                        <div class="form-group">
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm " name="keyword" type="text" value="{{ Request::get('keyword') }}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="row m-b-md">
                                <div class="col-sm-12 text-right">
                                    <button id="btn-search" type="submit" action="confirm" class="btn btn-large btn-default">
                                        <i class="fa fa fa-search"></i> {{ trans('button.search') }}
                                    </button>
                                    <a  href="{{ route('user.new') }}" class="btn btn-large btn-info">
                                        <i class="fa fa-plus-circle"></i> {{ trans('button.create') }}
                                    </a>
                                </div>
                            </div>
                        <!-- Search block END -->
                        <!-- List content START -->
                        @include('backend._partials.sort-field')
                        <div class="table-responsive">
                            <table class="table table-bordered js-table">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ trans('users.id') }} @include('backend._partials.controls.sort-icon', ['field' => 'id'])</th>
										<th>{{ trans('users.name') }} </th>
										<th>{{ trans('users.email') }} </th>
										<th>{{ trans('users.status') }} </th>

                                        {{-- --UPDATE_GENCODE_HEADER_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                        <th>{{ trans('label.edit') }}</th>
                                        <th>{{ trans('label.delete') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($usersData) > 0)
                                        @foreach($usersData as $user)
                                            <tr>
                                                <td class="text-center"> {!! $user->id !!}</td>
												<td class="text-left"> {!! $user->last_name . ' ' . $user->first_name !!}</td>
												<td class="text-left"> {!! $user->email !!}</td>
												<td class="text-center"><a href="javascript:void(0);" table="{{ $user->getTable() }}" id-value="{{ isset($user->id) ? $user->id : '' }}" column-name="status" boolean="{{ $user->status }}" class="js-table-change-boolean label @if($user->status == 1) label-success @else label-warning @endif"> {{ showBoolean($user->status) }} </a></td>

                                                {{-- --UPDATE_GENCODE_ROW_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                                <td class="text-center add-relation">
                                                    <a title="Edit" href="{{ getBeUrl('users/' . $user->id) }}"><i class="fa fa-edit"></i></a>
                                                </td>
                                                <td class="text-center">
                                                	<a href="javascript:void(0)" data-href="{{ asset(getBeUrl('users/delete/'.$user->id)) }}" class="js-delete" data-message="{{ trans('message.alert_delete') }}">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="11">{{ trans('message.no_record') }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <!-- Show pagination -->
                            {!! $usersData->appends(array(
                                                'grid_page_size'    => Request::get('grid_page_size'),
                                                'keyword'           => Request::get('keyword'),
                                                'sort_field'        => Request::get('sort_field'),
                                                'sort_type'         => Request::get('sort_type')
                                              ))->links('backend._partials.pagination'); !!}
                        </div>
                        {{ Form::close() }}
                    <!-- List content END -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('backend._partials.js.js-popup')
    <script>
		$('#btn-search').on('click', function(e) {
			e.preventDefault();
			// Get current URL
			 var url = window.location.href;

			var keyword = $("input[name='keyword']").val();

			var removeQueryString = url.split("?");
			if(removeQueryString.length == 2) {
				url = removeQueryString[0];
			}

			if(keyword) {
				url = url + '?keyword=' + keyword;
			}

			$('form').prop('action', url);
			$('form').submit();
		});
	</script>
@stop