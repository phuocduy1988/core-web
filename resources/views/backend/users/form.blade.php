@extends('backend.layouts.app')

@section('title') {{ trans('users.users') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

@include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('users.users'), 'link' => getBeUrl('users')]
    	)])

	<div class="wrapper wrapper-content animated fadeInRight">
		{!! Form::open(['method' => 'post', 'route' => 'user.save', 'files'=>true]) !!}
		{!! Form::hidden('id', $user->id) !!}
		<div class="row">
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
				    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('table.first_name') }}
                                        @if(true)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="first_name" type="text" value="{{ old('first_name', isset($user->first_name) ? $user->first_name : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('table.last_name') }}
                                        @if(true)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="last_name" type="text" value="{{ old('last_name', isset($user->last_name) ? $user->last_name : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('users.email') }}
                                        @if(true)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="email" type="text" value="{{ old('email', isset($user->email) ? $user->email : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('backend.update-password') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-2">
                                        <input class="form-control input-sm" name="password" type="password" value="{{ old('password', isset($user->password) ? $user->password : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('label.image') }}
                                         @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <div class="media-box">
                                            @if(old('image_url', isset($user->image) ? $user->image : '' ))
                                                <div id="js-img-preview" class="img-preview">
                                                    <img class="js-img-change img-responsive" src="{{ asset(old('image', isset($user->image) ? $user->image : '' )) }}" />
                                                </div>
                                            @else
                                                <div id="js-img-preview">
                                                    <img class="js-img-change img-responsive hide" src="" />
                                                </div>
                                            @endif
                                            <div class="media-action">
                                                <a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">
                                                    <i class="fa fa-warning"></i>
                                                    {{ trans('button.delete') }}
                                                </a>
                                            </div>
                                            <input type="hidden" class="media-change" name="image" value="{{ old('avatar', isset($user->image) ? $user->image : '' ) }}" />
                                            <button type="button" class="popup_selector" data-inputid="feature_image">
                                                {{ trans('message.select_image') }}
                                            </button>
                                            <span class="select-img-desc">{{ trans('message.select_image_desc') }}</span>
                                        </div>
                                    </div>
                                    {{ trans('message.image_type_require') }}
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('users.status') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" name="status" value="1" @if(isset($user->status)? $user->status : 1) checked @endif>
                                    </div>
                                </div>
{{-- --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                <hr />
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 text-center js-page-btn">
                                        <a href="{{ route('user.index') }}" class="btn btn-large btn-default">
                                        <i class="fa fa-caret-left"></i>
                                        {{ trans('button.cancel') }}
                                        </a>
                                        <button type="submit" action="confirm" class="btn btn-large btn-info">
                                        <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    <script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <!-- Use for insert media from file manager end -->
    <script src="{!! asset('backend/js/media.js') !!}"></script>
    <!-- Date time picker start -->
    <script src="{!! asset('backend/plugins/datetimepicker/moment-with-locales.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('backend/js/datetime-validation.js') !!}"></script>
    <!-- Date time picker end -->
    <script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('js-editor');
        });
        $('#js-viewdate').datetimepicker({
            format: 'YYYY/MM/DD HH:mm',
            locale: 'ja'
        });
    </script>
@stop
