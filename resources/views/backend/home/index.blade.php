@extends('backend.layouts.app')

@section('title')
    {{ trans('backend.home') }}
@stop

@section('css')
    <link href="{!! asset('backend/plugins/daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
    <script src="{{ asset('backend/js/vue.js') }}"></script>
    <script src="{{ asset('backend/js/axios.min.js') }}"></script>
    <script src="{{ asset('backend/js/jquery-3.3.1.js') }}"></script>
    <script src="{{ asset('backend/plugins/chart/chart.bundle.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('backend/js/daterangepicker.min.js') }}"></script>
    <script src="{{ asset('backend/js/moment.js') }}"></script>

@stop

@section('content')

    @include('backend._partials.breadcrumb',
    ['page_list' => array(
        ['title' => trans('backend.dashboard'), 'link' => '#']
    )])
    <div class="wrapper wrapper-content animated fadeInRight clearfix">
        <div class="row">
            @include('backend._partials.notifications')
            <div class="col-lg-6">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Most visit pages</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p></p>
@stop
@section('scripts')
@stop

