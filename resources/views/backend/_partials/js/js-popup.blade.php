<script>
    /**
     * Js common function relate to popup
     */

// Define param content
    var isDelete    = "{{ trans('message.is_delete') }}";
    var isProcess    = false;
    var isPrivate   = "{{ trans('message.is_boolean') }}";
    var isPublic    = "{{ trans('message.is_boolean') }}";
    var confirmBtn  = "{{ trans('button.yes') }}";
    var cancelBtn   = "{{ trans('button.no') }}";
    var publicText  = "{{ trans('label.true') }}";
    var privateText = "{{ trans('label.false') }}";
    /**
     * Clear ajax message content
     * @author: duylbp
     * @date: 2017-10-05
     */
    function clearAjaxMsg() {
        $('.ibox-content').find('.alert').remove();
    }

    /**
     * Show ajax message content
     * @author: duylbp
     * @date: 2017-10-05
     * @param  string  msg
     * @param  string  type
     */
    function showAjaxMsg(msg, type='success') {
        $('.wrapper-content').find('.alert').remove();
        if(type == 'success') {
            $('.ibox-content').prepend("<div class='alert alert-success'>" + msg + "</div>");
        }
        else {
            $('.ibox-content').prepend("<div class='alert alert-danger'>" + msg + "</div>");
        }
    }

    /**
     * Table delete item record function
     * @author: duylbp
     * @date: 2017-10-05
     */
    $('.js-table .js-table-delete').on('click', function() {
        // Get field name will show on popup confirm
        var content = $(this).closest('tr').find('td:eq(1)').text();
        // If this page is not list, will get name from input hidden field
        if(content.length == 0) {
            content = $('#js-popup-name').val();
        }
        content += isDelete;
        // Reference with $(this)
        var item = $(this);
        // Show popup confirm
        swal({
            title: "",
            html: true,
            text: content,
            showCancelButton: true,
            confirmButtonText: confirmBtn,
            cancelButtonText: cancelBtn,
            closeOnConfirm: true
        }, function() {
            // Submit form delete when ok
            item.closest('form').submit();
        });
    });

    /**
     * Table change status of item record
     * @author: duylbp
     * @date: 2017-10-05
     */
    $('.js-table .js-table-change-boolean').on('click', function() {
        if(isProcess) {
            console.log('check is process');
            return false;
        }
        var idValue     = $(this).attr('id-value');
        var tableValue  = $(this).attr('table');
        var booleanValue = $(this).attr('boolean');
        var columnName = $(this).attr('column-name');
        // Get field name will show on popup confirm
        var content = $(this).closest('tr').find('td:eq(1)').text();
        if(typeof booleanValue == 'undefined') {
            booleanValue = 0;
        }
        // If this page is not list, will get name from input hidden field
        if(content.length == 0) {
            content = $('#js-popup-name').val();
        }
        content = isPublic + ' ?';

        // if(booleanValue == 0) {
        // }
        // else {
        //     content += isPrivate;
        // }
        // Reference with $(this)
        var item = $(this);
        // Show popup confirm
        swal({
            title: "",
            html: true,
            text: content,
            showCancelButton: true,
            confirmButtonText: confirmBtn,
            cancelButtonText: cancelBtn,
            closeOnConfirm: true
        }, function() {
            var data = {
                id: idValue,
                table: tableValue,
                boolean: booleanValue,
                column_name: columnName,
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $(".ot-loading").show();
            isProcess = true;
            console.log('check is process = true');
            console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('common.change-boolean') }}",
                data: data,
                success: function(result) {
                    if(result == 0) {
                        // Change status fail
                        showAjaxMsg("{{ trans('message.updated_failed') }}", 'error');
                        $(".ot-loading").hide();
                        isProcess = false;
                    }
                    else {
                        // Update color and status value
                        if(booleanValue == 0 && item.hasClass('label-warning')) {
                            item.removeClass('label-warning')
                                .addClass('label-success')
                                .html(publicText)
                                .attr('boolean', 1);
                            isProcess = false;
                            console.log('check is process = false1');

                        }
                        else {
                            console.log('check is process = false2');
                            item.removeClass('label-success')
                                .addClass('label-warning')
                                .html(privateText)
                                .attr('boolean', 0);
                            isProcess = false;
                        }
                        $(".ot-loading").hide();
                    }
                },
                done: function(result) {
                    $(".ot-loading").hide();
                    isProcess = false;
                    console.log('check is process = false3');

                }
            });
        });
    });
</script>
