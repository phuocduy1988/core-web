@if($paginator->hasPages())
	<?php
    	$firstItem = ($paginator->currentPage() - 1) * $paginator->perPage() + 1;
    	$lastItem  = $firstItem + $paginator->perPage() - 1;
    	if($paginator->currentPage() == $paginator->lastPage()) {
    		$temp = ($paginator->total() - $firstItem);
    		$lastItem = $firstItem + $temp;
    	}
    ?>
	<div class="col-sm-5 no-padding">
		<div>{{ trans('label.page_total') }} {{ $paginator->total() }} {{ trans('label.page_from') }} {{ $firstItem }}～{{ $lastItem }} {{ trans('label.page_end') }}</div>
	</div>
	<div class="col-sm-7 text-right no-padding">
		<ul class="pagination">
            <!-- Previous Page Link -->
			@if($paginator->onFirstPage())
				<li class="disabled">
                    <span>&laquo; {{ trans('label.previous') }}</span>
                </li>
			@else
				<li>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo; {{ trans('label.previous') }}</a>
                </li>
			@endif

            <!-- Pagination Elements -->
			@foreach($elements as $element)
                <!-- "Three Dots" Separator -->
				@if(is_string($element))
					<li class="disabled"><span>{{ $element }}</span></li>
				@endif

				<!-- Array Of Links -->
				@if(is_array($element))
					@foreach($element as $page => $url)
						@if($page == $paginator->currentPage())
							<li class="active"><span>{{ $page }}</span></li>
						@else
							<li><a href="{{ $url }}">{{ $page }}</a></li>
						@endif
					@endforeach
				@endif
			@endforeach

			<!-- Next Page Link -->
			@if($paginator->hasMorePages())
				<li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next">{{ trans('label.next') }} &raquo;</a>
                </li>
			@else
				<li class="disabled"><span>{{ trans('label.next') }} &raquo;</span></li>
			@endif
		</ul>
	</div>
@endif
