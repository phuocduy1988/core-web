@extends('backend.layouts.app')

@section('title')
    {{ trans('backend.iframe') }}
@stop

@section('css')
<!-- Date range picker -->
@stop

@section('content')

    @include('backend._partials.breadcrumb',
	['page_list' => array(
        ['title' => trans('backend.iframe'), 'link' => '#']
	)])

	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
                    <div class="ibox-content">
						<iframe src="{{ $url }}" width="100%"></iframe>
                    </div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
@stop
