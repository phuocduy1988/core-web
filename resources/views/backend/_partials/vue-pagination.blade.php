<div class="text-right no-padding">
	<ul v-show="pagination.total > pagination.per_page" class="pagination">
		<!-- Previous Page Link -->
		<li v-if="pagination.current_page === 1"><span>&laquo;</span></li>
		<li v-else><a href="#" @click.prevent="changePage(pagination.current_page - 1)" rel="prev">&laquo;</a></li>
		<li v-for="page in pagination.last_page"
			:key="page"
			:class="{ 'active':pagination.current_page === page }"
		>
                                <span v-show="page > 0">
                                    <a href="#"
									   @click.prevent="changePage(page)"
									   class="page-link" v-html="page">
                                    </a>
                                </span>
		</li>
		<!-- Next Page Link -->
		<li v-if="pagination.last_page < pagination.current_page"><span>&raquo;</span></li>
		<li v-else><a href="#" @click.prevent="changePage(pagination.current_page + 1)" rel="prev">&raquo;</a></li>
	</ul>
</div>