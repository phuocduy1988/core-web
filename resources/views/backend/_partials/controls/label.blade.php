<label class="control-label {{ $class }}">
    {{ isset($name) ? $name : '' }}

    @if(isset($require) && $require == true)
        <span class="label label-danger">{{ trans('label.require') }}</span>
    @endif
</label>
