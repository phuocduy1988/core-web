<div class="sort-icon">
	<a href="javascript:void(0)" class="js-sort" sort-field="{{ $field }}" sort-type="asc"><i class="fa fa-sort-asc"></i></a>
	<a href="javascript:void(0)" class="js-sort" sort-field="{{ $field }}" sort-type="desc"><i class="fa fa-sort-desc"></i></a>
</div>
