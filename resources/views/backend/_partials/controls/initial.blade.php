<!--
* Custom view control generator
* @date: 2019-09-12
* @author: duylbp
* @param
* @return A control view
-->
@php
    $labelClass = isset($label_class) ? $label_class : 'col-sm-2';
    $controlClass = isset($control_class) ? $control_class : 'col-sm-6';
    $fieldName = '';
    $controlOption = '';
@endphp

@if(isset($controls) && is_array($controls))
    @foreach($controls as $control)
        @php
            $type = isset($control['type']) ? $control['type'] : '';
            $fieldName = isset($control['field_name']) ? $control['field_name'] : '';
            $controlOption = isset($control['options']) ? $control['options'] : [];
            $class = isset($options['class']) ? $options['class'] : '';
            $controlOption['class'] = 'form-control input-sm '.$class;
        @endphp

        @if($type =='text')
            <!-- params:
                    'field_name' => '',
                    'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {{ Form::text($fieldName, old($fieldName), $controlOption) }}
                </div>
                {{ isset($control['desc_text']) ? $control['desc_text'] : '' }}
            </div>
        @elseif($type =='textarea')
            <!-- params:
                    'field_name' => '',
                    'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {{ Form::textarea($fieldName, old($fieldName), $controlOption) }}
                </div>
            </div>

        @elseif($type =='password')
            <!-- params:
                    'field_name' => '',
                    'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {{ Form::input('password', $fieldName, old($fieldName), $controlOption) }}
                </div>
            </div>

        @elseif($type =='select')
            <!-- params:
                'field_name' => '',
                'value' => [1, 2, 3, 4],
                'selected' => 1,
                'options' => ['placeholder' => 'Select...']
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    @php
                        $controlOption['class'] = 'input-sm '.$class;
                    @endphp
                    {{ Form::select(
                        $fieldName,
                        isset($control['value']) ? $control['value'] : [],
                        isset($control['selected']) ? $control['selected'] : null,
                        $controlOption)
                    }}
                </div>
            </div>

        @elseif($type =='checkbox')
            <!-- params:
                'value' => '',
                'checked' => true,
                'text' => '',
                'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    <label class="font-normal">
                        @php
                            $controlOption['class'] = 'input-sm checkbox-inline '.$class;
                        @endphp

                        {{ Form::checkbox(
                            $fieldName,
                            isset($control['value']) ? $control['value'] : '',
                            isset($control['checked']) ? $control['checked'] : false,
                            $controlOption)
                        }}

                        @if(isset($control['text']))
                            {!! $control['text'] !!}
                        @endif
                    </label>
                </div>
            </div>

        @elseif($type =='multi_checkbox')
            <!-- params:
                'value'   => [], format is a array and contain item value
                'text'    => [], format is a array and contain item text
                'checked' => [], format is a array and contain value checked define
                'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    @if(isset($control['value']) && isset($control['text']))
                        @for($i=0; $i<count($control['value']); $i++)
                            @php
                                $isChecked = (isset($control['checked']) && in_array($control['value'][$i], $control['checked'])) ? true : false;
                                $controlOption['class'] = 'input-sm checkbox-inline '.$class;
                            @endphp
                            <label class="font-normal">
                                {{ Form::checkbox(
                                    $fieldName,
                                    isset($control['value'][$i]) ? $control['value'][$i] : '',
                                    $isChecked,
                                    $controlOption)
                                }}

                                @if(isset($control['text'][$i]))
                                    {!! $control['text'][$i] !!}
                                @endif
                            </label>
                        @endfor
                    @endif
                </div>
            </div>

        @elseif($type =='radio')
            <!-- params:
                'field_name' => '',
                'value'   => [], format is a array and contain item value
                'text'    => [], format is a array and contain item text
                'checked' => 1, format is a number or string value
                'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    @if(isset($control['value']) && isset($control['text']))
                        @for($i=0; $i<count($control['value']); $i++)
                            @php
                                $isChecked = (isset($control['checked']) && $control['checked'] == $control['value'][$i]) ? true : false;
                                $controlOption['class'] = 'input-sm checkbox-inline '.$class;
                            @endphp
                            {{ Form::radio(
                                $fieldName,
                                $control['value'][$i],
                                $isChecked,
                                $controlOption)
                            }} <span class="input-sm p-l-0">{!! $control['text'][$i] !!}</span>
                        @endfor
                    @endif
                </div>
            </div>

        @elseif($type =='upload_image')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    <div class="media-box">
                        @if(old($fieldName))
                            <div id="js-img-preview" class="img-preview">
                                <img class="js-img-change img-responsive" src="{{ asset(old($fieldName)) }}" />
                            </div>
                        @else
                            <div id="js-img-preview">
                                <img class="js-img-change img-responsive hide" src="" />
                            </div>
                        @endif
                        <div class="media-action">
                            <a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">
                                <i class="fa fa-warning"></i>
                                {{ trans('button.delete') }}
                            </a>
                        </div>
                        <input type="hidden" class="media-change" name="{{ $fieldName }}" value="{{ old($fieldName) }}" />
                        <button type="button" class="popup_selector" data-inputid="feature_image">
                            {{ trans('message.select_image') }}
                        </button>
                        <span class="select-img-desc">{{ trans('message.select_image_desc') }}</span>
                    </div>
                </div>
                {{ trans('message.image_type_require') }}
            </div>

        @elseif($type =='upload_audio')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    <div class="media-box">
                        @if(old($fieldName))
                            <a href="{{ asset(old($fieldName)) }}" class="js-audio-preview">{{ asset(old($fieldName)) }}</a>
                        @else
                            <a href="#" class="js-audio-preview"></a>
                        @endif
                        <div class="media-action">
                            <a href="javascript:void(0)" class="delete-btn js-media-delete" style="display:none;">
                                <i class="fa fa-warning"></i>
                                {{ trans('button.delete') }}
                            </a>
                        </div>
                        <input type="hidden" class="audio-change" name="{{ $fieldName }}" value="{{ old($fieldName) }}" />
                        <button type="button" class="popup_selector" data-inputid="feature_image">
                            {{ trans('message.select_audio') }}
                        </button>
                        <span class="select-img-desc">{{ trans('message.select_audio_desc') }}</span>
                    </div>
                </div>
                {{ trans('message.audio_type_require') }}
            </div>

        @elseif($type =='number')
            <!-- params:
                    'field_name' => '',
                    'options' => []
            -->
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {{ Form::number($fieldName, old($fieldName), $controlOption) }}
                </div>
            </div>

        @elseif($type =='date')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    <div class="col-xs-4 no-padding">
                        <div class="controls input-group date datepicker" @if(isset($control['options'])) @foreach($control['options'] as $key => $value) {{ $key.'='.$value }} @endforeach @endif>
                            {{ Form::text($fieldName, old($fieldName), array('class' => 'form-control')) }}
                            <span class="input-group-addon">
            					<span class="glyphicon glyphicon-calendar"></span>
            				</span>
                        </div>
                    </div>
                </div>
            </div>

        @elseif($type =='startdate_enddate')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                @php
                    $startFieldName = isset($control['start_options']['name']) ? $control['start_options']['name'] : 'start_date';
                    $endFieldName = isset($control['end_options']['name']) ? $control['end_options']['name'] : 'end_date';
                @endphp
                <div class="{{ $controlClass }} date-group">
                    <div class="js-date-start controls input-group date datepicker col-xs-4 pull-left">
                        <input autocomplete="off" class="form-control" size="16" type="text" value="{{ old($startFieldName, '') }}" name="{{ $startFieldName }}" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="span-text m-l-sm">~</span>
                    <div class="js-date-end controls input-group date datepicker col-xs-4 pull-left m-l-sm">
                        <input autocomplete="off" class="form-control" size="16" type="text" value="{{ old($endFieldName, '') }}" name="{{ $endFieldName }}" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    @if(isset($control['no_limit_options']))

                    <label class="font-normal m-l-md">
                        {{ Form::checkbox(
                            isset($control['no_limit_options']['name']) ? $control['no_limit_options']['name'] : 'viewdate_nolimit',
                            isset($control['no_limit_options']['value']) ? $control['no_limit_options']['value'] : 1,
                            isset($control['no_limit_options']['checked']) ? $control['no_limit_options']['checked'] : null,
                            array('class' => 'input-sm checkbox-inline') )
                        }}
                        {{ trans('label.viewdate_nolimit') }}
                    </label>
                    @endif
                </div>
            </div>

        @elseif($type =='map')


        @elseif($type == 'include')
            @if(strlen($control['path']) > 0)
                @include($control['path'])
            @endif

        @elseif($type == 'include_content')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    @if(strlen($control['path']) > 0)
                        @include($control['path'])
                    @endif
                </div>
            </div>

        @elseif($type == 'custom')
            {!! isset($control['content']) ? $control['content'] : '' !!}

        @elseif($type == 'custom_content')
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {!! isset($control['content']) ? $control['content'] : '' !!}
                </div>
            </div>

        @else
            <div class="form-group">
                @include('backend._partials.controls.label',
                        [   'class' => $labelClass,
                            'name' => isset($control['label']) ? $control['label'] : '',
                            'require' => isset($control['require']) ? $control['require'] : ''
                        ])
                <div class="{{ $controlClass }}">
                    {{ Form::text($fieldName, old($fieldName), $controlOption) }}
                </div>
            </div>
        @endif

    @endforeach
@endif
