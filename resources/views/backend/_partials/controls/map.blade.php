@php
    $mapAddress = '';
    $mapLatitude = '';
    $mapLongitude = '';
    $mapZoom = '';
    if(isset($data)) {
        $mapAddress = isset($data->map_address) ? $data->map_address : '';
        $mapLatitude = isset($data->map_latitude) ? $data->map_latitude : '';
        $mapLongitude = isset($data->map_longitude) ? $data->map_longitude : '';
        $mapZoom = isset($data->map_zoom) ? $data->map_zoom : '';
    }
@endphp
<div class="form-group">
    <label class="control-label {{ isset($label_class) ? $label_class : '' }}">
        {{ isset($label) ? $label : '' }}

        @if(isset($require) && $require == true)
            <span class="label label-danger">{{ trans('label.require') }}</span>
        @endif
    </label>

    <div class="{{ isset($control_class) ? $control_class : '' }}">
        <!-- show map error message here -->
        <div id="js-map-message" class="alert alert-danger hide"></div>
        <!-- google map general START -->
        <div>
            <p>{{ trans('message.google_map_require') }}</p>
            <div class="col-sm-10">
                <input id="js-map-address" type="text" class="form-control input-sm" name="map_address" value="{{ old('map_address', $mapAddress) }}">
            </div>
            <input id="js-map-submit" class="btn map-btn" type="button" value="{{ trans('button.map_search') }}">
        </div>
        <br />
        <div id="js-map" class="google-map"></div>
        <!-- google map general END -->
        <br />

        <div class="marker-info">
            <label class="control-label">
                {{ trans('label.map_marker') }}
                <span class="icon-need">{{ trans('label.require') }}</span>
            </label>
            <label class="control-label">
                {{ trans('label.map_latitude') }}
            </label>
            {{ Form::text('map_latitude', old('map_latitude', $mapLatitude), array('class' => 'form-control input-sm', 'id' => 'js-map-latitude')) }}

            <label class="control-label">
                {{ trans('label.map_longitude') }}
            </label>
            {{ Form::text('map_longitude', old('map_longitude', $mapLongitude), array('class' => 'form-control input-sm', 'id' => 'js-map-longitude')) }}
            <label class="control-label">
                {{ trans('label.map_zoom') }}
            </label>
            {{ Form::text('map_zoom', old('map_zoom', $mapZoom), array('class' => 'form-control input-sm zoom', 'id' => 'js-map-zoom')) }}
            <input id="js-map-reset" class="btn map-btn m-l-xs" type="button" value="{{ trans('button.reset') }}">
        </div>
    </div>
</div>
