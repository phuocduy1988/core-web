
<!--
* Custom button control generator
* @date: 2019-09-20
* @author: duylbp
* @param
* @return A control view
-->
<!--
    Input Param:
    - type:
        submit
        href
    - action
    - name
    - options (array)
-->
@php
    $labelClass = isset($label_class) ? $label_class : 'col-sm-2';
    $controlClass = isset($control_class) ? $control_class : 'col-sm-6';
    $fieldName = '';
    $controlOption = '';

    $btnSaveClass = 'btn-success';
    $btnEditClass = 'btn-primary';
    $btnConfirmClass = 'btn-info';
    $btnUpdateClass = 'btn-primary';
    $btnCancelClass = 'btn-default';
    $btnDeleteClass = 'btn-danger';

    $btnSaveText        = trans('button.save');
    $btnDraftSaveText   = trans('button.draft_save');
    $btnConfirmText     = trans('button.save');
    $btnEditText        = trans('button.edit');
    $btnUpdateText      = trans('button.update');
    $btnCancelText      = trans('button.cancel');
    $btnDeleteText      = trans('button.delete');
    $btnContinueText    = trans('button.continue_create');
@endphp
<div class="control-group form-group">
    <div class="{{ $labelClass }}"></div>
    <div class="{{ $controlClass }}">
        @if(isset($controls) && is_array($controls))
            @foreach($controls as $control)
                @php
                    $type = isset($control['type']) ? $control['type'] : '';
                    $fieldName = isset($control['field_name']) ? $control['field_name'] : '';
                    $options = isset($control['options']) ? $control['options'] : [];
                    $controlOption = array_merge(array('class' => 'form-control input-sm'), $options);
                    $action = isset($control['action']) ? $control['action'] : '';
                @endphp

                @if($type =='submit')
                    @if($action == 'save')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnSaveClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-check' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnSaveText }}
                        </button>
                    @elseif($action == 'draft_save')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnSaveClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-check' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnDraftSaveText }}
                        </button>
                    @elseif($action == 'confirm')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnConfirmClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-location-arrow' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnConfirmText }}
                        </button>
                    @elseif($action == 'edit')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnEditClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-pencil' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnEditText }}
                        </button>
                    @elseif($action == 'update')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnUpdateClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-check-square-o' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnUpdateText }}
                        </button>
                    @elseif($action == 'delete')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnDeleteClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-warning' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnDeleteText }}
                        </button>
                    @elseif($action == 'cancel')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnCancelClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-caret-left' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnCancelText }}
                        </button>
                    @elseif($action == 'continue_create')
                        <button type="submit" action="{{ $action }}" class="btn btn-large {{ $btnSaveClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-share' }}"></i> {{ isset($control['name']) ? $control['name'] : $btnContinueText }}
                        </button>
                    @endif

                @elseif($type == 'href')
                    @if($action == 'save')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnSaveClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-check' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnSaveText }}
                        </a>
                    @elseif($action == 'edit')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnEditClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-pencil' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnEditText }}
                        </a>
                    @elseif($action == 'update')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnUpdateClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-check-square-o' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnUpdateText }}
                        </a>
                    @elseif($action == 'delete')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnDeleteClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-warning' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnDeleteText }}
                        </a>
                    @elseif($action == 'cancel')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnCancelClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-caret-left' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnCancelText }}
                        </a>
                    @elseif($action == 'continue_create')
                        <a href="{{ isset($control['url']) ? $control['url'] : '#' }}" class="btn btn-large {{ $btnSaveClass }}">
                            <i class="fa {{ isset($control['icon']) ? $control['icon'] : 'fa-share' }}"></i>
                            {{ isset($control['name']) ? $control['name'] : $btnContinueText }}
                        </a>
                    @endif

                @elseif($type == 'include')
                    @if(strlen($control['path']) > 0)
                        @include($control['path'])
                    @endif

                @elseif($type == 'custom')
                    {!! isset($control['content']) ? $control['content'] : '' !!}

                @endif
            @endforeach
        @endif
    </div>
</div>
