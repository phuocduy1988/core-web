<div class="page-size">
	<label class="control-label">{{ trans('pagination.show_page_size') }}</label>
	<select class="input-sm" size="1" name="grid_page_size" onchange='changePageSize()'>
		<?php
			$pageSize = 25;

			$array = array(10, $pageSize, 50, 100);
			$grid_page_size = Request::get('grid_page_size', $pageSize);

			foreach($array as $item)
			{
				echo "pagesize $grid_page_size $item";
				echo "<option value='$item' ". (($grid_page_size == $item) ? " selected "  : ""). ">$item</option>";
			}
		?>
	</select>
</div>
