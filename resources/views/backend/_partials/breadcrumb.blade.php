<div class="row wrapper border-bottom white-bg page-heading sp-breadcrumb">
    <div class="col-lg-8">
        <ol class="breadcrumb">
            @foreach($page_list as $key=>$value)
                <li>
                    @if($key+1<sizeof($page_list))
                        <a href="{{ $value['link'] }}">{{ $value['title'] }}</a>
                    @else
                        <a href="{{ $value['link'] }}"><strong>{{ $value['title'] }}</strong></a>
                    @endif

                </li>
            @endforeach
        </ol>
    </div>
</div>
