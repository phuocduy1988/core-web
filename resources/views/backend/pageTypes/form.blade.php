@extends('backend.layouts.app')

@section('title') {{ trans('pageTypes.pageTypes') }} @stop

@section('css')
    <link href="{!! asset('/backend/plugins/color-box/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! asset('/backend/plugins/datetimepicker/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@stop

@section('content')

@include('backend._partials.breadcrumb',
    	['page_list' => array(
        	['title' => trans('pageTypes.pageTypes'), 'link' => route('pageType.index')]
    	)])

	<div class="wrapper wrapper-content animated fadeInRight clearfix">
		{!! Form::open(['method' => 'post', 'route' => 'pageType.save', 'files'=>true]) !!}
		{!! Form::hidden('id', $pageType->id) !!}
		<div class="row">
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
				    <div class="ibox-content">
                        @include('backend._partials.notifications')
                        <div class="box-content">
                            <p class="m-b-md"><strong>{{ trans('message.input_data_require') }}</strong></p>
							<fieldset>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('pageTypes.name') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control input-sm" name="name" type="text" value="{{ old('name', isset($pageType->name) ? $pageType->name : '' ) }}">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="text-right control-label col-sm-3 col-md-2">
                                        {{ trans('pageTypes.status') }}
                                        @if(false)
                                            <span class="label label-danger">{{ trans('table.required') }}</span>
                                        @endif
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="checkbox" name="status" value="1" @if(isset($pageType->status)? $pageType->status : 0) checked @endif>
                                    </div>
                                </div>

{{-- --UPDATE_GENCODE_FORM_LINE_DO_NOT_DELETE_OR_CHANGE_THIS-- --}}
                                <hr />
                                <div class="control-group form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 text-center js-page-btn">
                                        <a href="{{ route('pageType.index') }}" class="btn btn-large btn-default">
                                        <i class="fa fa-caret-left"></i>
                                        {{ trans('button.cancel') }}
                                        </a>
                                        <button type="submit" action="confirm" class="btn btn-large btn-info">
                                        <i class="fa fa-location-arrow"></i>
                                            {{ trans('button.save') }}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
					</div>
				</div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
@stop

@section('scripts')
    <!-- Use for insert media from file manager start -->
    <script src="{!! asset('packages/barryvdh/elfinder/js/standalonepopup.js') !!}"></script>
    <script src="{!! asset('backend/plugins/color-box/jquery.colorbox-min.js') !!}"></script>
    <!-- Use for insert media from file manager end -->
    <script src="{!! asset('backend/js/media.js') !!}"></script>
    <!-- Date time picker start -->
    <script src="{!! asset('backend/plugins/datetimepicker/moment-with-locales.js') !!}"></script>
    <script src="{!! asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('backend/js/datetime-validation.js') !!}"></script>
    <!-- Date time picker end -->
    <script type="text/javascript" src="{!! asset('ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('js-editor');
        });
        $('#js-viewdate').datetimepicker({
            format: 'YYYY/MM/DD HH:mm',
            locale: 'ja'
        });
    </script>
@stop
