<style>
	.cms-automatic-slider .bx-wrapper {
		margin: 0 auto;
	}

	.bx-caption {
		display: none;
	}

</style>
@php
	//[module:gallery,id,width]
	// $queryString = '[module:gallery,1,600]';
	$params = isset($queryString) ? $queryString : '';
	$params = \App\Helpers\Helper::getBetweenContent($params, '[module:', ']');
	$params = explode(',', $params);
	$sildeWidth = 0;
	$minSlide = 0;
	$maxSlide = 0;
	$galleryId = 0;
	if($params) {
		$galleryId = isset($params[1]) ? $params[1] : 0;
		$sildeWidth = isset($params[2]) && (int)$params[2] ? $params[2] : 600;
		$minSlide = $maxSlide = isset($params[3]) && (int)$params[3] ? $params[3] : 1;
	}
	$gallery = getGallery($galleryId);
	$photos = [];
	if($gallery && $gallery->photos) {
		$photos = json_decode($gallery->photos);
	}
@endphp
@if($gallery)
	<div class="slider cms-automatic-slider">
		<div class="bxslider-{{ $galleryId }}">
			@foreach($photos as $photo)
				<div class="slider-item">
					<a href="{{ isset($photo->url) ? $photo->url : 'javascript:void(0)' }}">
						<img
							src="{{ $photo->image }}"
							alt="{{ $photo->title }}"
							title="{{ isset($photo->description) ? nl2br($photo->description) : '' }}"
						/>
					</a>
				</div>
			@endforeach
		</div>
		@if($maxSlide <= 1)
			<div id="bx-pager" class="text-center">
				@foreach($photos as $key=>$photo)
					<a href="javascript:void(0)"
					   class="slider-thumb" data-slide-index="{{ $key }}">
						<div class="abound">
							<img src="{{ $photo->image }}"
							     alt="{{ $photo->title }}" />
						</div>
					</a>
				@endforeach
			</div>
		@endif
	</div>
	<script type="text/javascript">
		$(document).ready(function () {
			$basicSlider = $('.bxslider-{{ $galleryId }}').bxSlider({
				mode: 'horizontal',
				minSlides: {{ $minSlide }},
				maxSlides: {{ $maxSlide }},
				touchEnabled: true,
				controls: true,
				captions: true,
				hyperlinks: true,
				autoControls: false,
				slideWidth: {{ $sildeWidth }},
				pagerCustom: '#bx-pager'
			});
		});
		$(".slider-item").mouseenter(function () {
			$(this).find(".bx-caption").fadeIn();
		})
		$(".slider-item").click(function () {
			$(this).find(".bx-caption").fadeIn();
		})
		$(".slider-item").mouseleave(function () {
			$(this).find(".bx-caption").fadeOut();
		});
	</script>
@endif
