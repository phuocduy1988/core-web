<style>
	.ega-program {
		display: flex;
	}

	.ega-program-item {
		position: relative;
		width: 50%;
	}

	.ega-program-item .ega-program-item-image {
		display: block;
		width: 100%;
		height: auto;
	}

	.ega-program-item .ega-program-item-overlay {
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background: rgba(133, 18, 12, 0.70);
		color: #fff;
		visibility: hidden;
		opacity: 0;
		padding: 10px;
		/* transition effect. not necessary */
		transition: opacity .5s, visibility .5s;
	}

	.ega-program-item:hover .ega-program-item-overlay {
		opacity: 1;
		visibility: visible;
	}

	.ega-program-item:active .ega-program-item-overlay {
		opacity: 1;
		visibility: visible;
	}

	.ega-program-item:focus .ega-program-item-overlay {
		opacity: 1;
		visibility: visible;
	}

	@media screen and (max-width: 776px) {
		.ega-program {
			flex-wrap: wrap;
			justify-content: space-between;
		}
		.ega-program-item {
			width: 50%;
		}
	}
</style>
@php
	//[module:gallery,id,width]
	// $queryString = '[module:gallery,1,600]';
	$params = isset($queryString) ? $queryString : '';
	$params = \App\Helpers\Helper::getBetweenContent($params, '[module:', ']');
	$params = explode(',', $params);
	$sildeWidth = 0;
	$minSlide = 0;
	$maxSlide = 0;
	$galleryId = 0;
	if($params) {
		$galleryId = isset($params[1]) ? $params[1] : 0;
		$sildeWidth = isset($params[2]) && (int)$params[2] ? $params[2] : 600;
		$minSlide = $maxSlide = isset($params[3]) && (int)$params[3] ? $params[3] : 1;
	}
	$gallery = getGallery($galleryId);
	$photos = [];
	if($gallery && $gallery->photos) {
		$photos = json_decode($gallery->photos);
	}
@endphp
@if($gallery)
	<div class="ega-program">
		@foreach($photos as $photo)
			<div class="ega-program-item">
				<a href="{{ isset($photo->url) ? $photo->url : 'javascript:void(0)' }}">
					<img class="ega-program-item-image"
						src="{{ $photo->image }}"
						alt="{{ $photo->title }}"
					/>
					<div class="ega-program-item-overlay">
						{{ isset($photo->description) ? nl2br($photo->description) : '' }}
					</div>
				</a>
			</div>
		@endforeach
	</div>
	<script type="text/javascript">
		$(document).ready(function () {

		});
	</script>
@endif
