
<div class="wrap-contact-box clearfix">
    <div id="recruit" class="contact-box">
        {!! trans('fePages.call_action_message')  !!}
        <div class="contact">
            <a class="item" rel="nofollow" href="tel:{{ getSetting('phone') }}" target="_top"><i class="fa fa-phone"></i>{{ getSetting('phone') }}</a>
            <a class="item" rel="nofollow" href="mailto:{{ getSetting('email') }}?subject=[{{ getSetting('company_name') }}] Liên hệ hỗ trợ"><i class="fa fa-envelope"></i>{{ getSetting('email') }}</a>
        </div>
        <div class="contact modify">
            <a class="item btn btn-default" href="javascript:void(0)" @click.prevent="showAppyForm">
                <i class="fa fa-comments"></i>{{ trans('fePages.consulting') }}
            </a>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="popupAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-success alert-dismissible" v-html="this.message">
                        </div>
                        <div class="text-right"><button type="button" class="btn btn-secondary" data-dismiss="modal"><strong>✕</strong></button></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="popupApply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="text-left red-color">{{ trans('Hãy nhập thông tin bên dưới') }}</div>
                    </div>
                    <div class="modal-body text-left">
                        @include('frontend._partials.vue-notifications')
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 control-label text-right">
                                {{ trans('members.name') }}<span class="red-color">※</span></label>
                            <div class="col-xs-8 col-sm-9">
                                <input type="text" v-model="form.name" class="form-control" placeholder="{{ trans('members.name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 control-label text-right">
                                {{ trans('members.email') }}<span class="red-color">※</span></label>
                            <div class="col-xs-8 col-sm-9">
                                <input type="email" v-model="form.email" class="form-control" placeholder="{{ trans('members.email') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 control-label text-right">
                                {{ trans('members.phone') }}<span class="red-color">※</span></label>
                            <div class="col-xs-8 col-sm-9">
                                <input type="text" v-model="form.phone" class="form-control" placeholder="{{ trans('members.phone') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 control-label text-right">
                                {{ trans('fePages.message') }}<span class="red-color">※</span></label>
                            <div class="col-xs-8 col-sm-9">
                                <textarea rows="5" v-model="form.message" class="form-control" placeholder="{{ trans('fePages.message') }}"></textarea>
                            </div>
                        </div>
                        <div class="text-right">
                            <button @click.prevent="submitForm()" type="button" class="btn btn-primary">{{ trans('fePages.consulting') }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                {{ trans('button.cancel') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('frontend/js/vue.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/axios.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>

<script>
    $(document).ready(function(){
        var recruit = new Vue({
            el: '#recruit',
            data:{
                message: '',
                error: false,
                errors: {},
                errorMessage: '',
                isSuccess: false,
                isError: false,
                isRequest: false,
                showModal: false,
                isMobile: {!! isMobile() !!},
                user: {!! json_encode(feAuth()->user()) !!},
                lang: '{!! locale() !!}',
                form:{
                    job_id: {!! isset($clientJob->id) ? $clientJob->id : 0 !!},
                    name: '',
                    phone: '',
                    email: '',
                    message: '',
                },
            },
            mounted: function() {
                $('[data-toggle="popover"]').popover({
                });
                $('body').on('click', function (e) {
                    $('[data-toggle="popover"]').each(function () {
                        //the 'is' for buttons that trigger popups
                        //the 'has' for icons within a button that triggers a popup
                        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                            $(this).popover('hide');
                        }
                    });
                });
                if(this.user != null) {
                    this.form.name = this.user.name
                    this.form.email = this.user.email
                    this.form.phone = this.user.phone
                }
            },
            methods:{
                showAppyForm: function() {
                    $('#popupApply').modal('show')
                },
                messageTitleClass: function() {
                    var mtClass = [
                        'table-caption',
                        'caption-top',
                    ]
                    if(this.isError) {
                        mtClass.push('blue-background')
                    }
                    else if(this.isSuccess) {
                        mtClass.push('green-light-box')
                    }
                    else {
                        mtClass.push('bg-info')
                    }
                    return mtClass
                },
                submitForm: function(){
                    $('body').addClass('ot-fr-loading');
                    console.log(this.form)
                    axios.post("{!! route('vn.memberConsultingSave') !!}", this.form)
                        .then((response) => {
                            if(response.data.message){
                                $('#popupAlert').modal('show')
                                $('#popupApply').modal('hide')
                                this.message = response.data.message
                            }
                            console.log(response)
                        })
                        .catch((error) => {
                            this.error = true
                            var errors = error.response.data.errors
                            if(typeof(errors) === 'object') {
                                this.errors = errors
                                this.errorMessage = ''
                            }
                            else {
                                this.errors = {}
                                this.errorMessage = error.response.data.errorMessage
                            }
                        })
                        .then(function () {
                            $('body').removeClass('ot-fr-loading')
                        });
                },
            },
            watch:{
            }
        });
        $('#consulting').on('click', function(e) {
            recruit.showAppyForm();
        });
    });
</script>
