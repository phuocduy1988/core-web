<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>
        OneTech Generate CRUD
    </title>

    @section('css')
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/fonts/icomoon/icomoon.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/vendor/bootstrap-4-5.0.0-alpha.17/build/css/tempusdominus-bootstrap-4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/vendor/font-awesome/css/fontawesome-all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/css/onetech.css') }}" />
    <link rel="stylesheet" href="{{ asset('builder/onetech-unity/css/skins/'.config('onetech-unity.skin', 'blue').'.css') }}" />
    @show
    
    @yield('head-js')
</head>

<body>

    <!-- Loading starts -->
    <div class="loading-wrapper">
        <div class="loading">
            <span></span>
        </div>
    </div>
    <!-- Loading ends -->

    @yield('body')

    @section('js')
        <!-- jQuery first, then Tether, then other JS. -->
        <script src="{{ asset('builder/onetech-unity/js/jquery.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/js/tether.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/unifyMenu/unifyMenu.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/onoffcanvas/onoffcanvas.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/js/moment.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/bootstrap-4-5.0.0-alpha.17/build/js/tempusdominus-bootstrap-4.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/sweetalert-2.1.0/sweetalert.min.js') }}"></script>
        
        <!-- Peity JS -->
        <script src="{{ asset('builder/onetech-unity/vendor/peity/peity.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/peity/custom-peity.js') }}"></script>

        <!-- Circliful js -->
        <script src="{{ asset('builder/onetech-unity/vendor/circliful/circliful.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/circliful/circliful.custom.js') }}"></script>

        <!-- Slimscroll JS -->
        <script src="{{ asset('builder/onetech-unity/vendor/slimscroll/slimscroll.min.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/vendor/slimscroll/custom-scrollbar.js') }}"></script>

        <!-- Common JS -->
        <script src="{{ asset('builder/onetech-unity/js/common.js') }}"></script>
        <script src="{{ asset('builder/onetech-unity/js/onetech.js') }}"></script>

    @show

    @section('post-js')
    @show

    @section('modals')
    @show

</body>
</html>
