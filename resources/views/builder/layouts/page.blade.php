@extends('builder.layouts.master')

@section('css')
    @parent
@stop

@section('body')

    <!-- BEGIN .app-wrap -->
    <div class="app-wrap">
        <!-- BEGIN .app-heading -->


        <!-- END: .app-heading -->
        <!-- BEGIN .app-container -->
        <div class="app-container">

            <!-- BEGIN .app-side -->

            <!-- END: .app-side -->


            <!-- BEGIN .app-main -->
            <div class="app-main">
                <!-- BEGIN .main-heading -->
                <header class="main-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="page-icon">
                                    @section('page_icon')
                                        <i class="icon-laptop_windows"></i>
                                    @show
                                </div>
                                <div class="page-title">
                                    <h5>@yield('page_title')</h5>
                                    <h6 class="sub-heading">@yield('page_subtitle')</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                <div class="right-actions">
                                    <a style="color: white;" href="{{ route('builder.update-file') }}"><i class="fa fa-file-archive"></i> {{ trans('menu.update_file') }}</a> |
                                    <a style="color: white;" href="{{ route('builder') }}"><i class="fa fa-adjust"></i> {{ trans('menu.builder') }}</a> |
                                    <a style="color: white;" href="{{ route('builder.update') }}"><i class="fa fa-edit"></i> {{ trans('menu.builder_update') }}</a> |
                                    <a style="color: white;" href="{{ route('builder.create-project') }}"><i class="fa fa-plus-circle"></i> {{ trans('menu.create_project') }}</a> |
                                    <a style="color: white;" href="{{ route('builder.make-tools') }}"><i class="fa fa-building"></i> {{ trans('menu.make-tools') }}</a> |
                                    <a style="color: white;" href="{{ route('builder.relationships') }}"><i class="fa fa-dove"></i> {{ trans('menu.relationship') }}</a> |
                                    <a style="color: white;" href="{{ route('common.home') }}"><i class="fa fa-backward"></i> {{ trans('menu.back') }}</a>
                                    @yield('page_top_right_actions')
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

            @if(( isset($errors) && $errors->any()) || Session::has('error') || isset($error) || Session::has('message') || isset($message))

                    <div class="col-md-12" style="padding-top: 15px">
                        <div id="messageBar" class="animated fadeInDown">
                            @if(isset($message) || Session::has('message'))
                                <div class="alert custom alert-danger">
                                    <i class="icon-info-large"></i>
                                    {!! isset($message) ? $message : Session::get('message') !!}
                                </div>
                            @endif
                        </div>
                    </div>
            @endif

                <!-- END: .main-heading -->
                <!-- BEGIN .main-content -->
                <div class="main-content">
                    @yield('content')                   
                </div>
                <!-- END: .main-content -->
            </div>
            <!-- END: .app-main -->
        </div>
        <!-- END: .app-container -->
        <!-- BEGIN .main-footer -->
        {{--<footer class="main-footer">--}}
            {{--@section('footer')--}}
            {{--Powered By OneTech--}}
            {{--@stop--}}
        {{--</footer>--}}
        <!-- END: .main-footer -->
    </div>
    <!-- END: .app-wrap -->

    @if(isset($success) || Session::has('success'))
        @section('post-js')
            @parent
            <script>
                $(document).ready(function() {
                    swal({
                        text: '{!! json_encode(isset($success) ? $success : Session::get('success')) !!}',
                        icon: 'success',
                        confirmButtonText: 'OK'
                    });
                });
            </script>
        @stop
    @endif

@stop
