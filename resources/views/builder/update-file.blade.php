@extends('builder.layouts.page')

@section('title') Update File @stop

@section('page_title') Update File @stop
@section('page_subtitle') Overview @stop

@section('content')
<div class="row" id="app">
  <div class="col-md-3">

    <div class="card">
        <div class="card-header text-center" id="routesHeading">
            <a class="el-button el-button--success" href="/builder/postman/api_postman_collection.json">Download API Postman</a>
        </div>
    </div>

    <div id="routesAccordion">
      <div class="card">
        <div class="card-header" id="routesHeading">
          <h5 class="mb-0">
            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseRoutes" aria-expanded="true" aria-controls="collapseRoutes">
              Routes
            </a>
          </h5>
        </div>

        <div id="collapseRoutes" class="collapse show" aria-labelledby="routesHeading" data-parent="#routesAccordion">
          <div class="card-body">
              @foreach ($routes as $dir => $route)
                <ul class="list-group">
                  @foreach ($route as $file)
                    @if(in_array($file['name'], ['api', 'backend']))
                        <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                          <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                        </li>
                    @endif
                  @endforeach
                </ul>
              @endforeach
          </div>
        </div>
      </div>
    </div>

    <div id="modelsAccordion">
      <div class="card">
        <div class="card-header" id="modelsHeading">
          <h5 class="mb-0">
            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseModels" aria-expanded="true" aria-controls="collapseModels">
              Models
            </a>
          </h5>
        </div>

        <div id="collapseModels" class="collapse" aria-labelledby="modelsHeading" data-parent="#modelsAccordion">
          <div class="card-body">
            @foreach ($models as $dir => $model)
              <span class="dir">@if($dir) {{ $dir }} @else App\Models @endif</span>
              <ul class="list-group">
                @foreach ($model as $file)
                  <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                    <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                  </li>
                @endforeach
              </ul>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div id="langAccordion">
          <div class="card">
              <div class="card-header" id="modelsHeading">
                  <h5 class="mb-0">
                      <a class="btn btn-link" data-toggle="collapse" data-target="#collapseLang" aria-expanded="true" aria-controls="collapseLang">
                          Lang
                      </a>
                  </h5>
              </div>

              <div id="collapseLang" class="collapse" aria-labelledby="langHeading" data-parent="#langAccordion">
                  <div class="card-body">
                      @foreach ($langs as $dir => $lang)
                          @if($dir =='jp' )
                              <span class="dir">@if($dir) {{ $dir }} @else resources/lang @endif</span>
                              <ul class="list-group">
                                  @foreach ($lang as $file)
                                      @if(!in_array($file['name'], ['installer', 'console', 'reminders', 'status', 'validation', 'reminders', 'password', 'pagination', 'mailtemplate']))
                                          <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                                              <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                                          </li>
                                      @endif
                                  @endforeach
                              </ul>
                          @endif
                      @endforeach
                  </div>
              </div>
          </div>
      </div>

    <div id="viewsAccordion">
      <div class="card">
        <div class="card-header" id="viewsHeading">
          <h5 class="mb-0">
            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseViews" aria-expanded="true" aria-controls="collapseViews">
              Views
            </a>
          </h5>
        </div>

        <div id="collapseViews" class="collapse" aria-labelledby="viewsHeading" data-parent="#viewsAccordion">
          <div class="card-body">
            @foreach ($views as $dir => $view)
              <span class="dir">@if($dir) {{ $dir }} @else resources/views @endif</span>
              <ul class="list-group">
                @foreach ($view as $file)
                  <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                    <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                  </li>
                @endforeach
              </ul>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div id="controllersAccordion">
      <div class="card">
        <div class="card-header" id="controllersHeading">
          <h5 class="mb-0">
            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseControllers" aria-expanded="true" aria-controls="collapseControllers">
              Controllers
            </a>
          </h5>
        </div>

        <div id="collapseControllers" class="collapse" aria-labelledby="controllersHeading" data-parent="#controllersAccordion">
          <div class="card-body">
            @foreach ($controllers as $dir => $controller)
              <span class="dir">@if($dir) {{ $dir }} @else App\Http\Controllers @endif</span>
              <ul class="list-group">
                @foreach ($controller as $file)
                  <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                    <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                  </li>
                @endforeach
              </ul>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div id="migrationsAccordion">
      <div class="card">
        <div class="card-header" id="migrationsHeading">
          <h5 class="mb-0">
            <a class="btn btn-link" data-toggle="collapse" data-target="#collapseMigrations" aria-expanded="true" aria-controls="collapseMigrations">
              Migrations
            </a>
          </h5>
        </div>

        <div id="collapseMigrations" class="collapse" aria-labelledby="migrationsHeading" data-parent="#migrationsAccordion">
          <div class="card-body">
            @foreach ($migrations as $dir => $migration)
              <span class="dir">@if($dir) {{ $dir }} @else database\migrations @endif</span>
              <ul class="list-group">
                @foreach ($migration as $file)
                  <li :class="fileListItemClass({{ json_encode($file['path']) }})">
                    <a href="#" @click.prevent="loadFile({{ json_encode($file['path']) }}, {{ json_encode($file['lang']) }})">{{ $file['name'] }}.php</a>
                  </li>
                @endforeach
              </ul>
            @endforeach
          </div>
        </div>
      </div>
    </div>

  </div>

  <div class="col-md-9">
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills">

          <li class="nav-item">
            <a class="nav-link disabled" href="#">Change Theme:</a>
          </li>

          <!-- Light -->
          <li class="nav-item">
            <a :class="themeNavLink('kuroir')" href="#" @click.prevent="changeTheme('kuroir')">Kuroir</a>
          </li>
          <li class="nav-item">
            <a :class="themeNavLink('xcode')" href="#" @click.prevent="changeTheme('xcode')">Xcode</a>
          </li>

          <!-- Dark -->
          <li class="nav-item">
            <a :class="themeNavLink('monokai')" href="#" @click.prevent="changeTheme('monokai')">Monokai</a>
          </li>

          <li class="nav-item">
            <a :class="themeNavLink('twilight')" href="#" @click.prevent="changeTheme('twilight')">Twilight</a>
          </li>

          <li class="nav-item">
            <a :class="themeNavLink('tomorrow_night')" href="#" @click.prevent="changeTheme('tomorrow_night')">Tomorrow Night</a>
          </li>

        </ul>
      </div>

    </div>

    <div class="row">
      <div class="col-md-12">
        <div id="editor"></div>
      </div>
    </div>

    <div class="row" v-if="filePath !== ''">
      <div class="col-md-12">
          <br />
          <button class="btn btn-success btn-md" :disabled="saving" @click.prevent='saveChanges'>
            <span v-if="saving">Saving changes...</span>
            <span v-else>Save Changes</span>
          </button>
      </div>
    </div>

  </div>
</div>

@stop


@section('head-js')
    @parent
    <script src="/builder/onetech/vendor/vue/vue.min.js"></script>
    <script src="/builder/onetech/vendor/axios/axios.min.js"></script>
    <script src="/builder/onetech/vendor/element-ui/index.js"></script>
@stop


@section('post-js')
@parent
    <script src="/builder/onetech/vendor/ace/src-min/ace.js" type="text/javascript" charset="utf-8"></script>

    <script>
        var token = document.head.querySelector('meta[name="csrf-token"]');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        var app = new Vue({
            el: '#app',
            data: function () {
              return {
                filePath: '',
                fileContent: '',
                editorTheme: 'kuroir',
                editor: '',
                saving: false,
              }
            },
            computed: {

            },
            mounted: function () {
              this.initEditor()
            },
            methods:{
              themeNavLink: function (theme) {
                return this.editorTheme === theme ? 'nav-link active' : 'nav-link'
              },
              fileListItemClass: function (path) {
                return this.filePath === path ? 'list-group-item active' : 'list-group-item'
              },
              initEditor: function () {
                this.editor = ace.edit('editor')
                this.editor.setTheme('ace/theme/kuroir')
                this.editor.session.setMode('ace/mode/html')
              },
              changeTheme: function (theme) {
                this.editorTheme = theme
                this.editor.setTheme('ace/theme/' + theme)
              },
              loadFile: function (filePath, lang) {
                var self = this
                this.filePath = filePath
                var route = {!! json_encode(route('builder.code-editor.get-file-content')) !!}
                axios.post(route, { path: filePath, lang: lang })
                  .then(function (response) {
                    self.saving = false
                    self.fileContent = response.data.content
                    self.editor.session.setMode('ace/mode/' + lang)
                    self.editor.setValue(self.fileContent)
                      console.log(this.editor)

                  })
                  .catch(function (response) {
                    self.$message.error('Error loading file...')
                    console.log(JSON.stringify(response))
                  })
                // console.log(filePath)
              },
              saveChanges: function () {
                var self = this
                this.saving = true

                // Make sure there are no syntax errors
                var hasError = false
                var annotations = this.editor.getSession().getAnnotations()
                for(var i=0; i < annotations.length; i++) {
                  if(annotations[i]['type'] === 'error') {
                    hasError = true
                  }
                }

                if(hasError) {
                  this.$message.error('You have syntax errors in your code! Please fix them and try again.')
                  self.saving = false
                  return false
                }

                // Save changes
                var route = {!! json_encode(route('builder.code-editor.put-file-content')) !!}
                var content = this.editor.getValue()
                axios.post(route, { path: this.filePath, content: content })
                  .then(function (response) {
                    self.$message.success('File saved!')
                    self.saving = false
                  })
                  .catch(function (response) {
                    console.log(JSON.stringify(response))
                    self.$message.error('Error saving file...')
                    self.saving = false
                  })
              }
            },
            components: {
            }
        });
    </script>
@stop


@section('css')
@parent
<link rel="stylesheet" href="/builder/onetech/vendor/element-ui/index.css">
<style type="text/css">
  #editor { 
      position: relative;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      min-height: 700px;
      min-width: 100%;
  }
  .list-group-item {
    padding: 0.3rem 1.25rem;
  }
  .list-group-item.active a {
    color: #fff;
  }
  .list-group-item a {
    display: block;
    width: 100%;
  }
  .card .dir {
    margin-top: 10px;
      background: rgb(232, 233, 232);
      color: #aaa;
      padding: 5px;
      clear: both;
      position: relative;
      display: block;
      width: 70%;
      border-top-left-radius: 5px;
      border-top-right-radius: 5px;
  }
</style>

@stop