@extends('builder.layouts.page')

@section('title') Builder @stop

@section('page_title') OneTech Builder @stop
@section('page_subtitle') Generate CRUD @stop
@section('page_icon') <i class="icon-power"></i> @stop

@section('css')
@parent
    <link rel="stylesheet" type="text/css" href="/builder/onetech/css/custom.css">
    <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/animate.css/animate.css">
    <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/pretty-checkbox/pretty-checkbox.min.css">
    <link rel="stylesheet" href="/builder/onetech/vendor/element-ui/index.css">
    <style type="text/css">
        span.text-small {
            font-size: 10px;
        }
    </style>
@stop


@section('content')
    <button style="display: none;" id="trigger-overlay" type="button">Open Overlay</button>
    <div id="app">
        <div class="overlay overlay-contentscale">
            <button v-show="true" type="button" class="overlay-close" @click="resetProgress">Close</button>
            <nav>
                <ul id="progress">
                    <li v-if="error === false" style="display: none;" class="animated">Building Model</li>
                    <li v-if="error === false" style="display: none;" class="animated">Generating Controllers</li>
                    <li v-if="error === false" style="display: none;" class="animated">Generating Views</li>
                    <li v-if="error === false" style="display: none;" class="animated">Optimizing Files</li>
                    <li v-if="success === true" class="animated"><i class="fa fa-fw fa-check-circle"></i>Done!</li>
                    <li v-if="success === true" class="animated"><i class="fa fa-fw fa-refresh"></i>Reloading in 3 seconds...</li>
                    <li v-if="error === true" class="animated no-background">
                        <ul>
                            <li v-if="error === true"><code>Error!</code></li>
                            <li v-for="err in errors" class="error" v-if="err[0]" v-html="err[0]"></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                <form @submit.prevent="postForm" method="POST" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="model">Model</label><br>
                                <el-select class="" v-model="model" filterable placeholder="Select" @change="loadModel">
                                    <el-option
                                            v-for="model in models"
                                            :key="model"
                                            :label="model"
                                            :value="model">
                                    </el-option>
                                </el-select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Generate API</label><br />
                                    <el-checkbox v-model="api_code" :disabled="true" label="API code" border></el-checkbox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <h5>Table contents</h5>
                    <hr />
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="table-header-color">
                                <tr class="d-flex">
                                    <td class="col-2">&nbsp;</td>
                                    <td class="col-2">Data Type</td>
                                    <td class="col-2">Field Name</td>
                                    <td class="col-4">Options</td>
                                    <td class="col-2">More Options</td>

                                </tr>
                            </thead>

                            <tbody>
                                <tr
                                    :add="addInput"
                                    :remove="removeInput"
                                    is="migration"
                                    v-for="migration in migrations" 
                                    :id="migration.id"
                                    :migration="migration"
                                ></tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="panel-footer">
                        <el-button type="success" v-if="model != ''" icon="el-icon-check" :loading="submitted" @click.prevent="postForm">Build</el-button>
                        <el-button type="warning" icon="el-icon-check" :loading="submitted" @click.prevent="postFormAndClose">Build & Close</el-button>
                    </div>

                </form>
            </div>
        </div>
        <small class="float-right">OT v1.0.0</small>
    </div>
    <template id="migration">
        <tr class="d-flex">
            <td  class="col-2">
                <el-button type="default" @click.prevent="" icon="el-icon-remove" v-if="id === 1"></el-button>
                <el-button type="danger" @click.prevent="remove(id)" icon="el-icon-remove" v-else></el-button>
                <el-button type="success" @click.prevent="add()" icon="el-icon-circle-plus"></el-button>
            </td>
            <td class="col-2">
                  <el-select v-model="migration.data_type" filterable placeholder="Select" @change="dataTypeChange">
                    <el-option
                      v-for="type in dataTypes"
                      :key="type"
                      :label="type"
                      :value="type">
                    </el-option>
                  </el-select>
            </td>
            <td  class="col-2">
                <input type="text" v-model="migration.field_name" placeholder="Field name" class="form-control">
                <hr>
                <span class="text-small">Translation</span>
                <input type="text" v-model="migration.trans_field_name" placeholder="Trans field name" class="form-control">
                <hr>
                <span class="text-small">Default value</span>
                <input type="text" :disabled="!migration.show_index" v-model="migration.default" class="form-control">
            </td>
            <td  class="col-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be optional if ticked." placement="top">
                                    <span class="text-small">Null</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be shown in the form if ticked." placement="top">
                                    <span class="text-small">In Form</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be indexed for faster searching if ticked." placement="top">
                                    <span class="text-small">Index</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be shown in index if ticked." placement="top">
                                    <span class="text-small">Show In Index</span>
                                </el-tooltip>
                            </th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <el-checkbox v-model="migration.nullable" :disabled="migration.data_type === 'boolean'"></el-checkbox>
                        </td>

                        <td>
                            <el-checkbox :disabled="!migration.nullable" v-model="migration.in_form"></el-checkbox>
                        </td>

                        <td>
                            <el-checkbox v-model="migration.index"></el-checkbox>
                        </td>

                        <td>
                            <el-checkbox v-model="migration.show_index"></el-checkbox>
                        </td>
                    </tr>

                    <thead>
                        <tr>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be searchable if ticked." placement="top">
                                    <span class="text-small">Can Search</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will has to be unique when creating new item if ticked." placement="top">
                                    <span class="text-small">Unique</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="This field will be treated as file and system will generate and uploader for it in the form if ticked." placement="top">
                                    <span class="text-small">Is File</span>
                                </el-tooltip>
                            </th>
                            <th>
                                <el-tooltip class="item" effect="dark" content="Type of file to be uploaded." placement="top">
                                    <span class="text-small">File Type</span>
                                </el-tooltip>
                            </th>
                        </tr>
                    </thead>

                    <tr>
                        <td>
                            <el-checkbox :disabled="!migration.show_index" v-model="migration.can_search"></el-checkbox>
                        </td>

                        <td>
                            <el-checkbox v-model="migration.unique"></el-checkbox>
                        </td>

                        <td>
                            <el-checkbox v-model="migration.is_file"></el-checkbox>
                        </td>

                        <td>
                            <select :disabled="!migration.is_file" v-model="migration.file_type" class="form-control">
                                <option value="image">Image</option>
                                {{--<option value="file">File</option>--}}
                            </select>
                        </td>                 
                    </tr>
                    {{--<tr v-if="migration.in_form">--}}
                        {{--<td colspan="4" style="text-align: center;">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                </table>
            </td>
            <td class="col-2">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <span class="text-small">Is sort</span>
                            <el-checkbox :disabled="!migration.show_index" v-model="migration.is_sort"></el-checkbox>
                        </td>
                        <td colspan="2">
                            <span class="text-small">Is email</span>
                            <el-checkbox :disabled="(migration.data_type != 'string')" v-model="migration.email"></el-checkbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span class="text-small">col-sm:</span>
                            <el-slider :disabled="migration.data_type == 'longText'"
                                    v-model="migration.col_sm"
                                    :min="1"
                                    :max="10"
                                    :step="1"
                                    show-stops>
                            </el-slider>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </template>

@stop

@section('head-js')
    @parent
    <script src="/builder/onetech/vendor/modernizr/modernizr.custom.js"></script>
    <script src="/builder/onetech/vendor/vue/vue.min.js"></script>
    <script src="/builder/onetech/vendor/axios/axios.min.js"></script>
    <script src="/builder/onetech/vendor/element-ui/index.js"></script>
@stop

@section('post-js')
@parent
    <script src="/builder/onetech/vendor/classie/classie.js"></script>
    <script src="/builder/onetech/vendor/pluralize/pluralize.js"></script>
    <script>
        Vue.config.devtools = true;
        var token = document.head.querySelector('meta[name="csrf-token"]');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        Vue.component('migration', {
            template: '#migration',
            props: ['id', 'migration', 'add', 'remove'],
            data: function () {
                return {
                    dataTypes: {!! json_encode($dataTypes) !!},
                }
            },
            mounted: function () {

            },
            methods:{
                dataTypeChange: function () {
                    if(this.migration.data_type === 'boolean') {
                        this.migration.nullable = true
                    }
                    else {
                        if(this.migration.data_type != 'string') {
                            this.migration.email = false
                        }
                        if(this.migration.data_type === 'longText') {
                            this.migration.col_sm = 10
                        }
                        if(this.migration.data_type === 'dateTime') {
                            this.migration.col_sm = 3
                        }
                        this.migration.nullable = false
                    }
                }
            }
        })

        var app = new Vue({
            el: '#app',
            data: {
                relationshipIdentifiers: {!! json_encode($relationshipIdentifiers) !!},
                models: {!! json_encode($models) !!},
                migrations: [
                ],
                model: '',
                api_code: true,
                submitted: false,
                success: false,
                error: false,
                resultUrl: '',
                errors: {}
            },
            computed: {
            },
            mounted: function () {
            },
            methods:{
                loadModel: function () {
                    if(this.migrations.length <= 0) {
                        this.addMigration()
                    }
                },
                addMigration: function() {
                    var newInputId = 1
                    for (var i = 0; i < this.migrations.length; i++) {
                        newInputId = this.migrations[i].id + 1
                    }
                    this.migrations.push(
                        {
                            id: newInputId, data_type: 'string',
                            field_name: '', trans_field_name: '', nullable: 0, in_form: true,index: false, show_index: true,
                            can_search: false, unique: false, is_file: false, file_type: 'image', default: null, col_sm: 6, is_sort: false, email: false
                        }
                    )
                },
                addInput: function () {
                    this.addMigration()
                },
                removeInput: function (id) {
                   var index = this.migrations.findIndex(function (migration) {
                        return migration.id === id
                   })
                   this.migrations.splice(index, 1)
                },
                setProgress: function () {
                    var self = this
                    $("#progress li").each(function(i) {
                        $(this).delay(1000 * i)
                            .addClass('slideInUp')
                            .fadeIn(3000).delay(1000).fadeIn(function () {
                                if (self.success === true) {
                                    //debug here
                                    // return false
                                    window.location.replace(self.resultUrl)
                                    return true
                                } else {
                                    return false
                                }
                            })
                    })
                },
                resetProgress: function () {
                    // Remove errors
                    $("#progress li.error").each(function(i) {
                        $(this).remove()
                    })
                    $("#progress li").each(function(i) {
                        $(this).removeClass('slideInUp').hide()
                    })
                },
                pascalCase: function (str) {
                    var camel = str.replace(/_\w/g, (m) => m[1].toUpperCase())
                    return camel.charAt(0).toUpperCase() + camel.slice(1)
                },
                optimizeModelName: function () {
                    if (this.model) {
                        // Make it pascal case
                        this.model = this.pascalCase(this.model)
                        this.model = this.model.replace(/\s/g, '')
                        // Pluralize
                        this.model = pluralize(this.model, 1)
                    }
                },
                postForm: function () {
                    var self = this
                    $('#trigger-overlay').trigger('click')
                    self.setProgress()
                    self.submitted = true
                    self.error = false
                    self.success = false
                    self.errors = {}

                    var route = {!! json_encode(route('builder.update.post')) !!}
                    var postData = {
                            migration: this.migrations,
                            model: this.model,
                            trans_model: this.trans_model,
                            api_code: this.api_code
                        }
                    axios.post(route, postData)
                        .then(function (response) {
                            console.log(response);
                            self.submitted = false
                            self.resultUrl = '/be/builder/update'
                            self.setProgress()
                            self.success = true
                        })
                        .catch(function (error) {
                            self.setProgress()
                            console.log(JSON.stringify(error.response.data))
                            self.error = true
                            self.success = false
                            self.errors = error.response.status == 422 ? error.response.data.errors : {'message': error.response.message}
                            self.submitted = false
                        });
                },
                postFormAndClose: function () {
                    var self = this
                    $('#trigger-overlay').trigger('click')
                    self.setProgress()
                    self.submitted = true
                    self.error = false
                    self.success = false
                    self.errors = {}

                    var route = {!! json_encode(route('builder.update.post')) !!}
                    var postData = {
                            migration: this.migrations,
                            model: this.model,
                            trans_model: this.trans_model,
                            api_code: this.api_code
                        }
                    axios.post(route, postData)
                        .then(function (response) {
                            console.log(response);
                            self.submitted = false
                            self.resultUrl = response.data.route
                            self.setProgress()
                            self.success = true
                        })
                        .catch(function (error) {
                            self.setProgress()
                            console.log(JSON.stringify(error.response.data))
                            self.error = true
                            self.success = false
                            self.errors = error.response.status == 422 ? error.response.data.errors : {'message': error.response.message}
                            self.submitted = false
                        });
                },
            }
        });
    </script>
    <script src="/builder/onetech/js/overlay.js"></script>
@stop