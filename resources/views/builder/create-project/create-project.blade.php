<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create Project</title>
    <link rel="icon" type="image/png" href="/installer/img/favicon/favicon-16x16.png" sizes="16x16"/>
    <link rel="icon" type="image/png" href="/installer/img/favicon/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/installer/img/favicon/favicon-96x96.png" sizes="96x96"/>
    <link href="/installer/css/style.min.css" rel="stylesheet"/>

</head>
<body>
<div class="master">
    <div class="box">
        <div class="header">
            <h1 class="header__title">    <i class="fa fa-magic fa-fw" aria-hidden="true"></i>
                Create Project
            </h1>
        </div>
        <div class="main">
            <div class="tabs tabs-full">
                @if(session()->has('errors'))
                    <div class="alert alert-danger" id="error_alert">
                        <button type="button" class="close" id="close_alert" data-dismiss="alert" aria-hidden="true">
                            <i class="fa fa-close" aria-hidden="true"></i>
                        </button>
                        <h4>
                            <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                            {{ trans('installer.forms.errorTitle') }}
                        </h4>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ route('builder.create-project') }}" class="tabs-wrap">
                    <div class="" id="tab1content">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="app_name">
                                Project Name
                            </label>
                            <input type="text" name="project_name" id="project_name" value="{{ old('project_name') }}" placeholder="Name alphabet" required />
                        </div>

                        <div class="form-group">
                            <label for="app_url">
                                Subdomain
                            </label>
                            <input type="text" name="sub_domain" class="col-md-3 col-xs-3 col-lg-3" id="sub_domain" value="{{ old('sub_domain') }}" placeholder="sub_domain" required />
                            .tek1.top
                        </div>

                        <div class="buttons">
                            <button class="button" type="submit">
                                Create project
                                <i class="fa fa-angle-right fa-fw" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

</body>
</html>
