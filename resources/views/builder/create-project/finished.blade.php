@extends('builder.installer.layouts.master')

@section('template_title')
    {{ trans('installer.final.templateTitle') }}
@endsection

@section('title')
    <i class="fa fa-flag-checkered fa-fw" aria-hidden="true"></i>
    {{ trans('installer.final.title') }}
@endsection

@section('container')



@endsection

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Create Project</title>
	<link rel="icon" type="image/png" href="/installer/img/favicon/favicon-16x16.png" sizes="16x16"/>
	<link rel="icon" type="image/png" href="/installer/img/favicon/favicon-32x32.png" sizes="32x32"/>
	<link rel="icon" type="image/png" href="/installer/img/favicon/favicon-96x96.png" sizes="96x96"/>
	<link href="/installer/css/style.min.css" rel="stylesheet"/>

</head>
<body>
<div class="master">
	<div class="box">
		<div class="header">
			<h1 class="header__title">    <i class="fa fa-magic fa-fw" aria-hidden="true"></i>
				Create Project
			</h1>
		</div>
		<div class="main">
			<div class="tabs tabs-full">
					<div class="" id="tab1content">
                        <p><strong><small>Creation log entry</small></strong></p>
                        <pre style="padding: 10px;"><code>{{ $outLog }}</code></pre>
                        <br />
						<p><strong><small>Use url bellow to continue:</small></strong></p>
						<a href="http://{{ $url }}" target="_blank"><pre style="padding: 10px;"><code>http://{{ $url }}</code></pre></a>
						<div class="buttons">
							<a href="{{ url('/be/home') }}" class="button" target="_blank">{{ trans('installer.final.exit') }}</a>
						</div>
					</div>

			</div>
		</div>
	</div>
</div>

</body>
</html>

