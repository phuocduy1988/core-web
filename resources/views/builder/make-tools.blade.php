@extends('builder.layouts.page')

@section('title') Make Tools @stop

@section('page_title') OneTech Make Tools @stop
@section('page_subtitle') Run Laravel Commands @stop
@section('page_icon') <i class="icon-center_focus_strong"></i> @stop

@section('css')
@parent
    <link rel="stylesheet" type="text/css" href="/builder/onetech/css/custom.css">
    <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/animate.css/animate.css">
    <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/pretty-checkbox/pretty-checkbox.min.css">
    <style type="text/css">
      .btn-active {
        color: #fff !important;
      }
      .terminal-window {
        height: 550px;
        width: 100%;
        margin-top: 10px;
      }
      button {
        margin-bottom: 5px;
      }
      .btn {
        font-size: 0.8rem !important;
      }
      .cockpit {
        background: #E8E9E8;
        padding: 20px;
      }
    </style>
@stop

@section('content')
    
    <div id="app">

        <div class="card">

            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        
                        
                        <form @submit.prevent="postForm" method="POST" class="cockpit">

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="padding: 20px 0px;">GIT</h5>
                                    </div>
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('fetch')" @click.prevent="runGit('fetch')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Fetch--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('pull')" @click.prevent="runGit('pull')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Pull--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('push')" @click.prevent="runGit('push')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Push--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('commit')" @click.prevent="runGit('commit')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Commit--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('add')" @click.prevent="runGit('add')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Add--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    <div class="col-md-4">
                                        <button :class="boxClass('log')" @click.prevent="runGit('log')">
                                            <i class="fa fa-magic mr-2"></i>Log
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button :class="boxClass('status')" @click.prevent="runGit('status')">
                                            <i class="fa fa-magic mr-2"></i>Status
                                        </button>
                                    </div>
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('init')" @click.prevent="runGit('init')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Init--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('clone')" @click.prevent="runGit('clone')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Clone--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    <div class="col-md-4">
                                        <button :class="boxClass('checkout')" @click.prevent="runGit('checkout')">
                                            <i class="fa fa-magic mr-2"></i>Checkout
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button :class="boxClass('branch')" @click.prevent="runGit('branch')">
                                            <i class="fa fa-magic mr-2"></i>Branch
                                        </button>
                                    </div>
                                    {{--<div class="col-md-4">--}}
                                        {{--<button :class="boxClass('merge')" @click.prevent="runGit('merge')">--}}
                                            {{--<i class="fa fa-magic mr-2"></i>Merge--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                    <div class="col-md-4">
                                        <button :class="boxClass('reset')" @click.prevent="runGit('reset')">
                                            <i class="fa fa-magic mr-2"></i>Reset
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button :class="boxClass('stash')" @click.prevent="runGit('stash')">
                                            <i class="fa fa-terminal mr-2"></i>Stash
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button :class="boxClass('clean')" @click.prevent="runGit('clean')">
                                            <i class="fa fa-magic mr-2"></i>Clean All
                                        </button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                      <h5 style="padding: 20px 0px;">Make Commands</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <button :class="boxClass('model')" @click.prevent="generateMakeCommand('model')">
                                            <i class="fa fa-cube mr-2"></i>Model
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('view')" @click.prevent="generateMakeCommand('view')">
                                            <i class="fa fa-file-alt mr-2"></i>View
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('controller')" @click.prevent="generateMakeCommand('controller')">
                                            <i class="fa fa-cogs mr-2"></i>Controller
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('migration')" @click.prevent="generateMakeCommand('migration')">
                                            <i class="fa fa-database mr-2"></i>Migration
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('command')" @click.prevent="generateMakeCommand('command')">
                                            <i class="fa fa-terminal mr-2"></i>Command
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('middleware')" @click.prevent="generateMakeCommand('middleware')">
                                            <i class="fa fa-ban mr-2"></i>Middleware
                                        </button>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 20px;">

                                    <div class="col-md-6">
                                        <button :class="boxClass('mail')" @click.prevent="generateMakeCommand('mail')">
                                            <i class="fa fa-envelope mr-2"></i>Mail
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('provider')" @click.prevent="generateMakeCommand('provider')">
                                            <i class="fa fa-tasks mr-2"></i>Provider
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('event')" @click.prevent="generateMakeCommand('event')">
                                            <i class="fa fa-bullhorn mr-2"></i>Event
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('resource')" @click.prevent="generateMakeCommand('resource')">
                                            <i class="fa fa-paperclip mr-2"></i>Resource
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('request')" @click.prevent="generateMakeCommand('request')">
                                            <i class="fa fa-paper-plane mr-2"></i>Request
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button :class="boxClass('seeder')" @click.prevent="generateMakeCommand('seeder')">
                                            <i class="fa fa-info mr-2"></i>Seeder
                                        </button>
                                    </div>

                                </div>

                                <hr />
                                <div class="row">
                                  <div class="col-md-12">
                                    <h5 style="padding: 20px 0px;">System Commands</h5>
                                  </div>

                                  <div class="col-md-6">
                                      <button :class="boxClass('update')" @click.prevent="runCommand('update')">
                                          <i class="fa fa-terminal mr-2"></i>composer update
                                      </button>
                                  </div>
                                  <div class="col-md-6">
                                      <button :class="boxClass('install')" @click.prevent="runCommand('install')">
                                          <i class="fa fa-terminal mr-2"></i>composer install
                                      </button>
                                  </div>
                                  <div class="col-md-12">
                                      <button :class="boxClass('dump-autoload')" @click.prevent="runCommand('dump-autoload')">
                                          <i class="fa fa-terminal mr-2"></i>composer dump-autoload
                                      </button>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <h5 style="padding: 20px 0px;">Artisan Commands</h5>
                                  </div>
                                  <div class="col-md-6">
                                      <button :class="boxClass('migrate')" @click.prevent="artisanCommand('migrate')">
                                          <i class="fa fa-magic mr-2"></i>Migrate
                                      </button>
                                  </div>
                                  <div class="col-md-6">
                                      <button :class="boxClass('migrate:rollback')" @click.prevent="artisanCommand('migrate:rollback')">
                                          <i class="fa fa-magic mr-2"></i>Migrate Rollback
                                      </button>
                                  </div>
                                  <div class="col-md-6">
                                      <button :class="boxClass('clear-compiled')" @click.prevent="artisanCommand('clear-compiled')">
                                          <i class="fa fa-magic mr-2"></i>Clear Compiled
                                      </button>
                                  </div>
                                  <div class="col-md-6">
                                      <button :class="boxClass('cache:clear')" @click.prevent="artisanCommand('cache:clear')">
                                          <i class="fa fa-magic mr-2"></i>Clear Cache
                                      </button>
                                  </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 style="padding: 20px 0px;">Nginx</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <button :class="boxClass('reload')" @click.prevent="runNginx('reload')">
                                            <i class="fa fa-magic mr-2"></i>Reload
                                        </button>
                                    </div>
                                    <div class="col-md-6">
                                        <button :class="boxClass('status')" @click.prevent="runNginx('status')">
                                            <i class="fa fa-magic mr-2"></i>Status
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <div class="panel-footer">
                            </div>

                        </form>


                    </div>

                    <div class="col-md-8">

                        <div class="row">    
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label>Text input</label>
                                        <input type="text" v-model="commandParam" value="" :placeholder="capitalizeFirstLetter(command) + ' Name'" class="form-control" @blur="optimizeName" :disabled="!showCommandPalette">
                                    </div>

                                    <div class="col-md-5">
                                        <label>More Option</label>
                                        <input type="text" v-model="commandOption" value="" placeholder="option" class="form-control" @blur="optimizeName" :disabled="!showCommandPalette">
                                    </div>
                                </div>
                            </div>


                            <hr />
                            <div class="col-md-12">
                                <button class="btn btn-success btn-md" @click.prevent="postForm">
                                    <i v-if="submitted" class="fa fa-circle-notch fa-spin fa-fw"></i>
                                    <span>Run</span>
                                </button>
                                <button class="btn btn-info btn-md" @click.prevent="clearConsole">
                                    <span>Clear Console</span>
                                </button>
                            </div>
                        </div>
                        <div class="spacer-20"></div>

                        <div class="terminal-window">
                          <header>
                            <div class="button green"></div>
                            <div class="button yellow"></div>
                            <div class="button red"></div>
                          </header>
                          <section class="terminal">
                            <div class="history">
                                <template v-for="(line, index) in lines" :key="index">
                                    <span v-if="!line.response">$ @{{ line.text }}</span>
                                    <span v-if="working && index === lines.length - 1">
                                      <i class="fa fa-circle-notch fa-spin fa-fw"></i>
                                    </span>
                                    <span :class="line.green === true ? 'green' : 'red'" v-else>@{{ line.text }}</span>
                                </template>
                            </div>
                            $&nbsp;<span class="prompt" v-html="commandText"></span>
                            <span class="typed-cursor">|</span>
                            
                          </section>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('head-js')
    @parent
    <script src="/builder/onetech/vendor/modernizr/modernizr.custom.js"></script>
    <script src="/builder/onetech/vendor/vue/vue.min.js"></script>
    <script src="/builder/onetech/vendor/axios/axios.min.js"></script>
@stop

@section('post-js')
@parent
    <script src="/builder/onetech/vendor/classie/classie.js"></script>
    <script src="/builder/onetech/vendor/pluralize/pluralize.js"></script>
    <script>
        var token = document.head.querySelector('meta[name="csrf-token"]');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        var app = new Vue({
            el: '#app',
            data: {
                command: '',
                type: '',
                commandParam: '',
                commandOption: '',
                working: false,
                softdeletes: false,
                submitted: false,
                success: false,
                error: false,
                lines: [],
                errors: {}
            },
            computed: {
                commandText: function () {
                    var option = this.commandOption !== '' ? ' --' + this.commandOption : ''
                    if (this.type === 'make') {
                      return 'php artisan make:' + this.command + ' ' + this.commandParam + option
                    }
                    if (this.type === 'artisan') {
                      return 'php artisan ' + this.command
                    }
                    if (this.type === 'composer') {
                      return 'composer ' + this.command
                    }
                    if (this.type === 'git') {
                        return 'git ' + this.command + ' ' + this.commandParam + ' ' + this.commandOption
                    }

                    if (this.type === 'nginx') {
                        return 'nginx ' + this.command;
                    }
                },
                showCommandPalette: function () {
                  return (this.command !== '' && this.type === 'make') || this.type === 'git' || this.type === 'composer'
                }
            },
            mounted: function () {
            },
            methods:{
                capitalizeFirstLetter: function (string) {
                    return string.charAt(0).toUpperCase() + string.slice(1)
                },
                boxClass: function (makeName) {
                    var commonPrefix = 'btn btn-outline-primary btn-block'
                    return this.command === makeName ?  commonPrefix + ' btn-active' : commonPrefix
                },
                generateMakeCommand: function (makeName) {
                    this.type = 'make'
                    this.command = makeName
                },
                artisanCommand: function (command) {
                    this.type = 'artisan'
                    this.command = command
                },
                runCommand: function (command) {
                    this.type = 'composer'
                    this.command = command
                },
                runGit: function (command) {
                    this.type = 'git'
                    this.command = command
                    if(command == 'clean') {
                        this.commandParam = '-d -f'
                    } else {
                        this.commandParam = ''
                    }
                    this.commandOption = ''
                },
                runNginx: function (command) {
                    this.type = 'nginx'
                    this.command = command
                },
                pascalCase: function (str) {
                    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                },
                optimizeName: function () {
                    if (this.commandParam) {
                        if (this.type === 'git' || this.command === 'migration' || this.command === 'seeder' || this.command === 'view') {
                            // Make it lower case
                            this.commandParam = this.commandParam.toLowerCase()
                            if(this.type != 'git') {
                                this.commandParam = this.commandParam.replace(/\s/g, '_')
                            }
                        } else {
                            // Make it pascal case
                            this.commandParam = this.pascalCase(this.commandParam)
                            if (this.command === 'model') {
                                // Singularize
                                this.commandParam = pluralize(this.commandParam, 1)
                            }
                            this.commandParam = this.commandParam.replace(/\s/g, '')
                        }
                    }
                },
                addResponse: function (response) {
                    this.lines.push({text: response, green: this.success, response: true })
                },
                clearConsole: function () {
                  this.working = false
                  this.lines = []
                },
                postForm: function () {
                    var self = this
                    self.submitted = true
                    self.error = false
                    self.success = false
                    self.errors = {}
                    self.lines.push({text: self.commandText, green: false, response: false })
                    self.working = true
                    var route = {!! json_encode(route('builder.make-tools.post')) !!}
                    axios.post(route, { command: this.command, type: this.type, param: this.commandParam, option: this.commandOption })
                      .then(function (response) {
                        console.log('message' + response.data.message);
                        console.log(response.data);
                        self.submitted = false
                        self.success = true
                        self.working = false
                        self.addResponse(response.data.message)
                      })
                      .catch(function (error) {
                        console.log(JSON.stringify(error.response.data))
                        self.error = true
                        self.success = false
                        self.submitted = false
                        self.working = false
                        self.addResponse(error.response.data.message)
                      });
                }
            }
        });

    </script>
@stop