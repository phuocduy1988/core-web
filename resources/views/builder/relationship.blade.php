@extends('builder.layouts.page')

@section('title') Relation Ship @stop

@section('page_title') OneTech Relationship Builder @stop
@section('page_subtitle') Connect Models @stop
@section('page_icon') <i class="icon-layers"></i> @stop

@section('css')
  @parent
  <link rel="stylesheet" type="text/css" href="/builder/onetech/css/custom.css">
  <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/animate.css/animate.css">
  <link rel="stylesheet" type="text/css" href="/builder/onetech/vendor/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="/builder/onetech/vendor/element-ui/index.css">
  <style>
      .loading-p {
          position: fixed;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          /* background: #f2f3f9; */
          z-index: 10000;
      }
      .loading-p .loading {
          position: absolute;
          width: 70px;
          height: 70px;
          top: 50%;
          margin-top: -25px;
          text-align: center;
          left: 50%;
          margin-left: -25px;
      }
      .loading-p .loading span {
          display: block;
          width: 70px;
          height: 70px;
          background-color: #E91E63;
          -webkit-animation: loading 1s infinite ease-in-out;
          animation: loading 1s infinite ease-in-out;
      }
  </style>
@stop

@section('content')
    <div class="loading-p" style="display: none;">
        <div class="loading">
            <span></span>
        </div>
    </div>
    <div class="card" id="app">
        <div class="card-body">
          <a href="#" data-toggle="modal" data-target="#newRelation" class="btn btn-success pull-right"><i class="fa fa-code-fork"></i>New Relation</a>

          <div class="row">
            @foreach ($relations as $relation)
                @if(!in_array($relation['model'], ['ApiToken','BaseModel','LogActivity','LogError','Permission','Permissible','Role','Setting', 'User']))
                    <div class="col-md-12">
                      <div class="tree text-center">
                        <ul style="display: inline-block;">
                          <li>
                            <a href="#">{{ $relation['model'] }}</a>
                            <ul>
                              @foreach($relation['data'] as $data)
                                  <li>
                                    <a href="#">{{ $data['type'] }}</a>
                                    <ul>
                                      <li>
                                        <a href="#">{{ $data['model'] }}</a>
                                        <ul>
                                          <li><a href="#">{{ $data['foreign_key'] }}</a></li>
                                          <li><a href="#">{{ $data['local_key'] }}</a></li>
                                        </ul>
                                      </li>
                                    </ul>
                                  </li>
                              @endforeach
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                @endif
            @endforeach
          </div>
        
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="newRelation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Relation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="model">Base Model</label>
                        <el-select class="form-control" v-model="select_model" filterable placeholder="Select">
                            <el-option
                                    v-for="model in models"
                                    :key="model"
                                    :label="model"
                                    :value="model">
                            </el-option>
                        </el-select>
                    </div>

                    <div class="form-group">
                        <label for="relationship">Relationship</label>
                        <el-select class="form-control" v-model="select_relationship" filterable placeholder="Select">
                            <el-option
                                    v-for="relationshipType in relationshipTypes"
                                    :key="relationshipType"
                                    :label="relationshipType"
                                    :value="relationshipType">
                            </el-option>
                        </el-select>
                    </div>

                    <div class="form-group">
                        <label for="with">With</label>
                        <el-select class="form-control" v-model="select_with" filterable placeholder="Select" @change="relationChange">
                            <el-option
                                    v-for="model in models"
                                    :key="model"
                                    :label="model"
                                    :value="model">
                            </el-option>
                        </el-select>
                    </div>

                    <div class="form-group">
                        <label for="with">Column display</label>
                        <el-select :disabled="select_with == ''" class="form-control" v-model="select_column" filterable placeholder="Select">
                            <el-option
                                v-for="column in listColumn"
                                :key="column"
                                :label="column"
                                :value="column"
                            >
                            </el-option>
                        </el-select>
                    </div>
                    <div class="form-group" >
                        <label for="with">Show index</label>
                        <el-checkbox v-model="show_index"></el-checkbox>
                        <label for="with">In form</label>
                        <el-checkbox :disabled="show_index" v-model="in_form"></el-checkbox>
                    </div>

                </div>
                <div class="modal-footer">
                    <el-button class="btn btn-primary" type="success" icon="el-icon-check" :loading="submitted" @click.prevent="postForm">Build Relationship</el-button>
                    <el-button type="button" data-dismiss="modal">Close</el-button>
                </div>
            </div>
        </div>
        </div>
</div>

@stop
@section('head-js')
    @parent
    <script src="/builder/onetech/vendor/modernizr/modernizr.custom.js"></script>
    <script src="/builder/onetech/vendor/vue/vue.min.js"></script>
    <script src="/builder/onetech/vendor/axios/axios.min.js"></script>
    <script src="/builder/onetech/vendor/element-ui/index.js"></script>
    <script src="/builder/onetech/vendor/classie/classie.js"></script>
    <script src="/builder/onetech/vendor/pluralize/pluralize.js"></script>
@stop
@section('post-js')
    @parent
    <script>
        Vue.config.devtools = true;
        var token = document.head.querySelector('meta[name="csrf-token"]');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        var app = new Vue({
            el: '#app',
            data: {
               models: {!! json_encode($models) !!},
               listColumns: {!! json_encode($listColumns) !!},
               listColumn: {!! json_encode([]) !!},
               relationshipTypes: {!! json_encode($relationshipTypes) !!},
               select_model: '',
               select_relationship: 'belongsTo',
               select_with: '',
               select_column: '',
               submitted: false,
               show_index: true,
               in_form: true,
            },

            computed: {
            },
            mounted: function () {
            },
            methods:{
                relationChange: function () {
                    this.listColumn = this.listColumns[this.select_with]
                    this.select_column = ''
                },
                postForm: function () {
                    var route = {!! json_encode(route('builder.relationship.new')) !!}
                    console.log(route);
                    if(this.select_model.length < 1) {
                        alert('select_model can not null')
                        return false
                    }
                    if(this.select_relationship.length < 1) {
                        alert('select_relationship can not null')
                        return false
                    }
                    if(this.select_with.length < 1) {
                        alert('select_with can not null')
                        return false
                    }

                    if(this.select_column.length < 1) {
                        alert('select_column can not null')
                        return false
                    }

                    if(this.select_with === this.select_model) {
                        alert('select_with and select_model must be different')
                        return false
                    }


                    var postData = {
                        select_model: this.select_model,
                        select_relationship: this.select_relationship,
                        select_with: this.select_with,
                        select_column: this.select_column,
                        show_index: this.show_index,
                        in_form: this.in_form,
                    }
                    console.log(postData)
                    $(".loading-p").show()
                    axios.post(route, postData)
                        .then(function (response) {
                            $(".loading-p").hide()
                            console.log(response);
                            if(response.data.message.length > 0 && response.data.code != '201') {
                                alert(response.data.message)
                                return false
                            }

                            window.location.replace(response.data.route)
                            return true
                        })
                        .catch(function (error) {
                            console.log(JSON.stringify(error.response.data))
                        });
                }
            }
        });
    </script>

@stop