<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ getLang() }}">
<head>
	@if(!env('ALLOW_ROBOTS', true))
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	@endif
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<meta http-equiv="Content-type" content="text/html" />
	<title>
		@if(isset($data['title']))
			{{ $data['title'] }}
		@else
			@yield('title', getSetting('title'))
		@endif
	</title>
	<meta name="description" content="{{ isset($data['description']) ? $data['description'] : getSetting('title') }}" />
	<meta name="keywords" content="{{ isset($data['keyword']) ? $data['keyword'] : getSetting('keyword') }}" />
	<meta property="og:site_name" content="{{ isset($data['title']) ? $data['title'] : getSetting('title') }}" />
	<meta property="og:description" content="{{ isset($data['description']) ? $data['description'] : getSetting('description')  }}" />
	<meta property="og:title" content="{{ (isset($data['title'])) ? $data['title'] : getSetting('title') }}" />
	<meta property="og:url" content="{{ (isset($data['openGraphURL'])) ? $data['openGraphURL'] : Request::url()  }}" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="{{ (isset($data['imageUrl'])) ? asset('' . $data['imageUrl']) : asset('frontend/img/lg-main-vertical.png'.'?v='. env('CSS_VERSION', '0.0.0'))  }}" />
	<meta property="og:image:secure_url" content="{{ (isset($data['imageUrl'])) ? asset('' . $data['imageUrl']) : asset('frontend/img/lg-main-vertical.png'.'?v='. env('CSS_VERSION', '0.0.0'))  }}" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="627" />
	<meta property="og:locale" content="{{ getLang() }}" />
	<meta property="fb:app_id" content="228898814278616" />
	<meta name="google-site-verification" content="i05fRe5nt8Um-i7xGNr5XrIFMu9l2WjcGx5th1QImeg" />

	<Link rel="canonical" href="{{ Request::url() }}">

	@yield('schema')
	@if(isset($data['schema']) && strlen($data['schema']) > 20)
		{!! $data['schema'] !!}
	@endif

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff" />
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
	<meta name="theme-color" content="#ffffff" />
	<link href="{!! asset('frontend/css/font-awesome.min.css').'?v='. env('CSS_VERSION', '0.0.0') !!}" rel="stylesheet">
	<link href="{{ asset('frontend/css/bootstrap.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/common.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/default.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/smart-phone.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/common.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/style.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/growup.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/growup-v2.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/jquery.mCustomScrollbar.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/jquery.bxslider.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('frontend/css/client.min.css').'?v='. env('CSS_VERSION', '0.0.0') }}" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="{{ asset('frontend/js/jquery-1.11.3.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/function.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery.mCustomScrollbar.concat.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery.bxslider.min.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/ot-slider.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/ot-water-layout.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/common.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/ot-slide-in.js').'?v='. env('CSS_VERSION', '0.0.0') }}"></script>
	@yield('css')
</head>
<body>
@include('frontend._partials.googleAnalytics')
<div class="navbar-wrapper ot-header-menu t-fixed">
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0px;">
		<div class="container ot-container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">{{ trans('fePages.menu') }}</span>
					<span class="icon-bar ot-hd-mn-icon01"></span>
					<span class="icon-bar ot-hd-mn-icon02"></span>
					<span class="icon-bar ot-hd-mn-icon03"></span>
				</button>
				<div class="ot-login-button-sp">
					@if(feAuth()->check())
						<a href="{{ route('vn.logout') }}">{{ trans('fePages.logout') }}</a>
					@else
						<a href="{{ route('vn.login') }}">{{ trans('fePages.login') }}</a>
					@endif
				</div>
				<div class="navbar-header-left">
					<a class="navbar-brand ot-header-logo" href="/">
						<img data-pagespeed-no-transform="" src="{{ asset('frontend/img/new/lg-main-horizontal.png').'?v='. env('CSS_VERSION', '0.0.0') }}" height="60" class="header-logo-blue" alt="{{ getSetting('title') }}">
					</a>
				</div>
				<div class="navbar-header-right">
					<div class="above-menu">
						<div class="header-contact hidden-xs hotline">
							<div class="hotline-phone">
								<i class="fa fa-volume-control-phone"></i>
								<a class="main-color" href="tel:{{ getSetting('phone') }}" rel="nofollow">{{ getSetting('phone') }}</a>
							</div>
							@if(feAuth()->check())
								{{ ' | '. feAuth()->user()->name .' | '}}
								<a class="" href="{{ route('vn.logout') }}">{{ trans('fePages.logout') }} </a>
							@else
								<a class="login-link" href="{{ route('vn.login') }}" rel="nofollow">{{ trans('fePages.login') }}</a>
								<a class="register-link" href="{{ route('vn.register') }}" rel="nofollow">{{ trans('fePages.registation') }}</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="navbar" class="navbar-collapse collapse ot-header-rmenu hidden-sm hidden-lg hidden-md">
			<div class="container ot-container">
				@include('frontend._partials.menuTop')
				<div class="btn-close-menu hidden-sm hidden-lg hidden-md">
					<div class="profile-info">
						@if(feAuth()->check())
							{{ feAuth()->user()->name . ' | '}}
							<a class="" href="{{ route('vn.logout') }}" rel="nofollow">{{ trans('fePages.logout') }}</a>
						@else
							<a class="profile-link" href="{{ route('vn.register') }}" rel="nofollow">{{ trans('fePages.registation') }}</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</nav>
</div>
<div class="content clients">
	@yield('content')
</div>

<!-- footer by hungnv@onetech.vn START -->
<div class="ot-growup-footer clients small-fontsize">
	<div class="container ot-container ot-footer-top">
		<div class="ot-footer-box ot-table">
			<div>
				<div class="ot-table-cell logo">
					<a href="{{ route('vn.home') }}">
						<img src="{{ asset('frontend/img/lg-main-vertical-white.png').'?v='. env('CSS_VERSION', '0.0.0') }}"
						     class="img-responsive" alt="{{ getSetting('title') }}">
					</a>
				</div>
				<div class="ot-table-cell link">
					@include('frontend._partials.menuFooter')
				</div>
{{--				<div class="ot-table-cell facebook">--}}
{{--					@include('frontend._partials.addThisSocialShare')--}}
{{--					<div class="ot-dmca mt-10">--}}
{{--						<a href="//www.dmca.com/Protection/Status.aspx?ID=410507c6-e5a0-47f3-a641-440d94066d70"--}}
{{--						   title="DMCA.com Protection Status" class="dmca-badge"> <img--}}
{{--								src="https://images.dmca.com/Badges/dmca_protected_sml_120o.png?ID=410507c6-e5a0-47f3-a641-440d94066d70"--}}
{{--								alt="DMCA.com Protection Status" /></a>--}}
{{--						<script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"></script>--}}
{{--					</div>--}}
{{--				</div>--}}
			</div>
			<div class="ot-table content-wrap">
				<div class="ot-table-cell content-item">
					<div class="info">
						<h2 class="contact-box-title">{{trans("fePages.office_at_vietnam")}}</h2>
						<div class="contact-box-complex">
							<div class="contact-mail">
								<i class="fa fa-envelope-o"></i><a class="text-link" rel="nofollow"
								                                   href="mailto:{{ getSetting('email') }}?Subject="
								                                   target="_top">{{ getSetting('email') }}</a>
							</div>
						</div>
						<div class="contact-box-address">
							<i class="fa fa-phone"></i><a class="text-link" rel="nofollow"
							                              href="tel:{{ getSetting('phone') }}">{{ getSetting('phone') }}</a>
						</div>
						<p class="contact-box-address">
							<i class="fa fa-map-marker"></i><a class="text-link"
							                                   rel="nofollow">{{ getSetting('address1') }}</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ot-copyright text-center">Copyright © 2020 {{ getSetting('company_name') }}. All Rights Reserved</div>
</div>
<div class="js-social-sidebar-container social-sidebar-container hidden-xs">
	<div class="sidebar-item">
		<a class="text-link" rel="nofollow" href="{{ route('vn.register') }}">
			<i class="fa fa-pencil-square-o"></i>
			<span class="sidebar-item-text">Đăng ký</span>
		</a>
	</div>
	<div class="sidebar-item">
		<a class="text-link" rel="nofollow" href="tel:{{ getSetting('phone') }}"><i class="fa fa-phone"></i>
			<span class="sidebar-item-text">Tư vấn</span>
		</a>
	</div>
	<div class="sidebar-item">
		<a rel="nofollow" href="{{ route('vn.contactUs') }}">
			<i class="fa fa-envelope-o"></i>
			<span class="sidebar-item-text">Liên hệ</span>
		</a>
	</div>
</div>
@yield('js')
{{--@include('frontend._partials.fbChat')--}}
</body>
</html>
