@extends('frontend.layouts.app')
@section('content')
    @if(isset($data['is_large']) && $data['is_large'] == 1)
        <div class="banner-section">
            <a href="{!! isset($data['banner_url']) ? $data['banner_url'] : '' !!}">
                <img src="{!! isset($data['img_url']) ? $data['img_url'] : '' !!}" alt="{{ isset($data['name_img']) ? $data['name_img'] : '' }}" class="img-responsive">
            </a>
        </div>
    @else
        @if(isset($data['img_url']) && strlen($data['img_url']) > 0)
            <div class="title-banner-large">
                <a href="{!! isset($data['banner_url']) ? $data['banner_url'] : '' !!}">
                    <img src="{!!  $data['img_url'] !!}" alt="{{ isset($data['name_img']) ? $data['name_img'] : '' }}" class="img-banner-center img-responsive">
                </a>
            </div>
        @endif
    @endif
    <div class="container ot-container ot-single-container">
        {!! isset($data['content']) ? $data['content'] : '' !!}
    </div>
@endsection
