@extends('frontend.layouts.app')

@section('content')
    <div class="ot-service-page-content container ot-container ot-single-container">
	    <h2 class="ot-box-title-type-1">
			<span class="text">
				404 Not Found
				<i class="fa fa-eye-slash"></i>
			</span>
	    </h2>
	    <div class="text-center">{{ trans('fePages.404') }}</div>
    </div>
@endsection