<ul class="nav navbar-nav">
    <?php
    $menus = menuTopFE();
    ?>
    @if(!is_null($menus) && count($menus) > 0)
        @foreach($menus as $menu1)
            <?php
            $menu2s = findChildMenus($menus, $menu1->id)
            ?>
            @if($menu1->level == 1)
                @if(count($menu2s) > 0)
                    <li class="dropdown dropdown-li ">
                        <a class="page-scroll dropdown-link" href="{{ otGetUrl($menu1->url) }}" @if($menu1->nofollow) rel="nofollow" @endif>{{ $menu1->name }}</a>
                        <a class="dropdown-toggle dropdown-caret" data-toggle="dropdown"><b class="caret"></b></a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                            @foreach($menu2s as $menu2)
                                <?php
                                $menu3s = findChildMenus($menus, $menu2->id)
                                ?>
                                @if($menu2->level == 2)
                                    @if(count($menu3s) > 0)
                                        <li class="dropdown-submenu">
                                            <a class="page-scroll" href="{{ otGetUrl($menu2->url) }}" @if($menu2->nofollow) rel="nofollow" @endif>{{ $menu2->name }}</a>
                                            <ul class="dropdown-menu" role="sub-menu">
                                                @foreach($menu3s as $menu3)
                                                    <?php
                                                    $menu4s = findChildMenus($menus, $menu3->id)
                                                    ?>
                                                    @if($menu3->level == 3)
                                                        @if(count($menu4s) > 0)
                                                            <li class="dropdown-submenu ">
                                                                <a class="page-scroll" href="{{ otGetUrl($menu3->url) }}" @if($menu3->nofollow) rel="nofollow" @endif>{{ $menu3->name }}</a>
                                                                <ul class="dropdown-menu" role="sub-menu">
                                                                    @foreach($menu4s as $menu4)
                                                                        <?php
                                                                        $menu5s = findChildMenus($menus, $menu4->id)
                                                                        ?>
                                                                        @if($menu4->level == 4)
                                                                            @if(count($menu5s) > 0)
                                                                                <li class="dropdown-submenu ">
                                                                                    <a class="page-scroll" href="{{ otGetUrl($menu4->url) }}" @if($menu4->nofollow) rel="nofollow" @endif>{{ $menu4->name }}</a>
                                                                                    <ul class="dropdown-menu" role="sub-menu">
                                                                                        @foreach($menu5s as $menu5)
                                                                                            @if($menu5->level == 5)
                                                                                                <li class=""><a class="page-scroll" href="{{ otGetUrl($menu5->url) }}" @if($menu5->nofollow) rel="nofollow" @endif>{{ $menu5->name }}</a></li>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    </ul>
                                                                                </li>
                                                                            @else
                                                                                <li class=""><a class="page-scroll" href="{{ otGetUrl($menu4->url) }}" @if($menu4->nofollow) rel="nofollow" @endif>{{ $menu4->name }}</a></li>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </li>
                                                        @else
                                                            <li class=""><a class="page-scroll" href="{{ otGetUrl($menu3->url) }}" @if($menu3->nofollow) rel="nofollow" @endif>{{ $menu3->name }}</a></li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li class=""><a class="page-scroll" href="{{ otGetUrl($menu2->url) }}" @if($menu2->nofollow) rel="nofollow" @endif>{{ $menu2->name }}</a></li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class=""><a class="page-scroll" href="{{ otGetUrl($menu1->url) }}" @if($menu1->nofollow) rel="nofollow" @endif>{{ $menu1->name }}</a></li>
                @endif
            @endif
        @endforeach
    @endif
</ul>
