
<br>
━━━━━━━━━━━━━━━━━━━
<br>
{{ getSetting('company_name') }}
<br>
{{ trans('fePages.support_center') }}
<br>
Email:<a href="mailto:{{ getSetting('email') }}">{{ getSetting('email') }}</a>
<br>
Phone: <a href="tel:{{ getSetting('phone') }}">{{ getSetting('phone') }}</a>
<br>
URL: <a href="https://growupwork.com">https://growupwork.com</a>
<br>
━━━━━━━━━━━━━━━━━━━
