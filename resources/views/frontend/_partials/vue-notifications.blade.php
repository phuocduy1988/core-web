<div v-if="error" :class="{'alert alert-danger validation-error': error}">
        <p v-for="err in errors" v-html="err[0]"></p>
        <p v-html="errorMessage"></p>
</div>
<div v-if="message" :class="{'alert alert-success alert-dismissible': message}">
        <p v-html="message"></p>
</div>
