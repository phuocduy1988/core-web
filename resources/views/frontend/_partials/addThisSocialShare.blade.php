<a href="https://www.facebook.com/growupjv/" target="_blank" role="button" class="ot-social-button facebook-icon" rel="nofollow" style="margin-left: 0;">
	<svg
			xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
			version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook"><title
				id="at-svg-facebook-1">Facebook</title>
		<g>
			<path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z"
			      fill-rule="evenodd"></path>
		</g>
	</svg>
</a>
<a href="https://twitter.com/growupjv" target="_blank" role="button"
   class="ot-social-button twitter-icon" rel="nofollow">
	<svg
			xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
			version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter"><title
				id="at-svg-twitter-2">Twitter</title>
		<g>
			<path
					d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336"
					fill-rule="evenodd"></path>
		</g>
	</svg>
</a>

<a href="https://www.pinterest.com/growupjv/" target="_blank" role="button"
   class="ot-social-button pinterest-icon" rel="nofollow">
	<svg
			xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
			version="1.1" role="img" aria-labelledby="at-svg-pinterest_share-4" class="at-icon at-icon-pinterest_share">
		<title id="at-svg-pinterest_share-4">Pinterest</title>
		<g>
			<path
					d="M7 13.252c0 1.81.772 4.45 2.895 5.045.074.014.178.04.252.04.49 0 .772-1.27.772-1.63 0-.428-1.174-1.34-1.174-3.123 0-3.705 3.028-6.33 6.947-6.33 3.37 0 5.863 1.782 5.863 5.058 0 2.446-1.054 7.035-4.468 7.035-1.232 0-2.286-.83-2.286-2.018 0-1.742 1.307-3.43 1.307-5.225 0-1.092-.67-1.977-1.916-1.977-1.692 0-2.732 1.77-2.732 3.165 0 .774.104 1.63.476 2.336-.683 2.736-2.08 6.814-2.08 9.633 0 .87.135 1.728.224 2.6l.134.137.207-.07c2.494-3.178 2.405-3.8 3.533-7.96.61 1.077 2.182 1.658 3.43 1.658 5.254 0 7.614-4.77 7.614-9.067C26 7.987 21.755 5 17.094 5 12.017 5 7 8.15 7 13.252z"
					fill-rule="evenodd"></path>
		</g>
	</svg>
</a>

<a href="https://www.youtube.com/channel/UCa-HThQecujgvMfovUdLDgg" target="_blank" role="button"
   class="ot-social-button youtube-icon" rel="nofollow">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1"
	     role="img" aria-labelledby="at-svg-youtube-9" class="at-icon at-icon-youtube"><title id="at-svg-youtube-9">
			YouTube</title>
		<g>
			<path d="M13.73 18.974V12.57l5.945 3.212-5.944 3.192zm12.18-9.778c-.837-.908-1.775-.912-2.205-.965C20.625 8 16.007 8 16.007 8c-.01 0-4.628 0-7.708.23-.43.054-1.368.058-2.205.966-.66.692-.875 2.263-.875 2.263S5 13.303 5 15.15v1.728c0 1.845.22 3.69.22 3.69s.215 1.57.875 2.262c.837.908 1.936.88 2.426.975 1.76.175 7.482.23 7.482.15 0 .08 4.624.072 7.703-.16.43-.052 1.368-.057 2.205-.965.66-.69.875-2.262.875-2.262s.22-1.845.22-3.69v-1.73c0-1.844-.22-3.69-.22-3.69s-.215-1.57-.875-2.262z"
			      fill-rule="evenodd"></path>
		</g>
	</svg>
</a>

<a href="https://www.linkedin.com/company/growupwork/" target="_blank" role="button"
   class="ot-social-button linkedin-icon" rel="nofollow" style="margin-right: 0;">
	<svg
			xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32"
			version="1.1" role="img" aria-labelledby="at-svg-linkedin-5" class="at-icon at-icon-linkedin"><title
				id="at-svg-linkedin-5">LinkedIn</title>
		<g>
			<path
					d="M26 25.963h-4.185v-6.55c0-1.56-.027-3.57-2.175-3.57-2.18 0-2.51 1.7-2.51 3.46v6.66h-4.182V12.495h4.012v1.84h.058c.558-1.058 1.924-2.174 3.96-2.174 4.24 0 5.022 2.79 5.022 6.417v7.386zM8.23 10.655a2.426 2.426 0 0 1 0-4.855 2.427 2.427 0 0 1 0 4.855zm-2.098 1.84h4.19v13.468h-4.19V12.495z"
					fill-rule="evenodd"></path>
		</g>
	</svg>
</a>