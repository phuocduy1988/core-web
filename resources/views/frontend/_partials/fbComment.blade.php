<div class="pt-5 pb-5 button-only">
	<div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like"
	     data-size="small" data-show-faces="false" data-share="false"></div>
</div>

<div class="addthis-wrap">
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div class="addthis_inline_share_toolbox"></div>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d8c663904b15e11"></script>
</div>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0"></script>
<div class="fb-comments" data-href="{{ Request::url() }}" data-width="200%" data-numposts="5"></div>