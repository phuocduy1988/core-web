<h2 class="ot-box-title-type-1">
	<span class="text">
		{{ trans('fePages.registation') }}
		<i class="fa fa-user-plus"></i>
	</span>
</h2>
@include('backend._partials.notifications')
<div class="ot-login-box modified">
	<div class="ot-center public-facebook-login-button">
		<a href="{{ route('vn.redirectFacebook') }}?redirect_url={{ URL::previous() }}">
			<img width="70%" alt="Facebook login" src="{{ asset('frontend/img/dang-nhap-facebook.png') }}">
		</a>
	</div>
	<form method="POST" action="{{ route(locale().'.register.post') }}">
		<input type="hidden" name="referer_url" value="{{ URL::previous() }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="reg" value="1">
		<div class="form-group">
			<label for="email">{{ trans('fePages.email_login') }} <span class="red-color">※</span></label>
			<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
		</div>
        <div class="form-group">
            <label for="phone">{{ trans('members.phone') }} <span class="red-color">※</span></label>
            <input type="phone" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" required>
        </div>
		<div class="form-group">
			<label for="email">{{ trans('fePages.name') }} <span class="red-color">※</span></label>
			<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
		</div>
		<div class="form-group">
			<label for="pwd">{{ trans('fePages.password') }} <span class="red-color">※</span></label>
			<input type="password" class="form-control" id="pwd" name="password" value="{{ old('password') }}" required>
		</div>
		<div class="form-group">
			<label for="pwd">{{ trans('fePages.confirm_password') }} <span class="red-color">※</span></label>
			<input type="password" class="form-control" id="pwdcf" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
		</div>
		<div class="text-right">
			<button type="submit" class="btn btn-primary btn-has-icon">
				<i class="fa fa-paper-plane"></i>{{ trans('fePages.registation') }}</button>
		</div>
	</form>
</div>
