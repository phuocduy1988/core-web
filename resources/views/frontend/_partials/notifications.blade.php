@if(!empty($alert) || !empty(Session::get('alert')))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-info"></i> {{ $alert or Session::get('alert') }}
    </div>
@endif

@if(!empty($errorAlert) || !empty(Session::get('errorAlert')))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-info"></i> {{ $errorAlert or Session::get('errorAlert') }}
    </div>
@endif
@if(count($errors) > 0)
  <div class="alert alert-danger validation-error">
    @foreach ($errors->all() as $error)
        {!! $error !!} <br>
    @endforeach
  </div>
@endif

@if(isset($success))
    <div class="alert alert-success">
        {!! $success !!} <br>
    </div>
@endif
<div class="js-validate-error"></div>
