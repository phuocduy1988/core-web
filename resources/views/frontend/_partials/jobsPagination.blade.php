<div class="ot-pagination clearfix">
	<?php
	$firstItem = ($paginator->currentPage() - 1) * $paginator->perPage() + 1;
	$lastItem  = $firstItem + $paginator->perPage() - 1;
	if($paginator->currentPage() == $paginator->lastPage()) {
		$temp = ($paginator->total() - $firstItem);
		$lastItem = $firstItem + $temp;
	}
	?>
	{{--@if($paginator->hasPages())--}}
	<div class="text-center">
		@if($paginator->onFirstPage())
			<strong style="margin-right: 6px;" class="prev"></strong>
		@else
			<strong style="margin-right: 6px;" class="prev active"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"></a></strong>
		@endif

		<!-- Pagination Elements -->
		@foreach($elements as $element)
			<!-- Array Of Links -->
			@if(is_array($element))
				@foreach($element as $page => $url)
					@if($page == $paginator->currentPage())
						<strong class="active"><i class="fa fa-circle"></i></strong>
					@else
						<strong><a href="{{ $url }}"><i class="fa fa-circle"></i></a></strong>
					@endif
				@endforeach
			@endif
		@endforeach

		<!-- Next Page Link -->
		@if($paginator->hasMorePages())
			<strong style="margin-left: 6px;" class="next active"><a href="{{ $paginator->nextPageUrl() }}" rel="next"></a></strong>
		@else
			<strong style="margin-left: 6px;" class="next"></strong>
		@endif
	</div>
	{{--@endif--}}
</div>

