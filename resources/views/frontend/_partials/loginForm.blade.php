<h2 class="ot-box-title-type-1">
	<span class="text">
		{{ trans('fePages.login') }}
		<i class="fa fa-sign-in"></i>
	</span>
</h2>
<div class="ot-login-box modified">
    <div>
        @if (session('register_success'))
            <div class="alert alert-success">
                {{ session('register_success') }}
            </div>
        @endif
        @if (session('register_error'))
            <div class="alert alert-danger">
                {{ session('register_error') }}
            </div>
        @endif
        @if (session('login_error'))
            <div class="alert alert-danger">
                {{ session('login_error') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="public-facebook-login-button text-center inline-block">
            <a href="{{ route('vn.redirectFacebook') }}?redirect_url={{ URL::previous() }}">
                <img width="70%" alt="Facebook login" src="{{ asset('frontend/img/dang-nhap-facebook.png') }}">
            </a>
        </div>
    </div>
    <form method="POST" action="{{ route(locale().'.login.post') }}">
        <input type="hidden" name="referer_url" value="{{ URL::previous() }}">
        <input type="hidden" name="_token" id="token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <label for="email">{{ trans('fePages.email_login') }}</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label for="pwd">{{ trans('fePages.password') }}</label>
            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary btn-has-icon"><i class="fa fa-sign-in"></i>{{ trans('fePages.login') }}</button>
        </div>
    </form>
</div>
