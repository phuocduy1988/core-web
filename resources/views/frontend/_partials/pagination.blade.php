@if(isset($paginator) && $paginator->hasPages())
	<?php
    	$firstItem = ($paginator->currentPage() - 1) * $paginator->perPage() + 1;
    	$lastItem  = $firstItem + $paginator->perPage() - 1;
    	if($paginator->currentPage() == $paginator->lastPage()) {
    		$temp = ($paginator->total() - $firstItem);
    		$lastItem = $firstItem + $temp;
    	}
    ?>
	<ul class="pagination">
		@if($paginator->onFirstPage())
			<li class="disabled">
				<span>&laquo;</span>
			</li>
		@else
			<li>
				<a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a>
			</li>
		@endif
        @foreach($elements as $element)
        <!-- "Three Dots" Separator -->
            @if(is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

        <!-- Array Of Links -->
            @if(is_array($element))
                @foreach($element as $page => $url)
                    @if($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        <!-- Next Page Link -->
            @if($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next"> &raquo;</a>
                </li>
            @else
                <li class="disabled"><span>&raquo;</span></li>
            @endif
	</ul>
@else
	<div class="mb-20">
		<!-- Fixbug not margin bottom when not pagination -->
	</div>
@endif
