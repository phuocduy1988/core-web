<div id="jobLetterFooter">
	<div class="notify-wrap-modify">
		@include('frontend._partials.vue-notifications')
	</div>
	<div class="form-group form-mail">
		<input type="text" v-model="form.email" class="form-control input-sm" value="" placeholder="Email đăng ký">
		<button type="submit" class="btn btn-sm btn-primary" @click.prevent="submitForm()">Send</button>
	</div>
</div>


<script>
    $(document).ready(function(){
        var app = new Vue({
            el: '#jobLetterFooter',
            data:{
                message: '',
                error: false,
                errors: {},
                errorMessage: '',
                isSuccess: false,
                isError: false,
                form: {
                    email: '{{ isset(feAuth()->user()->email) ? feAuth()->user()->email : "" }}'
                },
            },
            mounted: function() {
            },
            methods:{
                submitForm: function(){
					$("body").append("<div class='ot-fr-loading fixed'></div>");
                    axios.post("{!! route('vn.mailSubscribe') !!}", this.form)
                        .then((response) => {
                            if(response.data.message){
                                this.message = response.data.message
                            }
                            this.error = false
                            this.errors = {}
                            this.errorMessage = ''
                            this.isSuccess = true
                        })
                        .catch((error) => {
                            this.error = true
                            this.message = ''
                            var errors = error.response.data.errors
                            if(typeof(errors) === 'object') {
                                this.errors = errors
                                this.errorMessage = error.response.data.message
                            }
                            else {
                                this.errors = {}
                                this.errorMessage = error.response.data.message
                            }
                        })
                        .then(function () {
							$("body .ot-fr-loading").fadeOut(500, function () {
								$(this).remove();
							});
                        });
                }
            },
        });
    });
</script>