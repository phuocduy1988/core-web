<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId            : '228898814278616',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v4.0'
		});
	};
	(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="1090366274471783"
     theme_color="#e70012"
     greeting_dialog_display="hide"
     logged_in_greeting="Xin chào, Bạn có cần hỗ trợ gì không?"
     logged_out_greeting="Xin chào, Bạn có cần hỗ trợ gì không?">
</div>