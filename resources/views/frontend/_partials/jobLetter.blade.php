<div id="jobLetter">
    <div class="ot-newsletter">
	    @include('frontend._partials.vue-notifications')
	    <div class="container ot-container">
	        <div class="box-newsletter">
                <h2>Việc làm</h2>
                <div class="content-mid">Hãy để lại email để <br>nhận tin tuyển dụng mới nhất</div>
                <div class="register">
                    <input type="text" v-model="form.email" class="form-control" value="" placeholder="Nhập email">
                    <button type="submit" class="btn btn-primary" @click.prevent="submitForm()">Đăng ký</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var app = new Vue({
            el: '#jobLetter',
            data:{
                message: '',
                error: false,
                errors: {},
                errorMessage: '',
                isSuccess: false,
                isError: false,
                form: {
                    email: '{{ isset(feAuth()->user()->email) ? feAuth()->user()->email : "" }}'
                },
            },
            mounted: function() {
            },
            methods:{
                submitForm: function(){
					$("body").append("<div class='ot-fr-loading fixed'></div>");
                    axios.post("{!! route('vn.mailSubscribe') !!}", this.form)
                        .then((response) => {
                            if(response.data.message){
                                this.message = response.data.message
                            }
                            this.error = false
                            this.errors = {}
                            this.errorMessage = ''
                            this.isSuccess = true
                        })
                        .catch((error) => {
                            this.error = true
                            this.message = ''
                            var errors = error.response.data.errors
                            if(typeof(errors) === 'object') {
                                this.errors = errors
                                this.errorMessage = error.response.data.message
                            }
                            else {
                                this.errors = {}
                                this.errorMessage = error.response.data.message
                            }
                        })
                        .then(function () {
							$("body .ot-fr-loading").fadeOut(500, function () {
								$(this).remove();
							});
                        });
                }
            },
        });
    });
</script>