<style>

    #subscribeModal .modal-content{
        overflow:hidden;
    }

    #subscribeModal .form-control {
        height: 34px;
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
        padding-left:10px;
        width: 220px;
        margin: 4px;

    }
    #subscribeModal .btn {
        /*border-top-right-radius: 0px;*/
        /*border-bottom-right-radius: 0px;*/
        padding-right:20px;
        background:#ff0000;
        border-color:#ff0000;
    }

    #subscribeModal .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #ff0000;
        outline: 0;
        box-shadow: none;
    }
    #subscribeModal .top-strip{
        height: 155px;
        background: #007b5e;
        transform: rotate(141deg);
        margin-top: -94px;
        margin-right: 190px;
        margin-left: -130px;
        border-bottom: 65px solid #f774a2;
        border-top: 10px solid #f774a2;
    }
    #subscribeModal .bottom-strip{
        height: 155px;
        background: #ff0000;
        transform: rotate(112deg);
        margin-top: -110px;
        margin-right: -215px;
        margin-left: 300px;
        border-bottom: 65px solid #f774a2;
        border-top: 10px solid #f774a2;
    }

    /**************************/
    /****** modal-lg stips *********/
    /**************************/
    #subscribeModal .modal-lg .top-strip {
        height: 155px;
        background: #ff0000;
        transform: rotate(141deg);
        margin-top: -106px;
        margin-right: 457px;
        margin-left: -130px;
        border-bottom: 65px solid #f774a2;
        border-top: 10px solid #f774a2;
    }
    #subscribeModal .modal-lg .bottom-strip {
        height: 155px;
        background: #ff0000;
        transform: rotate(135deg);
        margin-top: -115px;
        margin-right: -339px;
        margin-left: 421px;
        border-bottom: 65px solid #f774a2;
        border-top: 10px solid #f774a2;
    }

    #subscribeModal .mail-box {
     margin: 0 auto;
    }

    /****** extra *******/
    #Reloadpage{
        cursor:pointer;
    }
</style>
<div id="appSubscribeModal">
    <div class="modal fade text-center py-5 subscribeModal-lg"  id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="top-strip"></div>
                    @include('client._partials.vue-notifications')
                    <template v-if="!isSuccess">
                        <p class="h2 red-color" >Đăng ký nhận thông tin thực tập và việc làm</p>
                        {{--<h3 class="pt-5 mb-0 text-secondary"></h3>--}}
                        <p class="pb-1 text-muted"><small><a href="javascript:void(0)" @click.prevent="showLogin">Đăng nhập</a> hoặc điền email vào để nhận</small></p>
                        <div class="input-group mb-3 w-75 mx-auto mail-box">
                            {{--<div class="input-group-append">--}}
                                {{--<input type="name" class="form-control" placeholder="Tên của bạn" v-model="form.name" aria-label="Recipient's username" aria-describedby="button-addon2" required>--}}
                            {{--</div>--}}
                            <div class="input-group-append">
                                <input type="email" class="form-control" placeholder="Địa chỉ email" v-model="form.email" aria-label="Recipient's username" aria-describedby="button-addon2" required>
                            </div>
                            <a class="btn btn-primary" href="javascript:void(0)" @click.prevent="submitForm"><i class="fa fa-paper-plane"></i> Gửi cho tôi</a>
                        </div>
                        <p class="pb-1 text-muted"><small>Email của bạn sẽ được chúng tôi bảo mật và không bị spam</small></p>
                    </template>
                    <template>
                        <p>Khám phá những việc làm HOT tại Nhật đang cần tuyển gấp <br>
                        <a target="_blank" href="https://growup.vn/viec-lam" style="color: red; font-weight: bold; font-size: 20px;">Tại đây➢➢</a></p>
                    </template>
                    <div class="bottom-strip"></div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.members.popupLogin')
</div>
<script>
    $(document).ready(function(){
        var app = new Vue({
            el: '#appSubscribeModal',
            data:{
                message: '',
                error: false,
                errors: {},
                errorMessage: '',
                isSuccess: false,
                isError: false,
                form: {
                    name: '{{ isset(feAuth()->user()->name) ? feAuth()->user()->name : "" }}',
                    email: '{{ isset(feAuth()->user()->email) ? feAuth()->user()->email : "" }}'
                },
            },
            mounted: function() {
                $(window).on('load', function() {
                    var loadMailSub = {{ Request::get('ms', 1) }};
                    var checkSession = {{ \App\Helpers\Helper::getSession('SubEmail'.Request::userAgent()) ? \App\Helpers\Helper::getSession('SubEmail'.Request::userAgent()) : 0 }}
                    if(loadMailSub && checkSession != 1) {
                        // setTimeout(function(){
                        //     $('.subscribeModal-lg').modal('show')
                        // }, 0)
                        setTimeout(function () {
                            $('.subscribeModal-lg').modal('show');
                        }, Math.floor(Math.random() * 100000 + 100000 ));
                    }
                });
                $('#Reloadpage').click(function() {
                    location.reload()
                })
            },
            methods:{
                submitForm: function(){
                    $('body').addClass('ot-fr-loading');
                    console.log(this.form)
                    axios.post("{!! route('vn.mailSubscribe') !!}", this.form)
                    .then((response) => {
                        if(response.data.message){
                            this.message = response.data.message
                        }
                        this.error = false
                        this.errors = {}
                        this.errorMessage = ''
                        this.isSuccess = true
                        console.log(response)
                    })
                    .catch((error) => {
                        this.error = true
                        console.log(error)
                        var errors = error.response.data.errors
                        if(typeof(errors) === 'object') {
                            this.errors = errors
                            this.errorMessage = ''
                        }
                        else {
                            this.errors = {}
                            this.errorMessage = error.response.data.errorMessage
                        }
                    })
                    .then(function () {
                        $('body').removeClass('ot-fr-loading')
                    })
                },
                messageTitleClass: function() {
                    var mtClass = [
                        'table-caption',
                        'caption-top',
                    ]
                    if(this.isError) {
                        mtClass.push('blue-background')
                    }
                    else if(this.isSuccess) {
                        mtClass.push('green-light-box')
                    }
                    else {
                        mtClass.push('bg-info')
                    }
                    return mtClass
                },
                showLogin: function () {
                    $("#button_onetech_popup_login").trigger('click')
                }
            },
        });
    });
</script>