<div class="link-info">
	@php
		$menus = menuFooterFE();
		$chunkMenus = partition($menus->toArray(), 2);
	@endphp
	@foreach($chunkMenus as $index => $chunkItem)
		<div class="footer-d" style="vertical-align: top;">
			@foreach($chunkItem as $menu)
				<a href="{{ otGetUrl($menu['url']) }}" class="link-info-item" @if($menu['nofollow']) rel="nofollow" @endif><i class="fa fa-angle-right"></i>{{ $menu['name'] }}</a>
			@endforeach
		</div>
	@endforeach
</div>
