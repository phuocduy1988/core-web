<?php
// config
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>
@if ($paginator->lastPage() > 1)
<nav class="ot-pagination">
    <div class="nav-inner">
        <div class="ot-nav-links">
            <a class="ot-next ot-number {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $paginator->url(1) }}"><img src="frontend/img/new/icon-prev.png" /></a>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <?php
                    $half_total_links = floor($link_limit / 2);
                    $from = $paginator->currentPage() - $half_total_links;
                    $to = $paginator->currentPage() + $half_total_links;
                    if ($paginator->currentPage() < $half_total_links) {
                    $to += $half_total_links - $paginator->currentPage();
                    }
                    if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                        $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                    }
                ?>
                @if ($from < $i && $i < $to)
                    <a class="ot-number {{ ($paginator->currentPage() == $i) ? ' current' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                @endif
            @endfor
            <a class="ot-prev ot-number {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}" href="{{ $paginator->url($paginator->lastPage()) }}"><img src="frontend/img/new/icon-next.png" /></a>
        </div>
        <div class="ot-page-of">{{ 'Page ' . $paginator->currentPage() . ' of ' . $paginator->lastPage() }}</div>
    </div>
</nav>
@endif