<div id="someone-purchased" class="fomo-notification-animation-vertical fomo-notification-mobile-position-bottom fomo-notification-v2-curved fomo-notification fomo-notification-font-sm fade-out fomo-notification-hide"
     style="display: none;">
    <div class="fomo-notification-container" style="display:inline">
        <div style="display:inline" class="fomo-notification-image-wrapper">
            <img  alt="" class="fomo-notification-image" src="{{ asset('frontend/css/images/love.gif') }}" scale="0" style="cursor: pointer;">
        </div>
        <div style="display:inline" class="fomo-notification-content-wrapper">
            <p class="fomo-notification-content">
                <span id="user-order"></span>

            </p>
            <i id="user-activity-time"></i>

        </div>
        <span id="fomo-close"></span>
    </div>
</div>
<script>
    var users = {!! json_encode($memberNames, true) !!};
    var growUpServices = {!! json_encode($growUpServices, true) !!};
    var growUpTimeServices = {!! json_encode($growUpTimeServices, true) !!};
    function someonePurchasedLoop() {
        setTimeout(function () {
            someonePurchasedTracking();
            someonePurchasedLoop();
        }, Math.floor(Math.random() * 20000 + 20000 ));
    }

    function someonePurchasedTracking(){
        var user = users[Math.floor(Math.random()*users.length)];
        var growUpService = growUpServices[Math.floor(Math.random()*growUpServices.length)];
        var growUpTimeService = growUpTimeServices[Math.floor(Math.random()*growUpTimeServices.length)];
        jQuery("#user-order").text(user + growUpService);
        jQuery("#user-activity-time").text(growUpTimeService);
        jQuery("#someone-purchased").show();

        setTimeout(function () {
            jQuery("#someone-purchased").addClass("fomo-notification-position-bottom-left");
            jQuery("#someone-purchased").removeClass("fomo-notification-position-bottom-left");
            jQuery("#someone-purchased").hide();
        }, 4000);
    }


    jQuery(document).ready(function($) {
        setTimeout(function () {
            someonePurchasedTracking();
        }, Math.floor(Math.random() * 20000));
        someonePurchasedLoop();
    });
</script>