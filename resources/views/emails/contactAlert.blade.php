@extends('emails.master')
@section('body')
    <!-- START CENTERED WHITE CONTAINER -->
    <span class="preheader">Dear All,</span>
    <table class="main">

        <!-- START MAIN CONTENT AREA -->
        <tr>
            <td class="wrapper">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: middle" width="30%">Name</td>
                        <td style="vertical-align: middle">{{ $mailData['name'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Email</td>
                        <td style="vertical-align: middle">{{ $mailData['email'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Phone</td>
                        <td style="vertical-align: middle">{{ $mailData['phone'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Inquiry type</td>
                        <td style="vertical-align: middle">{{ $mailData['inquiry_type'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Company</td>
                        <td style="vertical-align: middle">{{ $mailData['company'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Message</td>
                        <td style="vertical-align: middle">{{ $mailData['message'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- END MAIN CONTENT AREA -->
    </table>
@endsection
