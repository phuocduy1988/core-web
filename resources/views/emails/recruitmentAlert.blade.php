@extends('emails.master')
@section('body')
    <!-- START CENTERED WHITE CONTAINER -->
    <span class="preheader">Dear All,</span>
    <table class="main">

        <!-- START MAIN CONTENT AREA -->
        <tr>
            <td class="wrapper">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: middle" width="30%">Vị trí ứng tuyển</td>
                        <td style="vertical-align: middle">{{ $mailData['job_title'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Fullname</td>
                        <td style="vertical-align: middle">{{ $mailData['fullname'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Email</td>
                        <td style="vertical-align: middle">{{ $mailData['email'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Điện thoại</td>
                        <td style="vertical-align: middle">{{ $mailData['phone'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Địa chỉ</td>
                        <td style="vertical-align: middle">{{ $mailData['address'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Kỹ năng</td>
                        <td style="vertical-align: middle">{{ $mailData['general_skill'] }}</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle" width="30%">Cv file</td>
                        <td style="vertical-align: middle">
                            <a target="_blank" href="{{ request()->getSchemeAndHttpHost() . $mailData['cv_file'] }}">
                                Download
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- END MAIN CONTENT AREA -->
    </table>
@endsection
