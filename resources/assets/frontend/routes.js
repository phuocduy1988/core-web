import App from "./views/App";
import ExaminationList from "./views/ExaminationList";
import ExaminationDetail from "./views/ExaminationDetail";
import ExaminationResult from "./views/ExaminationResult";
import Page404 from "./views/Page404";

export const routes = [
	{
		path: '/thanh-vien/danh-sach-bai-thi',
		component: App,
		children: [
			{
				path: '/',
				name: 'examimation-list',
				component: ExaminationList
			},
			{
				path: '/thanh-vien/danh-sach-bai-thi/bai-thi/:id',
				name: 'examination-detail',
				component: ExaminationDetail
			},
			{
				path: '/thanh-vien/danh-sach-bai-thi/ket-qua/:result_id',
				name: 'examination-result',
				component: ExaminationResult
			},
			{
				path: '/page-not-found',
				name: 'page-not-found',
				component: Page404
			},
			{
				path: '*',
				name: 'page-404',
				component: Page404
			}
		]
	}
];
