import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {routes} from './routes.js';
import StoreData from './store.js';
import Paginate from 'vuejs-paginate';
import VueSlimScroll from 'vue-slimscroll';
import ElementUI from 'element-ui';
import './elementui/index.css';
import locale from 'element-ui/lib/locale/lang/en';
import VueCountdownTimer from 'vuejs-countdown-timer';


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueSlimScroll);
Vue.use(VueCountdownTimer);
Vue.use(ElementUI, {locale});

Vue.component('paginate', Paginate);

// axios
import axios from "./axios.js";
Vue.prototype.$http = axios;

const store = new Vuex.Store(StoreData);

import App from './views/App';

const router = new VueRouter({
	routes,
	mode: 'history'
});

const app = new Vue({
	el: '#app',
	router,
	store,
	components: { App },
});
