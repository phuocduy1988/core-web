export default {
	state: {
		welcomeMessage: 'Welcome to vue app',
		loading: false
	},
	getters: {
		welcome(state) {
			//Call outside
			// return this.$store.getters.welcome
			return state.welcomeMessage;
		},
	},
	mutations: {
	},
	actions: {},
};
