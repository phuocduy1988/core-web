/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

window.Vue = require('vue');

import VueCroppie from 'vue-croppie';
import ElementUI from 'element-ui';
import axios from 'axios';
import 'element-ui/lib/theme-chalk/index.css';
import en from 'element-ui/lib/locale/lang/en';

// import {
//     Select,
//     Tabs,
//     TabPane,
// } from 'element-ui';


Vue.use(VueCroppie);
// Vue.use(Tabs, {en, size: 'medium'});
// Vue.use(TabPane, {en, size: 'medium'});

Vue.use(ElementUI, {en, size: 'medium'});

Object.defineProperty(Vue.prototype, '$axios', {
	get() {
		return axios;
	}
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('cv-tabs-component', require('./components/member-cv/tabs.vue'));
Vue.component('cv-basic-component', require('./components/member-cv/basic-info.vue'));
Vue.component('cv-education-component', require('./components/member-cv/education.vue'));
Vue.component('cv-language-component', require('./components/member-cv/language.vue'));
Vue.component('cv-history-component', require('./components/member-cv/history.vue'));
Vue.component('cv-work-component', require('./components/member-cv/find-work.vue'));
Vue.component('cv-intro-component', require('./components/member-cv/intro.vue'));
// Vue.component('cv-japan-component', require('./components/template-cv/japan-default-cv.vue'));
Vue.component('be-cv-japan-component', require('./components/template-cv/be-japan-default-cv.vue'));
Vue.component('member-cv-component', require('./components/member-cv/member-cv.vue'));
Vue.component('manege-cv-component', require('./components/member-cv/manege-cv.vue'));
Vue.component('change-password-component', require('./components/member-cv/change-password.vue'));
Vue.component('search-job-component', require('./components/clients/search-job.vue'));
Vue.component('job-detail-component', require('./components/clients/job-detail.vue'));
Vue.component('top-job-component', require('./components/clients/top-job.vue'));

Vue.component('manage-member-cv', require('./components/modules-template-cv/manage-member-cv.vue'));
Vue.component('manage-skill-sheet', require('./components/modules-template-cv/manage-skill-sheet.vue'));

Vue.component('cv-croppie-component', require('./components/croppie/croppie.vue'));
Vue.component('select2', require('./components/_partials/select2.vue'));

//hide warning of asnyc validator  - tanmnt
const warn = console.warn;
console.warn = (...args) => {
	if (typeof args[0] === 'string' && args[0].startsWith('async-validator:')) return;
	
	warn(...args);
};
// end - tanmnt

//Duy add isOffAlertJob
const app = new Vue({
	el: '#app',
	data() {
		return {
			isOffAlertJob: false
		}
	}
});