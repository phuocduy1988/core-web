module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            website: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                files: {
                    "public/frontend/css/common.min.css": "public/frontend/css/less/common.less",
                    "public/frontend/css/default.min.css": "public/frontend/css/less/default.less",
                    "public/frontend/css/smart-phone.min.css": "public/frontend/css/less/smart-phone.less",
                    "public/frontend/css/growup.min.css": "public/frontend/css/less/growup.less",
                    "public/frontend/css/growup-v2.min.css": "public/frontend/css/less/growup-v2.less",
                    "public/frontend/css/growup-modify.min.css": "public/frontend/css/less/growup-modify.less",
                    "public/frontend/css/client.min.css": "public/frontend/css/less/client.less",

                    "public/backend/css/common.min.css": "public/backend/css/less/common.less",
                    "public/backend/css/custom.min.css": "public/backend/css/less/custom.less",
                    "public/backend/css/horizontal-custom.min.css": "public/backend/css/less/horizontal-custom.less",


                }
            }
        },
        cssmin: {
            website: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                files: {
                    "public/frontend/css/common.min.css": "public/frontend/css/common.min.css",
                    "public/frontend/css/default.min.css": "public/frontend/css/default.min.css",
                    "public/frontend/css/smart-phone.min.css": "public/frontend/css/smart-phone.min.css",
                    "public/frontend/css/growup.min.css": "public/frontend/css/growup.min.css",
                    "public/frontend/css/growup-v2.min.css": "public/frontend/css/growup-v2.min.css",
                    "public/frontend/css/growup-modify.min.css": "public/frontend/css/growup-modify.min.css",
                    "public/frontend/css/client.min.css": "public/frontend/css/client.min.css",

                    "public/backend/css/common.min.css": "public/backend/css/common.min.css",
                    "public/backend/css/custom.min.css": "public/backend/css/custom.min.css",
                    "public/backend/css/horizontal-custom.min.css": "public/backend/css/horizontal-custom.min.css",
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['less:website', 'cssmin:website']);
};