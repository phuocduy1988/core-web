<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Builder

Route::group(['prefix' => 'be', 'middleware' => 'auth.admin'], function() {
	//Builder
    Route::get('/builder', 'BuilderController@index')->name('builder');
    Route::post('/builder', 'BuilderController@build')->name('builder.post');

	//Update Builder
	Route::get('/builder/update', 'BuilderController@update')->name('builder.update');
	Route::post('/builder/update', 'BuilderController@buildUpdate')->name('builder.update.post');

    //Dashboard manager
    Route::get('/builder/update-file', 'UpdateFileController@updateFile')->name('builder.update-file');

    // Make tools
    Route::get('/make-tools', 'MakeController@index')->name('builder.make-tools');
    Route::post('/make-tools', 'MakeController@post')->name('builder.make-tools.post');

    // File Editor
    Route::post('/code-editor/get-file-content', 'CodeEditorController@getFileContent')->name('builder.code-editor.get-file-content');
    Route::post('/code-editor/put-file-content', 'CodeEditorController@putFileContent')->name('builder.code-editor.put-file-content');

    //Relationship
	Route::get('/relationships', 'RelationshipController@index')->name('builder.relationships');
	Route::post('/relationships', 'RelationshipController@postNew')->name('builder.relationship.new');

});

//Create project
Route::group(['prefix' => 'be', 'namespace' => '\CreateProject'], function() {
    Route::any('/builder/create-project', 'CreateProjectController@createProject')->name('builder.create-project');
});

// Installer

Route::group(['prefix' => 'install','as' => 'LaravelInstaller::','namespace' => '\Installer'], function() {
    Route::get('/', [
        'as' => 'welcome',
        'uses' => 'WelcomeController@welcome'
    ]);

    Route::get('environment', [
        'as' => 'environment',
        'uses' => 'EnvironmentController@environmentMenu'
    ]);

    Route::get('environment/wizard', [
        'as' => 'environmentWizard',
        'uses' => 'EnvironmentController@environmentWizard'
    ]);

    Route::post('environment/saveWizard', [
        'as' => 'environmentSaveWizard',
        'uses' => 'EnvironmentController@saveWizard'
    ]);

    Route::get('environment/classic', [
        'as' => 'environmentClassic',
        'uses' => 'EnvironmentController@environmentClassic'
    ]);

    Route::post('environment/saveClassic', [
        'as' => 'environmentSaveClassic',
        'uses' => 'EnvironmentController@saveClassic'
    ]);

    Route::get('requirements', [
        'as' => 'requirements',
        'uses' => 'RequirementsController@requirements'
    ]);

    Route::get('permissions', [
        'as' => 'permissions',
        'uses' => 'PermissionsController@permissions'
    ]);

    Route::get('database', [
        'as' => 'database',
        'uses' => 'DatabaseController@database'
    ]);

    Route::get('final', [
        'as' => 'final',
        'uses' => 'FinalController@finish'
    ]);

});

Route::group(['prefix' => 'update','as' => 'LaravelUpdater::','namespace' => '\Installer'],function() {

    Route::get('/', [
        'as' => 'welcome',
        'uses' => 'UpdateController@welcome'
    ]);

    Route::get('overview', [
        'as' => 'overview',
        'uses' => 'UpdateController@overview'
    ]);

    Route::get('database', [
        'as' => 'database',
        'uses' => 'UpdateController@database'
    ]);

    // This needs to be out of the middleware because right after the migration has been
    // run, the middleware sends a 404.
    Route::get('final', [
        'as' => 'final',
        'uses' => 'UpdateController@finish'
    ]);
});
