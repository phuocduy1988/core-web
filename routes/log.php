<?php


/**
 * Log viewer
 */

Route::group(['prefix' => 'log-viewer', 'namespace' => 'Log'], function () {
	Route::get('/',['uses' => 'LogViewerController@index'])->name('log-viewer::dashboard');
	Route::get('/logs',['uses' => 'LogViewerController@listLogs'])->name('log-viewer::logs.list');
	Route::get('/logs/delete',['uses' => 'LogViewerController@delete'])->name('log-viewer::logs.delete');
	Route::get('/logs/{date}',['uses' => 'LogViewerController@show'])->name('log-viewer::logs.show');
	Route::get('/logs/{date}/download',['uses' => 'LogViewerController@download'])->name('log-viewer::logs.download');
	Route::get('/logs/{date}/{level}',['uses' => 'LogViewerController@showByLevel'])->name('log-viewer::logs.filter');
	Route::get('/logs/{date}/{level}/search',['uses' => 'LogViewerController@search'])->name('log-viewer::logs.search');
});