<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo 'You do not have permission!';
});

// Login
Route::get('login',['uses' => 'LoginController@showLoginForm'])->name('backend.login');
Route::post('login',['uses' => 'LoginController@postLogin']);
Route::any('logout',['uses' => 'LoginController@logout'])->name('backend.logout');


Route::group(['middleware' => 'auth.admin'], function ()
{
	//Home
	Route::any('/home', 'CommonController@home')->name('common.home');
	//Load iframe
	Route::any('/load-iframe', 'CommonController@iframe')->name('common.iframe');
	//Change status
	Route::any('/change-boolean', 'CommonController@changeBoolean')->name('common.change-boolean');

	//Auto generation here

	/*================================================
		   Activity Log
		================================================*/
	Route::any('/logs/activities', 'LogController@logActivity')->name('log.logActivity');
	Route::any('/logs/activities/list', 'LogController@getLogActivity')->name('log.getLogActivity');

	/*================================================
        Setting Route
    ================================================*/
	Route::any('/settings', 'SettingController@index')->name('setting.index');
	Route::get('/clear-caches', 'SettingController@clearCaches')->name('setting.clearCaches');
	Route::post('/settings/save', 'SettingController@post')->name('setting.save');
	Route::post('/settings/delete', 'SettingController@delete')->name('setting.delete');

	/*================================================
        User Route
    ================================================*/
	Route::any('/users', 'UserController@index')->name('user.index');

	Route::get('/users/new', 'UserController@form')->name('user.new');
	Route::get('/users/{user}', 'UserController@form')->name('user.form');
	Route::post('/users/save', 'UserController@post')->name('user.save');
	Route::get('/users/delete/{user}', 'UserController@delete')->name('user.delete');
	Route::get('/users/restore/{user}', 'UserController@restore')->name('user.restore');
	Route::get('/users/force-delete/{user}', 'UserController@forceDelete')->name('user.force-delete');

    /*================================================
        Page Route
    ================================================*/
    Route::any('/pages', 'PageController@index')->name('page.index');

    Route::get('/pages/new', 'PageController@form')->name('page.new');
    Route::get('/pages/{page}', 'PageController@form')->name('page.form');
    Route::post('/pages/save', 'PageController@post')->name('page.save');
    Route::get('/pages/delete/{page}', 'PageController@delete')->name('page.delete');
    Route::get('/pages/restore/{page}', 'PageController@restore')->name('page.restore');
    Route::get('/pages/force-delete/{page}', 'PageController@forceDelete')->name('page.force-delete');

    /*================================================
        PageType Route
    ================================================*/
    Route::any('/page-types', 'PageTypeController@index')->name('pageType.index');

    Route::get('/page-types/new', 'PageTypeController@form')->name('pageType.new');
    Route::get('/page-types/{pageType}', 'PageTypeController@form')->name('pageType.form');
    Route::post('/page-types/save', 'PageTypeController@post')->name('pageType.save');
    Route::get('/page-types/delete/{pageType}', 'PageTypeController@delete')->name('pageType.delete');
    Route::get('/page-types/restore/{pageType}', 'PageTypeController@restore')->name('pageType.restore');
    Route::get('/page-types/force-delete/{pageType}', 'PageTypeController@forceDelete')->name('pageType.force-delete');

});
