<?php

namespace OneTech\Base\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;

class BaseServiceProvider extends ServiceProvider
{

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

	/**
	 * Boot the service provider.
	 * @return void
	 */
	public function boot()
	{
		Schema::defaultStringLength(191);
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
